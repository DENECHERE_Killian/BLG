// noinspection ES6MissingAwait
const express    = require("express");
const MyEmbed    = require("./utilities/MyEmbed");
const Utils      = require("./utilities/Utils");
const dbInstance = require("./utilities/database/Database");
const router     = require("./routes/bot");

const BugReport = require("./modules/bugreport/BugReport");
const Werewolf  = require("./modules/werewolf/Werewolf");
const Music     = require("./modules/music/Music");
const Sutom     = require("./modules/sutom/Sutom");
const Emoji     = require("./modules/emoji/Emoji");
const Users     = require("./modules/users/Users");
const Xmas      = require("./modules/xmas-event/Xmas");

const social = new (require("./modules/socials"))();
global.bot   = new (require("./utilities/Bot"))();
global.database; // Used in the route file but declared in here to give it a value which is retrieved from an async function. That means it needs to be called from an async function in order to use the 'await' keyword and this action cannot be performed from the routes file.


//-----------------
// SERVER SETTINGS
//-----------------

botServer = express();

botServer.use(express.json());
botServer.use(express.urlencoded({ extended: true }));

botServer.listen(5001, async () => {
	console.log("Bot server started");

	// Start database connection
	if (process.env.LAUNCH_DB === "true")
		global.database = await dbInstance.getInstance();

	startBot();
});

//-----------------
// SERVER ROUTES
//-----------------

botServer.get("/users", router.user.get.users);
botServer.get("/users/:userId", router.user.get.user);
botServer.put("/users/:userId/password", router.user.put.userPassword);
botServer.delete("/users/:userId", router.user.delete.user);

botServer.post("/module/:moduleId", router.module.post.module);

//-----------------
// BOT EVENTS
//-----------------

function startBot() {
	// Event triggered when the bot is well-connected to Discord API
	global.bot.on("ready", async () => {
		console.log(`======== ${(process.env.TEST === "true" ? "TEST" : "PUBLIC")} BOT LAUNCHED ========`);

		global.ngrokUrl = process.argv[2];
		if (process.env.TEST !== "true") {
			const channel = global.bot.guilds.cache.get(process.env.DEV_GUILD_ID).channels.cache.get(process.env.DEV_CHANNEL_ID);
			channel.send({ embeds: [new MyEmbed("Ngrok URL", Utils.BLUE_COLOR).desc(`Voici le lien ngrok:\n${global.ngrokUrl}/admin`)] });
		}

		// noinspection ES6MissingAwait
		global.bot.refreshCommands();

		// Creation of classes for each guild using the bot
		global.bot.guilds.cache.each(async guild => {
			const guildId = guild.id;

			// Instantiate all modules for each guild
			global.bot.guildsDatas.set(guildId, {
				lg: new Werewolf(guildId),
				ms: new Music(guildId),
				su: new Sutom(guildId),
				us: new Users(guildId),
				xm: new Xmas(guildId),
				br: new BugReport(),
				em: new Emoji()
			});
		});

		// Update the bot profile
		if (process.env.TEST === "true")
			global.bot.user.setActivity(`son dev`, { type: "LISTENING" });
		else
			global.bot.updateActivity(true);
	});

	// Event triggered when a slash command is sent on Text Channel
	global.bot.on("interactionCreate", (interaction) => {
		if (!interaction.isCommand())
			return;

		const guildInfos = global.bot.guildsDatas.get(interaction.guildId);

		try {
			if (interaction.options._group || interaction.options.getSubcommand(false))
				guildInfos[interaction.commandName].runCommand(interaction.options._group ? interaction.options._group : interaction.options.getSubcommand(), interaction);
			else
				guildInfos[interaction.commandName].runCommand(interaction);
		} catch (err) {
			console.error(err);
		}
	});

	// Event triggered when a message is sent on a text channel
	global.bot.on("messageCreate", (message) => {
		if (message.author.bot)
			return;

		try {
			social.watchForSocialLink(message);
		} catch (err) {
			// Do nothing, the error has already been displayed in the module
		}
	});

	// Event triggered when a voice change is made on the guild the bot is on
	global.bot.on("voiceStateUpdate", (oldState, newState) => {
		let guildInfos = global.bot.guildsDatas.get(oldState.guild.id);

		if (!guildInfos.ms.playlist.hasPlayer())
			return;

		// The bot is not concerned by this update but is the last in the voice channel
		if (oldState.id !== global.bot.user.id && oldState.channel && oldState.channel.members.size === 1) {
			global.bot.warnForDisconnect(guildInfos.ms.lastMessageChannel, newState, oldState);
			return guildInfos.ms.playlist.player.pause(); // Pause the player because nobody is listening
		}

		// The bot is concerned by this update and was moved to another channel (not disconnected)
		if (oldState.id === global.bot.user.id && newState.channel && oldState.channelId !== newState.channelId) {
			if (newState.channel.members.size === 1) {
				global.bot.warnForDisconnect(guildInfos.ms.lastMessageChannel, newState, oldState);
				guildInfos.ms.playlist.player.pause(); // Pause the player because nobody is listening
			}

			console.log(`Bot moved on voice channel id ${newState.channelId}`);
		}
	});

	// Event triggered when an uncatched error is thrown
	global.bot.on("error", (err) => {
		console.error(err);
	});

	// Log in the bot
	global.bot.login(process.env.DC_API_TOKEN)
		.then(_ => console.log(`Successfully logged-in to Discord`))
		.catch(err => console.error(`Cannot connect to Discord API :\n${err}`));
}

// Separate warning and info message from other errors and log message from respective process pipe output
(function () {
	if (console.warn) {
		const old    = console.warn;
		console.warn = function () {
			Array.prototype.unshift.call(arguments, "WARN");
			old.apply(this, arguments);
		};
	}
	if (console.info) {
		const old    = console.info;
		console.info = function () {
			Array.prototype.unshift.call(arguments, "INFO");
			old.apply(this, arguments);
		};
	}
})();
