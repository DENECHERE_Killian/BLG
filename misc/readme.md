`blg.service` is a linux service automatically starting the bot on startup.
It should be placed on `/etc/systemd/system/` folder.

The default user is called `pi` (to adapt depending on your system).
You can check if the service is running by running
```sh
systemctl --type service --all | grep BLG server
# Or you can do
systemctl status blg
# You can ensure the service is lauched by checking that the circle on the first line is filled with another color than red.
```

To start the service
```sh
service blg start
```

To start the service
```sh
service blg stop
```

To restart the service
```sh
service blg restart
```

When you edit the service file, you should reload it with :
```shell
systemctl daemon-reload
```