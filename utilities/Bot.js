/**
 * @author Killian
 */

require("events").EventEmitter.defaultMaxListeners = 0; // In order to run more than 10 asynchronous function at the same time.

const { Routes } = require("discord-api-types/v9");
const Discord    = require("discord.js");
const Rest       = require("@discordjs/rest");

const CommandFileReader = require("./ModulesManager");
const MyEmbed           = require("./MyEmbed");
const Utils             = require("./Utils");

class Bot extends Discord.Client {

	constructor() {
		// Create Discord client
		super({
			intents: new Discord.Intents([
				Discord.Intents.FLAGS.GUILDS,
				Discord.Intents.FLAGS.GUILD_MEMBERS,
				Discord.Intents.FLAGS.GUILD_EMOJIS_AND_STICKERS,
				Discord.Intents.FLAGS.GUILD_VOICE_STATES,
				Discord.Intents.FLAGS.GUILD_PRESENCES,
				Discord.Intents.FLAGS.GUILD_MESSAGES,
				Discord.Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
				Discord.Intents.FLAGS.GUILD_MESSAGE_TYPING,
				Discord.Intents.FLAGS.DIRECT_MESSAGES,
				Discord.Intents.FLAGS.DIRECT_MESSAGE_REACTIONS,
				Discord.Intents.FLAGS.DIRECT_MESSAGE_TYPING
			]),
			retryLimit: Infinity,
			restRequestTimeout: 60000 // 1 min before timeout for each action : It's used to send big instagram videos into Discord
		});

		this.guildsDatas = new Map(); // (Guild id) => Map() The dictionary containing all the guild's datas.
		this.discordRest = new Rest.REST({ version: "9" }).setToken(process.env.DC_API_TOKEN);
	}

	/**
	 * Refresh the / commands displayed on discord interface when typing "/" on the chat.
	 * */
	async refreshCommands() {
		let activeCommands = [];
		CommandFileReader.getModules().forEach(commandsFilesContent => {
			if (commandsFilesContent.activated)
				activeCommands.push(commandsFilesContent.commands);
		});

		try {
			console.log("Started refreshing application (/) commands.");
			for (const guild_id of this.guilds.cache.map(guild => guild.id))
				await this.discordRest.put(Routes.applicationGuildCommands(this.user.id, guild_id), { body: activeCommands });
			console.log(`\t↪ Command refresh success`);
		} catch (err) {
			console.error(`\t↪ Command refresh failed :\n${err}`);
		}
	}

	async enableCommandModule(moduleName, enableState) {
		CommandFileReader.enableCommandModule(moduleName, enableState);
		await this.refreshCommands();
	}

	/**
	 * Update the Discord user activity of the bot each minute to display the time left until the next uptime verification.
	 *
	 * @param {boolean} loopMode=false If true, perform a call and call back itself after several time to constantly update the bot. Otherwise, perform just one call.
	 * */
	updateActivity(loopMode = false) {
		if (loopMode)
			setInterval(this.updateActivity.bind(this), 60000); // Updating the bot activity all minutes only the first call

		const totalMinutes = Math.floor(this.uptime / 60000);
		const totalHours   = Math.floor(totalMinutes / 60);
		const days         = Math.floor(totalHours / 24);
		const minutes      = totalMinutes % 60;
		const hours        = totalHours % 24;

		let pad = "";
		if (minutes < 10 && (hours > 0 || days > 0))
			pad = "0";

		let timeSpanString = `${pad}${minutes}min`;

		if (hours > 0 || days > 0) {
			pad = "";
			if (hours < 10 && days > 0)
				pad = "0";

			timeSpanString = `${pad}${hours}h${timeSpanString}`;
		}

		if (days > 0)
			timeSpanString = `${days}j${timeSpanString}`;

		this.user.setActivity(`depuis ${timeSpanString}.`, { type: "LISTENING" });
	}

	async recoverUsers() {
		const databaseResponse = await global.database.getAllUsersId();

		if (!databaseResponse.success) {
			return databaseResponse;
		}

		const players = [];

		for (const userId of databaseResponse.ids) {
			await this.users.fetch(userId["user_id"])
				.then(user => {
					players.push({
						id: userId["user_id"],
						picture: user.avatarURL(),
						pseudo: user.username
					});
				})
				.catch(err => console.error(`Failed to fetch user ${userId}: ${err}`));
		}

		return {success: true, users: players};
	}

	/**
	 * Send a message in a specific channel telling that the bot will disconnect itself automatically.
	 * @param channel {Discord.TextChannel} Where to send the message.
	 * @param newState {Discord.VoiceState} The voice state after a voice connection move in a server.
	 * @param oldState {Discord.VoiceState} The voice state before a voice connection move in a server.
	 */
	warnForDisconnect(channel, newState, oldState) {
		new MyEmbed(`Je suis seul, je me déconnecterai dans ${Utils.DISCONNECT_TIME} secondes`, Utils.BLUE_COLOR).send(channel).then(message => {
			Utils.deleteMessage(message);
			setTimeout(() => {
				// Still nobody verification
				if (newState.channel && newState.channel.members.size > 1) // Someone joined during the timer
					return;

				// There is still nobody in the channel
				Utils.disconnectVoiceConnection(oldState.guild.id);
			}, Utils.DISCONNECT_TIME * 1000);
		});
	}
}

module.exports = Bot;