const fs         = require("fs");

class ModulesManager {
	constructor() {
	}

	static getModules() {
		return fs.readdirSync("./modules/", { withFileTypes: true })
			.filter((item) => item.isDirectory() && fs.readdirSync(`./modules/${item.name}`).includes("commands.json"))
			.map((item) => item.name)
			.map(directoryName => {
				const path  = `./modules/${directoryName}/commands.json`;
				const datas = JSON.parse(fs.readFileSync(path).toString());
				datas.file  = path;
				return datas;
			});
	}

	static enableCommandModule(moduleName, enableState) {
		const module     = ModulesManager.getModules().find(module => module.commands.name === moduleName);
		module.activated = enableState;
		fs.writeFileSync(module.file, JSON.stringify(module, null, 4));
	}
}

module.exports = ModulesManager;