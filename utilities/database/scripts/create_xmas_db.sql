DROP DATABASE IF EXISTS xmas_event_2023;
CREATE DATABASE xmas_event_2023;

GRANT SELECT, INSERT, UPDATE, DELETE ON xmas_event_2023.* TO 'blg_user'@'localhost';

FLUSH privileges;

USE xmas_event_2023;

CREATE TABLE players
(
    player_id VARCHAR(18) NOT NULL,
    PRIMARY KEY (player_id)
);

CREATE TABLE game_1
(
    action_id    INT AUTO_INCREMENT NOT NULL,
    action_value INT                NOT NULL,
    action_type  VARCHAR(25),
    action_date  DATE               NOT NULL,
    action_user  VARCHAR(18)        NOT NULL,
    PRIMARY KEY (action_id)
);
CREATE TABLE game_2
(
    action_id    INT AUTO_INCREMENT NOT NULL,
    action_value INT                NOT NULL,
    action_type  VARCHAR(25),
    action_date  DATE               NOT NULL,
    action_user  VARCHAR(18)        NOT NULL,
    PRIMARY KEY (action_id)
);
CREATE TABLE game_3
(
    action_id    INT AUTO_INCREMENT NOT NULL,
    action_value INT                NOT NULL,
    action_type  VARCHAR(25),
    action_date  DATE               NOT NULL,
    action_user  VARCHAR(18)        NOT NULL,
    PRIMARY KEY (action_id)
);
CREATE TABLE game_4
(
    action_id    INT AUTO_INCREMENT NOT NULL,
    action_value INT                NOT NULL,
    action_type  VARCHAR(25),
    action_date  DATE               NOT NULL,
    action_user  VARCHAR(18)        NOT NULL,
    PRIMARY KEY (action_id)
);

SELECT "Xmas created";