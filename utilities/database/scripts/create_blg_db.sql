DROP DATABASE IF EXISTS blg_db;
CREATE DATABASE blg_db;

GRANT SELECT, INSERT, UPDATE, DELETE ON blg_db.* TO 'blg_user'@'localhost';

FLUSH privileges;

USE blg_db;

CREATE TABLE modules
(
    module_name VARCHAR(25) NOT NULL,
    is_active   BOOLEAN,
    PRIMARY KEY (module_name)
);

CREATE TABLE users
(
    user_id  VARCHAR(18) NOT NULL,
    pwd      VARCHAR(64) NOT NULL,
    is_admin BOOLEAN NOT NULL,
    registrer_date DATETIME NOT NULL,
    PRIMARY KEY (user_id)
);


INSERT INTO modules
VALUES ("bugreport", true),
       ("emoji", true),
       ("music", true),
       ("socials", true),
       ("sutom", true),
       ("werewolf", false);

SELECT "blg_db created";
