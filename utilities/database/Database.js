const mariadb = require("mariadb");
const MyEmbed = require("../MyEmbed");
const crypto  = require("crypto");
const Utils   = require("../Utils");

const CONNNECTION_TIMEOUT_RESET = 7; // Hours

class Database {

	static test = 5;

	constructor() {
	}

	/**
	 * Initialize the database connection
	 * @returns {Promise<boolean>} Returns true if the connection was successfully created, false otherwise.
	 */
	async init() {
		this.connection = await mariadb.createPool({
			host: "127.0.0.1",
			port: parseInt(process.env.DB_PORT),
			user: process.env.DB_USERNAME,
			password: process.env.DB_USER_PASSWORD,
			connectionLimit: 3
		});

		// Keep the established connection up
		setInterval(this.#maintainConnection.bind(this), CONNNECTION_TIMEOUT_RESET*3600000);

		return !!this.connection;
	}

	/**
	 * Set the module as active in the database.
	 * @param moduleName The module to activate.
	 * @returns {boolean} true if successfully set, false otherwise
	 */
	setModuleActive(moduleName) {
		if (!this.#checkForConnection())
			return false;

		this.connection.query("UPDATE blg_db.modules SET is_active=true WHERE module_name=?", [moduleName])
			.then(_ => {
				console.log(`Successfully set module ${moduleName} active on database`);
			})
			.catch(_ => {
				console.error(`Failed to set module ${moduleName} active on database`);
			});
	}

	/**
	 * Set the module as inactive in the database.
	 * @param moduleName The module to deactivate.
	 * @returns {boolean} true if successfully set, false otherwise
	 */
	setModuleInactive(moduleName) {
		if (!this.#checkForConnection())
			return false;

		this.connection.query("UPDATE blg_db.modules SET is_active=false WHERE module_name=?", [moduleName])
			.then(_ => {
				console.log(`Successfully set module ${moduleName} inactive on database`);
				return true;
			})
			.catch(_ => {
				console.error(`Failed to set module ${moduleName} inactive on database`);
				return false;
			});
	}

	/**
	 * Check if a user exist in the database.
	 * @param userId The id of the researched user.
	 * @returns {Promise<boolean>} true if the user exist, false otherwise
	 */
	async userExist(userId) {
		if (!this.#checkForConnection())
			return true;

		const rows = await this.connection.query("SELECT user_id FROM blg_db.users WHERE user_id=?", [userId]);
		return rows.length > 0;
	}

	/**
	 * Insert a new user in the database.
	 * @param id The id the user.
	 * @param pwd The hashed password of the user.
	 * @returns {Promise<boolean>} true if the user was inserted, false otherwise
	 */
	async insertUser(id, pwd) {
		if (!this.#checkForConnection())
			return false;

		const hashedPwd = crypto.createHash("sha256").update(pwd).digest("hex");
		return await this.connection.query("INSERT INTO blg_db.users VALUES (?, ?, ?)", [id, hashedPwd, new Date()])
			.then(_ => {
				console.log(`Successfully inserted user with id ${id}`);
				return true;
			})
			.catch(err => {
				console.error(`Error occurred when inserting user: ${err}`);
				return false;
			});
	}

	/**
	 * Verify that the credentials given by the user is correct.
	 * @param id The id of the user that want to log in.
	 * @param pwd The encrypted password given by the user.
	 * @returns {Promise<Object>} true if credentials given are correct, false otherwise
	 */
	async logUser(id, pwd) {
		if (!this.#checkForConnection())
			return false;

		const hashedPwd = crypto.createHash("sha256").update(pwd).digest("hex");
		const rows = await this.connection.query("SELECT pwd, is_admin FROM blg_db.users WHERE user_id=?", [id]);
		if (rows.length > 0 && hashedPwd === rows[0]["pwd"])
			return { success: true, isAdmin: rows[0]["is_admin"] === 1, message: "Bienvenue !" };
		return { success: false, message: "Identifiant et/ou mot de passe invalide(s)" };
	}

	/**
	 * Search all users' id.
	 * @returns {Promise<Object>} The status of the request and the list of all users' id
	 */
	async getAllUsersId() {
		if (!this.#checkForConnection())
			return { success: false, ids: [] };

		return { success: true, ids: await this.connection.query("SELECT user_id FROM blg_db.users") };
	}

	/**
	 * Check if a player exist in the database.
	 * @param playerId The id of the researched player.
	 * @returns {Promise<boolean>} true if the player exist, false otherwise
	 */
	async playerExist(playerId) {
		if (!this.#checkForConnection())
			return true;

		const rows = await this.connection.query("SELECT player_id FROM xmas_event_2023.players WHERE player_id=?", [playerId]);
		return rows.length > 0;
	}

	/**
	 * Insert a new player in the database.
	 * @param id The id the player.
	 * @returns {Promise<boolean>} true if the player was inserted, false otherwise
	 */
	async insertPlayer(id) {
		if (!this.#checkForConnection())
			return false;

		return await this.connection.query("INSERT INTO xmas_event_2023.players VALUES (?)", [id])
			.then(_ => {
				console.log(`Successfully inserted player with id ${id}`);
				return true;
			})
			.catch(err => {
				console.error(`Error occurred when inserting player: ${err}`);
				return false;
			});
	}

	/**
	 * Change the password of a user and communicate the new password to the user.
	 * @param userId {string} The id of the user to change password.
	 * @returns {Object} The result of the process, a booleans for the success and a message that explains the result
	 */
	async changeUserPwd(userId) {
		if (!this.#checkForConnection())
			return { success: false, message: "The database connection was not correctly established" };

		if (!await this.userExist(userId))
			return { success: false, message: "The user does not exist in the database" };

		const pwd       = `${parseInt(userId) * 2}`.split("").sort(_ => 0.5 - Math.random()).join("").substring(0, 18);
		const hashedPwd = crypto.createHash("sha256").update(pwd).digest("hex");
		const hashed2Pwd = crypto.createHash("sha256").update(hashedPwd).digest("hex");

		return await this.connection.query("UPDATE blg_db.users SET pwd=? WHERE user_id=?", [hashed2Pwd, userId])
			.then(async _ => {
				console.log(`Successfully change user's password with id ${userId} in database`);
				return await global.bot.users.fetch(userId)
					.then(async user => {
						return await user.send({
							embeds: [new MyEmbed("Changement des infos de connexion", Utils.BLUE_COLOR)
								.desc("Voilà ton identifiant et ton nouveau mot de passe pour te connecter sur le site web. Ne le communique à personne !")
								.addFields([
									{ name: "Identifiant :", value: userId },
									{ name: "Mot de passe :", value: pwd }])
								.setFooter({ text: "Tu peux avoir le lien vers le site en utilisant `/us link` dans un serveur où je suis présent !" })
							]
						})
							.then(_ => {
								console.log("Successfully change user password and send it in private message");
								return { success: true, message: "Mot de passe changé, utilisateur prévenu" };
							})
							.catch(err => {
								console.error(`Successfully change password, fail to send it in private message : ${err}`);
								return {
									success: false,
									message: `Mot de passe changé, l'utilisateur n'a pas pu être prévenu : ${err}`
								};
							});
					})
					.catch(err => {
						console.error(`Successfully change password, fail to send it in private message : ${err}`);
						return {
							success: false,
							message: `Mot de passe changé, l'utilisateur n'a pas pu être prévenu : ${err}`
						};
					});

			})
			.catch(err => {
				console.error(`Error occurred when changing user's password: ${err}`);
				return { success: false, message: "La base de données n'a pas pu changer le mot de passe" };
			});
	}

	/**
	 * Delete a user from the database. Remove the player associated to this user if it exists.
	 * @param userId {String} the id of the user to delete.
	 * @returns {Object} The result of the process, a booleans for the success and a message that explains the result
	 */
	async deleteUser(userId) {
		if (!this.#checkForConnection())
			return { success: false, message: "The database connection was not correctly established" };

		if (!await this.userExist(userId))
			return { success: false, message: "The user does not exist in the database" };

		return await this.connection.query("DELETE FROM blg_db.users WHERE user_id=?", [userId])
			.then(async _ => {
				console.log(`Successfully deleted user with id ${userId}`);
				if (await this.playerExist(userId)) {
					const deleteResult = await this.deletePlayer(userId);
					if (deleteResult.success) {
						return { success: true, message: "Utilisateur et joueur supprimé" };
					}
					console.log(`Successfully deleted player with id ${userId} but failed to delete associated player`);
					return { success: false, message: "Utilisateur supprimé, échec de la suppression du joueur" };
				}
				return { success: true, message: "Utilisateur supprimé" };
			})
			.catch(err => {
				console.error(`Error occurred when deleting user: ${err}`);
				return { success: false, message: "La base de données n'a pas pu retirer l'utilisateur" };
			});
	}

	/**
	 * Delete a player from the database.
	 * @param playerId {String} the id of the player to delete.
	 * @returns {Object} The result of the process, a booleans for the success and a message that explains the result
	 */
	async deletePlayer(playerId) {
		if (!this.#checkForConnection())
			return { success: false, message: "The database connection was not correctly established" };

		if (!await this.playerExist(playerId))
			return { success: false, message: "The player does not exist in the database" };

		return await this.connection.query("DELETE FROM xmas_event_2023.players WHERE player_id=?", [playerId])
			.then(_ => {
				console.log(`Successfully deleted player with id ${playerId}`);
				return { success: true, message: "Joueur supprimé" };
			})
			.catch(err => {
				console.error(`error occured when deleting player: ${err}`);
				return { success: false, message: "La base de données n'a pas pu retirer le joueur" };
			});
	}

	/**
	 * Ensure that the connection with the database is well established.
	 * @returns {boolean} false if connection is not properly established, true otherwise.
	 */
	#checkForConnection() {
		if (this.connection)
			return true;

		if (process.env.LAUNCH_DB === "false") {
			console.error(`You are trying to use database without setting the start database .env parameter to 'true'`);
			return false;
		}
		console.error(`The database connection was not correctly established`);
		return false;
	}

	/**
	 * Each 8 hours without query, by default, the database connection is stopped by mariadb leading to a connection reset error.
	 * This method perform a simple query each 7 hours in order to keep the connection up.
	 */
	#maintainConnection() {
		if (!this.#checkForConnection())
			return ;

		this.connection.query("SELECT 1")
			.then(_ => {
				console.log(`Database connection timeout reset query performed`);
			})
			.catch(err => {
				console.error(`Cannot query database to reset connection timeout : ${err}`);
			});
	}
}

const databaseInstance = new Database();
module.exports = {
	getInstance: async () => {
		if (process.env.LAUNCH_DB !== "true")
			return;

		if (databaseInstance.connection)
			return databaseInstance;

		return await databaseInstance.init().then(success => {
			if (success) {
				console.log(`Connection to database succeed`);
				return databaseInstance;
			} else {
				return console.error(`Connection to database was not properly done.`);
			}
		});
	}
};
