const fetch  = require("node-fetch");
const fs     = require("fs");
const logger = require("./log/Logger");

const PROJECT_ID = "20297412";
const BASE_URL   = "https://gitlab.com/api/v4";

class Gitlab {

	constructor() {
		this.webhookId = undefined;
	}

	createWebhook(url) {
		fetch(`${BASE_URL}/projects/${PROJECT_ID}/hooks`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Private-Token": `${process.env.GITLAB_TOKEN}`
			},
			body: JSON.stringify({
				"url": url + "/admin/hooks",
				"push_events": true,
				"tag_push_events": false,
				"merge_requests_events": false,
				"repository_update_events": false,
				"enable_ssl_verification": false
			})
		}).then(async (response) => {
			const body     = await response.json();
			this.webhookId = body.id;
			logger.log(`Successfully created the gitlab webhook ${this.webhookId}`);
		}).catch(err => {
			logger.error(`Failed to create the gitlab webhook :\n${err}`);
		});
	}

	async deleteWebhook() {
		if (this.webhookId)
			await fetch(`${BASE_URL}/projects/${PROJECT_ID}/hooks/${this.webhookId}`, {
				method: "DELETE",
				headers: {
					"Private-Token": `${process.env.GITLAB_TOKEN}`
				}
			}).then(_ => {
				logger.log("Successfully deleted the gitlab webhook");
			}).catch(err => {
				logger.error(`Failed to delete the gitlab webhook :\n${err}`);
			});
	}

	/**
	 * Query commit sha about the commit currently heading the git branch (= the last pulled commit).
	 */
	async getHeadCommit() {
		const rev = fs.readFileSync(".git/HEAD").toString().trim();
		if (rev.indexOf(":") === -1)
			return await this.getCommit(rev);
		else
			return await this.getCommit(fs.readFileSync(".git/" + rev.substring(5)).toString().trim());
	}

	async getWebhooks() {
		return fetch(`${BASE_URL}/projects/${PROJECT_ID}/hooks?per_page=100`, {
			method: 'GET',
			headers: {
				"Private-Token": process.env.GITLAB_TOKEN,
				"Accept": "application/json"
			}
		})
			.then(async res => await res.json())
			.catch(err => logger.error(err));
	}

	async removeWebhook(hookId) {
		return fetch(`${BASE_URL}/projects/${PROJECT_ID}/hooks/${hookId}`, {
			method: 'DELETE',
			headers: {
				"Private-Token": process.env.GITLAB_TOKEN,
				"Accept": "application/json"
			}
		})
			.then(async _ => {
				logger.log(`Succesfully deleted hook #${hookId} from admin panel`);
			})
			.catch(err => logger.error(err));
	}

	/**
	 * Query informations about a specific commit.
	 * @param commit_sha {string} The commit identifier (sha)
	 * @returns {Promise<json | void>} Return the json datas about this commit or nothing if an error occured
	 */
	getCommit(commit_sha) {
		return fetch(`${BASE_URL}/projects/${PROJECT_ID}/repository/commits/${commit_sha}`)
			.then(async res => await res.json())
			.catch(err => logger.error(err));
	}

	getBranches() {
		return fetch(`${BASE_URL}/projects/${PROJECT_ID}/repository/branches`)
			.then(async res => await res.json())
			.catch(err => logger.error(err));
	}
}

module.exports = Gitlab;