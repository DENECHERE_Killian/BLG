const fs   = require("fs");
const path = require("path");

const OLD_FILES_LIMIT = 10;

class Logger {
	LOGS_PATH      = path.join(process.cwd(), "utilities", "log", "logs");
	OLDS_LOGS_PATH = path.join(this.LOGS_PATH, "olds");
	LIVE_LOGS_PATH = path.join(this.LOGS_PATH, "live");
	
	LIVE_LOGS_FILENAME     = "";
	LIVE_LOGS_FILE_WATCHER = null;

	constructor() {
		try {
			// Check that each logging folder is created
			[this.LOGS_PATH, this.OLDS_LOGS_PATH, this.LIVE_LOGS_PATH].forEach(path => {
				if (!fs.existsSync(path))
					fs.mkdirSync(path);
			});
		} catch (err) {
			console.error(`Unable to create log files directory :\n`, err);
		}
	}

	/**
	 * Save a standard log message to the "live" log file of the bot.
	 * @param {String} msg The message to save.
	 * @param {('' | 'errorLog' | 'startLog' | 'infoLog' | 'warningLog')} css_class A css class to customize the message displaying on the admin panel.
	 */
	log(msg, css_class = "") {
		this.#writeToLiveFile(msg, css_class);
		console.log(msg);
	}

	/**
	 * Save an error log message to the "live" log file of the bot.
	 * @param {String} msg The error message to save.
	 */
	error(msg) {
		this.#writeToLiveFile(msg, "errorLog");
		console.error(msg);
	}

	/**
	 * Save an update log message to the "live" log file of the bot.
	 * @param {String} msg The update message to save.
	 */
	update(msg) {
		this.#writeToLiveFile(msg, "updateLog");
		console.log(msg);
	}

	/**
	 * Save a warning log message to the "live" log file of the bot.
	 * @param {String} msg The warning message to save.
	 */
	warn(msg) {
		this.#writeToLiveFile(msg, "warningLog");
		console.warn(msg);
	}

	/**
	 * Save an info log message to the "live" log file of the bot.
	 * @param {String} msg The warning message to save.
	 */
	info(msg) {
		this.#writeToLiveFile(msg, "infoLog");
		console.info(msg);
	}

	/**
	 * Write a string into the "live" log file of the obt.
	 * @param {String} msg The error message to save.
	 * @param {('' | 'errorLog' | 'startLog' | 'updateLog' | 'infoLog' | 'warningLog')} css_class A css class to customize the message displaying on the admin panel.
	 */
	#writeToLiveFile(msg, css_class = "") {
		if (!msg || msg === "")
			return;

		if (typeof msg == "object")
			msg = msg.toString();

		// Identify links to encapsulate those with a <a> tag. This identification is performed before the '<>' characters replacement because it could alter the link identified.
		const links = [... new Set(msg.match(/(http|https):\/\/([\w+?.])+([a-zA-Z0-9~!@#$%^&*()_\-=+\\\/?.:;]*)?/g))];

		// Replace all html tags with utf8 values in order those not to be rendered as html tags but string when displayed on the admin panel.
		msg = msg.replaceAll("<", "&lt;").replaceAll(">", "&gt;");

		// Encapsulate links in <a> tag
		links?.forEach(link => msg = msg.replaceAll(link, `<a href="${link}" target="_blank" rel="noreferrer">${link}</a>`));

		// Write filtered log into file
		fs.appendFileSync(this.getLiveFilePath(), `<pre class="${css_class}">[${new Date().toLocaleString("fr-FR").replace(", ", "-")}] ${msg}</pre>`, err => {
			if (err)
				console.error(err);
		});
	}

	/**
	 * Return the logs file content for the given type.
	 * Every log file in the "olds" folder corresponds to an old "live" log file meaning several new versions has been released since
	 * @param type {'live' | 'olds'}
	 */
	getFilesContent(type) {
		if (!type || type === "")
			return console.error("Logs type should be defined and non empty");

		const filesPath = path.join(this.LOGS_PATH, type);
		const files     = {};

		if (fs.existsSync(filesPath)) {
			if (type === "live")
				files["live"] = fs.readFileSync(path.join(filesPath, this.LIVE_LOGS_FILENAME)).toString();
			else
				for (const filename of fs.readdirSync(filesPath))
					files[filename
						.slice(4, -4)
						.split("_")
						.map((dateOrTime, index) => index === 0 ? dateOrTime.replaceAll("-", "/") : dateOrTime.replaceAll("-", ":"))
						.join("-")
						] = fs.readFileSync(path.join(filesPath, filename)).toString();
		} else {
			console.warn(`Try to get logs files on '${filesPath}' but this doesn't exists`);
		}

		return files;
	}

	/**
	 * Archive the current live log file into the olds logs files folder, create a new one and remove the extra files.
	 */
	changeLiveFile() {
		// Move previous live file
		fs.readdirSync(this.LIVE_LOGS_PATH).forEach(filename => {
			const filePath = path.join(this.LIVE_LOGS_PATH, filename);
			if (fs.statSync(path.join(filePath)).isFile())
				fs.renameSync(filePath, path.join(this.OLDS_LOGS_PATH, filename));
		});

		// Create the new live file
		this.LIVE_LOGS_FILENAME = `BLG_${new Date().toLocaleString("fr-FR")
			.replace(", ", "_")
			.replaceAll("/", "-")
			.replaceAll(":", "-")}.log`;
		fs.closeSync(fs.openSync(this.getLiveFilePath(), "w"));

		// Check number of old live files are stored
		const oldLogs = fs.readdirSync(this.OLDS_LOGS_PATH, { withFileTypes: true }).filter(elem => elem.isFile()).map(elem => elem.name);

		// Remove old files excess
		if (oldLogs.length > OLD_FILES_LIMIT) {
			const sortedOldLogs = oldLogs.sort((filename1, filename2) => {
				const file1birthtime = fs.statSync(path.join(this.OLDS_LOGS_PATH, filename1)).birthtime;
				const file2birthtime = fs.statSync(path.join(this.OLDS_LOGS_PATH, filename2)).birthtime;

				if (file1birthtime < file2birthtime)
					return -1;
				if (file1birthtime > file2birthtime)
					return 1;
				return 0;
			});

			while (sortedOldLogs.length > OLD_FILES_LIMIT) {
				// Suppress the oldest file
				fs.unlinkSync(path.join(this.OLDS_LOGS_PATH, sortedOldLogs.shift()));
			}
		}

		return this.LIVE_LOGS_FILENAME;
	}

	getLiveFilePath() {
		return path.join(this.LIVE_LOGS_PATH, this.LIVE_LOGS_FILENAME);
	}

	/**
	 * Create watcher on changes over the live logs file.
	 * If a watcher was already created, it is destroyed and a new one is created.
	 * Used for the websocket displaying automatically the new file content at each change.
	 * @param callback {function}
	 */
	setLiveFileWatcherCallback(callback) {

		// if (this.LIVE_LOGS_FILE_WATCHER)
		// 	fs.unwatchFile(this.LIVE_LOGS_FILE_WATCHER);

		this.LIVE_LOGS_FILE_WATCHER = fs.watch(this.getLiveFilePath(), callback);
	}
}

module.exports = new Logger();