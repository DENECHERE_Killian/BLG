/**
 * @author Killian
 */

const Discord = require("discord.js");
const Utils   = require("./Utils");

/**
 * This class represent a {@link Discord.MessageEmbed}. It simplifies the use of original discord method to have a shorter writing.
 */
class MyEmbed extends Discord.MessageEmbed {

	/**
	 * Create a Discord Embed Message with predefined elements to simplify the creation.
	 * @param title {string} The title of this message.
	 * @param [color=Utils.BLACK_COLOR] {Discord.ColorResolvable} The color of this message.
	 * @returns {Discord.MessageEmbed}
	 */
	constructor(title, color = Utils.BLACK_COLOR) {
		super().setTitle(title).setColor(color);
		this.mentionsAllowed = false;
	}

	/**
	 * Add a description to this message.
	 * @param description {string} The message description.
	 * @returns {MyEmbed} The message with the description.
	 */
	desc(description) {
		if (!description)
			return this;

		return this.setDescription(description);
	}

	/**
	 * Change the color of this already created embed
	 * @param color {Discord.ColorResolvable} The color of this message.
	 */
	col(color) {
		if (!color)
			return this;

		return this.setColor(color);
	}

	/**
	 * Send this object into the specified text channel.
	 * @param textChannel {Discord.TextChannel|Discord.DMChannel} The text channel to send the message in.
	 * @param components {Discord.MessageActionRowComponentResolvable[]} Buttons to add to the embed.
	 * @return {Promise<Discord.Message>} The message that has been sent.
	 */
	send(textChannel, components = null) {
		if (!textChannel)
			return null;

		let message = { embeds: [this], fetchReply: true, components: [], allowMentions: this.mentionsAllowed };

		if (components) {
			const row = new Discord.MessageActionRow();
			// noinspection JSCheckFunctionSignatures
			components.forEach(button => row.addComponents(button));
			message = { embeds: [this], fetchReply: true, components: [row], allowMentions: this.mentionsAllowed };
		}

		return textChannel.send(message)
			.catch(err => {
				console.error(`\t↪ Cannot send message on ${Utils.getChannelLocation(textChannel)} :\n${err}`);
			});
	}

	/**
	 * Enable mentions writing in this embed message.
	 * @returns {MyEmbed}
	 */
	allowMentions() {
		this.mentionsAllowed = true;
		return this;
	}

	/**
	 * Add a timer in this embed footer that update each second.
	 * @param message {Discord.Message} The Message that contains the embed to add a timer on.
	 * @param title {string} The title of this timer.
	 * @param secondsAmount {number} The seconds amount that the timer will be on.
	 * @return {?NodeJS.Timer} The message with the timer that started.
	 */
	static showTimer(message, title, secondsAmount) {
		if (!message || !title || !secondsAmount)
			return null;

		/*
		 let timeSpent = 0;
		 const timer = setInterval(() => {
		 timeSpent ++;
		 // noinspection JSIgnoredPromiseFromCall
		 message.edit({ embeds: [message.embeds[0].setFooter({ text: `${title} ${secondsAmount - timeSpent}s` })] });

		 if (timeSpent === secondsAmount)
		 clearInterval(timer);
		 }, 1000);*/

		// noinspection JSIgnoredPromiseFromCall
		message.edit({ embeds: [message.embeds[0].setFooter({ text: `${title} ${secondsAmount}s` })] });
		// return timer;
	}
}

module.exports = MyEmbed;