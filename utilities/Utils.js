// noinspection JSUnusedLocalSymbols,JSValidateJSDoc,JSUnusedGlobalSymbols
const voiceManager = require("@discordjs/voice");
const Discord      = require("discord.js");
const fetch        = require("node-fetch");

/**
 * @file Contains several s usable by all modules of the bot.
 * @module modules
 * @name shared.js
 * @author Killian
 */
class Utils {
	//==========
	// CONSTANTS
	//==========
	static DISCORD_MAX_EMBED_SIZE = 6000; // 6000 characters is the embed content limit
	static DISCORD_MAX_MEDIA_SIZE = 8000000; // 8Mo = 8000Ko = 8000000o. This is the limit size of files uploadable in Discord.
	static MESSAGE_DEL_TIME       = 5; // seconds
	static DISCONNECT_TIME        = process.env.TEST === "true" ? 5 : 15; // seconds
	static COLLECTOR_TIME         = process.env.TEST === "true" ? 10 : 25; // seconds

	static NUMBERS = new Map([[0, "0️⃣"], [1, "1️⃣"], [2, "2️⃣"], [3, "3️⃣"], [4, "4️⃣"], [5, "5️⃣"], [6, "6️⃣"], [7, "7️⃣"], [8, "8️⃣"], [9, "9️⃣"]]); // Dictionary with (int)key -> (String)number as emoji.
	static LETTERS = new Map([[0, "🇦"], [1, "🇧"], [2, "🇨"], [3, "🇩"], [4, "🇪"], [5, "🇫"], [6, "🇬"], [7, "🇭"], [8, "🇮"], [9, "🇯"], [10, "🇰"], [11, "🇱"], [12, "🇲"], [13, "🇳"], [14, "🇴"], [15, "🇵"], [16, "🇶"], [17, "🇷"], [18, "🇸"], [19, "🇹"], [20, "🇺"], [21, "🇻"], [22, "🇼"], [23, "🇽"], [24, "🇾"], [25, "🇿"]]); // Dictionary (int)key -> (String)letter at the key position in the alphabet as emoji.

	static DARK_BLUE_COLOR = "#122939";
	static PURPLE_COLOR    = "#9351c5";
	static ORANGE_COLOR    = "#fa7d09";
	static GREEN_COLOR     = "#16a086";
	static WHITE_COLOR     = "#ecf0f1";
	static BLACK_COLOR     = "#1d1f1e";
	static BLUE_COLOR      = "#297fb8";
	static RED_COLOR       = "#b40000";


	/**
	 * Transforms a ISO8601 duration into a well-formed string like : 12h 47m 21s.
	 * @param duration {string} The ISO8601 format duration.
	 * @return {string} The string equivalent if the duration time given as number with hours, minutes and seconds seperated (with or without spaces).
	 */
	static ISO8601ToString(duration) {
		return duration.replace(/[HMS]/g, (letter) => letter.toLowerCase() + (letter !== "S" ? " " : "")).replace(/[PT]/g, "");
	}

	static ISO8601ToSeconds(duration) {
		const onlyDuration = duration.replace(/[PTS]/g, "").replace(/[HM]/g, "/").split("/").map(element => Number(element));
		switch (onlyDuration.length) {
			case 1:
				return onlyDuration[0]; // Only seconds
			case 2:
				return onlyDuration[1] + onlyDuration[0] * 60; // Seconds and minutes
			case 3:
				return onlyDuration[2] + onlyDuration[1] * 60 + onlyDuration[0] * 3600; // Seconds, minutes and hours
			default:
				return 0;
		}
	}

	/**
	 * Convert an amount of seconds into a ISO8601 duration.
	 * @param secondsAmount {number} The amount of seconds to convert.
	 * @returns {string}
	 */
	static secondsDurationToISO8601(secondsAmount) {
		let isoValues = [0, 0, secondsAmount];
		isoValues[1] += Math.floor(isoValues[2] / 60); // Report the excedent amount of seconds into minutes
		isoValues[2]  = isoValues[2] % 60; // Update the seconds remaining
		isoValues[0] += Math.floor(isoValues[1] / 60); // Report the exceeding amount of minutes into hours
		isoValues[1]  = isoValues[1] % 60; // Update the minutes remaining
		return `PT${isoValues[0] > 0 ? isoValues[0] + "H" : ""}${isoValues[1] > 0 ? isoValues[1] + "M" : ""}${isoValues[2]}S`;
	}

	static secondsDurationToString(secondsAmount) {
		return Utils.ISO8601ToString(Utils.secondsDurationToISO8601(secondsAmount));
	}

	/**
	 * Convert a string duration into a ISO8601 duration. Ex 24:12 -> PT24M12S
	 * @param duration {string} the duration string formatted as HH:MM:SS
	 * @returns {?string}
	 */
	static stringDurationToISO8601(duration) {
		const durationParts = duration.split(":").length - 1;

		if (durationParts === 0)
			return `PT${duration}S`;
		if (durationParts === 1)
			return `PT${duration.replace(":", "M")}S`;
		if (durationParts === 2)
			return `PT${duration.replace(":", "H").replace(":", "M")}S`;
		return null;
	}

	/**
	 * Select and return a random element of a list.
	 * @param  list {any[]} The list this  will choose a random item from.
	 * @return {any} A random element of the argument list.
	 */
	static randomItem(list) {
		return list[Math.floor(Math.random() * list.length)];
	}

	/**
	 * Remoe and return an element of an array.
	 * @param list {any[]} The array to take a random element from.
	 * @returns {?any} A random element of this array that have been removed from it.
	 */
	static deleteRandomItem(list) {
		return Utils.shuffleArray(list).pop();
	}

	/**
	 * Remove elements from a list.
	 * @param originalList {any[]} The list to remove items from.
	 * @param itemsToRemoveList {any[]} The list of items to remove.
	 * @returns {any[]} The original list with items specified removed of those were present.
	 */
	static removeItems(originalList, itemsToRemoveList) {
		return originalList.filter(item => !itemsToRemoveList.includes(item));
	}

	/**
	 * Change the order of an array.
	 * @param arr {any[]} An array of elements.
	 * @returns {any} The same filled array with elements order different.
	 */
	static shuffleArray(arr) {
		return arr.sort(() => Math.random() - 0.5);
	}

	/**
	 * Delete a specific message after several seconds.
	 * @param message {Discord.Message} The Discord.Message to delete.
	 * @param timeoutMode {boolean} Wether to delete the message after a certain amount of time or immediately.
	 */
	static deleteMessage(message, timeoutMode = true) {
		if (!message || !message.deletable)
			return;

		setTimeout(() => {
			if (!message || !message.deletable)
				return;

			message.delete()
				.catch(err => console.error(`Error occurred when trying to delete a message on ${Utils.getMessageLocation(message)} :\n${err}`));
		}, timeoutMode ? Utils.MESSAGE_DEL_TIME * 1000 : 0);
	}

	/**
	 * Return a string containing the server name and channel name where a message has been sent.
	 * @param message {Discord.Message} The message to locate.
	 * @returns {string}
	 */
	static getMessageLocation(message) {
		return `"${message.guild.name} > ${message.channel.name}"`;
	}

	/**
	 * Return a string containing the server name and channel name.
	 * @param channel {Discord.TextChannel|Discord.VoiceChannel} The channel to locate.
	 * @returns {string}
	 */
	static getChannelLocation(channel) {
		return `"${channel.guild.name} > ${channel.name}"`;
	}

	/**
	 * Remove a local file if found on the given path.
	 * @param fileName{string} The path to the file to suppress.
	 * @returns {Promise<boolean>} Weather if a file was removed from the system or not.
	 */
	static supressFileIfExist(fileName) {
		return new Promise((resolve) => {
			if (fs.existsSync(fileName)) {
				fs.unlink(fileName, (err) => {
					if (err) {
						console.error(`\t↪ Cannot suppress media "${fileName}" :\n${err}`);
						resolve(false);
					}

					resolve(true);
				});
			} else {
				resolve(false);
			}
		});
	}

	/**
	 * Get the amount of bit taken by an online media identified by the parameter link.
	 * @param link{String} The link leading to the video or picture online.
	 * @returns {Promise<int>} The size of the media corresponding to the parameter link.
	 */
	static getOnlineMediasize(link) {
		return new Promise((resolve, reject) => {
			fetch(link)
				.then(res => {
					return resolve(parseInt(res.headers.get("content-length")));
				}).catch(err => {
				console.error(`\t↪ Cannot obtain media size for link "${link}" : ${err}`);
				reject(err);
			});
		});
	}

	static longStringToMultipleEmbeds(longString, options) {
		let embedsList = [];
		do {
			const embedElement = new Discord.MessageEmbed().setDescription(String(longString.substring(0, 4096)));
			if (options.embedsColor)
				embedElement.setColor(options.embedsColor);

			embedsList.push(embedElement);
			longString = longString.substring(4096);
		} while (longString.length > 0);

		if (options.title)
			embedsList[0].setTitle(options.title);
		if (options.footer)
			embedsList[embedsList.length - 1].setFooter(options.footer);

		return embedsList;
	}

	// ----------------
	// VOICE CONNECTION
	// ----------------

	/**
	 * Return the voice connection of this bot if there is one (if it is connected to a VoiceChannel). Otherwise, return null.
	 * @param {Discord.Snowflake} guildId
	 * @returns {?voiceManager.VoiceConnection}
	 */
	static getVoiceConnection(guildId) {
		return voiceManager.getVoiceConnection(guildId);
	}

	/**
	 * Disconnect the bot from a Vocal Channel if it is connected.
	 * @param {Discord.Snowflake} guildId
	 */
	static disconnectVoiceConnection(guildId) {
		const existingConnection = Utils.getVoiceConnection(guildId);
		if (!existingConnection)
			return false;

		return existingConnection.disconnect();
	}

	/**
	 * Connect the bot to the specified VoiceChannel.
	 * @param voiceChannel {Discord.VoiceChannel} The voice channel to connect the bot to.
	 * @returns {voiceManager.VoiceConnection} The created connection that can be used to wait for events (ready, paused etc ...).
	 */
	static createVoiceConnection(voiceChannel) {
		// noinspection JSCheckFunctionSignatures
		return voiceManager.joinVoiceChannel({
			channelId: voiceChannel.id,
			guildId: voiceChannel.guild.id,
			adapterCreator: voiceChannel.guild.voiceAdapterCreator
		});
	}
}

module.exports = Utils;