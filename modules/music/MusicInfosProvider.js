/**
 * @author Killian
 */

// noinspection JSUnusedLocalSymbols
const Discord = require("discord.js"); // Used for documentation.
const MyEmbed = require("../../utilities/MyEmbed");
const Stream  = require("./StreamPlatforms/");
const fetch   = require("node-fetch");
const Utils   = require("../../utilities/Utils");

class MusicInfosProvider {

	constructor() {
		this.soundcloud = new Stream.SoundCloud();
		this.youtube    = new Stream.YouTube();
		this.spotify    = new Stream.Spotify();
		this.deezer     = new Stream.Deezer();
	}

	/**
	 * Identify the streaming platform source of a provided link if its from YouTube, Soundcloud, Spotify or Deezer.
	 * Also identify if the link is for a video, a playlist, a song, an account or anything else for these platforms.
	 * If the link comes from another platform, return null.
	 * @param link {string} The link to identify the source platform from.
	 * @returns {Promise<string[]>} The platform and the type of this link with the following structure :
	 *
	 *  - so_type - SoundCloud
	 *  - sp_type - Spotify
	 *  - dz_type - Deezer
	 *  - yt_type - YouTube
	 *
	 * Where type could be album, playlist, track, video, channel, stream depending on the source platform.
	 */
	async validate(link) {
		if (!link)
			return [null, null];

		link = link.trim(); // Remove spaces
		if (!link.startsWith("https") && !link.startsWith("http")) // Identify if this is a link of a sample of words.
			return ["search", link];

		// The link is from Spotify
		if (link.indexOf("spotify") !== -1) {
			link       = link
				.split("?")[0]
				.replace("/embed/", "/"); // Remove arguments and embed
			const type = link.split("/")[3]; // Identify the type of media
			return [type ? `sp_${type}` : null, link];
		}

		// The link is from SoundCloud
		if (link.indexOf("soundcloud") !== -1) {
			link = link.split("?")[0]; // Remove arguments
			// noinspection HttpUrlsUsage
			const dividedURL = link
				.replace("https://", "")
				.replace("http://", "")
				.split("/")
				.slice(1);

			if (dividedURL.length === 1) // Profile case
				return ["so_profile", link];

			if (dividedURL.length === 2) // Track case
				return ["so_track", link];

			if (dividedURL.length === 3 && dividedURL[1] === "sets") // Playlist case
				return ["so_playlist", link];

			return [null, null];
		}

		// The link is from Deezer
		if (link.indexOf("deezer") !== -1) {
			link = link.split("?")[0]; // Remove arguments

			if (link.indexOf("deezer.page.link") !== -1) { // Short url
				// Retrieve orginal deezer link
				link = await fetch(link)
					.then(async datas => {
						if (datas.status === 404)
							return null;
						return datas.url.split("?")[0];
					}).catch(err => {
						console.error(err);
						return null;
					});

				if (!link)
					return [`dz_404`, null];
			}

			const type = link.split("/")[4]; // Identify the type
			return [type ? `dz_${type}` : null, link];
		}

		// The link is from YouTube
		const check = this.youtube.validate(link);
		return [check ? `yt_${check}` : null, link];
	}

	/**
	 * Search for elemnts on YouTube platform.
	 * @param searchTerms {string} Something to search on YouTube.
	 * @param interaction {Discord.CommandInteraction} The command interaction that lead to this research call.
	 * @param options {SearchOptions} Options to filter the results.
	 * @returns {Promise<Stream.YoutubePlayList|Stream.YoutubeVideo>}
	 */
	search(searchTerms, interaction, options) {
		return this.youtube.search(searchTerms, options)
			.catch(err => {
				if (err === Stream.YouTube.NO_RESULT) {
					interaction.editReply({ embeds: [new MyEmbed("Pas de résultat sur YouTube 😕", Utils.ORANGE_COLOR)] })
						.catch(err => console.error(`\t↪ Cannot edit :\n${err}`));
					return console.log(`\t↪ No result`);
				}

				interaction.editReply({ embeds: [new MyEmbed("J'ai rencontré un problème lors la recherche de cette musique sur YouTube 😕", Utils.RED_COLOR)] })
					.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
				return console.error(`\t↪ Cannot search :\n${err}`);
			});
	}

	// =============
	//    YOUTUBE
	// =============

	/**
	 * Get informations about a YouTube video identified by its URL.
	 * @param videoURL {string} The YouTube video URL to get informations about.
	 * @returns {Promise<Stream.YoutubeVideo>} The video's informations.
	 */
	getYoutubeVideoInfos(videoURL) {
		return this.youtube.getVideoInfos(videoURL);
	}

	/**
	 * Used to retrieve YouTube playlist informations.
	 * @param playlistURL{String} The YouTube playlist url.
	 * @returns {Promise<Stream.YoutubePlayList>} The json datas queried about this playlist.
	 */
	getYoutubePlaylistInfos(playlistURL) {
		console.log(`\t↪ Youtube playlist link received : ${playlistURL}`);

		return this.youtube.getPlaylistInfos(playlistURL);
	}

	// ================
	//    SOUNDCLOUD
	// ================

	/**
	 * Provide informations about a SoundCloud track identified by the soundCloudURL argument.
	 * @param soundCloudURL {string} The SoundCloud track URL that will be used to get informations from.
	 * @returns {Promise<Stream.SoundCloudTrack>}
	 */
	getSoundCloudTrackInfos(soundCloudURL) {
		return this.soundcloud.getInfos(soundCloudURL);
	}

	/**
	 * Provide informations about a SoundCloud playlist identified by the soundCloudURL argument.
	 * @param soundCloudURL {String} The playlist argument.
	 * @returns {Promise<Stream.SoundCloudPlaylist>}
	 */
	getSoundCloudPlaylistInfos(soundCloudURL) {
		return this.soundcloud.getInfos(soundCloudURL);
	}

	// =============
	//    SPOTIFY
	// =============

	/**
	 * Provide informations about a spotify track identified by the spotifyURL argument.
	 * @param spotifyURL {String} The track web url.
	 * @returns {Promise<Stream.SpotifyTrack>}
	 */
	getSpotifyTrackInfos(spotifyURL) {
		return this.spotify.getInfos(spotifyURL);
	}

	/**
	 * Provide informations about a spotify album identified by the spotifyURL argument.
	 * @param spotifyURL {String} The album web url.
	 * @returns {Promise<Stream.SpotifyAlbum>}
	 */
	getSpotifyAlbumInfos(spotifyURL) {
		return this.spotify.getInfos(spotifyURL);
	}

	/**
	 * Provide informations about a spotify playlist identified by the spotifyURL argument.
	 * @param spotifyURL {String} The playlist web url.
	 * @returns {Promise<Stream.SpotifyPlaylist>}
	 */
	getSpotifyPlaylistInfos(spotifyURL) {
		return this.spotify.getInfos(spotifyURL);
	}

	// ============
	//    DEEZER
	// ============

	/**
	 * Query informations about a specific Deezer track on the API.
	 * @param deezerURL {string} The Deezer track url.
	 * @returns {Promise<Stream.DeezerTrack>}
	 */
	getDeezerTrackInfos(deezerURL) {
		return this.deezer.getInfos(deezerURL);
	}

	/**
	 * Query informations about a specific Deezer playlist on the API.
	 * @param deezerURL {string} The Deezer playlist url.
	 * @returns {Promise<Stream.DeezerPlaylist>}
	 */
	getDeezerPlaylistInfos(deezerURL) {
		return this.deezer.getInfos(deezerURL);
	}

	/**
	 * Query informations about a specific Deezer album on the API.
	 * @param deezerURL {string} The Deezer album url.
	 * @returns {Promise<Stream.DeezerAlbum>}
	 */
	getDeezerAlbumInfos(deezerURL) {
		return this.deezer.getInfos(deezerURL);
	}
}

module.exports = MusicInfosProvider;