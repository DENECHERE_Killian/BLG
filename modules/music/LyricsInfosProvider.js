// noinspection JSUnusedLocalSymbols
/**
 * @author Killian
 */

const { YouTube } = require("./StreamPlatforms/YouTube"); // Use for doc.
const Discord     = require("discord.js");
const fetch       = require("node-fetch");
const Utils       = require("../../utilities/Utils");

const MUSIXMATCH_ICON = "https://assets.materialup.com/uploads/9c0c69e5-c90c-4e74-bc94-d994274c8810/0x0ss-85.jpg";
const GENIUS_ICON     = "https://images.genius.com/avatars/medium/3fa527d72ef620b2c0d497f8b23e28af";

const PLATFORMS_NAMES_BY_RELEVANCE = ["genius", "musixmatch"];
const MAX_EMBED_SENT               = 5; // The number of embeds sent at a time.
/**
 * @typedef {Object} PlatformsLyrics
 * @property {?LyricsInfos} genius The lyrics found on genius.
 * @property {?LyricsInfos} musixmatch The lyrics found on musixmatch.
 */

/**
 * @typedef {Object} LyricsInfos
 * @property {string} LyricsInfos.platform The name of the lyrics providing platform.
 * @property {string} LyricsInfos.lyrics The lyrics as a giant string.
 * @property {Discord.MessageEmbed[]} LyricsInfos.embedsList
 */

class LyricsInfosProvider {

	/**
	 * Fetch for lyrics through different sources and return the result as a string with the source given.
	 * @param title {string} The title of the song to search lyrics for.
	 * @returns {Promise<PlatformsLyrics>}
	 */
	static getLyrics(title) {
		return new Promise(async (resolve) => {
			console.log(`\t↪ Searching lyrics for "${title}"`);
			const clearedTitle = LyricsInfosProvider.cleanSearchTitle(title);

			// Seach for lyrics on the Genius platform
			const geniusLyrics     = await LyricsInfosProvider.searchLyricsOnGenius(clearedTitle)
				.catch(err => console.error(`\t\t↪ Error while searching lyrics on Genius :\n${err}`));
			// Seach for lyrics on the MusixMatch platform
			const musixmatchLyrics = await LyricsInfosProvider.searchLyricsOnMusixmatch(clearedTitle)
				.catch(err => console.log(`\t\t↪ Error while searching lyrics on MusixMatch :\n${err}`));

			// WARNING : EACH PLATFORM NAME (KEY OF THE RESULT OBJECT) NEED TO HAVE THE SAME NAME AS THE "platform" ATTRIBUTE.
			let lyricsInfos = {};
			if (geniusLyrics)
				lyricsInfos[PLATFORMS_NAMES_BY_RELEVANCE[0]] = { platform: PLATFORMS_NAMES_BY_RELEVANCE[0], lyrics: geniusLyrics, embedsList: Utils.longStringToMultipleEmbeds(geniusLyrics, { title: `Paroles pour "${title}"`, embedsColor: "#fffc65", footer: { text: "Source: Genius", iconURL: GENIUS_ICON } }) };
			if (musixmatchLyrics)
				lyricsInfos[PLATFORMS_NAMES_BY_RELEVANCE[1]] = { platform: PLATFORMS_NAMES_BY_RELEVANCE[1], lyrics: musixmatchLyrics, embedsList: Utils.longStringToMultipleEmbeds(musixmatchLyrics, { title: `Paroles pour "${title}"`, embedsColor: "#ff2073", footer: { text: "Source: MusixMatch", iconURL: MUSIXMATCH_ICON } }) };

			resolve(lyricsInfos);
		});
	}

	/**
	 * Search for lyrics on Musixmatch platform for a specific song.
	 * @param title {string} The song's name to search lyrics for.
	 * @returns {Promise<?string>} The found lyrics.
	 */
	static searchLyricsOnMusixmatch(title) {
		return new Promise((resolve, reject) => {
			const link = `https://www.musixmatch.com/fr/search/${title}/tracks`;
			console.log(`\t\t↪ Fetching ${link}`);

			fetch(link).then(async t => {
				const res = await t.text();

				if (res.indexOf("Aucune chanson trouvée") !== -1) {
					console.log("\t\t↪ No result on Musixmatch");
					return resolve();
				}

				const split       = "{\"res\":{\"res\":{ \"attributes\"" + res
						?.split("\"attributes\"")[1]
						?.split(",\"1\":")[0]
						?.split(",\"state\":function () {")[0]
					+ "}}";
				const searchDatas = JSON.parse(split);

				console.log(`\t\t↪ Search page datas obtained, fetching music page : https://www.musixmatch.com/fr/paroles/${searchDatas.res.res.vanityId}`);
				await fetch(`https://www.musixmatch.com/fr/paroles/${searchDatas.res.res.vanityId}`, { "headers": { "accept-language": "fr-FR;q=0.9" } })
					.then(async i => {
						const lyricsPageDatas = await i.text();

						if (lyricsPageDatas.indexOf("Paroles limitées") !== -1) {
							console.log("\t\t↪ Private lyrics found on Musixmatch");
							return resolve();
						}
						if (lyricsPageDatas.indexOf("Paroles non disponibles") !== -1) {
							console.log("\t\t↪ No lyrics found on Musixmatch");
							return resolve();
						}

						const lyrics = lyricsPageDatas
							?.split("<span class=\"lyrics__content__ok\">")
							.slice(1)
							.map(htmlBloc => htmlBloc.split("</span>")[0])
							.join("\n");

						console.log(`\t\t↪ Lyrics found on musixmatch`);
						resolve(lyrics);

					}).catch(err => {
						return reject(err);
					});
			}).catch(err => {
				return reject(err);
			});
		});
	}

	/**
	 * Search lyrics on Ggenius platform for a specific song.
	 * @param title {string} The song's name to search lyrics for.
	 * @return {Promise<?string>} The found lyrics.
	 */
	static searchLyricsOnGenius(title) {
		const link = `https://genius.com/api/search/multi?per_page=5&q=${title}`;
		console.log(`\t\t↪ fetching ${link}`);
		return new Promise((resolve, reject) => {
			fetch(link, { "headers": { "accept": "application/json, text/plain, */*", "accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7" } })
				.then(async res => {
					const resJSON = await res.json();

					// No lyrics
					if (resJSON.response.sections[0].hits.length === 0) {
						console.log(`\t\t↪ No lyrics found on Genius`);
						return resolve();
					}

					const lyrics          = await LyricsInfosProvider.getGeniusLyrics(resJSON.response.sections[0].hits[0].result.path);
					const processedLyrics = lyrics.replaceAll(/\[(.*?)]\n/g, "");
					console.log(`\t\t↪ Lyrics found on Genius`);

					resolve(processedLyrics);
				}).catch(err => {
				return reject(err);
			});
		});
	}

	/**
	 * Recover the lyrics from the Genius web page of the searched song.
	 * @param lyrics_path {string} The path to the web page of the lyrics.
	 * @returns {Promise<?string>} The scraped lyrics.
	 */
	static getGeniusLyrics(lyrics_path) {
		return new Promise((resolve, reject) => {
			fetch(`https://genius.com${lyrics_path}`, { "headers": { "accept": "application/json, text/plain, */*", "accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7" } })
				.then(async res => {
					const page = await res.text();

					let lyrics = page
						.split("window.__PRELOADED_STATE__ = JSON.parse('")[1]
						.split("');\n      window.__APP_CONFIG__ = ")[0]
						.split("\\\"body\\\":{\\\"html\\\":")[1]
						.split(",\\\"children\\\"")[0]
						.replaceAll("\\\"", "\"")
						.replaceAll("\\\\\"", "\\\"")
						.replaceAll("\\\\n", "\\n")
						.replaceAll(`\\'`, `'`);

					lyrics = JSON.parse(lyrics).replace(/<\/?\w+((\s+\w+(\s*=\s*(?:".*?"|'.*?'|[^'">\s]+))?)+\s*|\s*)\/?>/gi, "");

					resolve(lyrics);
				}).catch(err => {
				return reject(err);
			});
		});
	}

	/**
	 * Remove useless parts of a song title to upgrade chances to get lyrics search result on the different platforms.
	 * @param title {string} The song's title that need to be cleared.
	 * @returns {string} A new string with the cleared original song's title.
	 */
	static cleanSearchTitle(title) {
		return title
			.replace(/ç/gi, "c") // Change
			.replace(/\[(.*?)]|\((.*?)\)/gi, "") // Remove all content between brackets and parenthesis
			.replace(/\b(ft|feat|ft.|featuring)\s+(.*)$/gi, "") // Remove featuring artists names
			.match(/\b(?!Official|Officiel|Video|Music|Song|Clip|by|and|original|Hour|Hora|Lyric|remix)\w+\b/gi) // Remove video details
			.join("%20");
	}

	static getBestPlatform(platformsLyrics) {
		if (!platformsLyrics || Object.keys(platformsLyrics).length === 0)
			return null;

		for (const platformName of PLATFORMS_NAMES_BY_RELEVANCE)
			if (platformsLyrics[platformName])
				return { best: platformsLyrics[platformName], others: LyricsInfosProvider.getOtherPlatforms(platformsLyrics, platformName) };
	}

	static getOtherPlatforms(platformsLyrics, chosenPlatformName) {
		if (!platformsLyrics || Object.keys(platformsLyrics).length === 0 || !chosenPlatformName || !platformsLyrics[chosenPlatformName])
			return null;

		// delete platformsLyrics[chosenPlatformName];
		const { [chosenPlatformName]: unused, ...othersPlatforms } = platformsLyrics;
		return othersPlatforms;
	}

	static async showLyrics(lyricsFoundByPlatform, interaction, allowSwap = false, youtubeSong = null, playlist = null) {
		const relevanceDatas = LyricsInfosProvider.getBestPlatform(lyricsFoundByPlatform);
		let bestPlatform     = relevanceDatas.best;
		let otherPlatforms   = relevanceDatas.others;

		let lastMessage;
		for (let embed of bestPlatform.embedsList.slice(0, MAX_EMBED_SENT)) {
			if (bestPlatform.embedsList.indexOf(embed) === MAX_EMBED_SENT - 1 && bestPlatform.embedsList.length > MAX_EMBED_SENT) // If the embed
				embed = embed.setFooter({ text: `La limite de ${MAX_EMBED_SENT} embeds a été atteinte (il en restait ${bestPlatform.embedsList.length - MAX_EMBED_SENT} à envoyer)` });

			lastMessage = await interaction.channel.send({ embeds: [embed] })
				.catch(err => console.error(`\t\t↪ Cannot send song lyrics :\n${err}`));
		}

		if ((!allowSwap || !otherPlatforms) && !youtubeSong)
			return;

		let buttonsList = [];
		if (youtubeSong)
			buttonsList.push(new Discord.MessageButton().setCustomId("play_button").setLabel("Jouer sur YouTube").setStyle("SECONDARY").setEmoji(`🔎`));
		if (allowSwap && otherPlatforms)
			for (const name of Object.keys(otherPlatforms))
				buttonsList.push(new Discord.MessageButton().setCustomId(`${name}_swap_button`).setLabel(`Résultat de ${name}`).setStyle("SECONDARY").setEmoji(`🔁`));
		lastMessage.edit({ components: [new Discord.MessageActionRow().addComponents(buttonsList)] })
			.catch(err => console.error(`\t↪ Cannot edit :\n${err}`));

		const swapFilter    = i => i.user.id === lastMessage.author.id;
		const swapCollector = lastMessage.createMessageComponentCollector({ swapFilter, time: Utils.COLLECTOR_TIME * 1000 });

		swapCollector.on("collect", async i => {
			if (i.customId === "play_button") {
				console.log("\t\t↪ Start playing on YouTube");
				playlist.addSong(youtubeSong);
				// noinspection ES6MissingAwait
				playlist.playOrUpdate(interaction);
				swapCollector.stop("Play on youtube");
			}

			for (const otherPlatformName of Object.keys(otherPlatforms)) {
				if (i.customId !== `${otherPlatformName}_swap_button`)
					continue;

				console.log(`\t↪ Swap lyrics source platform to ${otherPlatformName}`);

				// Update message content and buttons
				otherPlatforms = LyricsInfosProvider.getOtherPlatforms(lyricsFoundByPlatform, otherPlatformName);
				bestPlatform   = lyricsFoundByPlatform[otherPlatformName];
				buttonsList    = [];
				for (const swapName of Object.keys(otherPlatforms))
					buttonsList.push(new Discord.MessageButton().setCustomId(`${swapName}_swap_button`).setLabel(`Chercher sur ${swapName}`).setStyle("SECONDARY").setEmoji(`🔁`));

				i.update({ embeds: bestPlatform.embedsList, components: [new Discord.MessageActionRow().addComponents(buttonsList)] })
					.catch(err => console.error(`\t↪ Cannot update :\n${err}`));

				swapCollector.resetTimer();
				break;
			}
		});

		swapCollector.on("end", async () => {
			lastMessage.edit({ components: [] })
				.catch(err => console.error(err));
		});
	}
}

module.exports = LyricsInfosProvider;