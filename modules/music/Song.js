/**
 * @author Killian
 */

const voiceManager = require("@discordjs/voice");
const Discord      = require("discord.js");
const MyEmbed      = require("../../utilities/MyEmbed");
const Stream       = require("./StreamPlatforms/");
const Utils        = require("../../utilities/Utils");
const lip          = require("./LyricsInfosProvider");

class Song {
	/**
	 * @param defaultMessageChannel {Discord.TextChannel} The text channel to send any song message if it is not possible to edit the normal one.
	 */
	constructor(defaultMessageChannel) {
		/**
		 * The title of this song.
		 * @type {string}
		 */
		this.title = undefined;

		/**
		 * The duration in ISO8601 format. See {@See Utils} for convertion functions.
		 * @type {string}
		 */
		this.duration = undefined;

		/**
		 * The seconds amount to skip on the video. If not defined, nothing to skip. See {@See Utils} for convertion functions.
		 * When the stream will start, it will seek to this seconds amount in the audio duration.
		 * @type {string}
		 */
		this.seek = undefined;

		/**
		 * The identifier of this song on the audio reading platform.
		 * That can be YouTube or SoundCloud.
		 * @type {number}
		 */
		this.id = undefined;

		/**
		 * The http url that leads to the online page of this song.
		 * @type {string}
		 */
		this.webURL = undefined;

		/**
		 * The http url that lead to the icon representing the currently streaming platform used to play audio.
		 * @type {string}
		 */
		this.streamPlatformThumbnail = undefined;

		/**
		 * The original object obtained from the API in order to get stream necessary informations.
		 * @type {YoutubeVideo|SoundCloudTrack|DeezerTrack|SpotifyTrack}
		 */
		this.originalPlatformAPIInfos = undefined;

		/**
		 * The http url that correspond to a picture representing this song.
		 * That can be a video thumbnail or an album front.
		 * @type {string}
		 */
		this.thumbnailURL = undefined;

		/**
		 * The main artist of this song.
		 * @type {string}
		 */
		this.ownerName = undefined;

		/**
		 * The http url that lead to the online page of this song's owner.
		 * @type {string}
		 */
		this.ownerURL = undefined;

		/**
		 * Wether this song is currently streamed or has ealready been and is entirely available.
		 * @type {boolean}
		 */
		this.isLive = undefined;

		/**
		 * Wheter this song is not streamable because of an age limitation or not.
		 * @type {boolean}
		 * @default false
		 */
		this.isAgeRestricted = false;

		/**
		 * Wheter this song is not entirely streamable because of a platform premium limitation or not.
		 * @type {boolean}
		 * @default false
		 */
		this.isSnipped = false;

		/**
		 * The Discord user username that asked for this song to be played.
		 * @type {string}
		 */
		this.commandAuthorName = undefined;

		/**
		 * The Discord user profile picture that asked for this song to be played.
		 * @type {undefined}
		 */
		this.commandAuthorThumbnailURL = undefined;

		/**
		 * This song playing message showing the details of it.
		 * @type {Discord.Message}
		 */
		this.playingMessage = undefined;

		/**
		 * The default textChannel to send informations to.
		 * @type {Discord.TextChannel}
		 */
		this.defaultMessageChannel = defaultMessageChannel;
	}

	/**
	 * Create a new song from an already existing one.
	 * @param existingSong {Song} A already existing Song.
	 * @returns {Song|null} This song object filled with datas.
	 */
	fromSong(existingSong) {
		if (!existingSong) {
			console.warn(`Try to create a new song from undefined one`);
			return null;
		}

		return Object.assign(this, existingSong);
	}

	/**
	 * Create a new song from the informations of a YouTube video.
	 * @param video {YoutubeVideo} The video infos.
	 * @param user {Discord.User} The user that launched the command that created this song.
	 * @returns {Song|null} This song object filled with datas.
	 */
	fromYoutubeVideo(video, user) {
		if (!video || !user) {
			console.warn(`Try to create a new song from an undefined YouTube video`);
			return null;
		}

		this.title                     = video.title;
		this.duration                  = video.duration;
		this.id                        = video.id;
		this.webURL                    = video.url;
		this.streamPlatformThumbnail   = Stream.YouTube.PLATFORM_ICON;
		this.originalPlatformAPIInfos  = video;
		this.thumbnailURL              = video.thumbnails.default.url;
		this.ownerName                 = video.ownerChannelTitle;
		this.ownerURL                  = video.ownerChannelUrl;
		this.isLive                    = video.isLive;
		this.commandAuthorName         = user.username;
		this.commandAuthorThumbnailURL = user.avatarURL();
		this.seek                      = video.seek;

		return this;
	}

	/**
	 * Create a new song from the informations of a SoundCloud track.
	 * @param soTrack {Stream.SoundCloudTrack} The track infos.
	 * @param user {Discord.User} The user that launched the command that created this song.
	 * @returns {Song|null} This song object filled with datas.
	 */
	fromSoundCloudTrack(soTrack, user) {
		if (!soTrack || !user) {
			console.warn(`Try to create a new song from an undefined SoundCloud track`);
			return null;
		}

		this.title                     = soTrack.title;
		this.duration                  = Utils.secondsDurationToISO8601(Math.floor(soTrack.duration / 1000));
		this.id                        = soTrack.id;
		this.webURL                    = soTrack.permalink_url;
		this.streamPlatformThumbnail   = Stream.SoundCloud.PLATFORM_ICON;
		this.originalPlatformAPIInfos  = soTrack;
		this.thumbnailURL              = soTrack.artwork_url;
		this.ownerName                 = soTrack.publisher_metadata.artist;
		this.ownerURL                  = soTrack.user.permalink_url;
		this.isLive                    = false;
		this.isSnipped                 = soTrack.media.transcodings[0].snipped;
		this.commandAuthorName         = user.username;
		this.commandAuthorThumbnailURL = user.avatarURL();

		return this;
	}

	/**
	 * Create a new song from the informations of a Deezer track.
	 * @param dzTrackInfos {Stream.DeezerTrack} The track infos.
	 * @param user {Discord.User} The user that launched the command that created this song.
	 * @returns {Song|null} This song object filled with datas.
	 */
	fromDeezerTrack(dzTrackInfos, user) {
		if (!dzTrackInfos || !user) {
			console.warn(`Try to create a new song from an undefined Deezer track`);
			return null;
		}

		this.title                     = dzTrackInfos.title;
		this.duration                  = Utils.secondsDurationToISO8601(30);
		this.id                        = dzTrackInfos.id;
		this.webURL                    = dzTrackInfos.link;
		this.streamPlatformThumbnail   = Stream.Deezer.PLATFORM_ICON;
		this.originalPlatformAPIInfos  = dzTrackInfos;
		this.thumbnailURL              = `https://e-cdns-images.dzcdn.net/images/cover/${dzTrackInfos.md5_image}/264x264-000000-80-0-0.jpg`;
		this.ownerName                 = dzTrackInfos.artist.name;
		this.ownerURL                  = dzTrackInfos.artist.link;
		this.isLive                    = false;
		this.isSnipped                 = true;
		this.commandAuthorName         = user.username;
		this.commandAuthorThumbnailURL = user.avatarURL();

		return this;
	}

	/**
	 * Create a new song from the informations of a Spotify track.
	 * @param spTrackInfos {Stream.SpotifyTrack} The track infos.
	 * @param user {Discord.User} The user that launched the command that created this song.
	 * @returns {Song|null} This song object filled with datas.
	 */
	fromSpotifyTrack(spTrackInfos, user) {
		if (!spTrackInfos || !user) {
			console.warn(`Try to create a new song from an undefined Spotify track`);
			return null;
		}

		this.title                     = spTrackInfos.name;
		this.duration                  = Utils.secondsDurationToISO8601(30);
		this.id                        = spTrackInfos.id;
		this.webURL                    = spTrackInfos.external_urls.spotify;
		this.streamPlatformThumbnail   = Stream.Spotify.PLATFORM_ICON;
		this.originalPlatformAPIInfos  = spTrackInfos;
		this.thumbnailURL              = spTrackInfos.album.images[0].url;
		this.ownerName                 = spTrackInfos.artists[0].name;
		this.ownerURL                  = spTrackInfos.artists[0].external_urls.spotify;
		this.isLive                    = false;
		this.isSnipped                 = true;
		this.commandAuthorName         = user.username;
		this.commandAuthorThumbnailURL = user.avatarURL();

		return this;
	}

	// --------
	// DISPLAYS
	// --------

	/**
	 * Create a EmbedMessage with all this song's informations.
	 * @returns {Discord.MessageEmbed}
	 */
	toEmbed() {
		const platform = (() => {
			if (this.originalPlatformAPIInfos instanceof Stream.YoutubeVideo)
				return { name: "YouTube", color: Stream.YouTube.PLATFORM_COLOR };
			if (this.originalPlatformAPIInfos instanceof Stream.SoundCloudTrack)
				return { name: "SoundCloud", color: Stream.SoundCloud.PLATFORM_COLOR };
			if (this.originalPlatformAPIInfos instanceof Stream.DeezerTrack)
				return { name: "Deezer", color: Stream.Deezer.PLATFORM_COLOR };
			if (this.originalPlatformAPIInfos instanceof Stream.SpotifyTrack)
				return { name: "Spotify", color: Stream.Spotify.PLATFORM_COLOR };
		})();

		const embed = new Discord.MessageEmbed()
			.setColor(platform.color)
			.setTitle(this.title)
			.setURL(this.webURL)
			.setThumbnail(this.thumbnailURL)
			.addFields([
				{ name: "Chaine", value: `[${this.ownerName}](${this.ownerURL} 'Lien vers la page web')`, inline: true },
				{ name: "Durée", value: this.isLive ? "Vidéo en direct" : (Utils.ISO8601ToString(this.duration) + (this.seek ? ` (Débuté à ${Utils.secondsDurationToString(this.seek)})` : "")), inline: true }
			])
			.setFooter({ text: this.isSnipped ? "Tu peux tenter une recherche sur YouTube pour obtenir la chanson en entière." : "ㅤ", iconURL: this.streamPlatformThumbnail });

		if (this.isSnipped)
			embed.setDescription(`Musique ${platform.name} en accès limité : tu n'aura qu'un extrait de 30 secondes maximum.`);

		return embed;
	}

	/**
	 * Create a EmbedMessage with all this song's informations.
	 * You can also select a color and a text to showList on top of the embed message near the command author's profile picture.
	 * @param message {string} The text to showList as a title for the embed message.
	 * @param [color] {Discord.ColorResolvable} The color if the embed. See {@link Discord.ColorResolvable}.
	 * @returns {Discord.MessageEmbed} The created embed message ready to send.
	 */
	toPlayingEmbed(message, color = null) {
		const embed = this.toEmbed()
			.setDescription("")
			.setAuthor({ name: `${this.commandAuthorName} - ${message}`, iconURL: this.commandAuthorThumbnailURL });

		if (color)
			embed.setColor(color);
		return embed;
	}

	// ------------------
	// MESSAGE MANAGEMENT
	// ------------------

	/**
	 * Edit the playingMessage of this song (if there is one) to change the buttons shown, deactivate/activate the skip buttons and change the main text.
	 * @param buttonsRowList {Discord.MessageActionRow[]} The buttons to showList under the playing message.
	 * @param [options] {Object}
	 * @param {boolean} options.deactivatePreviousButton = false If true, the previous button will be unusable, otherwise it will be usable.
	 * @param {boolean} options.deactivateNextButton = false If true, the skip button will be unusable, otherwise it will be usable.
	 * @param {string} options.message The main message of the playing song
	 */
	editPlayMessageButtons(buttonsRowList, options = {}) {
		buttonsRowList[0].components[0].setDisabled(options.deactivatePreviousButton ?? false);
		buttonsRowList[0].components[2].setDisabled(options.deactivateNextButton ?? false);

		options.message ??= "Lecture en cours";

		if (!this.playingMessage || !this.playingMessage.editable)
			this.defaultMessageChannel.send({ embeds: [this.toPlayingEmbed(options.message)], components: buttonsRowList })
				.then(m => this.playingMessage = m)
				.catch(err => console.error(`\t↪ Cannot send song playing message :\n${err}`));
		else
			this.playingMessage.edit({ embeds: [this.toPlayingEmbed(options.message)], components: buttonsRowList })
				.catch(err => console.error(`\t↪ Cannot edit song playing message :\n${err}`));
	}

	/**
	 * Edit the playingMessage of this song (if there is one) to showList that the audio streaming has ended.
	 */
	showStopMessage() {
		if (!this.playingMessage || !this.playingMessage.editable)
			return;

		this.playingMessage.edit({ embeds: [this.toPlayingEmbed("Lecture stoppée/terminée", Utils.RED_COLOR).setDescription(this.isAgeRestricted ? "Lecture impossible : limite d'âge" : "")], components: [] })
			.catch(err => console.error(`\t↪ Cannot edit song playing message :\n${err}`));
	}

	/**
	 * Edit this song playing message to let the user know about the age limitation that is blocking the streaming of this song.
	 */
	showLimitAgeMessage() {
		if (!this.playingMessage || !this.playingMessage.editable)
			return;

		this.isAgeRestricted = true;

		this.playingMessage.edit({ embeds: [this.toPlayingEmbed("", Utils.RED_COLOR)], components: [] })
			.catch(err => {
				console.error(`\t↪ Cannot edit song playing message :\n${err}`);
			});
	}

	/**
	 * Send message(s) containing the lyrics found (or not) for this {@link Song}.
	 * @returns {Promise<void>}
	 */
	async showLyricsMessage(interaction) {
		await interaction.reply({ embeds: [new MyEmbed(`Je recherche les paroles de cette musique`, Utils.BLUE_COLOR)], ephemeral: true })
			.catch(err => console.error(`\t↪ Cannot reply to lyrics music button :\n${err}`));

		await lip.getLyrics(this.title)
			.then(async lyricsFoundByPlatform => {
				if (Object.keys(lyricsFoundByPlatform).length === 0) {
					console.log(`\t↪ No result for song "${this.title}"`);
					return interaction.editReply({ embeds: [new MyEmbed("Pas de résultat 😕", Utils.ORANGE_COLOR)] })
						.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
				}

				await lip.showLyrics(lyricsFoundByPlatform, interaction, true);

				interaction.editReply({ embeds: [new MyEmbed("J'ai affiché le résultat 🙂", Utils.GREEN_COLOR)] })
					.catch(err => console.error(`\t↪ Cannot edit songs lyrics reply :\n${err}`));
			}).catch(err => {
				console.error(`\t\t↪ Error while showing lyrics for playing song :\n${err}`);
				interaction.editReply({ embeds: [new MyEmbed("J'ai rencontré une recherche lors de ma recherche 😕", Utils.RED_COLOR)] })
					.catch(err => console.error(`\t↪ Cannot edit songs lyrics reply :\n${err}`));
			});
	}

	// ----------
	// AUDIO PART
	// ----------

	/**
	 * Create an audio ressource from this song informations in order to stream it.
	 * @param {{quality: number, seek: number}} options
	 * @returns {Promise<?voiceManager.AudioResource>}
	 */
	async getAudioRessource(options = {}) {
		if (!this.originalPlatformAPIInfos ||
			!this.originalPlatformAPIInfos instanceof Stream.YoutubeVideo ||
			!this.originalPlatformAPIInfos instanceof Stream.DeezerTrack ||
			!this.originalPlatformAPIInfos instanceof Stream.SpotifyTrack ||
			!this.originalPlatformAPIInfos instanceof Stream.SoundCloudTrack) {
			console.error(`Try to stream ${typeof this.originalPlatformAPIInfos} that is not streamable.`);
			return null;
		}

		if (!options.seek && this.seek)
			options.seek = Number(this.seek);

		const source = await this.originalPlatformAPIInfos.stream(options);

		// noinspection JSCheckFunctionSignatures
		return voiceManager.createAudioResource(source.stream, { inputType: source.type, inlineVolume: true, metadata: { song: this } });
	}

	// -------------
	// OTHER METHODS
	// -------------

	/**
	 * Get a copy of this song's informations.
	 * @returns {Song} A JSON object with a copy of all the informations stored for this song.
	 */
	copy() {
		return JSON.parse(JSON.stringify(this));
	}

}

module.exports = Song;