/**
 * @author Killian
 */

const voiceManager = require("@discordjs/voice");
const Discord      = require("discord.js");
const MyEmbed      = require("../../utilities/MyEmbed");
const Utils        = require("../../utilities/Utils");
const Song         = require("./Song");

// noinspection JSCheckFunctionSignatures
class Playlist {
	static MAX_SONG_ADD_AMOUNT = process.env.TEST === "true" ? 5 : 20; // Amount

	/**
	 * Buttons corresponding to Previous, Pause, Skip, Stop, Lyrics
	 */
	BUTTONS_PREV_PAU_SKI_STO_LYR = [new Discord.MessageActionRow().addComponents([
		new Discord.MessageButton().setCustomId("previous_button").setStyle("PRIMARY").setEmoji("⏮️"),
		new Discord.MessageButton().setCustomId("pause_button").setStyle("PRIMARY").setEmoji("⏸️"),
		new Discord.MessageButton().setCustomId("skip_button").setStyle("PRIMARY").setEmoji("⏭️"),
		new Discord.MessageButton().setCustomId("stop_button").setStyle("DANGER").setEmoji("◻️"),
		new Discord.MessageButton().setCustomId("lyrics_button").setStyle("SECONDARY").setEmoji("📃")
	])];

	/**
	 * Buttons corresponding to Previous, Play, Skip, Stop, Lyrics
	 */
	BUTTONS_PREV_PLA_SKI_STO_LYR = [new Discord.MessageActionRow().addComponents([
		new Discord.MessageButton().setCustomId("previous_button").setStyle("PRIMARY").setEmoji("⏮️"),
		new Discord.MessageButton().setCustomId("play_button").setStyle("PRIMARY").setEmoji("▶️"),
		new Discord.MessageButton().setCustomId("skip_button").setStyle("PRIMARY").setEmoji("⏭️"),
		new Discord.MessageButton().setCustomId("stop_button").setStyle("DANGER").setEmoji("◻️"),
		new Discord.MessageButton().setCustomId("lyrics_button").setStyle("SECONDARY").setEmoji("📃")
	])];

	/**
	 * Buttons corresponding to Previous, Pause, Skip, Stop, Lyrics, Playlist
	 */
	BUTTONS_PREV_PAU_SKI_STO_LYR_PLA = [...this.BUTTONS_PREV_PAU_SKI_STO_LYR,
		new Discord.MessageActionRow().addComponents([
			new Discord.MessageButton().setLabel("Playlist").setStyle("LINK").setURL("")
		])
	];

	/**
	 * Buttons corresponding to Previous, Play, Skip, Stop, Lyrics, Playlist
	 */
	BUTTONS_PREV_PLA_SKI_STO_LYR_PLA = [...this.BUTTONS_PREV_PLA_SKI_STO_LYR,
		new Discord.MessageActionRow().addComponents([
			new Discord.MessageButton().setLabel("Playlist").setStyle("LINK").setURL("")
		])
	];

	//-------------
	// CONSTRUCTORS
	//-------------


	/**
	 * Create a new Playlist object.
	 * @param defaultMessageChannel {Discord.TextChannel} The text channel to send any playlist message if it is not possible to edit the normal one.
	 * @param guildId {Discord.Snowflake} This playlist associated guild id
	 */
	constructor(defaultMessageChannel, guildId) {
		/**
		 * The content of the playlist
		 * @type {Song[]}
		 * @default []
		 */
		this.songsList = [];

		/**
		 * Corresponds to the currently plaed song index in the playlist's songs list.
		 * @type {number}
		 * @default 0
		 */
		this.playingIndex = 0;

		/**
		 * The list of songs in this playlist.
		 * @type {Discord.Message}
		 */
		this.embedMessage = null;

		/**
		 * Whether to play back or not the same song as it end.
		 * @type {boolean}
		 * @default false
		 */
		this.loopMode = false;

		/**
		 * The last text channel were a music command was sent.
		 * @type {Discord.TextChannel}
		 */
		this.lastMessageChannel = defaultMessageChannel;

		/**
		 * The Discord provided voice manager used to play song in voice channel.
		 * @type {voiceManager.AudioPlayer}
		 */
		this.player = null;

		/**
		 * The audio informations needed to play a specific song.
		 * @type {voiceManager.AudioResource}
		 */
		this.audioRessource = null;

		this.guildId = guildId;
	}

	/**
	 * Create a new Playlist object and fill itself with copies of each song from the argument songs list.
	 * @param songsList {Song[]} The list of songs to copy in order to fill the new Playlist.
	 * @returns {Playlist} The playlist filled with the arguments list's songs.
	 */
	fromPlaylist(songsList) {
		for (const song of songsList)
			this.addSong(new Song(this.lastMessageChannel).fromSong(song));

		return this;
	}

	//----------------------
	// SONGS LIST MANAGEMENT
	//----------------------

	/**
	 * Remove a certain amount of songs from this playlist between two songs index.
	 * @param startIndex {number} The deletion will startGame from this track index included. The first track is placed at the index 0.
	 * @param deleteCount {number} The deletion will end at this index included.
	 * @returns {Song[]} The songs that were removed from this playlist content.
	 */
	removeSongs(startIndex, deleteCount) {
		if (startIndex + deleteCount >= this.getLength())
			return null;
		return this.songsList.splice(startIndex, deleteCount);
	}

	/**
	 * Empty the playlist songs list.
	 */
	empty() {
		this.getPlayingSong()?.showStopMessage();
		this.songsList = [];
		this.#deleteMessage();
	}

	/**
	 * Add a song to the end of the playlist.
	 * @param song {Song} The song to add.
	 */
	addSong(song) {
		if (!song)
			return null;

		this.songsList.push(song);
	}

	/**
	 * Merge the content of a playlist inside this one.
	 * @param playlist {Playlist} The playlist to merge the content from.
	 */
	addSongsFromPlaylist(playlist) {
		if (!playlist || playlist.isEmpty())
			return null;

		playlist.songsList.filter(song => !!song); // Remove null elements
		this.songsList = [...this.songsList, ...playlist.songsList];
	}

	/**
	 * Add several songs to this playlist from a videos list obtained through ytdl-core package.
	 * @param videosList {YoutubeVideo[]} The videos list containing each video to add to this playlist.
	 * @param user {Discord.User} The user that emitted this request.
	 */
	addSongsFromYoutubeVideoList(videosList, user) {
		if (!videosList || videosList.length === 0 || !user)
			return null;

		for (const video of videosList)
			this.addSong(new Song(this.lastMessageChannel).fromYoutubeVideo(video, user));
	}

	//----------------------
	// AUDIO PLAYING ACTIONS
	//----------------------

	/**
	 * Stop the actually playing song and call for the next one to be played.
	 */
	#next(i) {
		console.log("\t↪ Launching next song");
		i.reply({ embeds: [new MyEmbed("Passage à la musique suivante.", Utils.BLUE_COLOR)], fetchReply: true })
			.catch(err => console.error(`\t↪ Cannot reply to next button :\n${err}`))
			.then(Utils.deleteMessage);

		this.getPlayingSong()?.showStopMessage();
		this.playingIndex++;
		this.player.stop();
	}

	/**
	 * Stop the actually playing song and call for the previous one to be played.
	 */
	#previous(i) {
		console.log("\t↪ Launching previous song");
		i.reply({ embeds: [new MyEmbed("Passage à la musique précédente.", Utils.BLUE_COLOR)], fetchReply: true })
			.catch(err => console.error(`\t↪ Cannot reply to previous button :\n${err}`))
			.then(Utils.deleteMessage);

		this.getPlayingSong()?.showStopMessage();
		this.playingIndex--;
		this.player.stop();
	}

	/**
	 * Send a message telling the playlist is over and that the bot will be disconnected in several seconds.
	 * Also call for the bot to disconnect after several saconds.
	 */
	#end() {
		new MyEmbed(`La playlist est terminée, je me déconnecterai dans ${Utils.DISCONNECT_TIME} secondes.`, Utils.BLUE_COLOR)
			.send(this.lastMessageChannel)
			.catch(err => console.error(`\t↪ Cannot send the message on ${Utils.getChannelLocation(this.lastMessageChannel)} :\n${err}`))
			.then(Utils.deleteMessage);

		setTimeout(() => {
			Utils.disconnectVoiceConnection(this.guildId);
		}, Utils.DISCONNECT_TIME * 1000);
	}

	/**
	 * Play the first song of the playlist if there is one and no other song playing.
	 * Send the playlist content message or update it if it has already been sent.
	 * Also update the running song's message if there is one.
	 * @param {Discord.CommandInteraction} interaction
	 * @returns {Promise<null|boolean>} True if the music was not running and is starting.
	 */
	async playOrUpdate(interaction) {
		if (this.isEmpty())
			return false;

		// Play the song only if there is not already one playing
		if (!this.player) {
			if (await this.startMusicStream(interaction.member.voice.channel))
				return false;

			const song = this.getPlayingSong();
			if (!this.isEmpty()) {
				await this.showContent(interaction);
				this.#updatePlaylistLinkButton();
				song.editPlayMessageButtons(this.player.state.status === "playing" ? this.BUTTONS_PREV_PAU_SKI_STO_LYR_PLA : this.BUTTONS_PREV_PLA_SKI_STO_LYR_PLA, { deactivatePreviousButton: true, deactivateNextButton: !this.songsList[this.playingIndex + 1] });
			} else {
				song.editPlayMessageButtons(this.player.state.status === "playing" ? this.BUTTONS_PREV_PAU_SKI_STO_LYR : this.BUTTONS_PREV_PLA_SKI_STO_LYR, { deactivatePreviousButton: true, deactivateNextButton: !this.songsList[this.playingIndex + 1] });
			}

			return true;
		}

		if (this.embedMessage) {
			this.updateMessageContent();
		} else {
			// noinspection JSIgnoredPromiseFromCall
			await this.showContent(interaction);
		}

		this.#updatePlaylistLinkButton();

		// Update of the actually runnning song message
		const song = this.getPlayingSong();
		if (song)
			song.editPlayMessageButtons(this.player.state.status === "playing" ? this.BUTTONS_PREV_PAU_SKI_STO_LYR_PLA : this.BUTTONS_PREV_PLA_SKI_STO_LYR_PLA, { deactivatePreviousButton: this.songsList.indexOf(song) === 0, deactivateNextButton: !this.songsList[this.playingIndex + 1] });

		return false;
	}

	/**
	 * Connect the bot to a certain voice channel and start to stream the first playlist song.
	 * @param voiceChannel {Discord.VoiceChannel} The voice channel to stream the music into.
	 * @returns {Promise<boolean>} True if ane error occured
	 */
	async startMusicStream(voiceChannel) {
		const wellVoiceConnected = await this.#checkVoiceConnection(voiceChannel)
			.catch(err => console.warn(`\t↪ Unable to connect to VoiceChannel ${Utils.getChannelLocation(voiceChannel)} :\n${err}`));

		if (!wellVoiceConnected) // Unable to connect to voice channel
			return true;

		// Bot voice connection is ok : can play the song.
		const existingConnection = Utils.getVoiceConnection(this.guildId);
		let songToPlay           = this.getPlayingSong(); // The playing song has been updated in another place
		console.log(`\t↪ Creating music stream ${songToPlay.seek ? `(starting at ${Utils.secondsDurationToString(songToPlay.seek)})` : ""}`);

		// An error occured when fetching video informations.
		if (!songToPlay) {
			console.warn(`\t↪ The song is null.`);
			this.removeSongs(this.playingIndex, 1); // Remove the problematic song.
			this.player.stop(); // skip to next song whithout changin the playing index.
			return true;
		}

		// Displaying the currently playing song
		songToPlay.playingMessage = (await (new MyEmbed((this.loopMode ? "**Mode boucle activé**\n" : "") + (songToPlay.isLive ? "Lancement du live, cela peut prendre quelques secondes." : `Lancement de la musique "${songToPlay.title}"`), Utils.GREEN_COLOR))
				.send(this.lastMessageChannel)
				.catch(err => console.error(`\t↪ Cannot send the message on ${Utils.getChannelLocation(this.lastMessageChannel)} :\n${err}`))
		);

		// Update the playlist link song button to counter the potential suppression of the previous playlist message
		if (this.embedMessage)
			this.#updatePlaylistLinkButton();

		// Get audio informations and play those in the voice channel
		this.audioRessource = await songToPlay.getAudioRessource();
		if (!this.audioRessource) {
			console.warn(`\t↪ Cannot get audio ressource of "${songToPlay.title}".`);
			this.removeSongs(this.playingIndex, 1); // Remove the problematic song.
			this.player.stop(); // skip to next song whithout changing the playing index.
			return true;
		}

		existingConnection.subscribe(this.player);

		try {
			if (global.bot.guilds.cache.get(this.guildId).channels.cache.get(existingConnection.joinConfig.channelId).members.size > 1)
				this.player.play(this.audioRessource);
		} catch (e) {
			console.log(`\t↪ Disconnected while trying to launch a music.`);
			return true;
		}
	}

	/**
	 * Connect the bot to voice channel if not already connected and creates the player that will be used to stream music into the voice channel.
	 * Also set the player's events (pause/play, stop, error).
	 * @param voiceChannel {Discord.VoiceChannel} The voice channel that the bot should connect of it's not yet.
	 * @returns {Promise<unknown>} The promise to wait fot the bot to be connected.
	 */
	#checkVoiceConnection(voiceChannel) {
		return new Promise((resolve, reject) => {

			if (Utils.getVoiceConnection(this.guildId))
				return resolve(true);

			if (!voiceChannel.joinable) {
				new MyEmbed(`Je ne peux pas accéder au salon vocal "${voiceChannel.name}" 😕`, Utils.RED_COLOR)
					.send(this.lastMessageChannel)
					.catch(err => console.error(`\t↪ Cannot send the message on ${Utils.getChannelLocation(voiceChannel)} :\n${err}`));
				return reject(false);
			}

			const existingConnection = Utils.createVoiceConnection(voiceChannel);

			const networkStateChangeHandler = (oldNetworkState, newNetworkState) => {
				const newUdp = Reflect.get(newNetworkState, 'udp');
				clearInterval(newUdp?.keepAliveInterval);
			}

			existingConnection.on('stateChange', (oldState, newState) => {
				const oldNetworking = Reflect.get(oldState, 'networking');
				const newNetworking = Reflect.get(newState, 'networking');

				oldNetworking?.off('stateChange', networkStateChangeHandler);
				newNetworking?.on('stateChange', networkStateChangeHandler);
			});

			existingConnection.on(voiceManager.VoiceConnectionStatus.Ready, () => {
				console.log("\t↪ Ready to play audio");
				if (this.player)
					return resolve(true);

				this.player = voiceManager.createAudioPlayer(); // Create the player that will be used to play musics until the bot disconnect from the voice channel

				this.player.on(voiceManager.AudioPlayerStatus.Playing, () => {
					console.log("\t↪ Playing audio");
					// Song playing message
					const song = this.getPlayingSong();
					if (!song)
						return;

					// Change the button to pause
					song.editPlayMessageButtons(this.embedMessage ? this.BUTTONS_PREV_PAU_SKI_STO_LYR_PLA : this.BUTTONS_PREV_PAU_SKI_STO_LYR, { deactivatePreviousButton: this.songsList.indexOf(song) === 0, deactivateNextButton: !this.songsList[this.playingIndex + 1] });

					// Buttons collector
					const filter            = async i => await this.checkCommandValidity(i);
					const playlistCollector = song.playingMessage.createMessageComponentCollector({ filter });

					playlistCollector.on("collect", async i => {
						switch (i.customId) {
							case "previous_button":
								this.#previous(i);
								break;

							case "pause_button":
								i.reply({ embeds: [new MyEmbed("La musique est en pause.", Utils.BLUE_COLOR)], fetchReply: true })
									.catch(err => console.error(`\t↪ Cannot reply to the pause music button :\n${err}`))
									.then(Utils.deleteMessage);

								this.player.pause();
								break;

							case "skip_button":
								this.#next(i);
								break;

							case "stop_button":
								existingConnection.disconnect();
								i.reply({ embeds: [new MyEmbed("Arrêt de la musique", Utils.BLUE_COLOR)], fetchReply: true })
									.catch(err => console.error(`\t↪ Cannot reply to the stop music button :\n${err}`))
									.then(Utils.deleteMessage);
								break;

							case "lyrics_button":
								// noinspection ES6MissingAwait
								this.getPlayingSong().showLyricsMessage(i);
								return;
						}

						playlistCollector.stop();
					});
				});

				this.player.on(voiceManager.AudioPlayerStatus.Idle, () => {
					console.log(`\t↪ The song ended ${this.loopMode ? "(loop mode activated, playing back this music)" : ""}`);
					this.getPlayingSong()?.showStopMessage(); // Show the message if not already displayed

					/**
					 * Since the index of the playing song change when a prev/next button is pressed, when the current music automaticly ends :
					 *  - If the loopMode is disabled, the playing song index did not change, so it plays to the next song in the playlist by incresing the playing index value.
					 *  - If the loopMode is enabled, it plays back the ended song.
					 * If the current music ends because of someone pressed a previous/skip button, the index of the playing song changed and the loop mode does not have an impact on this action.
					 *
					 * TLDR: The loopMode has an impact when the song ends and the system launch the next song on the playlist.
					 * If someone presses a prev/next button, it will change whithout looking for the loopMode value.
					 */
					if (!this.loopMode && this.songsList.indexOf(this.getPlayingSong()) === this.songsList.indexOf(this.audioRessource.metadata.song)) {
						console.log("\t↪ Increasing the playing index because the playing song ended automatically.");
						this.playingIndex++;
					}

					this.updateMessageContent();

					if (!this.isEmpty() && this.playingIndex < this.getLength()) {
						console.log("Launching the next music");
						return this.startMusicStream(voiceChannel);
					}

					// Otherwise, the playlist has ended
					console.log(`\t↪ The playlist is empty`);
					this.#end();
				});

				this.player.on(voiceManager.AudioPlayerStatus.Paused, () => {
					console.log("\t↪ Paused audio");

					// Song playing message
					const song = this.getPlayingSong();
					if (!song)
						return;

					song.editPlayMessageButtons(this.embedMessage ? this.BUTTONS_PREV_PLA_SKI_STO_LYR_PLA : this.BUTTONS_PREV_PLA_SKI_STO_LYR, { deactivatePreviousButton: this.songsList.indexOf(song) === 0, deactivateNextButton: !this.songsList[this.playingIndex + 1], message: "Lecture en pause" });

					const filter            = async i => await this.checkCommandValidity(i);
					const playlistCollector = song.playingMessage.createMessageComponentCollector({ filter });

					playlistCollector.on("collect", async i => {
						switch (i.customId) {
							case "previous_button":
								this.player.unpause(); // Unpause the player because the pause statement block every action
								this.#previous(i);
								break;

							case "play_button":
								this.player.unpause();
								i.reply({ embeds: [new MyEmbed("Lecture de la musique.", Utils.BLUE_COLOR)], fetchReply: true })
									.catch(err => console.error(`\t↪ Cannot reply to play button :\n${err}`))
									.then(Utils.deleteMessage);
								break;

							case "skip_button":
								this.player.unpause(); // Unpause the player because the pause statement block every action
								this.#next(i);
								break;

							case "stop_button":
								this.player.unpause(); // Unpause the player because the pause statement block every action
								existingConnection.disconnect();
								i.reply({ embeds: [new MyEmbed("Arrêt de la musique.", Utils.BLUE_COLOR)], fetchReply: true })
									.catch(err => console.error(`\t↪ Cannot reply to stop music button :\n${err}`))
									.then(Utils.deleteMessage);
								break;

							case "lyrics_button":
								// noinspection ES6MissingAwait
								this.getPlayingSong().showLyricsMessage(i);
								return;
						}
						playlistCollector.stop();
					});
				});

				this.player.on("error", error => {
					if (error.message === "Status code: 410") { // Age restriction
						console.log(`\t↪ Cannot play video : age restriction`);
						this.getPlayingSong().showLimitAgeMessage();
					} else {
						console.error(`\t↪ Player error with track "${this.getPlayingSong().title}" :\n${error}`);
						new MyEmbed("Quelque chose m'empêche de lire cette musique 😕", Utils.RED_COLOR)
							.send(this.lastMessageChannel)
							.catch(err => console.error(`\t↪ Cannot send embed message on ${Utils.getChannelLocation(this.lastMessageChannel)} :\n${err}`))
							.then(Utils.deleteMessage);
					}
				});

				return resolve(true);
			});

			existingConnection.on(voiceManager.VoiceConnectionStatus.Disconnected, async () => {

				try {
					await Promise.race([
						voiceManager.entersState(existingConnection, voiceManager.VoiceConnectionStatus.Signalling, 5_000),
						voiceManager.entersState(existingConnection, voiceManager.VoiceConnectionStatus.Connecting, 5_000)
					]); // Seems to be reconnecting to a new channel -> ignore disconnect
				} catch (error) {
					// Seems to be a real disconnect which SHOULDN'T be recovered from
					this.getPlayingSong()?.showStopMessage();
					this.empty();
					this.#deleteMessage();

					existingConnection.destroy();
					this.player.stop();
					this.player       = null;
					this.playingIndex = 0;
					console.log("\t↪ Music stopped, playlist cleared, voice disconnected");
				}
			});
		});
	}

	/**
	 * Verify if the interaction author satisfy the requirements for performing certain music command.
	 * It checks if :
	 *  - The command author is voice connected.
	 *  - A music is actually playing.
	 *  - The command author is connected in the same voice channel as the bot.
	 *
	 * If any of these requirements is not valid, the return value is false, otherwise true.
	 * @param interaction {Discord.CommandInteraction} The command interaction made by a guild member.
	 * @returns {Promise<boolean>} True if the command context is valid.
	 */
	checkCommandValidity(interaction) {
		return new Promise(async (resolve) => {
			if (!interaction.member.voice.channel) {
				console.log(`\t↪ User not voice connected`);
				await interaction.reply({ embeds: [new MyEmbed("Tu dois être connecté dans un salon vocal pour utiliser cette commande.", Utils.ORANGE_COLOR)], ephemeral: true })
					.catch(err => console.error(`\t↪ Cannot reply :\n${err}`));

				return resolve(false);
			}

			const exisitingConnection = Utils.getVoiceConnection(this.guildId);
			if (!this.player || !exisitingConnection) {
				console.log(`\t↪ There is no music running`);
				await interaction.reply({ embeds: [new MyEmbed("Il n'y a pas de musique en cours", Utils.ORANGE_COLOR)], ephemeral: true })
					.catch(err => console.error(`\t↪ Cannot reply :\n${err}`));

				return resolve(false);
			}

			if (interaction.member.voice.channelId !== exisitingConnection.joinConfig.channelId) {
				console.log(`\t↪ User connected in a different channel than the bot`);
				await interaction.reply({ embeds: [new MyEmbed("Tu dois être connecté dans le même salon vocal que le bot pour utiliser cette commande.", Utils.RED_COLOR)], ephemeral: true })
					.catch(err => console.error(`\t↪ Cannot reply :\n${err}`));

				return resolve(false);
			}

			resolve(true);
		});
	}

	//--------------
	// EMBED MESSAGE
	//--------------

	/**
	 * Create an EmbedMessage representing the playlist content (25 songs only are displayed).
	 * @param title {string} A message to showList on the top of the playlist embed.
	 * @param avatarURL {string} A URL corresponding to the icon of the person who wanted to showList this playlist content.
	 * @returns {Discord.MessageEmbed} The embed message showing the first 25 traks.
	 */
	toEmbed(title, avatarURL) {
		// Convert all songs to a field
		let fieldsList = [];
		for (const song of this.songsList) {
			if (!song)
				continue;

			const songIndex = this.songsList.indexOf(song);
			const duration  = song.isLive ? "En direct" : Utils.ISO8601ToString(song.duration);
			fieldsList.push({
				name: songIndex === this.playingIndex ? `**${song.title}**` : `*\`${song.title}\`*`,
				value: `${songIndex === this.playingIndex ? "🔊" : "🍩"} ${songIndex + 1}${("⠀".repeat(3 - ((songIndex + 1).toString().length)))} :clock930: ${duration}${("⠀".repeat((11 - duration.length) > 0 ? (11 - duration.length) : 1))} :art: [${song.ownerName}](${song.ownerURL} 'Lien vers la chaine youtube')`
			});
		}

		// Because it is possible to go back in the playlist, we have to keep some songs before and after the actually played one.
		// Since we can display 25 fields in a simple embed, we will keep 4 songs before the played one, the played one and the 20 after.
		if (fieldsList.length > 25) {
			const keepingSongs = this.playingIndex - 4;
			fieldsList.splice(0, keepingSongs); // Remove all songs from 0 to the four before the playing one
			// Remove from the 20th song after the playing one until the end.
			// If the playing song's index is 0, 1, 2, 3 (wich means there are no fields removed before the playing song's one), then add this amount of playlist lasts songs fields.
			// For example, if the playing song index is 2, and we want to keep 4 songs displayed before the playing one.
			//      Because the playing index is 2, two songs have already been played (index 0 and 1) and are displayed.
			//      That mean there are 4 - 2 = 2 empty fields non-displayed that can be used to display other playlist songs to the end of it.
			//      So, there will be 2 songs displayed before the playing one, the playing one and 20 + 2 = 22 songs after. 25 songs are displayed.
			fieldsList.splice(this.playingIndex + 21 + (keepingSongs < 0 ? Math.abs(keepingSongs) : 0)); // 20 songs after the playing one + 1 to count the playing one
		}

		// Create the embed with 25 fields at maximum
		const embed = new Discord.MessageEmbed()
			.setColor(Utils.BLUE_COLOR)
			.setFooter({ text: `${this.getTotalDuration()} au total ${(this.getLength() - 25) > 0 ? "• " + (this.getLength() - 25) + " musique(s) non affichée(s)" : ""}` })
			.setDescription(`${this.getLength()} musique${this.getLength() > 1 ? "s" : ""} • mode boucles ${this.loopMode ? "activé" : "désactivé"}`)
			.setAuthor({
				name: title,
				iconURL: avatarURL
			});

		for (const field of fieldsList) {
			if ((embed.length + field.name.length + field.value.length) > Utils.DISCORD_MAX_EMBED_SIZE)
				break;
			embed.addFields([{ name: field.name, value: field.value }]);
		}

		return embed;
	}

	/**
	 * Set an existing message as the one which shows this playlist content.
	 * @param message {Discord.Message} The message showing this playlist songs list.
	 */
	#setMessage(message) {
		this.embedMessage = message;
	}

	/**
	 * Change the message displayed on the top of this playlist content message if there is one.
	 * @param [title] {string} The new title to set.
	 */
	updateMessageContent(title = "Contenu de la playlist") {
		if (this.isEmpty())
			return;

		if (!this.embedMessage || !this.embedMessage.editable)
			this.lastMessageChannel.send({ embeds: [this.toEmbed(title, this.songsList[0].commandAuthorThumbnailURL)] })
				.then(m => this.embedMessage = m)
				.catch(err => console.error(`\t↪ Cannot send playlist message :\n${err}`));
		else
			this.embedMessage.edit({ embeds: [this.toEmbed(title, this.embedMessage.embeds[0].author.iconURL)] })
				.catch(err => console.error(`\t↪ Cannot edit playlist message :\n${err}`));
	}

	/**
	 * Delete the message showing this playlist content if there is one.
	 */
	#deleteMessage() {
		if (!this.embedMessage || !this.embedMessage.deletable)
			return;

		this.embedMessage.delete()
			.catch(err => console.error(`\t↪ Cannot delete playlist message :\n${err}`));
		this.embedMessage = undefined;
	}

	/**
	 * Send an embed message showing the actually playing playlist content if there is one.
	 * The message will be sent in the parameter interaction channel.
	 * @param interaction {Discord.CommandInteraction} The command interaction that asked for the playlist to be displayed.
	 * @returns {Promise<void>}
	 */
	async showContent(interaction) {
		if (this.isEmpty()) {
			console.log(`\t↪ Playlist empty`);
			const errorOptions = { embeds: [new MyEmbed("La playlist est vide.", Utils.ORANGE_COLOR)], ephemeral: true };

			if (interaction.replied)
				this.lastMessageChannel.send(errorOptions)
					.catch(err => console.error(`\t↪ Cannot send empty playlist message on ${Utils.getChannelLocation(this.lastMessageChannel)} :\n${err}`))
					.then(Utils.deleteMessage);
			else
				interaction.reply(errorOptions)
					.catch(err => console.error(`\t↪ Cannot reply empty playlist message on ${Utils.getChannelLocation(this.lastMessageChannel)} :\n${err}`));
			return null;
		}

		await this.lastMessageChannel.sendTyping()
			.catch(err => console.error(`\t↪ Cannot send typing on ${Utils.getChannelLocation(this.lastMessageChannel)} :\n${err}`));
		const embedPlaylist = this.toEmbed("Contenu de la playlist", interaction.user.avatarURL());

		if (interaction.replied)
			this.#setMessage(await this.lastMessageChannel
					.send({ embeds: [embedPlaylist] })
					.catch(err => console.error(`\t↪ Cannot send playlist message on ${Utils.getChannelLocation(this.lastMessageChannel)} :\n${err}`))
				?? this.embedMessage);
		else
			this.#setMessage(await interaction
					.reply({ embeds: [embedPlaylist], fetchReply: true })
					.catch(err => console.error(`\t↪ Cannot reply playlist message on ${Utils.getChannelLocation(this.lastMessageChannel)} :\n${err}`))
				?? this.embedMessage);

		this.#updatePlaylistLinkButton();
	}

	/**
	 * Update the button link used to go on the playlist from the song's playing message.
	 */
	#updatePlaylistLinkButton() {
		if (!this.embedMessage)
			return null;

		this.BUTTONS_PREV_PAU_SKI_STO_LYR_PLA[1].components[0].setURL(this.embedMessage.url); // Playlist button on row with play button
		this.BUTTONS_PREV_PLA_SKI_STO_LYR_PLA[1].components[0].setURL(this.embedMessage.url); // Playlist button on row with pause button
	}

	//--------------
	// OTHER GETTERS
	//--------------

	/**
	 * Get the number of tracks in this playlist.
	 * @returns {number} The length of this playlist : the number of tracks in it.
	 */
	getLength() {
		return this.songsList.length;
	}

	/**
	 * Get the total duration of this playlist as a string.
	 * @returns {string}
	 */
	getTotalDuration() {
		if (this.isEmpty())
			return `0s`;

		let sum = [0, 0, 0]; // Hours, Minutes, Seconds
		this.songsList.forEach(song => {
			if (song.isLive || !song.duration)
				return "∞";

			song.duration.replace(/[HM]/g, ",").replace(/[PTS]/g, "").split(",").map((val, index) => {
				let adjustement = 0;
				if (song.duration.search("H") === -1)
					adjustement++;
				if (song.duration.search("M") === -1)
					adjustement++;

				sum[index + adjustement] += parseInt(val);
			});
		});

		sum[1] += Math.floor(sum[2] / 60); // Report the excess amount of seconds into minutes
		sum[2] = sum[2] % 60; // Update the seconds remaining
		sum[0] += Math.floor(sum[1] / 60); // Report the excess amount of minutes into hours
		sum[1] = sum[1] % 60; // Update the minutes remaining

		return `${sum[0] > 0 ? sum[0] + "h" : ""} ${sum[1]}m ${sum[2]}s`;
	}

	/**
	 * Get the playlist content.
	 * @returns {Song[]} The songs list contained in this playlist.
	 */
	getSongsList() {
		return this.songsList;
	}

	getPlayingSong() {
		return this.songsList[this.playingIndex];
	}

	//--------------
	// OTHER SETTERS
	//--------------

	/**
	 * Change the state of the loop mode for this playlist.
	 * @param activate {boolean} If the loop mode need to be activated or not.
	 */
	setLoopMode(activate) {
		this.loopMode = activate;
	}

	/**
	 * CHange the last message channel attribute of this playlist.
	 * @param channel {Discord.TextChannel} The new channel;
	 */
	setLastMessageChannel(channel) {
		this.lastMessageChannel = channel;
	}

	//--------------
	// OTHER METHODS
	//--------------

	/**
	 * Tell if the current playlist contains songs or not anymore.
	 * @returns {boolean} True if this playlist is empty.
	 */
	isEmpty() {
		return this.songsList.length === 0;
	}

	/**
	 * Indicates if a music is currently being played in a voice channel or not.
	 * @returns {boolean} True if a music is played, false otherwise.
	 */
	hasPlayer() {
		return this.player != null;
	}

	isPaused() {
		return this.player && this.player.state.status === "paused";
	}
}

module.exports = Playlist;