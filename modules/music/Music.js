// noinspection ES6MissingAwait
/**
 * @author Killian
 */

const Playlist = require("./Playlist");
const Discord  = require("discord.js");
const MyEmbed  = require("../../utilities/MyEmbed");
const Stream   = require("./StreamPlatforms/");
const Utils    = require("../../utilities/Utils");
const Song     = require("./Song");
const Mip      = require("./MusicInfosProvider");
const lip      = require("./LyricsInfosProvider");

class Music {

	// Interaction buttons
	// noinspection JSCheckFunctionSignatures
	BUTTONS_OK_CANCEL         = new Discord.MessageActionRow().addComponents([
		new Discord.MessageButton().setCustomId("success_button").setLabel("Ok").setStyle("SUCCESS").setEmoji(`👍`),
		new Discord.MessageButton().setCustomId("cancel_button").setLabel("Finalement non").setStyle("DANGER").setEmoji(`👎`)
	]);
	// noinspection JSCheckFunctionSignatures
	BUTTONS_OK_CANCEL_SEEK    = new Discord.MessageActionRow().addComponents([
		new Discord.MessageButton().setCustomId("success_button").setLabel("Ok").setStyle("SUCCESS").setEmoji(`👍`),
		new Discord.MessageButton().setCustomId("cancel_button").setLabel("Finalement non").setStyle("DANGER").setEmoji(`👎`),
		new Discord.MessageButton().setCustomId("seek_button").setStyle("SECONDARY").setEmoji(`⌚`) // The label is set when used because the seek time is written in.
	]);
	// noinspection JSCheckFunctionSignatures
	BUTTONS_OK_CANCEL_YOUTUBE = new Discord.MessageActionRow().addComponents([
		new Discord.MessageButton().setCustomId("success_button").setLabel("Ok").setStyle("SUCCESS").setEmoji(`👍`),
		new Discord.MessageButton().setCustomId("cancel_button").setLabel("Finalement non").setStyle("DANGER").setEmoji(`👎`),
		new Discord.MessageButton().setCustomId("search_yt_button").setLabel("Jouer sur YouTube").setStyle("SECONDARY").setEmoji(`🔎`)
	]);

	constructor(guildId) {
		this.guildId = guildId;

		this.lastMessageChannel = global.bot.guilds.cache.get(guildId).systemChannel;
		this.playlist           = new Playlist(this.lastMessageChannel, guildId);
		this.mip                = new Mip();
	}

	/**
	 * Launch a specific / command of the music module
	 * @param command {string} The command name.
	 * @param interaction {Discord.CommandInteraction} The command interaction created by a user.
	 */
	runCommand(command, interaction) {
		this.lastMessageChannel = interaction.channel;
		this.playlist.setLastMessageChannel(interaction.channel);

		switch (command) {
			case "play":
				console.log(`Play command`);
				// noinspection JSIgnoredPromiseFromCall
				this.playCommand(interaction);
				break;
			case "resume":
			case "pause":
				console.log(`Resume/pause command`);
				// noinspection JSIgnoredPromiseFromCall
				this.pauseCommand(interaction);
				break;
			case "skip":
				console.log(`Skip command`);
				// noinspection JSIgnoredPromiseFromCall
				this.skipCommand(interaction);
				break;
			case "stop":
				console.log(`Stop command`);
				// noinspection JSIgnoredPromiseFromCall
				this.stopCommand(interaction);
				break;
			case "remove":
				console.log(`Remove command`);
				// noinspection JSIgnoredPromiseFromCall
				this.removeCommand(interaction);
				break;
			case "lyrics":
				console.log(`Lyrics command`);
				// noinspection JSIgnoredPromiseFromCall
				this.lyricsCommand(interaction);
				break;
			case "loop":
				console.log(`Loop command`);
				// noinspection JSIgnoredPromiseFromCall
				this.loopCommand(interaction);
				break;
			case "volume":
				console.log(`Volume command`);
				// noinspection JSIgnoredPromiseFromCall
				this.volumeCommand(interaction);
				break;
			case "playlist":
				console.log(`Playlist command`);
				// noinspection JSIgnoredPromiseFromCall
				this.playlistCommand(interaction);
				break;
			case "discover":
				console.log(`Discover command`);
				// noinspection JSIgnoredPromiseFromCall
				this.playCommand(interaction, true);
				break;
			case "help":
				console.log(`Help command`);
				this.helpCommand(interaction);
				break;
			default:
				console.log(`Not recognized command : ${command}`);
				interaction.reply({ embeds: [new MyEmbed("Je ne connais pas cette commande 😕", Utils.RED_COLOR)], fetchReply: true })
					.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`))
					.then(Utils.deleteMessage);
		}
	}

	// --------
	// COMMANDS
	// --------

	/**
	 * Activate/de activate the music loop mode. When activated, the current song will be played from beginning to end again and again.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @returns {Promise<void>}
	 */
	async loopCommand(interaction) {
		const isValid = await this.playlist.checkCommandValidity(interaction);
		if (!isValid)
			return null;

		this.playlist.setLoopMode(interaction.options.get("activate").value);
		console.log(`\t↪ The loop mode is now ${this.playlist.loopMode ? "on" : "off"}`);

		interaction.reply({ embeds: [new MyEmbed("La lecture en boucle est maintenant " + (this.playlist.loopMode ? "activée." : "désactivée."), Utils.BLUE_COLOR)], fetchReply: true })
			.catch(err => console.error(`\t↪ Cannot reply the interaction :\n${err}`))
			.then(Utils.deleteMessage);
		this.playlist.updateMessageContent();
	}

	/**
	 * Send a message that displays the lyrics of a specific song.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @returns {Promise<void>}
	 */
	async lyricsCommand(interaction) {
		const research = interaction.options.get("title").value;

		await interaction.reply({ embeds: [new MyEmbed(`Je cherche des paroles pour "${research}".`, Utils.BLUE_COLOR)], ephemeral: true })
			.catch(err => console.error(`\t↪ Cannot reply :\n${err}`));

		await lip.getLyrics(research)
			.then(async lyricsFoundByPlatform => {
				if (Object.keys(lyricsFoundByPlatform).length === 0) {
					console.log(`\t↪ No result for song "${research}"`);
					return interaction.editReply({ embeds: [new MyEmbed("Pas de résultat 😕", Utils.ORANGE_COLOR)] })
						.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
				}

				// Search for a video to play on YouTube
				const youtubeVideo = await this.mip.search(research, interaction, { source: { youtube: "video" }, limit: 1 });

				if (youtubeVideo && youtubeVideo instanceof Stream.YoutubeVideo) {
					await lip.showLyrics(lyricsFoundByPlatform, interaction, true, new Song(this.lastMessageChannel).fromYoutubeVideo(youtubeVideo, interaction.user), this.playlist);

					interaction.editReply({ embeds: [new MyEmbed("J'ai affiché le résultat 🙂", Utils.GREEN_COLOR)] })
						.catch(err => console.error(`\t↪ Cannot edit songs lyrics reply :\n${err}`));
				}
			}).catch(err => {
				console.error(`\t↪ Cannot find lyrics for "${research}" :\n${err}`);
				return interaction.editReply({ embeds: [new MyEmbed("Une erreur est survenue lors de ma recherche ...", Utils.RED_COLOR)] })
					.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
			});
	}

	/**
	 * Pause/unpause the actually played music.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @returns {Promise<void>}
	 */
	async pauseCommand(interaction) {
		if (!await this.playlist.checkCommandValidity(interaction))
			return console.log(`\t↪ Not valid`);

		if (this.playlist.isPaused()) {
			this.playlist.player.unpause();
			console.log(`\t↪ Music unpaused`);
			interaction.reply({ embeds: [new MyEmbed("La musique est relancée.", Utils.BLUE_COLOR)], fetchReply: true })
				.then(Utils.deleteMessage)
				.catch(err => {
					console.error(`\t↪ Cannot reply :\n${err}`);
				});
		} else {
			this.playlist.player.pause();
			console.log(`\t↪ Music paused`);
			interaction.reply({ embeds: [new MyEmbed("La musique est en pause.", Utils.BLUE_COLOR)], fetchReply: true })
				.then(Utils.deleteMessage)
				.catch(err => {
					console.error(`\t↪ Cannot reply :\n${err}`);
				});
		}
	}

	/**
	 * Launch a music by its web link or its title. If the command was discover, it will call for searching related song to add to the playlist.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @param discoverMode {boolean} Whether to search for other songs to add to the playlist or not.
	 * @returns {Promise<any|void|undefined>}
	 */
	async playCommand(interaction, discoverMode = false) {
		if (!interaction)
			return;

		if (interaction.member.voice.channel === null) {
			console.log(`\t↪ Command author not connected to voice channel`);
			return interaction.reply({ embeds: [new MyEmbed("Tu dois être connecté dans un salon vocal pour utiliser cette commande.", Utils.ORANGE_COLOR)], ephemeral: true })
				.catch(err => console.error(`\t↪ Cannot reply :\n${err}`));
		}

		const answerOptions = { ephemeral: true, embeds: [new MyEmbed("Bien reçu, je te préviens dès que j'ai un résultat 👍.", Utils.BLUE_COLOR)] };
		if (interaction.replied)
			await interaction.editReply(answerOptions)
				.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
		else
			await interaction.reply(answerOptions)
				.catch(err => console.error(`\t↪ Cannot reply :\n${err}`));

		// The command emitter is well-connected to a voice channel
		let link                            = interaction.options.get("element").value;
		const [streamPlatform, clearedLink] = await this.mip.validate(link);
		link                                = clearedLink ? clearedLink : link; // The cleared link contains only the necessary elements of the url. If null, there was no platform detected and the link do not need to change.

		/*
		 * so - SoundCloud
		 * sp - Spotify
		 * dz - Deezer
		 * yt - YouTube
		 */
		// noinspection FallThroughInSwitchStatementJS
		switch (streamPlatform) {
			case "so_playlist":
				console.log(`\t↪ SoundCloud playlist detected`);
				const soPlaylistInfos = await this.mip.getSoundCloudPlaylistInfos(link)
					.catch(err => {
						if (err === Stream.SoundCloud.INVALID_ID || err === Stream.SoundCloud.INVALID_TYPE) { // Invalid id
							console.log(`\t↪ Invalid SoundCloud link : code ${err}`);
							interaction.editReply({ embeds: [new MyEmbed("Cette playlist est introuvable, ton lien SoudCloud est-il bien écrit ?", Utils.ORANGE_COLOR)] })
								.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
							return;
						}

						console.error(`\t↪ Cannot get SoundCloud playlist :\n${err}`);
					});

				if (!soPlaylistInfos)
					return interaction.editReply({ embeds: [new MyEmbed("Je n'ai pas trouvé de playlist pour ce lien 😕", Utils.ORANGE_COLOR)] })
						.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));

				// noinspection ES6MissingAwait
				this.#addSongsToPlaylist((await soPlaylistInfos.tracks).splice(0, Playlist.MAX_SONG_ADD_AMOUNT), interaction, "so");
				break;

			case "so_track":
				console.log(`\t↪ SoundCloud track detected`);
				const soTrack = await this.mip.getSoundCloudTrackInfos(link)
					.catch(err => {
						if (err === Stream.SoundCloud.INVALID_ID || err === Stream.SoundCloud.INVALID_TYPE) { // Invalid id
							console.log(`\t↪ Invalid SoundCloud link : code ${err}`);
							interaction.editReply({ embeds: [new MyEmbed("Cette musique est introuvable, ton lien SoudCloud est-il bien écrit ?", Utils.ORANGE_COLOR)] })
								.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
							return;
						}

						console.error(`\t↪ Cannot get SoundCloud track :\n${err}`);
					});
				if (!soTrack)
					return interaction.editReply({ embeds: [new MyEmbed("Je n'ai pas trouvé de musique pour ce lien 😕", Utils.ORANGE_COLOR)] })
						.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));

				if (discoverMode) {
					const youtubeResult = await this.mip.search(`${soTrack.name} - ${soTrack.publisher.artist}`, interaction, { source: { youtube: "video" }, limit: 1 });

					if (youtubeResult && youtubeResult instanceof Stream.YoutubeVideo) {
						// noinspection ES6MissingAwait
						this.#addSongToPlaylist(new Song(this.lastMessageChannel).fromYoutubeVideo(youtubeResult, interaction.user), youtubeResult, interaction, discoverMode);
						return null;
					}
				}

				// noinspection ES6MissingAwait
				this.#addSongToPlaylist(new Song(this.lastMessageChannel).fromSoundCloudTrack(soTrack, interaction.user), soTrack, interaction, false);

				break;

			case "sp_track":
				console.log(`\t↪ Spotify track detected`);
				const spTrackInfos = await this.mip.getSpotifyTrackInfos(link)
					.catch(err => {
						if (err === Stream.Spotify.UNKNOWN_MEDIA || err === Stream.Spotify.INVALID_ID || err === Stream.Spotify.INVALID_TYPE) { // Invalid id
							console.log(`\t↪ Invalid Spotify link : code ${err}`);
							interaction.editReply({ embeds: [new MyEmbed("Cette musique est introuvable, ton lien Spotify est-il bien écrit ?", Utils.ORANGE_COLOR)] })
								.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
							return;
						}

						console.error(`\t↪ Cannot get Spotify track infos :\n${err}`);
						interaction.editReply({ embeds: [new MyEmbed("Je ne parviens pas à récupérer les infos de cette musique 😕", Utils.RED_COLOR)] })
							.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
					});

				if (!spTrackInfos) // An error occured during the process of getting track infos
					return;

				// noinspection ES6MissingAwait
				this.#addSongToPlaylist(new Song(this.lastMessageChannel).fromSpotifyTrack(spTrackInfos, interaction.user), spTrackInfos, interaction, discoverMode);

				/*
				 console.log(`\t↪ Searching on YouTube for "${spTrackInfos.name} - ${spTrackInfos.artists[0].name}" song`);
				 const youtubeResult = await this.mip.search(`${spTrackInfos.name} - ${spTrackInfos.artists[0].name}`, interaction, { source: { youtube: "video" }, limit: 1 })

				 if (youtubeResult && youtubeResult instanceof Stream.YoutubeVideo) {
				 // noinspection ES6MissingAwait
				 this.#addSongToPlaylist(new Song(this.lastMessageChannel).fromYoutubeVideo(youtubeResult, interaction.user), youtubeResult, interaction, discoverMode);
				 }*/
				break;

			case "sp_album":
				console.log(`\t↪ Spotify album detected`);
				const spAlbumInfos = await this.mip.getSpotifyAlbumInfos(link)
					.catch(err => {
						if (err === Stream.Spotify.UNKNOWN_MEDIA || err === Stream.Spotify.INVALID_ID || err === Stream.Spotify.INVALID_TYPE) { // Invalid id
							console.log(`\t↪ Invalid Spotify link : code ${err}`);
							interaction.editReply({ embeds: [new MyEmbed("Cet album est introuvable, ton lien Spotify est-il bien écrit ?", Utils.ORANGE_COLOR)] })
								.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
							return;
						}

						console.error(`\t↪ Cannot get Spotify album infos :\n${err}`);
						interaction.editReply({ embeds: [new MyEmbed("Je ne parviens pas à récupérer les infos de cet album 😕", Utils.RED_COLOR)] })
							.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
					});

				if (!spAlbumInfos) // An error occured during the process of getting track infos
					return;

				if (spAlbumInfos.tracks.items.length === 0) {
					console.log(`\t↪ Empty Spotify album`);
					return interaction.editReply({ embeds: [new MyEmbed("Cet album Spotify est vide ...", Utils.ORANGE_COLOR)] })
						.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
				}

				let youtubeSPAlbumVideosList = [];
				for (const item of spAlbumInfos.tracks.items.slice(0, Playlist.MAX_SONG_ADD_AMOUNT))
					youtubeSPAlbumVideosList.push(await this.mip.search(`${item.name} - ${item.artists[0].name}`, interaction, { source: { youtube: "video" }, limit: 1 }));

				// noinspection ES6MissingAwait
				this.#addSongsToPlaylist(youtubeSPAlbumVideosList, interaction, "yt");
				break;

			case "sp_playlist":
				console.log(`\t↪ Spotify playlist detected`);
				const spPlaylistInfos = await this.mip.getSpotifyPlaylistInfos(link)
					.catch(err => {
						if (err === Stream.Spotify.UNKNOWN_MEDIA || err === Stream.Spotify.INVALID_ID || err === Stream.Spotify.INVALID_TYPE) { // Invalid id
							console.log(`\t↪ Invalid Spotify link : code ${err}`);
							interaction.editReply({ embeds: [new MyEmbed("Cette playlist est introuvable, ton lien Spotify est-il bien écrit ?", Utils.ORANGE_COLOR)] })
								.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
							return;
						}

						console.error(`\t↪ Cannot get Spotify playlist infos :\n${err}`);
						interaction.editReply({ embeds: [new MyEmbed("Je ne parviens pas à récupérer les infos de cette playlist 😕", Utils.RED_COLOR)] })
							.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
					});

				if (!spPlaylistInfos) // An error occured during the process of getting playlist infos
					return;

				if (spPlaylistInfos.tracks.items.length === 0) {
					console.log(`\t↪ Empty Spotify playlist`);
					return interaction.editReply({ embeds: [new MyEmbed("Cette playlist Spotify est vide ...", Utils.ORANGE_COLOR)] })
						.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
				}

				let youtubePlaylistVideosList = [];
				for (const item of spPlaylistInfos.tracks.items.slice(0, Playlist.MAX_SONG_ADD_AMOUNT))
					youtubePlaylistVideosList.push(await this.mip.search(`${item.track.name} - ${item.track.artists[0].name}`, interaction, { source: { youtube: "video" }, limit: 1 }));

				// noinspection ES6MissingAwait
				this.#addSongsToPlaylist(youtubePlaylistVideosList, interaction, "yt");
				break;

			case "dz_track":
				console.log(`\t↪ Deezer track detected`);
				const dzTrackInfos = await this.mip.getDeezerTrackInfos(link)
					.catch(err => {
						if (err === Stream.Deezer.NO_DATA || err === Stream.Deezer.INVALID_TYPE || err === Stream.Deezer.INVALID_ID) {
							console.log(`\t↪ Invalid Deezer link : code ${err}`);
							interaction.editReply({ embeds: [new MyEmbed("Cette musique est introuvable, ton lien Deezer est-il bien écrit ?", Utils.ORANGE_COLOR)] })
								.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
							return;
						}

						console.error(`\t↪ Cannot get Deezer track infos :\n${err}`);
						interaction.editReply({ embeds: [new MyEmbed("Je ne parviens pas à récupérer les infos de cette musique 😕", Utils.RED_COLOR)] })
							.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
					});

				if (!dzTrackInfos) // An error occured during the process of getting track infos
					return;

				// noinspection ES6MissingAwait
				this.#addSongToPlaylist(new Song(this.lastMessageChannel).fromDeezerTrack(dzTrackInfos, interaction.user), dzTrackInfos, interaction, discoverMode);
				/*
				 console.log(`\t↪ Searching on YouTube for "${dzTrackInfos.title} - ${dzTrackInfos.artist.name}" song`);
				 const dzTrackOnYoutube = await this.mip.search(`${dzTrackInfos.title} - ${dzTrackInfos.artist.name}`, interaction, { source: { youtube: "video" }, limit: 1 })

				 if (dzTrackOnYoutube && dzTrackOnYoutube instanceof Stream.YoutubeVideo) {
				 // noinspection ES6MissingAwait
				 this.#addSongToPlaylist(new Song(this.lastMessageChannel).fromYoutubeVideo(dzTrackOnYoutube, interaction.user), dzTrackOnYoutube, interaction, discoverMode);
				 }*/
				break;

			case "dz_playlist":
				console.log(`\t↪ Deezer playlist detected`);
				const dzPlaylist = await this.mip.getDeezerPlaylistInfos(link)
					.catch(err => {
						if (err === Stream.Deezer.NO_DATA || err === Stream.Deezer.INVALID_TYPE || err === Stream.Deezer.INVALID_ID) {
							console.log(`\t↪ Invalid Deezer link : code ${err}`);
							interaction.editReply({ embeds: [new MyEmbed("Cette playlist est introuvable, ton lien Deezer est-il bien écrit ?", Utils.ORANGE_COLOR)] })
								.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
							return;
						}

						console.error(`\t↪ Cannot get Deezer playlist infos :\n${err}`);
						interaction.editReply({ embeds: [new MyEmbed("Je ne parviens pas à récupérer les infos de cette playlist 😕", Utils.RED_COLOR)] })
							.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
					});

				if (!dzPlaylist) // An error occured during the process of getting track infos
					return;

				if (dzPlaylist.isEmpty()) {
					console.log(`\t↪ Empty Deezer playlist`);
					return interaction.editReply({ embeds: [new MyEmbed("Cette playlist Deezer est vide ...", Utils.ORANGE_COLOR)] })
						.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
				}

				let youtubeDZPlaylistVideosList = [];
				for (const dzTrack of dzPlaylist.tracks.data.slice(0, Playlist.MAX_SONG_ADD_AMOUNT))
					youtubeDZPlaylistVideosList.push(await this.mip.search(`${dzTrack.title} - ${dzTrack.artist.name}`, interaction, { source: { youtube: "video" }, limit: 1 }));

				// noinspection ES6MissingAwait
				this.#addSongsToPlaylist(youtubeDZPlaylistVideosList, interaction, "yt");
				break;

			case "dz_album":
				console.log(`\t↪ Deezer album detected`);
				const dzAlbum = await this.mip.getDeezerAlbumInfos(link)
					.catch(err => {
						if (err === Stream.Deezer.NO_DATA || err === Stream.Deezer.INVALID_TYPE || err === Stream.Deezer.INVALID_ID) {
							console.log(`\t↪ Invalid Deezer link : code ${err}`);
							interaction.editReply({ embeds: [new MyEmbed("Cet album est introuvable, ton lien Deezer est-il bien écrit ?", Utils.ORANGE_COLOR)] })
								.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
							return;
						}

						console.error(`\t↪ Cannot get Deezer album infos :\n${err}`);
						interaction.editReply({ embeds: [new MyEmbed("Je ne parviens pas à récupérer les infos de cet album 😕", Utils.RED_COLOR)] })
							.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
					});

				if (!dzAlbum) // An error occured during the process of getting track infos
					return;

				if (dzAlbum.isEmpty()) {
					console.log(`\t↪ Empty Deezer album`);
					return interaction.editReply({ embeds: [new MyEmbed("Cet album Deezer est vide ...", Utils.ORANGE_COLOR)] })
						.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
				}

				let youtubeDZAlbumVideosList = [];
				for (const dzTrack of dzAlbum.tracks.slice(0, Playlist.MAX_SONG_ADD_AMOUNT))
					youtubeDZAlbumVideosList.push(await this.mip.search(`${dzTrack.title} - ${dzTrack.artist.name}`, interaction, { source: { youtube: "video" }, limit: 1 }));

				// noinspection ES6MissingAwait
				this.#addSongsToPlaylist(youtubeDZAlbumVideosList, interaction, "yt");
				break;

			case "yt_playlist":
				console.log(`\t↪ YouTube playlist detected`);
				const complete = await this.mip.getYoutubePlaylistInfos(link)
					.then(playlistInfos => {
						this.#addSongsToPlaylist(playlistInfos.videosList.slice(0, Playlist.MAX_SONG_ADD_AMOUNT), interaction, "yt");
						return true;
					})
					.catch(async err => {
						if (err.code === 404) {
							console.log(`\t↪ Cannot retrieve infos : the playlist doesn't exist or is private.`);
							await interaction.editReply({ embeds: [new MyEmbed("La playlist que tu m'a donné est privée ou n'existe pas 😕", Utils.ORANGE_COLOR)] })
								.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
						} else {
							console.error(`\t↪ Cannot retrieve playlist infos :\n${err}`);
							await interaction.editReply({ embeds: [new MyEmbed("J'ai rencontré une erreur lors de l'obtention des informations de ta playlist 😕", Utils.RED_COLOR)] })
								.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
						}

						return false;
					});

				if (complete) // No need to search for a YT video instead.
					break;

				// In this case, we reach the yt_video case below to play the video instead of the playlist.
				// We just wait several seconds to let the user see the error message.
				await new Promise((resolve) => {
					setTimeout(() => {
						resolve();
					}, Utils.MESSAGE_DEL_TIME * 1000);
				});

			case "yt_video":
				console.log("\t↪ YouTube video detected");
				const ytVideo = await this.mip.getYoutubeVideoInfos(link)
					.catch(err => {
						if (err === Stream.YouTube.AGE_RESTICTED_VIDEO) { // Age restriction
							console.log(`\t↪ Cannot add video : age restriction`);
							interaction.editReply({ embeds: [new MyEmbed("Impossible de lire cette musique à cause de la restriction d'âge 😕", Utils.ORANGE_COLOR)] })
								.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
							return null;
						}
						if (err === Stream.YouTube.NO_RESULT) { // The id is probably wrong
							console.log(`\t↪ Cannot add video : video does not exists`);
							interaction.editReply({ embeds: [new MyEmbed("Cette musique n'existe pas 😕", Utils.ORANGE_COLOR)] })
								.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
							return null;
						}

						console.error(`\t↪ Cannot get YouTube video infos :\n${err}`);
						interaction.editReply({ embeds: [new MyEmbed("Impossible d'identifier ta vidéo: as-tu bien écrit le lien/titre ?", Utils.RED_COLOR)] })
							.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
					});

				if (!ytVideo)
					return;

				// noinspection ES6MissingAwait
				this.#addSongToPlaylist(new Song(this.lastMessageChannel).fromYoutubeVideo(ytVideo, interaction.user), ytVideo, interaction, discoverMode);
				break;

			case "search":
				console.log("\t↪ Search terms detected");
				const youtubeVideo = await this.mip.search(link, interaction, { source: { youtube: "video" }, limit: 1 });

				if (youtubeVideo && youtubeVideo instanceof Stream.YoutubeVideo) {
					// noinspection ES6MissingAwait
					this.#addSongToPlaylist(new Song(this.lastMessageChannel).fromYoutubeVideo(youtubeVideo, interaction.user), youtubeVideo, interaction, discoverMode);
				}
				break;

			default:
				const recognizedPlatform = streamPlatform ? streamPlatform.slice(0, 3) : null;
				if (recognizedPlatform === "so_" || recognizedPlatform === "yt_" || recognizedPlatform === "dz_" || recognizedPlatform === "sp_") {
					if (streamPlatform === "dz_404") {
						console.log(`\t↪ Deezer wrong short link : 404 code`);
						return interaction.editReply({ embeds: [new MyEmbed("Impossible d'identifier le lien original Deezer à partir de ton lien : l'as-tu bien écrit ?", Utils.ORANGE_COLOR)] })
							.catch(err => console.error(`\t↪ Cannot reply :\n${err}`));
					}

					console.log(`\t↪ Unhandled link detected : ${streamPlatform}`);
					return interaction.editReply({ embeds: [new MyEmbed("Ce genre de lien n'est pas géré", Utils.ORANGE_COLOR)] })
						.catch(err => console.error(`\t↪ Cannot reply :\n${err}`));
				}

				console.log(`\t↪ Did not identified any supported media : ${streamPlatform}`);
				interaction.editReply({ embeds: [new MyEmbed("Je ne trouve pas de résultat associé à ton lien : est-il bien écrit ?", Utils.ORANGE_COLOR)] })
					.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
				break;
		}
	}

	/**
	 * Let the user choosing between displaying the actually played playlist or managing its saved playlists.
	 * If the first option is selected, a message will be sent containing each song of the playlist.
	 * If the other option is chosen, the user could save the current playlist or reload/erase/replace a previously saved one.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 */
	playlistCommand(interaction) {
		// noinspection JSIgnoredPromiseFromCall
		this.playlist.showContent(interaction);
	}

	/**
	 * Remove a certain amount of music from the playlist.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @returns {Promise<any>}
	 */
	async removeCommand(interaction) {
		if (!await this.playlist.checkCommandValidity(interaction))
			return;

		if (this.playlist.isEmpty())
			return interaction.reply({ embeds: [new MyEmbed("Il n'y a aucune musique dans la playlist.", Utils.ORANGE_COLOR)], ephemeral: true })
				.catch(err => console.error(`\t↪ Cannot reply :\n${err}`));

		switch (interaction.options.getSubcommand()) {
			case "one":
				const indexToRemove = interaction.options.getInteger("number") ? (interaction.options.getInteger("number")) - 1 : 0;

				if (indexToRemove >= this.playlist.getLength())
					return interaction.reply({ embeds: [new MyEmbed("Aucune musique ne porte ce numéro 😕", Utils.ORANGE_COLOR)], ephemeral: true })
						.catch(err => console.error(`\t↪ Cannot reply :\n${err}`));

				const removeSongs = this.playlist.removeSongs(indexToRemove, 1);
				interaction.reply({ embeds: [new MyEmbed(`La musique "${removeSongs[0].title}" a été retirée.`, Utils.GREEN_COLOR)] })
					.catch(err => console.error(`\t↪ Cannot reply :\n${err}`));
				this.playlist.updateMessageContent();
				console.log(`\t↪ Supressed one`);
				break;

			case "all":
				const message = await interaction.reply({ embeds: [new MyEmbed(`Tu va retirer ` + (this.playlist.isEmpty() ? `la seule musique` : `les ${this.playlist.getLength()} musiques`) + ` de la playlist.`, Utils.BLUE_COLOR)], ephemeral: true, components: [this.BUTTONS_OK_CANCEL], fetchReply: true });

				const filter    = i => i.user.id === message.interaction.user.id;
				const collector = message.createMessageComponentCollector({ filter, time: Utils.COLLECTOR_TIME * 1000, max: 1 });

				collector.on("collect", async i => {
					if (i.customId === "success_button") {
						console.log(`\t↪ Clear all`);
						const exisitingConnection = Utils.getVoiceConnection(this.guildId);
						if (exisitingConnection)
							exisitingConnection.disconnect();

						console.log(`Music stopped and playlist cleared`);
						await i.update({ embeds: [new MyEmbed("La playlist est maintenant vide.", Utils.GREEN_COLOR)], components: [] })
							.catch(err => console.error(`\t↪ Cannot update :\n${err}`));
					} else if (i.customId === "cancel_button") {
						console.log(`\t↪ Aborted`);
						await i.update({ embeds: [new MyEmbed("Suppression annulée.", Utils.ORANGE_COLOR)], components: [] })
							.catch(err => console.error(`\t↪ Cannot update :\n${err}`));
					}
				});
				collector.on("end", async () => {
					if (collector.endReason !== "limit") {
						console.log(`\t↪ Did not answered in time`);
						await interaction.editReply({ embeds: [new MyEmbed("Suppression annulée : tu n'as pas répondu dans le temps imparti.", Utils.ORANGE_COLOR)], components: [] })
							.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
					}
				});
				break;

			default:
				console.log("A remove sub-command was received but not recognized 😕");
		}
	}

	/**
	 * Stop the music playing process. Also erase the playlist and voice disconnect the bot.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @returns {Promise<void|*>}
	 */
	async stopCommand(interaction) {
		if (!await this.playlist.checkCommandValidity(interaction))
			return console.log(`\t↪ Not valid`);

		const exisitingConnection = Utils.getVoiceConnection(this.guildId);
		if (!exisitingConnection) {
			console.log(`\t↪ Bot not connected`);
			return interaction.reply({ embeds: [new MyEmbed("Je ne suis pas connecté.", Utils.RED_COLOR)], ephemeral: true });
		}

		this.playlist.empty();
		this.playlist.player.stop();
		exisitingConnection.disconnect();
		interaction.reply({ embeds: [new MyEmbed("Arrêt de la musique.", Utils.GREEN_COLOR)], fetchReply: true })
			.then(Utils.deleteMessage);
	}

	/**
	 * Skip one or more songs from the playlist. That will stop the actually played one and launch the song coming next after the skipped one(s).
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @returns {Promise<*>}
	 */
	async skipCommand(interaction) {
		if (!await this.playlist.checkCommandValidity(interaction))
			return;

		if (this.playlist.isEmpty() && !this.playlist.hasPlayer()) {
			console.log(`\t↪ No music to skip.`);
			return interaction.reply({ embeds: [new MyEmbed("Il n'y a aucune musique à passer.", Utils.ORANGE_COLOR)], ephemeral: true });
		}

		let amountToSkip = interaction.options.get("amount") ? interaction.options.get("amount").value : 1; // If a command parameter was specified, taking in, otherwise considering one.
		amountToSkip     = (amountToSkip > this.playlist.getLength()) ? this.playlist.getLength() : amountToSkip; // Cannot remove more songs than the playlist contains

		if (this.playlist.isPaused())
			this.playlist.player.unpause();
		this.playlist.getPlayingSong().showStopMessage();
		this.playlist.playingIndex += amountToSkip;
		this.playlist.player.stop();

		console.log(`\t↪ ${amountToSkip} music passed by ${interaction.user.username}`);
		interaction.reply({ embeds: [new MyEmbed(amountToSkip + (amountToSkip > 1 ? " musiques ont été passées." : " musique a été passée."), Utils.GREEN_COLOR)], fetchReply: true })
			.catch(err => console.error(`\t↪ Cannot reply :\n${err}`))
			.then(Utils.deleteMessage);
	}

	/**
	 * Change the music playing volume.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @returns {Promise<void>}
	 */
	async volumeCommand(interaction) {
		if (!await this.playlist.checkCommandValidity(interaction))
			return;

		const volumeValue = interaction.options.get("value").value;
		// noinspection JSUnresolvedFunction
		this.playlist.audioRessource.volume.setVolume(volumeValue);

		console.log(`\t↪ Changed to ${volumeValue}`);
		interaction.reply({ embeds: [new MyEmbed(`Le volume est maintenant à ${volumeValue}.`, Utils.GREEN_COLOR)], fetchReply: true })
			.catch(err => console.error(`\t↪ Cannot reply :\n${err}`))
			.then(Utils.deleteMessage);
	}

	/**
	 * Send a message listing all available commands
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 */
	helpCommand(interaction) {
		// noinspection JSCheckFunctionSignatures
		interaction.reply({
			embeds: [new Discord.MessageEmbed().setTitle("🎵⠀__*MUSIQUE*__⠀🎵").setColor(Utils.PURPLE_COLOR).addFields([
				{ name: "/ms play [video | recherche | playlist | album]", value: "**`Joue de la musique issue de YouTube, YouTube Music, SoundCloud, Spotify ou Deezer`**" },
				{ name: "/ms discover [lien musique | recherche]", value: "**`Ajoute des vidéos en lien avec une musique à la playlist`**" },
				{ name: "/ms pause | resume", value: "**`Met en pause ou en lecture la musique`**" },
				{ name: "/ms stop ", value: "**`Arrête la musique et déconnecte le bot`**" },
				{ name: "/ms skip (nombre)", value: "**`Passe à la musique suivante`**" },
				{ name: "/ms remove [one | all] (numero X)", value: "**`Retire une ou toutes les musiques de la playlist`**" },
				{ name: "/ms lyrics [recherche]", value: "**`Affiche les paroles d'une musique`**" },
				{ name: "/ms playlist [showList | saves]", value: "**`Affiche la playlist en cours ou gère tes sauvegardes de playlists`**" },
				{ name: "/ms volume [0.5-2.5]", value: "**`Modifie le volume d'émission du bot`**" },
				{ name: "/ms loop [True | False]", value: "**`Active ou désactive la lecture en boucle de la musique`**" },
				{ name: "/help", value: "**`Affiche ce message :D`**" }
			])], ephemeral: true
		}).catch(err => console.error(`\t↪ Cannot reply :\n${err}`));
		console.log(`\t↪ Help message sent.`);
	}

	// --------------------
	// PLAY RELATED METHODS
	// --------------------

	/**
	 * Ask the user to add a specific song in the playlist. Also launch the research of related songs if the user performed a Discover command.
	 * @param song {Song} The song that will be added if the user accept it.
	 * @param mediaDatas {Stream.YoutubeVideo|Stream.SoundCloudTrack|Stream.DeezerTrack|Stream.SpotifyTrack} The media datas representing the song into the API.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @param discoverMode {boolean} Whether to search for other songs to add to the playlist or not.
	 * @returns {Promise<void>}
	 */
	async #addSongToPlaylist(song, mediaDatas, interaction, discoverMode = false) {
		if (!mediaDatas) {
			interaction.editReply({ embeds: [new MyEmbed("Une erreur est survenue lors de la récupération des infos de ta musique 😕", Utils.RED_COLOR)] })
				.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));

			return console.log(`\t↪ Tried to add a song to playlist but no infos given.`);
		}

		if (song.seek)
			this.BUTTONS_OK_CANCEL_SEEK.components[2].setLabel(`À partir de ${Utils.secondsDurationToString(song.seek)}`);

		// Send a message to whether valid or not the music adding.
		const message = await interaction.editReply({
			content: `Veux-tu ajouter cette musique à la playlist ${discoverMode ? "**avant de voir le résultat du discover**" : ""} ?`,
			embeds: [song.toEmbed()],
			components: song.isSnipped ? [this.BUTTONS_OK_CANCEL_YOUTUBE] : [song.seek ? this.BUTTONS_OK_CANCEL_SEEK : this.BUTTONS_OK_CANCEL]
		}).catch((error) => {
			console.error(`\t↪ Cannot edit reply to adding song's question message :\n${error}`);
			return interaction.editReply({ content: " ", embeds: [new MyEmbed("Une erreur est survenue lors de la création de la fiche correspondant à ta musique 😕", Utils.RED_COLOR)] })
				.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
		});

		const songFilter    = i => i.user.id === message.author.id;
		// noinspection JSCheckFunctionSignatures
		const songCollector = message.createMessageComponentCollector({ songFilter, time: Utils.COLLECTOR_TIME * 1000, max: 1 });

		songCollector.on("collect", async i => {
			// noinspection FallThroughInSwitchStatementJS
			switch (i.customId) {
				case "success_button":
					song.seek = undefined; // Setting seek to undefined indicates to the audio player to not seek through time when playing
				case "seek_button":
					this.playlist.addSong(song);

					if (discoverMode) { // Discover mode means no adding notification for the first song
						await i.update({ content: " ", embeds: [new MyEmbed("Bien compris, je recherche d'autres vidéos à ajouter.", Utils.BLUE_COLOR)], components: [] })
							.catch(err => console.error(`\t↪ Cannot update :\n${err}`));

						const musicWillStart = await this.#showDiscoveredSongs(interaction, mediaDatas);
						if (!musicWillStart) {
							// noinspection ES6MissingAwait
							this.playlist.startMusicStream(interaction.member.voice.channel);
						}

					} else {
						// Start the playlist or update if there was already a running song.
						const started = this.playlist.playOrUpdate(interaction);
						await i.update({ content: " ", embeds: !started ? [new MyEmbed("Ta musique sera lu bientôt.", Utils.BLUE_COLOR)] : [new MyEmbed("Ta musique a été ajouté à la playlist.", Utils.GREEN_COLOR)], components: [] })
							.catch(err => console.error(`\t↪ Cannot update :\n${err}`));
					}
					break;

				case "cancel_button":
					console.log(`\t↪ Addition canceled by user`);
					await i.update({ content: " ", embeds: [new MyEmbed("Ajout annulé" + (discoverMode ? " : je cherche des vidéos en lien." : "."), Utils.ORANGE_COLOR)], components: [] })
						.catch(err => console.error(`\t↪ Cannot update :\n${err}`));

					if (discoverMode) {
						// noinspection ES6MissingAwait
						this.#showDiscoveredSongs(interaction, mediaDatas);
					}
					break;

				case "search_yt_button":
					console.log(`\t↪ Search song's free version on YouTube`);

					const youtubeResult = await this.mip.search(`${song.title} - ${song.ownerName}`, interaction, { source: { youtube: "video" }, limit: 1 });

					if (youtubeResult && youtubeResult instanceof Stream.YoutubeVideo) {
						// noinspection ES6MissingAwait
						this.#addSongToPlaylist(new Song(this.lastMessageChannel).fromYoutubeVideo(youtubeResult, interaction.user), youtubeResult, interaction, discoverMode);
						// noinspection ES6MissingAwait
						i.deferUpdate();
						return null;
					}
					break;
			}
		});
		songCollector.on("end", async () => {
			if (songCollector.endReason !== "limit") {
				console.log(`\t↪ Addition canceled : reply takes too long`);
				await interaction.editReply({ content: " ", embeds: [new MyEmbed("Ajout annulé : tu n'as pas répondu dans le temps imparti.", Utils.ORANGE_COLOR)], components: [] })
					.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));

				if (discoverMode) {
					// noinspection ES6MissingAwait
					this.#showDiscoveredSongs(interaction, mediaDatas);
				}
			}
		});
	}

	/**
	 * Add a list of songs to the current playlist.
	 * @param videosList {(SoundCloudTrack[]|YoutubeVideo[])} The tracks list to add to the current playlist.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @param streamOrigin {string} If the tracks would be streamed from YouTube or SoundCloud.
	 */
	async #addSongsToPlaylist(videosList, interaction, streamOrigin) {
		const message = await interaction.editReply({
			embeds: [new MyEmbed(`Veut-tu ajouter ${videosList.length} musiques à la playlist ?`, Utils.BLUE_COLOR)],
			components: [this.BUTTONS_OK_CANCEL]
		}).catch(err => {
			console.error(`\t↪ Cannot edit reply :\n${err}`);
			interaction.reply({ embeds: [new MyEmbed("Une erreur est survenue lors de la création de me réponse 😕", Utils.RED_COLOR)], ephemeral: true })
				.catch(err => console.error(`\t↪ Cannot reply :\n${err}`));
		});

		const filter    = i => i.user.id === message.interaction.user.id;
		const collector = message.createMessageComponentCollector({ filter, time: Utils.COLLECTOR_TIME * 1000, max: 1 });

		collector.on("collect", async i => {
			if (i.customId === "success_button") {
				await interaction.editReply({ embeds: [new MyEmbed(`0/${videosList.length} musiques ont été ajoutées à la playlist.`, Utils.BLUE_COLOR)], components: [] })
					.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));

				for (const video of videosList) {
					switch (streamOrigin) {
						case "yt":
							this.playlist.addSong(new Song(this.lastMessageChannel).fromYoutubeVideo(video, interaction.user));
							break;
						case "so":
							this.playlist.addSong(new Song(this.lastMessageChannel).fromSoundCloudTrack(video, interaction.user));
							break;
						default:
							console.log("Call to addSongsToPlaylist without the origin specified.");
					}

					interaction.editReply({ embeds: [new MyEmbed(`${videosList.indexOf(video) + 1}/${videosList.length} musiques ont été ajoutées à la playlist.`, Utils.BLUE_COLOR)], components: [] })
						.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));
				}

				console.log(`\t↪ ${videosList.length} song(s) added to playlist`);
				await interaction.editReply({ embeds: [new MyEmbed(`${videosList.length} musique${videosList.length > 1 ? "s ont été ajoutées" : "a été ajoutée"} à la playlist.`, Utils.GREEN_COLOR)], components: [] })
					.catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));

				// noinspection ES6MissingAwait
				this.playlist.playOrUpdate(interaction);

			} else if (i.customId === "cancel_button") {
				console.log(`\t↪ Adding canceled by user`);
				await i.update({ embeds: [new MyEmbed("Ajout annulé.", Utils.ORANGE_COLOR)], components: [] })
					.catch(err => console.error(`\t↪ Cannot update add abort message :\n${err}`));
			}
		});

		collector.on("end", async () => {
			if (collector.endReason !== "limit") {
				console.log(`\t↪ Addition canceled : reply takes too long`);
				await interaction.editReply({ embeds: [new MyEmbed("Ajout annulé : tu n'as pas répondu dans le temps imparti.", Utils.ORANGE_COLOR)], components: [] })
					.catch(err => console.error(`\t↪ Cannot edit "aborted adding due to time" reply :\n${err}`));
			}
		});
	}

	/**
	 * Shows the command music link related musics found and ask the user if he wants to add them to the playlist or not.
	 * In the first case, also start the streaming if not already started.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @param video {YoutubeVideo} The original command video to find related videos from.
	 * @returns {Promise<boolean>} Whether the found songs were added to the playlist or not.
	 */
	#showDiscoveredSongs(interaction, video) {
		return new Promise(async (resolve) => {
			const discoverPlaylist = new Playlist(this.lastMessageChannel, this.guildId);

			await video.findRelatedVideos();
			discoverPlaylist.addSongsFromYoutubeVideoList(video.relatedVideos, interaction.user);

			const message = await interaction.editReply({
				content: "**Ces musiques là te conviennent ?**",
				embeds: [discoverPlaylist.toEmbed("Résultat du discover", interaction.user.avatarURL())],
				components: [this.BUTTONS_OK_CANCEL]
			}).catch(err => console.error(`\t↪ Cannot edit reply :\n${err}`));

			const playlistFilter    = i => i.user.id === message.author.id;
			// noinspection JSCheckFunctionSignatures
			const playlistCollector = message.createMessageComponentCollector({ playlistFilter, time: Utils.COLLECTOR_TIME * 1000, max: 1 });

			playlistCollector.on("collect", async i => {
				if (i.customId === "success_button") {
					console.log("\t↪ Adding discovered songs to playlist");
					this.playlist.addSongsFromPlaylist(discoverPlaylist);
					await interaction.editReply({ content: " ", embeds: [new MyEmbed("Les musiques ont bien été ajoutées.", Utils.GREEN_COLOR)], components: [] })
						.catch(err => console.error(`\t↪ Cannot edit reply for a success button pression :\n${err}`));

					// noinspection ES6MissingAwait
					this.playlist.playOrUpdate(interaction);
					return resolve(true);

				} else if (i.customId === "cancel_button") {
					console.log(`\t↪ Addition canceled by user`);
					await interaction.editReply({ content: " ", embeds: [new MyEmbed("Ajout annulé.", Utils.ORANGE_COLOR)], components: [] })
						.catch(err => console.error(`\t↪ Cannot edit reply for a cancel button pression :\n${err}`));
					this.playlist.updateMessageContent();
					return resolve(false);
				}
			});

			playlistCollector.on("end", async () => {
				if (playlistCollector.endReason !== "limit") {
					console.log(`\t↪ Addition canceled : reply takes too much time`);
					await interaction.editReply({ embeds: [new MyEmbed("Ajout annulé : tu n'as pas répondu dans le temps imparti.", Utils.ORANGE_COLOR)], components: [] })
						.catch(err => console.error(`\t↪ Cannot edit reply for a too late answer :\n${err}`));
					this.playlist.updateMessageContent();
					return resolve(false);
				}
			});
		});
	}
}

module.exports = Music;