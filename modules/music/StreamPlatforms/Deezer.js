// noinspection JSUnusedGlobalSymbols,DuplicatedCode

/**
 * @author Killian
 */

const fetch = require("node-fetch");

const VALID_TYPES = ["track", "album", "playlist"];

class Deezer {
	static PLATFORM_COLOR = "#2692b7";
	static PLATFORM_ICON  = "https://www.macupdate.com/images/icons512/60905.png";
	static NO_DATA        = 800; // The media id doesn't exist.
	static INVALID_TYPE   = 401;
	static INVALID_ID     = 400;

	constructor() {
	}

	/**
	 * Query informations about a specific Deezer track/playlist/album on the API.
	 * @param deezerURL {string} The Deezer track url.
	 * @returns {Promise<DeezerTrack|DeezerAlbum|DeezerPlaylist>}
	 */
	getInfos(deezerURL) {
		return new Promise(async (resolve, reject) => {
			const split = deezerURL.split("/");
			const type  = split[4];
			const id    = split[5];

			if (!type || !VALID_TYPES.includes(type))
				return reject(Deezer.INVALID_TYPE);
			if (!id)
				return reject(Deezer.INVALID_ID);

			// Fetch json datas
			const datas = await this.#fetchForDatas(`https://api.deezer.com/${type}/${id}`)
				.catch(err => reject(err));
			if (!datas)
				return;

			switch (type) {
				case "track":
					return resolve(new DeezerTrack().fromAPI(datas));

				case "album":
					return resolve(new DeezerAlbum().fromAPI(datas));

				case "playlist":
					return resolve(new DeezerPlaylist().fromAPI(datas));
			}
		});
	}

	/**
	 * Fetch the API for a certain url
	 * @param url {string} The Deezer album/playlist/track url.
	 * @returns {Promise<json>}
	 */
	#fetchForDatas(url) {
		return new Promise((resolve, reject) => {
			fetch(url)
				.then(async datas => {
					const json = await datas.json();
					if (json.error)
						return reject(json.error.code);

					resolve(json);
				}).catch(err => {
				reject(err.code);
			});
		});
	}
}

class DeezerTrack {

	constructor() {
		/**
		 * Ex : 1644464022
		 * @type {number}
		 */
		this.id = undefined;

		/**
		 * Ex : true
		 * @type {boolean}
		 */
		this.readable = undefined;

		/**
		 * Ex : "Calm Down"
		 * @type {string}
		 */
		this.title = undefined;

		/**
		 * Ex : "Calm Down"
		 * @type {string}
		 */
		this.title_short = undefined;

		/**
		 * Ex : ""
		 * @type {string}
		 */
		this.title_version = undefined;

		/**
		 * Ex : "NGA3B2214004"
		 * @type {string}
		 */
		this.isrc = undefined;

		/**
		 * Ex : "https://www.deezer.com/track/1644464022"
		 * @type {string}
		 */
		this.link = undefined;

		/**
		 * Ex : "https://www.deezer.com/track/1644464022?utm_source=deezer&utm_content=track-1644464022&utm_term=0_1655477004&utm_medium=web"
		 * @type {string}
		 */
		this.share = undefined;

		/**
		 * Ex : 219
		 * @type {number}
		 */
		this.duration = undefined;

		/**
		 * Ex : 1
		 * @type {number}
		 */
		this.track_position = undefined;

		/**
		 * Ex : 1
		 * @type {number}
		 */
		this.disk_number = undefined;

		/**
		 * Ex : 999502
		 * @type {number}
		 */
		this.rank = undefined;

		/**
		 * Ex : "2022-02-10"
		 * @type {string}
		 */
		this.release_date = undefined;

		/**
		 * Ex : false
		 * @type {boolean}
		 */
		this.explicit_lyrics = undefined;

		/**
		 * Ex : 0
		 * @type {number}
		 */
		this.explicit_content_lyrics = undefined;

		/**
		 * Ex : 0
		 * @type {number}
		 */
		this.explicit_content_cover = undefined;

		/**
		 * Ex : "https://cdns-preview-d.dzcdn.net/stream/c-ddf3ecfe031b0e38be1f7cef597d6af1-7.mp3"
		 * @type {string}
		 */
		this.preview = undefined;

		/**
		 * Ex : 0
		 * @type {number}
		 */
		this.bpm = undefined;

		/**
		 * Ex : -10
		 * @type {number}
		 */
		this.gain = undefined;

		/**
		 * Ex : ["AE", "AF", "AG", "AI", "AL", "AM", "AQ", "AR", "AS", "AT", "AU", "AZ", "BA", "BB", "BD", "BE", "BG", "BH", "BN", "BO", "BQ", "BR", "BT", "BV", "BY", "CA", "CC", "CH", "CK", "CL", "CO", "CR", "CU", "CW", "CX", "CY", "CZ", "DE", "DK", "DM", "DO", "DZ", "EC", "EE", "EG", "EH", "ES", "FI", "FJ", "FK", "FM", "FR", "GB", "GD", "GE", "GR", "GS", "GT", "GU", "HK", "HM", "HN", "HR", "HU", "ID", "IE", "IL", "IN", "IQ", "IR", "IS", "IT", "JM", "JO", "JP", "KG", "KH", "KI", "KN", "KP", "KR", "KW", "KY", "KZ", "LA", "LB", "LC", "LK", "LT", "LU", "LV", "LY", "MA", "MD", "ME", "MH", "MK", "MM", "MN", "MP", ...]
		 * @type {string[]}
		 */
		this.available_countries = undefined;

		/**
		 * @type {DeezerArtist[]}
		 */
		this.contributors = undefined;

		/**
		 * Ex : "3071378af24d789b8fc69e95162041e4"
		 * @type {string}
		 */
		this.md5_image = undefined;

		/**
		 * @type {DeezerArtist}
		 */
		this.artist = undefined;

		/**
		 * @type {DeezerAlbum}
		 */
		this.album = undefined;

		/**
		 * Ex : "track"
		 * @type {string}
		 */
		this.type = undefined;
	}

	/**
	 *
	 * @param jsonDatas {json} This DeezerTrack json infos from API.
	 * @param jsonDatas.type {string} The type of datas.
	 * @returns {DeezerTrack}
	 */
	fromAPI(jsonDatas) {
		if (jsonDatas.type !== "track") {
			console.warn(`\t↪ Try to create a Deezer track from the json datas of a ${jsonDatas.type}`);
			return null;
		}

		return Object.assign(this, jsonDatas);
	}

	async stream(_) {
		// Cannot stream more than a preview of 30 seconds.
		return { stream: this.preview, type: "mp3" };
	}
}

class DeezerPlaylist {

	constructor() {
		/**
		 * Ex : 53362031
		 * @type {number}
		 */
		this.id = undefined;

		/**
		 * Ex : "Les titres du moment"
		 * @type {string}
		 */
		this.title = undefined;

		/**
		 * Ex : "Plus besoin de chercher, tous les sons du moment sont ici."
		 * @type {string}
		 */
		this.description = undefined;

		/**
		 * Ex : 11271
		 * @type {number}
		 */
		this.duration = undefined;

		/**
		 * Ex : true
		 * @type {boolean}
		 */
		this.public = undefined;

		/**
		 * Ex : false
		 * @type {boolean}
		 */
		this.is_loved_track = undefined;

		/**
		 * Ex : false
		 * @type {boolean}
		 */
		this.collaborative = undefined;

		/**
		 * Ex : 60
		 * @type {number}
		 */
		this.nb_tracks = undefined;

		/**
		 * Ex : 10178110
		 * @type {number}
		 */
		this.fans = undefined;

		/**
		 * Ex : "https://www.deezer.com/playlist/53362031"
		 * @type {string}
		 */
		this.link = undefined;

		/**
		 * Ex : "https://www.deezer.com/playlist/53362031?utm_source=deezer&utm_content=playlist-53362031&utm_term=0_1655488333&utm_medium=web"
		 * @type {string}
		 */
		this.share = undefined;

		/**
		 * Ex : "https://api.deezer.com/playlist/53362031/image"
		 * @type {string}
		 */
		this.picture = undefined;

		/**
		 * Ex : "https://e-cdns-images.dzcdn.net/images/playlist/881daaee4f02817616e927b2575b1ee3/56x56-000000-80-0-0.jpg"
		 * @type {string}
		 */
		this.picture_small = undefined;

		/**
		 * Ex : "https://e-cdns-images.dzcdn.net/images/playlist/881daaee4f02817616e927b2575b1ee3/250x250-000000-80-0-0.jpg"
		 * @type {string}
		 */
		this.picture_medium = undefined;

		/**
		 * Ex : "https://e-cdns-images.dzcdn.net/images/playlist/881daaee4f02817616e927b2575b1ee3/500x500-000000-80-0-0.jpg"
		 * @type {string}
		 */
		this.picture_big = undefined;

		/**
		 * Ex : "https://e-cdns-images.dzcdn.net/images/playlist/881daaee4f02817616e927b2575b1ee3/1000x1000-000000-80-0-0.jpg"
		 * @type {string}
		 */
		this.picture_xl = undefined;

		/**
		 * Ex : "bb7d466bf5fc20c5d06b8ec3ae14028a"
		 * @type {string}
		 */
		this.checksum = undefined;

		/**
		 * Ex : "https://api.deezer.com/playlist/53362031/tracks"
		 * @type {string}
		 */
		this.tracklist = undefined;

		/**
		 * Ex : "2017-05-17 12:12:00"
		 * @type {string}
		 */
		this.creation_date = undefined;

		/**
		 * Ex : "881daaee4f02817616e927b2575b1ee3"
		 * @type {string}
		 */
		this.md5_image = undefined;

		/**
		 * Ex : "playlist"
		 * @type {string}
		 */
		this.picture_type = undefined;

		/** @type {DeezerUser} */
		this.creator = undefined;

		/**
		 * Ex : "playlist"
		 * @type {string}
		 */
		this.type = undefined;

		/**
		 * Ex : Object {data: Array(60), checksum: "bb7d466bf5fc20c5d06b8ec3ae14028a"}
		 * @type {{data: DeezerTrack[], checksum: string}}
		 */
		this.tracks = undefined;
	}

	isEmpty() {
		return this.nb_tracks === 0;
	}

	fromAPI(jsonDatas) {
		if (jsonDatas.type !== "playlist") {
			console.warn(`\t↪ Try to create a Deezer playlist from the json datas of a ${jsonDatas.type}`);
			return null;
		}

		return Object.assign(this, jsonDatas);
	}
}

class DeezerAlbum {
	constructor() {
		/**
		 * Ex : 292908332
		 * @type {number}
		 */
		this.id = undefined;

		/**
		 * Ex : "Calm Down"
		 * @type {string}
		 */
		this.title = undefined;

		/**
		 * Ex : "https://www.deezer.com/album/292908332"
		 * @type {string}
		 */
		this.link = undefined;

		/**
		 * Ex : "https://api.deezer.com/album/292908332/image"
		 * @type {string}
		 */
		this.cover = undefined;

		/**
		 * Ex : "https://e-cdns-images.dzcdn.net/images/cover/3071378af24d789b8fc69e95162041e4/56x56-000000-80-0-0.jpg"
		 * @type {string}
		 */
		this.cover_small = undefined;

		/**
		 * Ex : "https://e-cdns-images.dzcdn.net/images/cover/3071378af24d789b8fc69e95162041e4/250x250-000000-80-0-0.jpg"
		 * @type {string}
		 */
		this.cover_medium = undefined;

		/**
		 * Ex : "https://e-cdns-images.dzcdn.net/images/cover/3071378af24d789b8fc69e95162041e4/500x500-000000-80-0-0.jpg"
		 * @type {string}
		 */
		this.cover_big = undefined;

		/**
		 * Ex : "https://e-cdns-images.dzcdn.net/images/cover/3071378af24d789b8fc69e95162041e4/1000x1000-000000-80-0-0.jpg"
		 * @type {string}
		 */
		this.cover_xl = undefined;

		/**
		 * Ex : "3071378af24d789b8fc69e95162041e4"
		 * @type {string}
		 */
		this.md5_image = undefined;

		/**
		 * Ex : "2022-02-11"
		 * @type {string}
		 */
		this.release_date = undefined;

		/**
		 * Ex : "https://api.deezer.com/album/292908332/tracks"
		 * @type {string}
		 */
		this.tracklist = undefined;

		/**
		 * Ex : "album"
		 * @type {string}
		 */
		this.type = undefined;

		/**
		 * Tracks on this album.
		 * @type {DeezerTrack[]}
		 */
		this.tracks = undefined;

		/**
		 * Ex : "602445519101"
		 * @type {string}
		 */
		this.upc = undefined;

		/**
		 * Ex : "https://www.deezer.com/album/292908332?utm_source=deezer&utm_content=album-292908332&utm_term=0_1655542692&utm_medium=web"
		 * @type {string}
		 */
		this.share = undefined;

		/**
		 * Ex : -1
		 * @type {number}
		 */
		this.genre_id = undefined;

		/**
		 * @type {{data: DeezerGenre[]}}
		 */
		this.genres = undefined;

		/**
		 * Ex : "Mavin Records / Jonzing World"
		 * @type {string}
		 */
		this.label = undefined;

		/**
		 * Ex : 2
		 * @type {number}
		 */
		this.nb_tracks = undefined;

		/**
		 * Ex : 423
		 * @type {number}
		 */
		this.duration = undefined;

		/**
		 * Ex : 3770
		 * @type {number}
		 */
		this.fans = undefined;

		/**
		 * Ex : "single"
		 * @type {string}
		 */
		this.record_type = undefined;

		/**
		 * Ex : true
		 * @type {boolean}
		 */
		this.available = undefined;

		/**
		 * Ex : true
		 * @type {boolean}
		 */
		this.explicit_lyrics = undefined;

		/**
		 * Ex : 4
		 * @type {number}
		 */
		this.explicit_content_lyrics = undefined;

		/**
		 * Ex : 0
		 * @type {number}
		 */
		this.explicit_content_cover = undefined;

		/**
		 * @type {DeezerArtist[]}
		 */
		this.contributors = undefined;

		/**
		 * @type {DeezerArtist}
		 */
		this.artist = undefined;
	}

	/**
	 * Return true if this Deezer album does not contain any track.
	 * @returns {boolean}
	 */
	isEmpty() {
		return this.tracks.length === 0;
	}

	fromAPI(jsonDatas) {
		if (jsonDatas.type !== "album") {
			console.warn(`\t↪ Try to create a Deezer album from the json datas of a ${jsonDatas.type}`);
			return null;
		}

		// Populate object
		Object.assign(this, jsonDatas);

		// Convert all tracks to Track objects
		this.tracks = [];
		for (const trackInfos of jsonDatas.tracks.data)
			this.tracks.push(new DeezerTrack().fromAPI(trackInfos));
		return this;
	}
}

class DeezerArtist {
	constructor() {
		/**
		 * Ex : 5259966
		 * @type {number}
		 */
		this.id = undefined;

		/**
		 * Ex : "Rema"
		 * @type {string}
		 */
		this.name = undefined;

		/**
		 * Ex : "https://www.deezer.com/artist/5259966"
		 * @type {string}
		 */
		this.link = undefined;

		/**
		 * Ex : "https://www.deezer.com/artist/5259966?utm_source=deezer&utm_content=artist-5259966&utm_term=0_1655477710&utm_medium=web"
		 * @type {string}
		 */
		this.share = undefined;

		/**
		 * Ex : "https://api.deezer.com/artist/5259966/image"
		 * @type {string}
		 */
		this.picture = undefined;

		/**
		 * Ex : "https://e-cdns-images.dzcdn.net/images/artist/2dad7eee97711b9a91803fad9d6c3bab/56x56-000000-80-0-0.jpg"
		 * @type {string}
		 */
		this.picture_small = undefined;

		/**
		 * Ex : "https://e-cdns-images.dzcdn.net/images/artist/2dad7eee97711b9a91803fad9d6c3bab/250x250-000000-80-0-0.jpg"
		 * @type {string}
		 */
		this.picture_medium = undefined;

		/**
		 * Ex : "https://e-cdns-images.dzcdn.net/images/artist/2dad7eee97711b9a91803fad9d6c3bab/500x500-000000-80-0-0.jpg"
		 * @type {string}
		 */
		this.picture_big = undefined;

		/**
		 * Ex : "https://e-cdns-images.dzcdn.net/images/artist/2dad7eee97711b9a91803fad9d6c3bab/1000x1000-000000-80-0-0.jpg"
		 * @type {string}
		 */
		this.picture_xl = undefined;

		/**
		 * Ex : true
		 * @type {boolean}
		 */
		this.radio = undefined;

		/**
		 * Ex : "https://api.deezer.com/artist/5259966/top?limit=50"
		 * @type {string}
		 */
		this.tracklist = undefined;

		/**
		 * Ex : "artist"
		 * @type {string}
		 */
		this.type = undefined;
	}

	fromAPI(jsonDatas) {
		if (jsonDatas.type !== "artist") {
			console.warn(`\t↪ Try to create a Deezer artist from the json datas of a ${jsonDatas.type}`);
			return null;
		}

		return Object.assign(this, jsonDatas);
	}
}

class DeezerUser {
	constructor() {
		/**
		 * Ex : 1687950
		 * @type {number}
		 */
		this.id = undefined;

		/**
		 * Ex : "Alexandre -Deezer Editeur France"
		 * @type {string}
		 */
		this.name = undefined;

		/**
		 * Ex : "https://api.deezer.com/user/1687950/flow"
		 * @type {string}
		 */
		this.tracklist = undefined;

		/**
		 * Ex : "user"
		 * @type {string}
		 */
		this.type = undefined;
	}
}

/**
 * @typedef {Object} DeezerGenre
 */

module.exports = { Deezer, DeezerTrack, DeezerPlaylist, DeezerAlbum, DeezerArtist, DeezerUser };