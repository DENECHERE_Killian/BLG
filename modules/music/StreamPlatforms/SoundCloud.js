// noinspection JSUnusedGlobalSymbols

/**
 * @author Killian
 */

const { IncomingMessage } = require("node:http");
const { request_stream }  = require("../play-dl/Request/");
const { StreamType }      = require("../play-dl/YouTube/stream");
const { Readable }        = require("node:stream");
const { Timer }           = require("../play-dl/YouTube/classes/LiveStream");
const fetch               = require("node-fetch");

const VALID_TYPES = ["track", "playlist"];

class SoundCloud {
	static PLATFORM_COLOR = "#ff680d";
	static PLATFORM_ICON  = "https://cdn3.iconfinder.com/data/icons/capsocial-round/500/soundcloud-1024.png";
	static INVALID_TYPE   = 401;
	static INVALID_ID     = 400;

	constructor() {
		this.token = null;
	}

	/**
	 * Retrieve the API informations about a playlist/track identified by its link.
	 * @param soundCloudURL {string} A clean SoundCloud link.
	 * @returns {Promise<SoundCloudTrack|SoundCloudPlaylist>}
	 */
	async getInfos(soundCloudURL) {
		return new Promise(async (resolve, reject) => {
			// Fetch json datas
			const datas = await this.#fetchForDatas(soundCloudURL)
				.catch(err => reject(err));
			if (!datas)
				return;

			if (!datas.kind || !VALID_TYPES.includes(datas.kind))
				return reject(SoundCloud.INVALID_TYPE);

			switch (datas.kind) {
				case "track":
					return resolve(new SoundCloudTrack().fromAPI(datas, this.token));

				case "playlist":
					return resolve(new SoundCloudPlaylist().fromAPI(datas, this.token));
			}
		});
	}

	/**
	 * Get a usable free spotify client id that allow to query informations about a tracks or other things they store inside their databases.
	 * The URL used was taken from a Spotify embed in Discord.
	 * @returns {Promise<string>} The accessToken.
	 */
	#getFreeToken() {
		// This is required to query datas from the SoundCloud provenance.
		return new Promise(async (resolve, reject) => {
			const homeBody          = await fetch("https://soundcloud.com/").then(async res => await res.text());
			const scripts           = homeBody.split("<script crossorigin src=\"");
			const scriptsURLs       = scripts.filter(url => url.startsWith("https")).map(url => url.split("\"")[0]);
			const lastScriptContent = await fetch(scriptsURLs[scriptsURLs.length - 1]).then(async res => await res.text());
			this.token              = lastScriptContent.split(",client_id:\"")[1].split("\"")[0];

			if (!this.token)
				return reject(`SoundCloud token is null`);

			console.log(`\t↪ Soundcloud token set to "${this.token}"`);

			resolve(this.token);
		});
	}

	/**
	 * Query json datas on diffetents endpoints of SoundCloud API.
	 * @param soundCloudURL {string} The url that will be fetched to get json datas from.
	 * @returns {Promise<json>} The datas returned by the fetch request as json.
	 */
	#fetchForDatas(soundCloudURL) {
		return new Promise(async (resolve, reject) => {
			if (!this.token)
				await this.#getFreeToken()
					.catch(err => console.error(`\t↪ Cannot get free SoundCloud token :\n${err}`));

			return fetch(`https://api-v2.soundcloud.com/resolve?url=${soundCloudURL}&client_id=${this.token}`).then(async datas => {
				const json = await datas.json();
				// if (json.error)
				// 	return reject(json.error.status);

				resolve(json);
			}).catch(err => {
				reject(err);
			});
		});
	}
}

class SoundCloudTrack {

	constructor() {
		/**
		 * Ex: "https://i1.sndcdn.com/artworks-000200589567-zhv0jb-large.jpg"
		 * @type {string}
		 */
		this.artwork_url = undefined;

		/**
		 * Ex: null
		 * @type {string}
		 */
		this.caption = undefined;

		/**
		 * Ex: true
		 * @type {boolean}
		 */
		this.commentable = undefined;

		/**
		 * Ex: 4763
		 * @type {number}
		 */
		this.comment_count = undefined;

		/**
		 * Ex: "2016-12-30T14:28:45Z"
		 * @type {string}
		 */
		this.created_at = undefined;

		/**
		 * Ex: "Download on beatport => https://goo.gl/DBsxsG\nListen on spotify => https://goo.gl/7oioWo"
		 * @type {string}
		 */
		this.description = undefined;

		/**
		 * Ex: false
		 * @type {boolean}
		 */
		this.downloadable = undefined;

		/**
		 * Ex: 0
		 * @type {number}
		 */
		this.download_count = undefined;

		/**
		 * Ex: 268380
		 * @type {number}
		 */
		this.duration = undefined;

		/**
		 * Ex: 268380
		 * @type {number}
		 */
		this.full_duration = undefined;

		/**
		 * Ex: "all"
		 * @type {string}
		 */
		this.embeddable_by = undefined;

		/**
		 * Ex: "Deep House"
		 * @type {string}
		 */
		this.genre = undefined;

		/**
		 * Ex: false
		 * @type {boolean}
		 */
		this.has_downloads_left = undefined;

		/**
		 * Ex: 300191980
		 * @type {number}
		 */
		this.id = undefined;

		/**
		 * Ex: "track"
		 * @type {string}
		 */
		this.kind = undefined;

		/**
		 * Ex: "Enormous Tunes"
		 * @type {string}
		 */
		this.label_name = undefined;

		/**
		 * Ex: "2022-06-15T14:44:58Z"
		 * @type {string}
		 */
		this.last_modified = undefined;

		/**
		 * Ex: "all-rights-reserved"
		 * @type {string}
		 */
		this.license = undefined;

		/**
		 * Ex: 108292
		 * @type {number}
		 */
		this.likes_count = undefined;

		/**
		 * Ex: "nora-en-pure-diving-with-whales-daniel-portman-radio-mix"
		 * @type {string}
		 */
		this.permalink = undefined;

		/**
		 * Ex: "https://soundcloud.com/noraenpure/nora-en-pure-diving-with-whales-daniel-portman-radio-mix"
		 * @type {string}
		 */
		this.permalink_url = undefined;

		/**
		 * Ex: 5791991
		 * @type {number}
		 */
		this.playback_count = undefined;

		/**
		 * Ex: true
		 * @type {boolean}
		 */
		this.public = undefined;

		/**
		 * @type {SoundCloudPlublisher}
		 */
		this.publisher_metadata = undefined;

		/**
		 * Ex: "download / stream"
		 * @type {string}
		 */
		this.purchase_title = undefined;

		/**
		 * Ex: "https://goo.gl/DBsxsG"
		 * @type {string}
		 */
		this.purchase_url = undefined;

		/**
		 * Ex: "2016-12-30T00:00:00Z"
		 * @type {string}
		 */
		this.release_date = undefined;

		/**
		 * Ex: 2716
		 * @type {number}
		 */
		this.reposts_count = undefined;

		/**
		 * Ex: null
		 * @type {string}
		 */
		this.secret_token = undefined;

		/**
		 * Ex: "public"
		 * @type {string}
		 */
		this.sharing = undefined;

		/**
		 * Ex: "finished"
		 * @type {string}
		 */
		this.state = undefined;

		/**
		 * Ex: true
		 * @type {boolean}
		 */
		this.streamable = undefined;

		/**
		 * Ex: ""Enormous Tunes" "Helvetic Nerds" "Nora En Pure" "Daniel Portman" Remix "Progressive House" "New House" Ibiza Trance"
		 * @type {string}
		 */
		this.tag_list = undefined;

		/**
		 * Ex: "Nora En Pure - Diving With Whales (Daniel Portman Radio Mix)"
		 * @type {string}
		 */
		this.title = undefined;

		/**
		 * Ex: "single-track"
		 * @type {string}
		 */
		this.track_format = undefined;

		/**
		 * Ex: "https://api.soundcloud.com/tracks/300191980"
		 * @type {string}
		 */
		this.uri = undefined;

		/**
		 * Ex: "soundcloud:tracks:300191980"
		 * @type {string}
		 */
		this.urn = undefined;

		/**
		 * Ex: 8040278
		 * @type {number}
		 */
		this.user_id = undefined;

		/**
		 * Ex: null
		 * @type {?}
		 */
		this.visuals = undefined;

		/**
		 * Ex: "https://wave.sndcdn.com/ilWjbTCw9eAd_m.json"
		 * @type {string}
		 */
		this.waveform_url = undefined;

		/**
		 * Ex: "2016-12-30T14:28:45Z"
		 * @type {string}
		 */
		this.display_date = undefined;

		/**
		 * Ex: Object {transcodings: Array(2)}
		 * @type {{transcodings: SoundCloudTranscoding[]}}
		 */
		this.media = undefined;

		/**
		 * Ex: "soundcloud:system-playlists:track-stations:300191980"
		 * @type {string}
		 */
		this.station_urn = undefined;

		/**
		 * Ex: "track-stations:300191980"
		 * @type {string}
		 */
		this.station_permalink = undefined;

		/**
		 * Ex: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJnZW8iOiJGUiIsInN1YiI6IiIsInJpZCI6ImY4NDZhNWI5LWUxMmQtNDVlMS1iZjU3LWRlZDZhZGJkOTVjYyIsImlhdCI6MTY1NTgxOTM0Mn0.vmC-Gn1emYY0Wg_xylo4W0zEU0-cy_GrsThwruB5m_w"
		 * @type {string}
		 */
		this.track_authorization = undefined;

		/**
		 * Ex: "BLACKBOX"
		 * @type {string}
		 */
		this.monetization_model = undefined;

		/**
		 * Ex: "MONETIZE"
		 * @type {string}
		 */
		this.policy = undefined;

		/**
		 * Ex: Object {avatar_url: "https://i1.sndcdn.com/avatars-eu2AzkUkitQiPCEQ-1xNtqQ-large.jpg", city: "", comments_count: 0, country_code: null, created_at: "2011-10-12T23:57:02Z", ...}
		 * @type {SoundCloudUser}
		 */
		this.user = undefined;
	}

	fromAPI(jsonDatas, token) {
		if (jsonDatas.kind !== "track") {
			console.warn(`\t↪ Try to create a SoundCloud track from the json datas of a ${jsonDatas.type}`);
			return null;
		}

		Object.assign(this, jsonDatas);
		this.#setToken(token);
		return this;
	}

	#setToken(token) {
		this.token = token;
	}

	async stream(options) {
		const onlyHLSStreams = this.media.transcodings.filter(transcoding => transcoding.format.protocol === "hls");

		// Choosing stream quality
		if (!options.quality || options.quality >= onlyHLSStreams.length)
			options.quality = onlyHLSStreams.length - 1; // Best quality
		else if (options.quality < 0)
			options.quality = 0; // Poorest qualty

		// Query stream datas
		const streamByQuality = onlyHLSStreams[options.quality];
		const streamType      = streamByQuality.format.mime_type.startsWith("audio/ogg") ? "ogg/opus" : "arbitrary";
		const streamDatasURL  = await fetch(`${streamByQuality.url}?client_id=${this.token}`)
			.then(async res => (await res.json()).url);

		// noinspection JSCheckFunctionSignatures
		return new SoundCloudStream(streamDatasURL, streamType);
	}
}

class SoundCloudPlaylist {

	constructor() {
		/**
		 * Ex: "https://i1.sndcdn.com/artworks-000160048609-w2k01g-large.jpg"
		 * @type {string}
		 */
		this.artwork_url = undefined;

		/**
		 * Ex: "2013-05-21T03:49:26Z"
		 * @type {string}
		 */
		this.created_at = undefined;

		/**
		 * Ex: "The Bleach Original Soundtrack 1, composed by Shiro Sagisu among other various artists."
		 * @type {string}
		 */
		this.description = undefined;

		/**
		 * Ex: 2896055
		 * @type {number}
		 */
		this.duration = undefined;

		/**
		 * Ex: "all"
		 * @type {string}
		 */
		this.embeddable_by = undefined;

		/**
		 * Ex: "Soundtrack"
		 * @type {string}
		 */
		this.genre = undefined;

		/**
		 * Ex: 5891772
		 * @type {number}
		 */
		this.id = undefined;

		/**
		 * Ex: "playlist"
		 * @type {string}
		 */
		this.kind = undefined;

		/**
		 * Ex: ""
		 * @type {string}
		 */
		this.label_name = undefined;

		/**
		 * Ex: "2016-04-27T01:02:25Z"
		 * @type {string}
		 */
		this.last_modified = undefined;

		/**
		 * Ex: "all-rights-reserved"
		 * @type {string}
		 */
		this.license = undefined;

		/**
		 * Ex: 4390
		 * @type {number}
		 */
		this.likes_count = undefined;

		/**
		 * Ex: false
		 * @type {boolean}
		 */
		this.managed_by_feeds = undefined;

		/**
		 * Ex: "bleach-ost-1"
		 * @type {string}
		 */
		this.permalink = undefined;

		/**
		 * Ex: "https://soundcloud.com/bleach_ost/sets/bleach-ost-1"
		 * @type {string}
		 */
		this.permalink_url = undefined;

		/**
		 * Ex: true
		 * @type {boolean}
		 */
		this.public = undefined;

		/**
		 * Ex: null
		 * @type {string}
		 */
		this.purchase_title = undefined;

		/**
		 * Ex: null
		 * @type {string}
		 */
		this.purchase_url = undefined;

		/**
		 * Ex: null
		 * @type {string}
		 */
		this.release_date = undefined;

		/**
		 * Ex: 457
		 * @type {number}
		 */
		this.reposts_count = undefined;

		/**
		 * Ex: null
		 * @type {string}
		 */
		this.secret_token = undefined;

		/**
		 * Ex: "public"
		 * @type {string}
		 */
		this.sharing = undefined;

		/**
		 * Ex: "bleach ost original soundtrack soul reaper society rukia kuchiki shinigami kon asterisk creeping shawdows sad piano violin battle ignition never meant to belong will of the heart requiem comical"
		 * @type {string}
		 */
		this.tag_list = undefined;

		/**
		 * Ex: "Bleach Original Soundtrack 1"
		 * @type {string}
		 */
		this.title = undefined;

		/**
		 * Ex: "https://api.soundcloud.com/playlists/5891772"
		 * @type {string}
		 */
		this.uri = undefined;

		/**
		 * Ex: 13198893
		 * @type {number}
		 */
		this.user_id = undefined;

		/**
		 * Ex: ""
		 * @type {string}
		 */
		this.set_type = undefined;

		/**
		 * Ex: false
		 * @type {boolean}
		 */
		this.is_album = undefined;

		/**
		 * Ex: null
		 * @type {string}
		 */
		this.published_at = undefined;

		/**
		 * Ex: "2013-05-21T03:49:26Z"
		 * @type {string}
		 */
		this.display_date = undefined;

		/**
		 * @type {SoundCloudUser}
		 */
		this.user = undefined;

		/**
		 * @type {SoundCloudTrack[]}
		 */
		this.tracks = undefined;

		/**
		 * Ex: 20
		 * @type {number}
		 */
		this.track_count = undefined;
	}

	fromAPI(jsonDatas, token) {
		if (jsonDatas.kind !== "playlist") {
			console.warn(`\t↪ Try to create a SoundCloud playlist from the json datas of a ${jsonDatas.type}`);
			return null;
		}

		Object.assign(this, jsonDatas);
		this.tracks = [];
		jsonDatas.tracks.forEach(track => this.tracks.push(new SoundCloudTrack().fromAPI(track, token)));
		return this;
	}
}

class SoundCloudUser {

	constructor() {
		/**
		 * Ex: "https://i1.sndcdn.com/avatars-eu2AzkUkitQiPCEQ-1xNtqQ-large.jpg"
		 * @type {string}
		 */
		this.avatar_url = undefined;

		/**
		 * Ex: ""
		 * @type {string}
		 */
		this.city = undefined;

		/**
		 * Ex: 0
		 * @type {number}
		 */
		this.comments_count = undefined;

		/**
		 * Ex: null
		 * @type {number}
		 */
		this.country_code = undefined;

		/**
		 * Ex: "2011-10-12T23:57:02Z"
		 * @type {string}
		 */
		this.created_at = undefined;

		/**
		 * @type {Object[]}
		 */
		this.creator_subscriptions = undefined;

		/**
		 * @type {Object}
		 */
		this.creator_subscription = undefined;

		/**
		 * Ex: "Purified Radioshow premiering every Saturday on Sirius XM Chill 10pm ET. \nMondays on Soundcloud, Youtube & iTunes: bit.ly/1BfI0TR\n\nNora En Pure is the deep house & indie dance music sensation that is rippling through the sonic sphere with her beautiful blend of electronic beats.\n\nA core member of the Helvetic Nerds group, Nora leads listeners out of the darker realms of deep house with intricate instrumentals and enlightening melodies. Every track she gets her hands on turns to gold and every performance she gives leaves the audience in awe. \n\nGracing the decks of some of the most revered dance music spots over the years, Nora has played sets at the likes of Ultra Music Festival, Coachella, Tomorrowland, Ushuaïa Ibiza, Mysteryland USA and held residencies at the world's largest club Privilege Ibiza and Marquee Dayclub Las Vegas. Touring relentlessly all over the globe to bring her scintillating sound to as many countries as humanly possible, her loyal and ever-growing fan base is scatt"... (length: 2,119)
		 * @type {string}
		 */
		this.description = undefined;

		/**
		 * Ex: 243217
		 * @type {number}
		 */
		this.followers_count = undefined;

		/**
		 * Ex: 34
		 * @type {number}
		 */
		this.followings_count = undefined;

		/**
		 * Ex: "Nora"
		 * @type {string}
		 */
		this.first_name = undefined;

		/**
		 * Ex: "Nora En Pure"
		 * @type {string}
		 */
		this.full_name = undefined;

		/**
		 * Ex: 0
		 * @type {number}
		 */
		this.groups_count = undefined;

		/**
		 * Ex: 8040278
		 * @type {number}
		 */
		this.id = undefined;

		/**
		 * Ex: "user"
		 * @type {string}
		 */
		this.kind = undefined;

		/**
		 * Ex: "2022-06-04T00:54:38Z"
		 * @type {string}
		 */
		this.last_modified = undefined;

		/**
		 * Ex: "En Pure"
		 * @type {string}
		 */
		this.last_name = undefined;

		/**
		 * Ex: 65
		 * @type {number}
		 */
		this.likes_count = undefined;

		/**
		 * Ex: 5
		 * @type {number}
		 */
		this.playlist_likes_count = undefined;

		/**
		 * Ex: "noraenpure"
		 * @type {string}
		 */
		this.permalink = undefined;

		/**
		 * Ex: "https://soundcloud.com/noraenpure"
		 * @type {string}
		 */
		this.permalink_url = undefined;

		/**
		 * Ex: 72
		 * @type {number}
		 */
		this.playlist_count = undefined;

		/**
		 * Ex: null
		 * @type {number}
		 */
		this.reposts_count = undefined;

		/**
		 * Ex: 603
		 * @type {number}
		 */
		this.track_count = undefined;

		/**
		 * Ex: "https://api.soundcloud.com/users/8040278"
		 * @type {string}
		 */
		this.uri = undefined;

		/**
		 * Ex: "soundcloud:users:8040278"
		 * @type {string}
		 */
		this.urn = undefined;

		/**
		 * Ex: "Nora En Pure"
		 * @type {string}
		 */
		this.username = undefined;

		/**
		 * Ex: true
		 * @type {boolean}
		 */
		this.verified = undefined;

		/**
		 * Ex: Object {urn: "soundcloud:users:8040278", enabled: true, visuals: Array(1), tracking: null}
		 * @type {Object}
		 */
		this.visuals = undefined;

		/**
		 * @type {SoundCloudBadges}
		 */
		this.badges = undefined;

		/**
		 * Ex: "soundcloud:system-playlists:artist-stations:8040278"
		 * @type {string}
		 */
		this.station_urn = undefined;

		/**
		 * Ex: "artist-stations:8040278"
		 * @type {string}
		 */
		this.station_permalink = undefined;

	}
}

/**
 * @typedef {object} SoundCloudBadges
 * @property {boolean} SoundCloudBadges.pro Ex: false
 * @property {boolean} SoundCloudBadges.pro_unlimited Ex: true
 * @property {boolean} SoundCloudBadges.verified Ex: true
 */
/**
 * @typedef {object} SoundCloudPlublisher
 * @property {number} SoundCloudPlublisher.id Ex: 300191980
 * @property {string} SoundCloudPlublisher.urn Ex: "soundcloud:tracks:300191980"
 * @property {string} SoundCloudPlublisher.artist Ex: "Nora En Pure"
 * @property {boolean} SoundCloudPlublisher.contains_music Ex: true
 * @property {string} SoundCloudPlublisher.publisher Ex: "Sirup Music (SUISA)"
 * @property {boolean} SoundCloudPlublisher.explicit Ex: false
 * @property {string} SoundCloudPlublisher.p_line Ex: "Enormous Tunes, a Division Of GNKP Goodnight Kiss Productions Gmbh"
 * @property {string} SoundCloudPlublisher.p_line_for_display Ex: "℗ Enormous Tunes, a Division Of GNKP Goodnight Kiss Productions Gmbh"
 * @property {string} SoundCloudPlublisher.writer_composer Ex: "Nora En Pure"
 */
/**
 * @typedef {object} SoundCloudTranscoding
 * @property {string} SoundCloudTranscoding.url Ex: "https://api-v2.soundcloud.com/media/soundcloud:tracks:300191980/872b39ce-8233-40af-b25a-29b4660f17cf/stream/hls"
 * @property {string} SoundCloudTranscoding.preset Ex: "mp3_0_0"
 * @property {number} SoundCloudTranscoding.duration Ex: 268380
 * @property {boolean} SoundCloudTranscoding.snipped Ex: false
 * @property {Object} SoundCloudTranscoding.format Ex: Object {protocol: "hls", mime_type: "audio/mpeg"}
 * @property {string} SoundCloudTranscoding.quality Ex: "sq"
 */


// noinspection ES6MissingAwait,JSIgnoredPromiseFromCall,DuplicatedCode
/**
 * SoundCloud Stream class. code souce taken from [playDL repository](https://github.com/play-dl/play-dl)
 */
class SoundCloudStream {
	/**
	 * Constructor for SoundCloud Stream
	 * @param url {string} Dash url containing dash file.
	 * @param type {StreamType} Stream Type
	 */
	constructor(url, type = StreamType.Arbitrary) {

		/**
		 * Readable Stream through which data passes.
		 * @type {Readable}
		 */
		this.stream = new Readable({
			highWaterMark: 5 * 1000 * 1000, read() {
			}
		});

		/**
		 * Type of audio data that we received.
		 * @type {StreamType}
		 */
		this.type = type;

		/**
		 * Dash Url containing segment urls.
		 * @private
		 * @type {string}
		 */
		this.url = url;

		/**
		 * Total time of downloaded segments data.
		 * @private
		 * @type {number}
		 * @default 0
		 */
		this.downloaded_time = 0;

		/**
		 * Timer for looping code every 5 minutes.
		 * @private
		 * @type {Timer}
		 */
		this.timer = new Timer(() => {
			this.timer.reuse();
			this.#start();
		}, 280); // Time in seconds

		/**
		 * Total segments Downloaded so far.
		 * @private
		 * @type {number}
		 * @default 0
		 */
		this.downloaded_segments = 0;

		/**
		 * Incoming message that we recieve.
		 *
		 * Storing this is essential.
		 * This helps to destroy the TCP connection completely if you stopped player in between the stream
		 * @private
		 * @type {IncomingMessage|null}
		 */
		this.request = null;

		/**
		 * Array of segment time. Useful for calculating downloaded_time.
		 * @type {number[]}
		 */
		this.time = [];

		/**
		 * Array of segment_urls in dash file.
		 * @type {string[]}
		 */
		this.segment_urls = [];

		this.stream.on("close", this.#cleanup);
		this.#start();
	}

	/**
	 * Parses SoundCloud dash file.
	 * @private
	 */
	async #parseSegments() {
		const trackSegmentsFile = await fetch(this.url)
			.then(async res => await res.text())
			.catch((err) => err);

		if (trackSegmentsFile instanceof Error)
			throw trackSegmentsFile;

		trackSegmentsFile.split("\n").forEach((line) => {
			if (line.startsWith("#EXTINF:"))
				this.time.push(parseFloat(line.replace("#EXTINF:", "")));
			else if (line.startsWith("https"))
				this.segment_urls.push(line);
		});
	}

	/**
	 * Starts looping of code for getting all segments urls data
	 */
	async #start() {
		if (this.stream.destroyed)
			return this.#cleanup();

		this.time            = [];
		this.segment_urls    = [];
		this.downloaded_time = 0;
		await this.#parseSegments();
		this.segment_urls.splice(0, this.downloaded_segments);
		this.#loop();
	}

	/**
	 * Main Loop function for getting all segments urls data.
	 */
	async #loop() {
		if (this.stream.destroyed)
			return this.#cleanup();

		if (this.time.length === 0 || this.segment_urls.length === 0) {
			this.stream.push(null);
			return this.#cleanup();
		}

		this.downloaded_time += this.time.shift();
		this.downloaded_segments++;
		const nextSegmentStream = await request_stream(this.segment_urls.shift())
			.catch((err) => err);
		if (nextSegmentStream instanceof Error) {
			this.stream.emit("error", nextSegmentStream);
			return this.#cleanup();
		}

		this.request = nextSegmentStream;
		nextSegmentStream.on("data", (c) => {
			this.stream.push(c);
		});
		nextSegmentStream.on("end", () => {
			if (this.downloaded_time >= 300)
				return;

			this.#loop();
		});
		nextSegmentStream.once("error", (err) => {
			this.stream.emit("error", err);
		});
	}

	/**
	 * This cleans every used variable in class.
	 *
	 * This is used to prevent re-use of this class and helping garbage collector to collect it.
	 */
	#cleanup() {
		this.timer?.destroy();
		this.request?.destroy();

		this.downloaded_segments = 0;
		this.downloaded_time     = 0;
		this.segment_urls        = [];
		this.request             = null;
		this.time                = [];
		this.url                 = "";
	}

	/**
	 * Pauses timer.
	 * Stops running of loop.
	 *
	 * Useful if you don't want to get excess data to be stored in stream.
	 */
	pause() {
		this.timer.pause();
	}

	/**
	 * Resumes timer.
	 * Starts running of loop.
	 */
	resume() {
		this.timer.resume();
	}
}

module.exports = { SoundCloud, SoundCloudTrack, SoundCloudPlaylist, SoundCloudUser, SoundCloudStream };