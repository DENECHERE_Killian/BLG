// noinspection JSUnusedGlobalSymbols

/**
 * @author Killian
 */
const fetch = require("node-fetch");

const VALID_TYPES = ["tracks", "albums", "playlists"];

class Spotify {
	static PLATFORM_COLOR = "#2ebd59";
	static PLATFORM_ICON  = "https://logospng.org/download/spotify/logo-spotify-icon-1536.png";
	static UNKNOWN_MEDIA  = 404;
	static INVALID_TYPE   = 401;
	static INVALID_ID     = 400;

	constructor() {
		this.token = null;
	}

	/**
	 * Retrieve the API informations about a playlist identified by its link.
	 * @param spotifyURL {string} A clean Spotify link.
	 * @returns {Promise<SpotifyTrack|SpotifyAlbum|SpotifyPlaylist>}
	 */
	getInfos(spotifyURL) {
		return new Promise(async (resolve, reject) => {
			const split = spotifyURL.split("/");
			const type  = split[3] + "s";
			const id    = split[4];

			if (!type || !VALID_TYPES.includes(type))
				return reject(Spotify.INVALID_TYPE);
			if (!id)
				return reject(Spotify.INVALID_ID);

			// Fetch json datas
			const datas = await this.#fetchForDatas(`https://api.spotify.com/v1/${type}/${id}?market=FR`)
				.catch(err => reject(err));
			if (!datas)
				return;

			switch (type) {
				case "tracks":
					return resolve(new SpotifyTrack().fromAPI(datas));

				case "albums":
					return resolve(new SpotifyAlbum().fromAPI(datas));

				case "playlists":
					return resolve(new SpotifyPlaylist().fromAPI(datas));
			}
		});
	}

	/**
	 * Get a usable free spotify client id that allow to query informations about a tracks or other things they store inside their databases.
	 * The URL used was taken from a Spotify embed in Discord.
	 * @returns {Promise<string>} The accessToken.
	 */
	async #getFreeToken() {
		return new Promise((resolve, reject) => {
			fetch("https://open.spotify.com/get_access_token", {
				headers: { "Content-Type": "application/json", "Accept": "application/json" }
			}).then(async datas => {
				this.token = (await datas.json()).accessToken;
				if (!this.token)
					return reject(`Spotify token is null`);

				console.log(`\t↪ Spotify token set to "${this.token}"`);
				resolve(this.token);
			}).catch(err => {
				reject(err);
			});
		});
	}

	/**
	 * Query json datas on diffetents endpoints of Spotify API.
	 * @param url {string} The url that will be fetched to get json datas from.
	 * @returns {Promise<json>} The datas returned by the fetch request as json.
	 */
	#fetchForDatas(url) {
		return new Promise(async (resolve, reject) => {
			if (!this.token)
				await this.#getFreeToken()
					.catch(err => console.error(`\t↪ Cannot get free Spotify token :\n${err}`));

			return fetch(url, {
				headers: { "Content-Type": "application/json", Accept: "application/json", Authorization: `Bearer ${this.token}` }
			}).then(async datas => {
				const json = await datas.json();
				if (json.error)
					return reject(json.error.status);

				resolve(json);
			}).catch(err => {
				reject(err);
			});
		});
	}
}

class SpotifyPlaylist {

	constructor() {
		/**
		 * Ex: false
		 * @type {boolean}
		 */
		this.collaborative = undefined;

		/**
		 * Ex: "With RIDSA, Angele, Guigou, ILLANCTO and more"
		 * @type {string}
		 */
		this.description = undefined;

		/**
		 * Ex: Object {spotify: "https://open.spotify.com/playlist/37i9dQZF1E8LAfc1hCWUMF"}
		 * @type {SpotifyExternalUrls}
		 */
		this.external_urls = undefined;

		/**
		 * Ex: Object {href: null, total: 78}
		 * @type {SpotifyFollowers}
		 */
		this.followers = undefined;

		/**
		 * Ex: "https://api.spotify.com/v1/playlists/37i9dQZF1E8LAfc1hCWUMF"
		 * @type {string}
		 */
		this.href = undefined;

		/**
		 * Ex: "37i9dQZF1E8LAfc1hCWUMF"
		 * @type {string}
		 */
		this.id = undefined;

		/**
		 * @type {SpotifyImage[]}
		 */
		this.images = undefined;

		/**
		 * Ex: "Santa Maria Radio"
		 * @type {string}
		 */
		this.name = undefined;

		/**
		 * @type {SpotifyUser}
		 */
		this.owner = undefined;

		/**
		 * Ex: null
		 * @type {string}
		 */
		this.primary_color = undefined;

		/**
		 * Ex: false
		 * @type {boolean}
		 */
		this.public = undefined;

		/**
		 * Ex: "MTY1NTgwNzE0MSwwMDAwMDAwMGI1MzgxZTU4MzIyYjA0ODRmZDhhOWI4OWFjNDRlN2Y1"
		 * @type {string}
		 */
		this.snapshot_id = undefined;

		/**
		 * Ex: Object {href: "https://api.spotify.com/v1/playlists/37i9dQZF1E8LAfc1hCWUMF/tracks?offset=0&limit=100&market=FR", items: Array(50), limit: 100, next: null, offset: 0, ...}
		 * @type {SpotifyPlaylistTracks}
		 */
		this.tracks = undefined;

		/**
		 * Ex: "playlist"
		 * @type {string}
		 */
		this.type = undefined;

		/**
		 * Ex: "spotify:playlist:37i9dQZF1E8LAfc1hCWUMF"
		 * @type {string}
		 */
		this.uri = undefined;
	}

	fromAPI(jsonDatas) {
		if (jsonDatas.type !== "playlist") {
			console.warn(`\t↪ Try to create a Spotify playlist from the json datas of a ${jsonDatas.type}`);
			return null;
		}

		return Object.assign(this, jsonDatas);
	}
}

class SpotifyTrack {

	constructor() {
		/**
		 * @type {SpotifyAlbum}
		 */
		this.album = undefined;

		/**
		 * @type {SpotifyArtist[]}
		 */
		this.artists = undefined;

		/**
		 * Ex: 1
		 * @type {number}
		 */
		this.disc_number = undefined;

		/**
		 * Ex: 224932
		 * @type {number}
		 */
		this.duration_ms = undefined;

		/**
		 * Ex: false
		 * @type {boolean}
		 */
		this.episode = undefined;

		/**
		 * Ex: false
		 * @type {boolean}
		 */
		this.explicit = undefined;

		/**
		 * Ex: Object {isrc: "FR0U91401276"}
		 * @type {Object}
		 */
		this.external_ids = undefined;

		/**
		 * @type {SpotifyExternalUrls}
		 */
		this.external_urls = undefined;

		/**
		 * Ex: "https://api.spotify.com/v1/tracks/2u3QV8YZ9Rwwx11K8G5FI2"
		 * @type {string}
		 */
		this.href = undefined;

		/**
		 * Ex: "2u3QV8YZ9Rwwx11K8G5FI2"
		 * @type {string}
		 */
		this.id = undefined;

		/**
		 * Ex: false
		 * @type {boolean}
		 */
		this.is_local = undefined;

		/**
		 * Ex: true
		 * @type {boolean}
		 */
		this.is_playable = undefined;

		/**
		 * Ex : "Nous et seulement nous"
		 * @type {string}
		 */
		this.name = undefined;

		/**
		 * Ex: 40
		 * @type {number}
		 */
		this.popularity = undefined;

		/**
		 * Ex: "https://p.scdn.co/mp3-preview/1a497c6947c1501c0829b3d58b4a0ae82731949a?cid=f6a40776580943a7bc5173125a1e8832"
		 * @type {string}
		 */
		this.preview_url = undefined;

		/**
		 * Ex: true
		 * @type {boolean}
		 */
		this.track = undefined;

		/**
		 * Ex: 15
		 * @type {number}
		 */
		this.track_number = undefined;

		/**
		 * Ex: "track"
		 * @type {string}
		 */
		this.type = undefined;

		/**
		 * Ex: "spotify:track:2u3QV8YZ9Rwwx11K8G5FI2"
		 * @type {string}
		 */
		this.uri = undefined;
	}

	fromAPI(jsonDatas) {
		if (jsonDatas.type !== "track") {
			console.warn(`\t↪ Try to create a Spotify track from the json datas of a ${jsonDatas.type}`);
			return null;
		}

		return Object.assign(this, jsonDatas);
	}

	async stream(_) {
		// Cannot stream more than a preview of 30 seconds.
		return { stream: this.preview_url, type: "mp3" };
	}

}

class SpotifyAlbum {

	constructor() {
		/**
		 * Ex: "album"
		 * @type {string}
		 */
		this.album_type = undefined;

		/**
		 * @type {SpotifyArtist[]}
		 */
		this.artists = undefined;

		/**
		 * Ex: Object {spotify: "https://open.spotify.com/album/1080IslYijdzXpPkjsoLq1"}
		 * @type {SpotifyExternalUrls}
		 */
		this.external_urls = undefined;

		/**
		 * Ex: "https://api.spotify.com/v1/albums/1080IslYijdzXpPkjsoLq1"
		 * @type {string}
		 */
		this.href = undefined;

		/**
		 * Ex: "1080IslYijdzXpPkjsoLq1"
		 * @type {string}
		 */
		this.id = undefined;

		/**
		 * Ex: Array(3) [Object, Object, Object]
		 * @type {SpotifyImage[]}
		 */
		this.images = undefined;

		/**
		 * Ex: "Mes histoires"
		 * @type {string}
		 */
		this.name = undefined;

		/**
		 * Ex: "2014-06-02"
		 * @type {string}
		 */
		this.release_date = undefined;

		/**
		 * Ex: "day"
		 * @type {string}
		 */
		this.release_date_precision = undefined;

		/**
		 * Ex: 15
		 * @type {number}
		 */
		this.total_tracks = undefined;

		/**
		 * Ex: "album"
		 * @type {string}
		 */
		this.type = undefined;

		/**
		 * Ex: "spotify:album:1080IslYijdzXpPkjsoLq1"
		 * @type {string}
		 */
		this.uri = undefined;
	}

	fromAPI(jsonDatas) {
		if (jsonDatas.type !== "album") {
			console.warn(`\t↪ Try to create a Spotify album from the json datas of a ${jsonDatas.type}`);
			return null;
		}

		return Object.assign(this, jsonDatas);
	}
}

class SpotifyUser {

	constructor() {
		/**
		 * Ex: "Spotify"
		 * @type {string}
		 */
		this.display_name = undefined;

		/**
		 * Ex: Object {spotify: "https://open.spotify.com/user/spotify"}
		 * @type {SpotifyExternalUrls}
		 */
		this.external_urls = undefined;

		/**
		 * Ex: "https://api.spotify.com/v1/users/spotify"
		 * @type {string}
		 */
		this.href = undefined;

		/**
		 * Ex: "spotify"
		 * @type {string}
		 */
		this.id = undefined;

		/**
		 * Ex: "user"
		 * @type {string}
		 */
		this.type = undefined;

		/**
		 * Ex: "spotify:user:spotify"
		 * @type {string}
		 */
		this.uri = undefined;
	}
}

class SpotifyArtist {

	constructor() {
		/**
		 * Ex: Object {spotify: "https://open.spotify.com/artist/4TGltjqP0MQxdGeSIrM4es"}
		 * @type {SpotifyExternalUrls}
		 */
		this.external_urls = undefined;

		/**
		 * Ex: "https://api.spotify.com/v1/artists/4TGltjqP0MQxdGeSIrM4es"
		 * @type {string}
		 */
		this.href = undefined;

		/**
		 * Ex: "4TGltjqP0MQxdGeSIrM4es"
		 * @type {string}
		 */
		this.id = undefined;

		/**
		 * Ex: "RIDSA"
		 * @type {string}
		 */
		this.name = undefined;

		/**
		 * Ex: "artist"
		 * @type {string}
		 */
		this.type = undefined;

		/**
		 * Ex: "spotify:artist:4TGltjqP0MQxdGeSIrM4es"
		 * @type {string}
		 */
		this.uri = undefined;
	}
}

/**
 * @typedef {Object} SpotifyImage
 * @property {string} SpotifyImage.height
 * @property {string} SpotifyImage.url
 * @property {string} SpotifyImage.width
 */
/**
 * @typedef {Object} SpotifyExternalUrls
 * @property {string} SpotifyExternalUrls.spotify
 */
/**
 * @typedef {Object} SpotifyFollowers
 * @property {string} SpotifyFollowers.href
 * @property {number} SpotifyFollowers.total
 */
/**
 * @typedef {Object} SpotifyPlaylistTracks
 * @property {string} SpotifyPlaylistTracks.href Ex: "https://api.spotify.com/v1/playlists/37i9dQZF1E8LAfc1hCWUMF/tracks?offset=0&limit=100&market=FR"
 * @property {SpotifyPlaylistTrackInfos[]} SpotifyPlaylistTracks.items
 * @property {number} SpotifyPlaylistTracks.limit Ex: 100
 * @property {?} SpotifyPlaylistTracks.next Ex: null
 * @property {number} SpotifyPlaylistTracks.offset Ex: 0
 * @property {?} SpotifyPlaylistTracks.previous Ex: null
 * @property {number} SpotifyPlaylistTracks.total Ex: 50
 */
/**
 * @typedef {Object} SpotifyPlaylistTrackInfos
 * @property {string} SpotifyPlaylistTrackInfos.added_at Ex: "1970-01-01T00:00:00Z"
 * @property {SpotifyUser} SpotifyPlaylistTrackInfos.added_by
 * @property {boolean} SpotifyPlaylistTrackInfos.is_local Ex: false
 * @property {string} SpotifyPlaylistTrackInfos.primary_color Ex: null
 * @property {SpotifyTrack} SpotifyPlaylistTrackInfos.track Ex: Object {album: Object, artists: Array(2), disc_number: 1, duration_ms: 224932, episode: false, ...}
 * @property {Object} SpotifyPlaylistTrackInfos.video_thumbnail Ex: Object {url: null}
 */

module.exports = { Spotify, SpotifyPlaylist, SpotifyTrack, SpotifyAlbum, SpotifyUser, SpotifyArtist };