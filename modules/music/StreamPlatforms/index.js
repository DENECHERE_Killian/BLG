/**
 * @description This file exports all compatible stream platorms class in order to have lighter require statements in other files.
 * @author Killian
 */

const {
	      Spotify, SpotifyPlaylist, SpotifyTrack, SpotifyAlbum, SpotifyUser, SpotifyArtist
      } = require("./Spotify");

const {
	      SoundCloud, SoundCloudTrack, SoundCloudPlaylist, SoundCloudUser, SoundCloudStream
      } = require("./SoundCloud");

const {
	      YouTube, YoutubeVideo, YoutubePlayList
      } = require("./YouTube");

const {
	      Deezer, DeezerTrack, DeezerPlaylist, DeezerAlbum, DeezerArtist, DeezerUser
      } = require("./Deezer");

/**
 * Exports all compatible stream platorms class in order to have lighter require statements in other files.
 */
module.exports = {
	Spotify,
	SpotifyTrack,
	SpotifyAlbum,
	SpotifyPlaylist,
	SpotifyUser,
	SpotifyArtist,
	SoundCloud,
	SoundCloudTrack,
	SoundCloudPlaylist,
	SoundCloudUser,
	SoundCloudStream,
	YouTube,
	YoutubeVideo,
	YoutubePlayList,
	Deezer,
	DeezerTrack,
	DeezerPlaylist,
	DeezerAlbum,
	DeezerArtist,
	DeezerUser
};