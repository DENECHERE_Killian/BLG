/**
 * @author Killian
 */

const playDl = require("../play-dl/index");
const fetch  = require("node-fetch");
const Utils  = require("../../../utilities/Utils");

const MAX_SEARCH_RESULTS = process.env.TEST === "true" ? 10 : 20; // Amount
const CHANNEL_LINK       = `https://www.youtube.com/channel`; // The link to a channel page.
const SEARCH_LINK        = `https://www.youtube.com/results`; // The link to the search page.
const VIDEO_LINK         = `www.youtube.com/watch`; // The link to the video watching page.
const API_LINK           = `https://youtube.googleapis.com/youtube/v3`; // The link to fetch informations through API.
const SEARCH_TYPES       = {
	PlayList: "EgIQAw%253D%253D",
	Channel: "EgIQAg%253D%253D",
	Video: "EgIQAQ%253D%253D",
	Movie: "EgIQBA%253D%253D"
}; // Different media types to search for on YouTube search page.


/**
 * @typedef {Object} SearchOptions Parameters to filter searching results.
 * @property {"PlayList"|"Channel"|"Video"|"Movie"} [type="Video"] Filter the type of media to search for.
 * @property {number} [limit=infini] The max result amount
 */

/**
 * This class represent the music module YouTube part. It is used to query informations about videos and playlist from YouTube.
 */
class YouTube {
	static AGE_RESTICTED_VIDEO = 410; // code
	static PLATFORM_COLOR      = "#fe0000";
	static PLATFORM_ICON       = "https://www.vectorico.com/?wpfilebase_thumbnail=1&fid=27&name=youtube-red-square.png";
	static BOT_DETECTED        = 807; // code
	static NO_RESULT           = 404; // code
	static NOT_VALID_KEY       = 400; // code

	constructor() {

	}

	/**
	 * Query specific YouTube playlist informations through the YouTube official API.
	 * @param playlistURL {string} The YouTube playlist url to fetch for informations about.
	 * @returns {Promise<YoutubePlayList>} A {@link YoutubePlayList} filled with found informations is returned.
	 */
	getPlaylistInfos(playlistURL) {
		return new Promise(async (resolve, reject) => {
			if (!this.#isYouTubeURL(playlistURL))
				return reject("Not a valid YT url");

			const parameters = this.#extractURLParameters(playlistURL);
			if (!parameters.list)
				return reject("Cannot retrieve playlist id in this link");

			// Get playlist videos id
			const playlistDatas = await fetch(`${API_LINK}/playlistItems?part=contentDetails&maxResults=${MAX_SEARCH_RESULTS}&playlistId=${parameters.list}&key=${process.env.YT_API_TOKEN}`)
				.then(async res => {
					return await res.json();
				}).catch(err => {
					reject(err);
				});

			// Cannot obtain playlist datas
			if (playlistDatas.error)
				return reject(playlistDatas.error);

			// String containing each video id of the playlist
			const videosIdString = playlistDatas.items.map(videoDatas => videoDatas.contentDetails.videoId).join(",");

			// Query informations about each playlist video in 1 request.
			await fetch(`${API_LINK}/videos?part=status,snippet,contentDetails&id=${videosIdString}&key=${process.env.YT_API_TOKEN}`)
				.then(async res => {
					resolve(new YoutubePlayList().fromYouTubeAPI(await res.json()));
				}).catch(err => {
					reject(err);
				});
		});
	}

	/**
	 * Query specific YouTube video informations through the YouTube official API.
	 * @param videoURL {string} The YouTube video url to fetch for informations about.
	 * @returns {Promise<YoutubeVideo>} A {@link YoutubeVideo} filled with found informations is returned.
	 */
	getVideoInfos(videoURL) {
		return new Promise(async (resolve, reject) => {
			const parameters = this.#extractURLParameters(videoURL);
			if (!parameters.v)
				return reject("Cannot retrieve video id in this link");

			// Query video infos
			await fetch(`${API_LINK}/videos?part=status,snippet,contentDetails&id=${parameters.v}&key=${process.env.YT_API_TOKEN}`)
				.then(async res => {
					if (!res)
						return reject("Unknown page : did the link is well written ?");

					const json = await res.json();
					if (json.code) {
						console.error(json.message);
						return reject(YouTube.NOT_VALID_KEY);
					}

					const videoDatas = json.items[0];
					if (!videoDatas)
						return reject(YouTube.NO_RESULT);

					if (videoDatas.contentDetails.contentRating.ytRating)
						return reject(YouTube.AGE_RESTICTED_VIDEO); // The video is age restricted

					const ytVideo = new YoutubeVideo().fromYouTubeAPI(videoDatas);
					if (parameters.t) {
						const duration = Utils.ISO8601ToSeconds(ytVideo.duration);
						parameters.t   = parameters.t > duration - 5 ? duration - 5 : parameters.t;
						parameters.t   = parameters.t < 0 ? 0 : parameters.t;
						ytVideo.seek   = parameters.t;
					}
					resolve(ytVideo);
				}).catch(err => {
					reject(err);
				});
		});
	}

	/**
	 * Search any media on YouTube platform.
	 * @param search {string} Something to search on YouTube.
	 * @param options {SearchOptions} Options to filter the results.
	 * @returns {Promise<YoutubePlayList|void|YoutubeVideo>}
	 */
	search(search, options) {
		return new Promise(async (resolve, reject) => {
			if (!options.type || !Object.keys(SEARCH_TYPES).includes(options.type))
				options.type = "Video";

			search    = search.replaceAll(" ", "%20");
			const url = `${SEARCH_LINK}?search_query=${search}&sp=${SEARCH_TYPES[options.type]}`;

			console.log(`\t↪ Searching on YouTube at ${url}`);
			const body = await fetch(url, {
				headers: { "accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7" }
			}).then(res => {
				return res.text();
			}).catch(err => {
				return reject(err);
			});

			if (!body)
				return reject(YouTube.NO_RESULT);

			if (body.indexOf("Our systems have detected unusual traffic from your computer network.") !== -1) {
				console.error("\t↪ Captcha page: YouTube has detected that you are a bot!");
				return reject(YouTube.BOT_DETECTED);
			}

			const data = JSON.parse(body
				.split("var ytInitialData = ")[1]
				.split(";</script>")[0]
				.split(";var meta = ")[0]);

			const resultsList = data.contents.twoColumnSearchResultsRenderer.primaryContents.sectionListRenderer.contents;
			let videosList    = resultsList[0].itemSectionRenderer.contents;

			if (videosList[0].backgroundPromoRenderer)
				return reject(YouTube.NO_RESULT);

			if (videosList.length === 1) // Promoted first content
				videosList = resultsList[1].itemSectionRenderer.contents;

			// There are no result for the exact research so the first item contains the research correction datas
			if (!videosList[0].videoRenderer)
				videosList.shift();

			// No limit means the entire videoList.
			if (!options.limit)
				options.limit = videosList.length;

			if (options.limit === 1)
				return resolve(new YoutubeVideo().fromYouTubeWEBVideoRenderer(videosList[0].videoRenderer));
			return resolve(new YoutubePlayList().fromYouTubeWEB(videosList.splice(0, options.limit)));
		});
	}


	/**
	 * This method extracts the url parameters
	 * @param youtubeURL {String} The YouTube link.
	 */
	#extractURLParameters(youtubeURL) {
		if (!this.#isYouTubeURL(youtubeURL))
			return null;

		if (youtubeURL.search("youtu.be") !== -1)
			youtubeURL = youtubeURL.replace("?", "&").replace("youtu.be/", `${VIDEO_LINK}?v=`);

		const res        = JSON.parse("{}");
		const parameters = youtubeURL.trim().split("?")[1];
		if (parameters) {
			parameters.split("&").map(param => {
				param = param.split("=");
				return res[param[0]] = param[1];
			});
		}

		return res;
	}

	// noinspection JSMethodCanBeStatic
	/**
	 * Used to check if a link is from YouTube or not.
	 * @param url {String} The link to check the provenance.
	 * @returns {boolean} True if the link provided is a YT link.
	 */
	#isYouTubeURL(url) {
		if (!url || typeof url !== "string")
			return false;

		return !(url.search("www.youtube.com") === -1 &&
			url.search("youtu.be") === -1 &&
			url.search("music.youtube.com") === -1);
	}

	/**
	 * Identify if the link provided comes from YouTube and identify the kind of link that it is.
	 * @param url {String} The url to check the provenance.
	 */
	validate(url) {
		if (!this.#isYouTubeURL(url))
			return null;

		const parameters = this.#extractURLParameters(url);
		if ((url.search("/playlist") !== -1 && parameters.list) ||
			(url.search("/watch") !== -1 && parameters.list))
			return "playlist";

		if ((url.search("/watch") !== -1 && parameters.v) ||
			url.search("/embed/") !== -1 ||
			url.search("/youtu.be/") !== -1)
			return "video";

		if (url.search("/channel/") !== -1 ||
			url.search("/c/") !== -1)
			return "channel";

		if (url.search("/shorts/") !== -1)
			return "shorts";
	}
}

/**
 * This class represent a YouTube video object. It is used to store in a unique format all the different sources coming informations (API, WEB).
 */
class YoutubeVideo {
	constructor() {
		this.id                = null;
		this.title             = null;
		this.channelId         = null;
		this.channelTitle      = null;
		this.ownerChannelId    = null;
		this.ownerChannelTitle = null;
		this.ownerChannelUrl   = null;

		this.duration      = null;
		this.seek          = null;
		this.isLive        = null;
		this.url           = null;
		this.description   = null;
		this.thumbnails    = null;
		this.privacyStatus = null;

		/**
		 * @type {YoutubeVideo[]}
		 */
		this.relatedVideos = [];
	}

	/**
	 * Populate this {@link YoutubeVideo} with datas extracted from the YouTube API.
	 * @param jsonQueryResult {json} Json object containing all datas required to populate this object.
	 * @returns {YoutubeVideo|void} Returns this object filled with the right datas if there was no error. Otherwise, return nothing.
	 */
	fromYouTubeAPI(jsonQueryResult) {
		if (!jsonQueryResult || jsonQueryResult.kind !== "youtube#video")
			return console.error(`\t↪ Not a YT video : ${jsonQueryResult.kind}`);

		this.id                = jsonQueryResult.id;
		this.title             = jsonQueryResult.snippet.title;
		this.channelId         = jsonQueryResult.snippet.channelId;
		this.channelTitle      = jsonQueryResult.snippet.channelTitle;
		this.ownerChannelId    = jsonQueryResult.snippet.videoOwnerChannelId ? jsonQueryResult.snippet.videoOwnerChannelId : this.channelId;
		this.ownerChannelTitle = jsonQueryResult.snippet.videoOwnerChannelTitle ? jsonQueryResult.snippet.videoOwnerChannelTitle : this.channelTitle;
		this.ownerChannelUrl   = `${CHANNEL_LINK}/${this.ownerChannelId}`;

		this.duration      = jsonQueryResult.contentDetails.duration; // ISO 8601 format
		this.url           = `https://${VIDEO_LINK}?v=${this.id}`;
		this.isLive        = jsonQueryResult.snippet.liveBroadcastContent !== "none";
		this.description   = jsonQueryResult.snippet.description;
		this.thumbnails    = jsonQueryResult.snippet.thumbnails;
		this.privacyStatus = jsonQueryResult.status.privacyStatus;

		return this;
	}

	/**
	 * Populate this {@link YoutubeVideo} with datas extracted from the YouTube Website (a video renderer is encountered when fetching a YouTube video web page).
	 * @param jsonScrappingResult {json} Json object containing all datas required to populate this object.
	 * @returns {YoutubeVideo|void} Returns this object filled with the right datas if there was no error. Otherwise, return nothing.
	 */
	fromYouTubeWEBVideoRenderer(jsonScrappingResult) {
		if (!jsonScrappingResult)
			return console.error(`\t↪ Not a YT video : ${jsonScrappingResult}`);

		this.id                = jsonScrappingResult.videoId;
		this.title             = jsonScrappingResult.title.runs[0].text;
		this.channelId         = jsonScrappingResult.ownerText.runs[0].navigationEndpoint.browseEndpoint.browseId;
		this.channelTitle      = jsonScrappingResult.ownerText.runs[0].text;
		this.ownerChannelId    = this.channelId;
		this.ownerChannelTitle = this.channelTitle;
		this.ownerChannelUrl   = `${CHANNEL_LINK}/${this.ownerChannelId}`;

		this.duration      = Utils.stringDurationToISO8601(jsonScrappingResult.lengthText.simpleText); // ISO 8601 format
		this.url           = `https://${VIDEO_LINK}?v=${this.id}`;
		this.isLive        = !jsonScrappingResult.lengthText;
		this.description   = "Not queried";
		this.thumbnails    = {
			default: jsonScrappingResult.thumbnail.thumbnails[0]
		};
		this.privacyStatus = false;

		return this;
	}

	/**
	 * Populate this {@link YoutubeVideo} with datas extracted from the YouTube Website (a compact video renderer is encountered when fetching a YouTube search web page).
	 * @param jsonScrappingResult {json} Json object containing all datas required to populate this object.
	 * @returns {YoutubeVideo|void} Returns this object filled with the right datas if there was no error. Otherwise, return nothing.
	 */
	fromYouTubeWEBCompactVideoRenderer(jsonScrappingResult) {
		if (!jsonScrappingResult)
			return console.error(`\t↪ Not a YT video : ${jsonScrappingResult}`);

		this.id                = jsonScrappingResult.videoId;
		this.title             = jsonScrappingResult.title.simpleText;
		this.channelId         = jsonScrappingResult.longBylineText.runs[0].navigationEndpoint.browseEndpoint.browseId;
		this.channelTitle      = jsonScrappingResult.longBylineText.runs[0].text;
		this.ownerChannelId    = this.channelId;
		this.ownerChannelTitle = this.channelTitle;
		this.ownerChannelUrl   = `${CHANNEL_LINK}/${this.ownerChannelId}`;

		this.duration      = Utils.stringDurationToISO8601(jsonScrappingResult.lengthText ? jsonScrappingResult.lengthText.simpleText : "0"); // ISO 8601 format
		this.url           = `https://${VIDEO_LINK}?v=${this.id}`;
		this.isLive        = !jsonScrappingResult.lengthText;
		this.description   = "Not queried";
		this.thumbnails    = {
			default: jsonScrappingResult.thumbnail.thumbnails[0]
		};
		this.privacyStatus = false;

		return this;
	}

	/**
	 * Find this {@link YoutubeVideo} related videos on YouTube.
	 * @returns {Promise<void>} Add the found results videos into the {@link this.relatedVideos} attributes.
	 */
	async findRelatedVideos() {
		// this.relatedVideos = (await ytdl.getInfo(this.url)).related_videos;
		if (!this.url)
			return console.error(`Need to have a video url to fetch in order to find related videos.`);

		if (this.relatedVideos.length > 0)
			console.warn(`Related videos have already been added and fetching for others is useless : remove redondent call.`);

		const body = await fetch(this.url, { headers: { "accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7" } })
			.then(res => res.text());

		if (body.indexOf("Our systems have detected unusual traffic from your computer network.") !== -1)
			return console.error("\t↪ Captcha page: YouTube has detected that you are a bot!");

		const data = JSON.parse(body
			.split("var ytInitialData = ")[1]
			.split(";</script>")[0]
			.split(";var meta = ")[0]);

		if (!data)
			return console.error("\t↪ Cannot retrieve json datas from youtube video page.");

		data.contents.twoColumnWatchNextResults.secondaryResults.secondaryResults.results.forEach(videoInfos => {
			if (videoInfos.compactVideoRenderer) // Ignore first and last results that are not videos
				this.relatedVideos.push(new YoutubeVideo().fromYouTubeWEBCompactVideoRenderer(videoInfos.compactVideoRenderer));
		});
	}

	stream(options) {
		return playDl.stream(this.url, options);
	}
}

/**
 * This class represent a YouTube playlist object. It is used to store in a unique format all the different sources coming informations (API, WEB).
 */
class YoutubePlayList {

	constructor() {
		/**
		 * @type {YoutubeVideo[]}
		 */
		this.videosList = [];
	}

	/**
	 * Populate this {@link YoutubePlayList} with datas extracted from the YouTube API.
	 * @param jsonQueryResult {json} Json object containing all datas required to populate this object.
	 * @returns {YoutubePlayList|void} Returns this object filled with the right datas if there was no error. Otherwise, return nothing.
	 */
	fromYouTubeAPI(jsonQueryResult) {
		if (this.videosList.length > 0)
			console.warn(`You are adding videos to a non empty videos list.`);

		if (jsonQueryResult.kind !== "youtube#videoListResponse")
			return console.error(`\t↪ Not a YT playlist : ${jsonQueryResult.kind}`);

		jsonQueryResult.items.forEach(video => this.videosList.push(new YoutubeVideo().fromYouTubeAPI(video)));

		return this;
	}

	/**
	 * Populate this {@link YoutubePlayList} with videos datas extracted from the YouTube Website (a list of video renderer).
	 * The videos are added into {@link this.videosList} attributes.
	 * @param videosList {json[]} Json object containing all datas required to populate this object.
	 * @returns {YoutubePlayList|void} Returns this object filled with the right datas.
	 */
	fromYouTubeWEB(videosList) {
		if (this.videosList.length > 0)
			console.warn(`You are adding videos to a non empty videos list.`);

		for (const video of videosList)
			this.videosList.push(new YoutubeVideo().fromYouTubeWEBVideoRenderer(video));

		return this;
	}
}

module.exports = { YouTube, YoutubeVideo, YoutubePlayList };