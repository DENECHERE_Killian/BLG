// noinspection JSUnresolvedFunction

/**
 * @author Xavier
 */

const MyEmbed = require("../../utilities/MyEmbed");
const Discord = require("discord.js");
const Utils   = require("../../utilities/Utils");

const target = {
	guildId: process.env.TEST === "true" ? "676126628451057717" : "618512072640299011", // Dev BLG id, PPDS guild id
	channelId: process.env.TEST === "true" ? "772798313756885012" : "619088385667104768"  // Test-blg-dev channel id - Dev BLG, Bot channel id - PPDS
};

const devTags = [
	"485500963269115917",
	"363343090028642325"
];

const embedContents = [
	"__Intitulé du bug :__\n" +
	"Impossible d'écrire dans le salon <#963518464763502644>\n" +
	"__Description :__\n" +
	"J'essaye de me plaindre dans le salon <#963518464763502644> mais je n'arrive pas à écrire mon message\n" +
	"__Comment le reproduire :__\n" +
	"- Aller dans le salon <#963518464763502644>\n" +
	"- Sélectionner la barre pour écrire\n" +
	"__Fréquence :__\n" +
	"Tout le temps",

	"__Intitulé du bug :__\n" +
	"**R**ésultats des partielles\n" +
	"__Description :__\n" +
	"**I**magine un peu ma réaction quand j'ai vu que j'ai eu 6/20 en anglais...\n" +
	"**C**omment je vais faire pour rattraper ça ?! \n" +
	"**K**o direct mon année là...\n" +
	"__Comment le reproduire :__\n" +
	"- **R**éviser\n" +
	"- **O**ublier tout ce qu'on a appris\n" +
	"- **L**aisser faire le stylo sur la feuille\n" +
	"__Fréquence :__\n" +
	"**L**undi dernier",

	"__Intitulé du bug :__\n" +
	"Impossible d'allumer mon pc\n" +
	"__Description :__\n" +
	"En redémarrant la box rien ne se passe, mon pc reste toujours éteint.\n" +
	"J'ai bien vérifié : pas d'électricité chez moi.\n" +
	"Je ne sais pas comment je peux écrire ici d'ailleurs.\n" +
	"__Comment le reproduire :__\n" +
	"- Acheter un pc\n" +
	"- Le brancher\n" +
	"- Appuyer sur le bouton\n" +
	"__Fréquence :__\n" +
	"Une fois pour toute",

	"__Intitulé du bug :__\n" +
	"Empêcher une mise à jour Windows\n" +
	"__Description :__\n" +
	"Windows propose souvent des mises à jours et se permet de les télécharger en arrière plan sans me prévenir !\n" +
	"__Comment le reproduire :__\n" +
	"- Lancer Windows\n" +
	"__Fréquence :__\n" +
	"De temps en temps",

	"__Intitulé du bug :__\n" +
	"Je ne peux pas fermer vim.\n" +
	"__Description :__\n" +
	"J'ai beau tester tous les raccourcis clavier, impossible de fermer ce truc.\n" +
	"__Comment le reproduire :__\n" +
	"- Ouvrir un terminal\n" +
	"- Ouvrir un fichier dans vim\n" +
	"__Fréquence :__\n" +
	"À tous les coups",

	"__Intitulé du bug :__\n" +
	"Je ne peux pas détruire un IS-2\n" +
	"__Description :__\n" +
	"Dans toutes mes parties sur WT il y a au moins un IS-2 quasi impossible à détruire\n" +
	"__Comment le reproduire :__\n" +
	"- Lancer WT\n" +
	"- Lancer une partie\n" +
	"- Tomber contre un joueur de IS-2\n" +
	"__Fréquence :__\n" +
	"90% de mes parties récentes !",

	"__Intitulé du bug :__\n" +
	"Ma cafetière fait des siennes\n" +
	"__Description :__\n" +
	"J'obtiens du thé au lieu du café 🤔 \n" +
	"__Comment le reproduire :__\n" +
	"- Installer la cafetière\n" +
	"- Ne rien mettre dans le filtre à café\n" +
	"- Remplir le réservoir de thé chaud\n" +
	"- Lancer la machine\n" +
	"__Fréquence :__\n" +
	"Depuis que je remplie le réservoir avec du thé"];

/**
 * This class represent the BugReport module of this bot.
 * It is used to create thread to allow user to report bug.
 * Only User from the bot owners guild can use this module and only in a specific channel.
 */
class BugReport {

	constructor() {
	}

	/**
	 * Handle the report command.
	 */
	runCommand(interaction) {
		console.log(`Bug report command`);
		this.#reportBug(interaction);
	}

	/**
	 * Ensure that the command has been sent in the right guild and channel.
	 * Then create a thread.
	 * Add the command sender and both all developers to the thread.
	 * Send an embed message in the thread to finalize the thread creation.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 */
	#reportBug(interaction) {
		if (interaction.guild.id !== target.guildId) {
			interaction.reply({
				embeds: [new MyEmbed("Désolé !", Utils.ORANGE_COLOR)
					.desc("La commande n'est pas disponible sur ce serveur ! 😕")], ephemeral: true
			}).catch((err) => console.error(`\t↪ Cannot reply : \n${err}`));

			return console.log(`\t↪ Command not available in this server`);
		}

		if (interaction.channel.isThread()
			&& interaction.channel.parentId === target.channelId
			&& interaction.channel.name.startsWith("Bug report n°")) {

			console.log(`\t↪ Command in a bug report thread by a dev`);
			// noinspection JSIgnoredPromiseFromCall
			this.#sendDevMessage(interaction);
			return;
		}

		if (interaction.channel.id !== target.channelId) {
			interaction.reply({
				embeds: [new MyEmbed("Désolé !", Utils.ORANGE_COLOR)
					.desc(`La commande ne peut être utilisée que dans le salon <#${target.channelId}> ! 😕`)], ephemeral: true
			}).catch((err) => console.error(`\t↪ Cannot reply : \n${err}`));

			return console.log(`\t↪ Command not available in this channel`);
		}

		console.log(`\t↪ New bug report created`);

		const targetChannel = global.bot.channels.cache.get(target.channelId);

		let maxBRId = 0;
		for (const [, thread] of targetChannel.threads.cache) {
			if (thread.ownerId === global.bot.id && thread.name.startsWith("Bug report n°")) {
				const brId = parseInt(thread.name.split("°")[1]);
				if (brId > maxBRId) {
					maxBRId = brId;
				}
			}
		}

		let brThreads = (maxBRId + 1).toString();

		while (brThreads.length < 2)
			brThreads = `0${brThreads}`;

		const sender = interaction.member;
		const title  = `Bug report n°${brThreads}`;

		targetChannel.threads.create({
			"name": title,
			"reason": "Report a new bug",
			"autoArchiveDuration": 10080
		}).then(async thread => {
			// Add thread participants
			for (const tag of devTags) // devs
				await thread.members.add(tag);
			await thread.members.add(sender.id); // sender

			// Send thread message
			thread.send({
				embeds: [
					new MyEmbed(title, Utils.BLUE_COLOR)
						.desc(`Nouveau bug <@${devTags.join(">, <@")}> !\n\n` +
							`<@${sender.id}>, voilà le thread pour le report de ton bug.\n"` +
							"Pour faciliter la prise en charge du report de bug, essaye de respecter le schéma ci-dessous ! ⤵️\n\n" +
							Utils.randomItem(embedContents))
						.setFooter({
							text: `Reporté par ${sender.user.username}`,
							iconURL: sender.displayAvatarURL()
						})
				]
			}).catch(err => console.error(`\t↪ Cannot send message :\n${err}`));

			interaction.reply({
				embeds: [new MyEmbed("À l'écoute", Utils.BLUE_COLOR)
					.desc("Ton thread est prêt !")], ephemeral: true
			}).catch(err => console.error(`\t↪ Cannot reply :\n${err}`));
		}).catch(err => console.error(`\t↪ Cannot create thread :\n${err}`));
	}

	/**
	 * Handle when the command is sent in a Bug Report thread by a developer.
	 * Send a message that allow to manage the current thread (either archive or delete it).
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @returns {Promise<void>}
	 */
	async #sendDevMessage(interaction) {
		const archiveButton = new Discord.MessageButton().setCustomId("archive_button").setLabel("Archiver").setStyle("SUCCESS").setEmoji("🗃️");
		const deleteButton  = new Discord.MessageButton().setCustomId("delete_button").setLabel("Supprimer").setStyle("DANGER").setEmoji("🗑️");
		// noinspection JSCheckFunctionSignatures
		const buttons       = new Discord.MessageActionRow().addComponents([archiveButton, deleteButton]);

		interaction.reply({
			embeds: [
				new MyEmbed("Gestion du bug report", Utils.BLUE_COLOR)
					.desc("Que dois-je faire de ce thread de bug report ? 🐛")
					.setFooter({ text: "Ensemble, luttons contre les bugs ! ✋" })
			],
			components: [buttons],
			ephemeral: true,
			fetchReply: true
		})
			.then(async message => {
				const interactionFilter  = i => devTags.includes(i.user.id);
				const componentCollector = message.createMessageComponentCollector({ interactionFilter, time: Utils.COLLECTOR_TIME * 1000, max: 1 });

				componentCollector.on("collect", async i => {
					switch (i.customId) {
						case "archive_button":
							console.log("\t↪ Collect archive button");
							await i.reply({
								embeds: [
									new MyEmbed("Situation du bug report", Utils.RED_COLOR)
										.desc("Malheureusement ma formidable équipe n'a pas réussi à reproduire le bug. " +
											"Peut-être a-t-il prit la fuite le couard !\n" +
											"J'archive donc ce thread jusqu'à ce qu'il repointe le bout de ses pattes.")
										.setFooter({ text: "Merci quand même pour ton aide 😘" })
								]
							});
							await interaction.channel.setArchived(true, "Unable to reproduce the bug")
								.then(() => {
									console.log("\t↪ Archived thread");
								})
								.catch(err => {
									console.log(`\t↪ Cannot archive the thread :\n${err}`);
								});
							break;

						case "delete_button":
							console.log("\t↪ Collect delete button");
							await i.reply({
								embeds: [
									new MyEmbed("Situation du bug report", Utils.RED_COLOR)
										.desc("YOUHOU !\n" +
											"Le bug a été éradiqué !")
										.setFooter({ text: "Merci de ton aide 😘" })
								]
							});
							setTimeout(async () => await interaction.channel.delete()
								.then(() => console.log("\t↪ Deleted thread"))
								.catch(err => {
									console.log(`\t↪ Cannot delete the thread :\n${err}`);
								}), 2500);
							break;

						default:
							await i.deferReply().catch(err => {
								console.log(`\t↪ Cannot defer reply :\n${err}`);
							});
							console.error("\t↪ Unknown button");
					}
				});
			})
			.catch(err => console.error(`\t↪ Cannot reply :\n${err}`));
	}
}

module.exports = BugReport;
