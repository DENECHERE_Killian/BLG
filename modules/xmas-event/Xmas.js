const { EmbedBuilder } = require("discord.js");
const MyEmbed          = require("../../utilities/MyEmbed");
const crypto           = require("crypto");
const Utils            = require("../../utilities/Utils");
const Users            = require("../users/Users");

class Xmas {

	/**
	 * Create a Xmas module for a specific guild
	 * @param guildId {Discord.Snowflake} The current guild id.
	 */
	constructor(guildId) {
		this.guildId = guildId;
	}

	/**
	 * Handle a command on the Xmas module.
	 * @param command {string} The command name.
	 * @param interaction {Discord.CommandInteraction} The originating command.
	 */
	runCommand(command, interaction) {
		switch (command) {
			case "link":
				console.log(`\t↪ Xmas link command`);
				this.#linkCommand(interaction).then();
				break;
			case "register":
				console.log(`\t↪ Xmas register command`);
				this.#registerPlayer(interaction).then();
				break;
		}
	}

	/**
	 * Give the link to the player.
	 * @param {Discord.CommandInteraction} interaction The command interaction created by the player.
	 * @returns {Promise<any>}
	 */
	#linkCommand(interaction) {
		if (!(interaction.guildId === process.env.PDDS_GUILD_ID || interaction.guildId === process.env.DEV_GUILD_ID))
			return false;

		return interaction.reply({
			embeds: [
				new MyEmbed("Lien pour te connecter", Utils.BLUE_COLOR)
					.desc(`Connecte toi ici : ${global.ngrokUrl} puis rends toi dans l'onglet jeux.`) //TODO: Change the tab name.
			],
			ephemeral: true
		}).catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));
	}

	/**
	 * Add the player to the player list.
	 * @param {Discord.CommandInteraction} interaction The command interaction created by the user.
	 * @returns {Promise<boolean>} true if the player was added, false otherwise
	 */
	async #registerPlayer(interaction) {
		if (!(interaction.guildId === process.env.PDDS_GUILD_ID || interaction.guildId === process.env.DEV_GUILD_ID))
			return false;

		const id = interaction.member.user.id;

		if (await global.database.playerExist(id)) {
			interaction.reply({
				embeds: [
					new MyEmbed("Déjà enregistré(e)", Utils.ORANGE_COLOR)
						.desc(`Tu es déjà enregistré(e) comme joueur. ` +
							`Regarde dans tes messages privés avec moi pour retrouver tes identifiants et rendez-vous ici : ${global.ngrokUrl} dans l'onglet jeux.`) //TODO: Change the tab name
				],
				ephemeral: true
			}).catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));

			if (!await global.database.userExist(id))
				Users.insertUser(id);

			return false;
		}

		if (await global.database.userExist(id)) {
			if (await global.database.insertPlayer(id)){
				interaction.reply({
					embeds: [
						new MyEmbed("Ajouté(e) en tant que joueur", Utils.BLUE_COLOR)
							.desc(`Tu comptais déjà parmi les utilisateurs du site web. ` +
								`Tu es désormais inscrit(e) en tant que joueur aussi. ` +
								`Connectes toi comme d'habitude sur le site et rends toi dans l'onglet jeux.`) //TODO: Change the tab name
					],
					ephemeral: true
				});
				return true;
			}

			interaction.reply({
				embeds: [
					new MyEmbed("Echec de l'ajout", Utils.RED_COLOR)
						.desc(`Je n'ai pas réussi à t'ajouter en tant que joueur.`)
				],
				ephemeral: true
			});
			return false;
		}

		if (await Users.insertUser(id)) {
			if (await global.database.insertPlayer(id)) {
				interaction.reply({
					embeds: [
						new MyEmbed("Ajouté(e) en tant que joueur", Utils.BLUE_COLOR)
							.desc(`Tu as été ajouté(e) en tant que joueur. ` +
								`Retrouve tes identifiants dans tes messages privés avec moi et rendez-vous ici : ${global.ngrokUrl} dans l'onglet jeux.`) //TODO: Change the tab name
					],
					ephemeral: true
				});
				return true;
			}

			interaction.reply({
				embeds: [
					new MyEmbed("Echec de l'ajout", Utils.RED_COLOR)
						.desc(`Tu as été ajouté(e) aux utilisateur du site. Mais je n'ai pas réussi à t'ajouter en tant que joueur.`)
				],
				ephemeral: true
			});
			return false;
		}

		interaction.reply({
			embeds: [
				new MyEmbed("Echec de l'ajout", Utils.RED_COLOR)
					.desc(`Je n'ai pas réussi à t'ajouter en tant que joueur.`)
			],
			ephemeral: true
		});

		return false;
	}

	/**
	 * Display to the players the story for a given week.
	 * @param week The week to display.
	 */
	#playIntroduction(week) {
		const channel = global.bot.guilds.cache.get(process.env.PDDS_GUILD_ID).channels.cache.get(process.env.EVENT_CHANNEL_ID);

		const introductionSentences = JSON.parse(fs.readFileSync(path.join(process.cwd, "modules", "xmas-event", "story", week)).toString())["introduction"];
		for (const introElem of introductionSentences) {
			channel.send({
				embeds: new EmbedBuilder()
					.setColor(BLUE_COLOR)
					.setDescription(introElem)
			});
		}
	}
}

module.exports = Xmas;
