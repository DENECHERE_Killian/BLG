/**
 * @author Xavier
 */

const emojiRegex = require("emoji-regex");
// noinspection JSUnusedLocalSymbols
const Discord    = require("discord.js"); // Use for doc
const MyEmbed    = require("../../utilities/MyEmbed");
const Utils      = require("../../utilities/Utils");

class Emoji {

	constructor() {
	}

	/**
	 * Launch a specific / command of the emoji module.
	 * @param command {string} The command name.
	 * @param interaction {Discord.CommandInteraction} The command interaction created by a user.
	 */
	runCommand(command, interaction) {
		switch (command) {
			case "add":
				console.log(`Add emojis command`);
				// noinspection JSIgnoredPromiseFromCall
				this.#addCommand(interaction);
				break;
			case "remove":
				console.log(`Remove emojis command`);
				// noinspection JSIgnoredPromiseFromCall
				this.#removeCommand(interaction);
				break;
			case "remove_all":
				console.log(`Remove all emojis command`);
				// noinspection JSIgnoredPromiseFromCall
				this.#removeAllCommand(interaction);
				break;
			default:
				console.warn(`Not recognized command : ${command}`);
				interaction.reply({ embeds: [new MyEmbed("Je ne connais pas cette commande 😕", Utils.RED_COLOR)], fetchReply: true })
					.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`))
					.then(Utils.deleteMessage);
		}
	}

	/**
	 * Add emojis to a message.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @returns {Promise<void>}
	 */
	async #addCommand(interaction) {
		const errorEmbed = await this.#messageCheck(interaction, interaction.options.get("link").value);

		if (errorEmbed)
			return interaction.reply(errorEmbed).catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`)).then(Utils.deleteMessage);

		const message   = await this.#fetchMessage(interaction.options.get("link").value);
		const emojiList = Emoji.#getEmojisFromString(interaction.options.get("emojis").value);

		if (emojiList.length === 0) {
			return interaction.reply({
				embeds: [new MyEmbed(`Pas d'emoji !`, Utils.ORANGE_COLOR)
					.desc("La chaîne de caractère devant contenir les emojis n'en contient pas une seule !")], ephemeral: true
			})
				.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));
		}

		const notAdded = [];

		await interaction.deferReply({ ephemeral: true });

		for (const emoji of emojiList) {
			try {
				await message.react(emoji);
			} catch (_) {
				notAdded.push(emoji);
			}
		}

		if (notAdded.length > 0) {
			return interaction.editReply({
				embeds: [new MyEmbed(`Trop d'emojis`, Utils.ORANGE_COLOR)
					.desc(`Je n'ai pas pu ajouter toutes les emojis. Je n'ai soit pas trouvé les emojis soit l'ajout à fait dépasser la limite de 20 emojis.`)]
			})
				.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));
		}


		return interaction.editReply({
			embeds: [new MyEmbed(`C'est fait !`, Utils.GREEN_COLOR)
				.desc("Les emojis ont été ajoutées ! 👍")]
		})
			.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));
	}

	/**
	 * Remove emojis from a message.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @returns {Promise<void>}
	 */
	async #removeCommand(interaction) {
		const errorEmbed = await this.#messageCheck(interaction, interaction.options.get("link").value);

		if (errorEmbed)
			return interaction.reply(errorEmbed).catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`)).then(Utils.deleteMessage);

		const message   = await this.#fetchMessage(interaction.options.get("link").value);
		const emojiList = Emoji.#getEmojisFromString(interaction.options.get("emojis").value);

		if (emojiList.length === 0) {
			return interaction.reply({
				embeds: [new MyEmbed(`Pas d'emoji !`, Utils.RED_COLOR)
					.desc("La chaîne de caractère devant contenir les emoji n'en contient pas une seule !")], ephemeral: true
			})
				.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));
		}

		await interaction.deferReply({ ephemeral: true });

		for (const emoji of emojiList) {
			const reaction = await message.reactions.resolve(emoji);
			if (reaction) {
				await reaction.users.remove(global.bot.id);
			}
		}

		return interaction.editReply({
			embeds: [new MyEmbed(`C'est fait !`, Utils.GREEN_COLOR)
				.desc("Toutes les emojis ont été retirées ! 👍")]
		})
			.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));
	}

	/**
	 * Remove all emojis from a message.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @returns {Promise<void>}
	 */
	async #removeAllCommand(interaction) {
		const errorEmbed = await this.#messageCheck(interaction, interaction.options.get("link").value);

		if (errorEmbed)
			return interaction.reply(errorEmbed).catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`)).then(Utils.deleteMessage);

		const message      = await this.#fetchMessage(interaction.options.get("link").value);
		const botReactions = message.reactions.cache.filter(reaction => reaction.client.id === global.bot.id);

		await interaction.deferReply({ ephemeral: true });

		for (const reaction of botReactions.values())
			await reaction.users.remove(global.bot.id);

		return interaction.editReply({
			embeds: [new MyEmbed(`C'est fait !`, Utils.GREEN_COLOR)
				.desc("Toutes les emojis ont été retirées ! 👍")]
		})
			.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));
	}

	/**
	 * Ensure that the message given by the user exists, is accessible by the bot and corresponds to a sender's message.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @param link {string} The link of the message.
	 * @returns {Promise<{embeds: MyEmbed[], fetchReply: boolean}>} The embed that contains the message error if the link is not correct else null.
	 */
	async #messageCheck(interaction, link) {
		const splitLink = interaction.options.get("link").value.split("/");

		if (splitLink.length !== 7 && link.startsWith("https://discord.com/channels/")) {
			return {
				embeds: [new MyEmbed("Lien incorrect", Utils.RED_COLOR)
					.desc("Vérifie que le lien que tu m'as fourni est bien celui d'un message !")], fetchReply: true, ephemeral: true
			};
		}

		const guildId   = splitLink[splitLink.length - 3];
		const channelId = splitLink[splitLink.length - 2];
		const messageId = splitLink[splitLink.length - 1];

		const guild = await global.bot.guilds.fetch(guildId).catch(_ => {
		});
		if (!guild) {
			return {
				embeds: [new MyEmbed("Serveur inaccessible", Utils.RED_COLOR)
					.desc("Je n'ai pas accès au serveur Discord dans lequel ce trouve le message auquel tu veux que je réagisse.")], fetchReply: true, ephemeral: true
			};
		}

		const channel = await global.bot.channels.fetch(channelId).catch(_ => {
		});
		if (!channel) {
			return {
				embeds: [new MyEmbed("Salon inaccessible", Utils.RED_COLOR)
					.desc("Je n'ai pas accès au salon dans lequel ce trouve le message auquel tu veux que je réagisse.")], fetchReply: true, ephemeral: true
			};
		}

		const message = await channel.messages.fetch(messageId).catch(_ => {
		});
		if (!message) {
			return {
				embeds: [new MyEmbed("Message introuvable", Utils.RED_COLOR)
					.desc("Je ne trouve pas le message que tu m'as envoyé.")], fetchReply: true, ephemeral: true
			};
		}

		if (message.author.id !== interaction.user.id) {
			return {
				embeds: [new MyEmbed("Tu n'es pas l'auteur", Utils.RED_COLOR)
					.desc("Le bot ne peux réagir qu'au message que tu as envoyé.")], fetchReply: true, ephemeral: true
			};
		}
	}

	/**
	 * Fetch the message with the given id in the channel with the given channel.
	 * @param {string} link The link of the message.
	 * @returns {Promise<Message>} The message.
	 */
	async #fetchMessage(link) {
		const splitLink = link.split("/");
		const channel   = await global.bot.channels.fetch(splitLink[splitLink.length - 2]);

		return channel.messages.fetch(splitLink[splitLink.length - 1]);
	}

	/**
	 * Collect all emojis from a string.
	 * @param {string} str The string to search in.
	 * @returns {String[]} A list of emojis. Each one contained in a string.
	 */
	static #getEmojisFromString(str) {
		const emojisFound = [];

		const customEmojiRegex  = /<:[^:\s]+:\d+>|<a:[^:\s]+:\d+>/g;
		const unicodeEmojiRegex = emojiRegex();
		const emR               = new RegExp(customEmojiRegex.source + "|" + unicodeEmojiRegex.source);

		let found;
		while (found = emR.exec(str)) {
			str = str.replace(found, "");
			emojisFound.push(found[0]);
		}

		return emojisFound;
	}
}

module.exports = Emoji;
