const RoleType = require("./RoleType");
const Discord  = require("discord.js");
const MyEmbed  = require("../../utilities/MyEmbed");

const BASE_CARDS_URL = "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles";

class Role {
	/**
	 * This class is abstract ad cannot be instantiated.
	 */

	  // NON VILLAGE FRIENDLY ROLES
	static WEREWOLF = 1;

	// VILLAGE FRIENDLY ROLES
	static VILLAGER   = 2;
	static YOUNG_GIRL = 3;
	static HUNTER     = 4;
	static CUPIDON    = 5;
	static WITCH      = 6;
	static PSYCHIC    = 7;
	static MEDIUM     = 8;

	// OBJECTS
	static WITCH_SAVING_POTION  = 1;
	static WITCH_KILLING_POTION = 2;

	/**
	 * Create a role Object that will be played by a player. A role is a game character that can be played.
	 * @param player {Player} The player related to this role instance.
	 * @param roleType {number} The role type of this role that can be {@link WEREWOLF}, {@link VILLAGER}, {@link YOUNG_GIRL}, {@link HUNTER}, {@link CUPIDON}, {@link WITCH}, {@link PSYCHIC} or {@link MEDIUM}.
	 */
	constructor(player, roleType) {
		this.player = player;
		this.game   = this.player.game;

		switch (roleType) {
			case Role.WEREWOLF:
				this.type = new RoleType(
					Role.WEREWOLF,
					"Loup-Garou",
					process.env.TEST === "true" ? 10 : 30,
					false,
					"Chaque nuit, choisie avec ses compagnons un joueur à **dévorer**. Le jour, il essai de **masquer son identité animal** pour échapper à la vindicte populaire. Ils sont 1 à 6 suivant le nombre de joueurs.",
					"En aucun cas un Loup-Garou ne peut dévorer un autre Loup-Garou.",
					`${BASE_CARDS_URL}/originales/Loup-garou.png`,
					`${BASE_CARDS_URL}/custom/Loup-garou.png`,
					this.#werewolfAction,
					null,
					[{ varName: "text_werewolf", name: "Loups", options: { type: "GUILD_TEXT", position: 2, topic: "Déterminez une cible à dévorer." } }]
				);
				break;

			case Role.VILLAGER:
				this.type = new RoleType(
					Role.VILLAGER,
					"Villageois",
					-1,
					true,
					"Ses seules armes sont sa **capacité d’analyse** des comportements pour identifier les Loups-Garous ainsi que sa **force de conviction** pour empêcher l’exécution de l’innocent qu’il est.",
					"C'est le rôle le plus nul.",
					`${BASE_CARDS_URL}/originales/Villageois.png`,
					`${BASE_CARDS_URL}/custom/Villageois.png`,
					null,
					null,
					[]
				);
				break;

			case Role.YOUNG_GIRL:
				this.type = new RoleType(
					Role.YOUNG_GIRL,
					"Petite-Fille",
					Role.WEREWOLF.turnDuration,
					true,
					"Chaque nuit, la Petite-Fille peut tenter d'espionner les Loups-Garous **pendant qu'ils choisissent leur victime**.",
					"Attention à ne pas se faire repérer par les loups !",
					`${BASE_CARDS_URL}/originales/Petite-fille.png`,
					`${BASE_CARDS_URL}/custom/Petite-fille.png`,
					this.#youngGirlAction,
					null,
					[{ varName: "text_littleGirl", name: "Petite-Fille", options: { type: "GUILD_TEXT", position: 3, topic: "Espionnez les discussions des loups discrètement." }, playersPerms: { deny: Discord.Permissions.SEND_MESSAGES } }]
				);
				break;

			case Role.HUNTER:
				this.type = new RoleType(
					Role.HUNTER,
					"Chasseur",
					process.env.TEST === "true" ? 10 : 30,
					true,
					"S’il se fait dévorer par les Loups-Garous ou exécuter malencontreusement par le village, le Chasseur **doit répliquer** avant de rendre l’âme en éliminant immédiatement un joueur de son choix.",
					"Il a un fusil :gun:.",
					`${BASE_CARDS_URL}/originales/Chasseur.png`,
					`${BASE_CARDS_URL}/custom/Chasseur.png`,
					null,
					this.#hunterKilledAction
				);
				break;

			case Role.CUPIDON:
				this.type = new RoleType(
					Role.CUPIDON,
					"Cupidon",
					30,
					true,
					"En décochant ses célèbres flèches magiques, Cupidon a le pouvoir de rendre **2 personnes amoureuses à jamais**.\n" +
					"**Au premier tour**\nIl **désigne** les 2 joueurs amoureux (lui y compris, s'il le souhaite).\n\n" +
					"**Au cours de la partie**\nSi l’un des amoureux est éliminé, **l’autre meurt** immédiatement de chagrin. " +
					"En tant que couple, aucun d'entre-eux ne doit porter préjudice à son bien aimé, ni voter contre lui **(même pour faire semblant !)**.",
					"Si les 2 amoureux ne sont pas dans le même camp (l'un pour le villae et l'autre pour les loups), le but de la partie change pour eux!\n" +
					"Pour vivre en paix leur amour, ils doivent éliminer tous les autres joueurs (Loups-Garous et Villageois) pour gagner la partie.",
					`${BASE_CARDS_URL}/originales/Cupidon.png`,
					`${BASE_CARDS_URL}/custom/Cupidon.png`,
					this.#cupidonAction,
					null,
					[{ varName: "text_cupidon", name: "Cupidon", options: { type: "GUILD_TEXT", position: 5, topic: "Liez 2 villageois par l'amour." }, playersPerms: { deny: Discord.Permissions.SEND_MESSAGES } }]
				);
				break;

			case Role.WITCH:
				this.potions = [Role.WITCH_SAVING_POTION, Role.WITCH_KILLING_POTION]; // The potion order is important for thi role action

				this.type = new RoleType(
					Role.WITCH,
					"Sorcière",
					30,
					true,
					"Elle sait concocter 2 potions extrêmement puissantes à usage unique. Chaque nuit, elle peut en choisir une :\n" +
					"• **Guérison**, pour ressusciter le joueur venant d'être tué par les Loups-Garous.\n" +
					"• **Empoisonnement**, pour éliminer un joueur.Elle est limitée à l'utilisation d'une seule potion par nuit.",
					"La Sorcière peut utiliser les potions à son profit, et donc se guérir ou se suicider.",
					`${BASE_CARDS_URL}/originales/Sorciere.png`,
					`${BASE_CARDS_URL}/custom/Sorciere.png`,
					this.#witchAction,
					null,
					[{ varName: "text_witch", name: "Sorcière", options: { type: "GUILD_TEXT", position: 7, topic: "Usez de vos potios pour sauver ou tuer un villageois." }, playersPerms: { deny: Discord.Permissions.SEND_MESSAGES } }]
				);
				break;

			case Role.PSYCHIC:
				this.type = new RoleType(
					Role.PSYCHIC,
					"Voyante",
					process.env.TEST === "true" ? 20 : 30,
					true,
					"Chaque nuit, **elle découvre la vraie personnalité** d’un joueur de son choix. Elle doit aider les autres Villageois, mais rester discrète pour ne pas être démasquée par les Loups-Garous.",
					"Elle aime les licornes :unicorn:.",
					`${BASE_CARDS_URL}/originales/Voyante.png`,
					`${BASE_CARDS_URL}/custom/Voyante.png`,
					this.#psychicAction,
					null,
					[{ varName: "text_psychic", name: "Voyante", options: { type: "GUILD_TEXT", position: 6, topic: "Découvrez les rôles de chacun." }, playersPerms: { deny: Discord.Permissions.SEND_MESSAGES } }]
				);
				break;

			case Role.MEDIUM:
				this.type = new RoleType(
					Role.MEDIUM,
					"Médium",
					30,
					true,
					"Le Médium peut **entendre les morts**. Chaque nuit, les joueurs décédés pourront lui faire part de leur souvenirs.\n Les morts ne doivent pas dévoiler leur identitée mais peuvent désigner un ou plusieurs villageois douteu.",
					"Une petite partie de ouija ?",
					`${BASE_CARDS_URL}/originales/Medium.png`,
					`${BASE_CARDS_URL}/custom/Medium.png`,
					this.#mediumAction,
					null,
					[{ varName: "text_medium", name: "Médium", options: { type: "GUILD_TEXT", position: 4, topic: "Récoltez les dernières volontées des fantômes." }, playersPerms: { deny: Discord.Permissions.SEND_MESSAGES } }]
				);
				break;

			default:
				console.log(`The role ${this.type} was not recognized.`);
		}
	}

	/**
	 * Provide and embed Message showing this role's specificities and objectives.
	 * @returns {MyEmbed} The EmbedMessage detailing this role objectives.
	 */
	toEmbed() {
		return new MyEmbed(`Fiche du rôle "${this.type.name}"`, Utils.BLUE_COLOR)
			.desc(this.type.description)
			.setThumbnail(this.type.customCardURL)
			.addField("Spécificité", this.type.specificity)
			.setFooter({ text: "Carte écrite avec amour", iconURL: "https://whatemoji.org/wp-content/uploads/2020/07/Red-Heart-Emoji.png" });
	}

	/**
	 * Let this role play its turn. The little-girl's one is at the same time of the werewolves, so it cannot be launched lonely.
	 * @param {*} [argument=null] An argument to pass to the normal role action function.
	 * @returns {Promise<void>}
	 */
	async play(argument = null) {
		if (this.type.normalAction) {
			console.log(`Launching ${this.type.name}'s normal action.`);
			await this.type.normalAction(argument);
		}
	}

	/**
	 * Launch the killed action of this role type if there is one registered.
	 * @returns {Promise<void>}
	 */
	async kill() {
		if (this.type.killedAction) {
			console.log(`Starting ${this.type.name}'s killed action.`);
			await this.type.killedAction();
		}
	}

	// -------------
	// ROLES ACTIONS
	// -------------

	/**
	 * Perform the Werewolves role night action.
	 * @returns {Promise<void>}
	 */
	#werewolfAction() {
		return new Promise(async (resolve) => {
			const youngGirl = this.game.getOneRoleTypePlayer(Role.YOUNG_GIRL);
			if (youngGirl)
				youngGirl.play();

			await new MyEmbed("Les Loups-Garous se réveillent et discutent pour désigner une victime").send(this.game.channels.text_village);
			const werewolvesPlayers = this.game.getRoleTypePlayers(Role.WEREWOLF);
			const selectablePlayers = removeItems(this.game.getAlivePlayers(), werewolvesPlayers);
			const werewolvesChoices = {};
			werewolvesPlayers.forEach(p => werewolvesChoices[p.user.id] = { victimName: "", victimId: null, finalChoice: false });

			// Writing enabled
			for (const player of werewolvesPlayers)
				await this.game.channels.text_werewolf.permissionOverwrites.edit(player.guildMember, { SEND_MESSAGES: true });

			const message = new MyEmbed("À vous de jouer !", Utils.BLUE_COLOR).desc(werewolvesPlayers.map(player => `${player.name} propose : personne`).toString().replaceAll(",", "\n"));
			// noinspection JSUnusedAssignment, JSCheckFunctionSignatures
			const poll    = await this.game.channels.text_werewolf.send({
				embeds: [message],
				components: [new Discord.MessageActionRow().addComponents([
					new Discord.MessageSelectMenu({
						customId: "select_player",
						placeholder: "Choisi un joueur",
						maxValues: 1,
						options: selectablePlayers.map(p => p = { label: p.name, value: p.user.id })
					})
				]), new Discord.MessageActionRow().addComponents([
					new Discord.MessageButton({
						customId: "validate_button",
						label: "C'est mon dernier choix",
						style: "PRIMARY"
					})
				])]
			});

			MyEmbed.showTimer(poll, "Vous disposez de", this.type.turnDuration);
			const collector = poll.createMessageComponentCollector({ time: this.type.turnDuration * 1000 });

			collector.on("collect", i => {
				if (i.isSelectMenu()) {
					// noinspection JSUnresolvedVariable
					werewolvesChoices[i.user.id] = { victimName: this.game.getPlayer(i.values[0]).name, victimId: i.values[0], finalChoice: false };
					poll.edit({ embeds: [new MyEmbed("À vous de jouer !", Utils.BLUE_COLOR).desc(werewolvesPlayers.map(player => `${player.name} ${werewolvesChoices[player.user.id].finalChoice ? "a choisi" : "propose"} : ${werewolvesChoices[player.user.id].victimName}`).toString().replaceAll(",", "\n"))] });
				}

				if (i.isButton() && werewolvesChoices[i.user.id].victimId && !werewolvesChoices[i.user.id].finalChoice) {
					werewolvesChoices[i.user.id].finalChoice = true;
					poll.edit({ embeds: [new MyEmbed("À vous de jouer !", Utils.BLUE_COLOR).desc(werewolvesPlayers.map(player => `${player.name} ${werewolvesChoices[player.user.id].finalChoice ? "a choisi" : "propose"} : ${werewolvesChoices[player.user.id].victimName}`).toString().replaceAll(",", "\n"))] });

					if (werewolvesChoices.every(c => c.finalChoice))
						collector.stop("final_choice");
				}
			});

			collector.on("end", async () => {
				let victims = {}; // Each victim id is assigned to its vote amount
				werewolvesChoices.forEach(choice => {
					if (choice.finalChoice && choice.victimId) // Each vote that have been validated
						victims[choice.victimId] ? victims[choice.victimId]++ : victims[choice.victimId] = 1;
				});

				let victim;
				if (victims.length === 0) {
					// Nobody voted, need to randomly choose one.
					victim = randomItem(selectablePlayers);
				} else {
					const maxVoteAmount    = Math.max(...Object.values(victims));
					const mostVotedVictims = Object.entries(victims).filter(v => v[1] === maxVoteAmount);

					// A victim was mainly chosen if length is 1.
					// Otherwise, multiples victims were selected and a random choice is performed.
					victim = mostVotedVictims.length === 1 ? this.game.getPlayer(mostVotedVictims[0][0]) : randomItem(mostVotedVictims);
				}

				victim.isNightVictim = true;
				await poll.edit({ embeds: [new MyEmbed("Résultat du vote", Utils.GREEN_COLOR).desc(`Le joueur que vous allez dévorer est **${victim.name}**`)], components: [] });
				await new MyEmbed("Les Loups-Garous ont finit de débattre, ils se rendorment", Utils.BLUE_COLOR).send(this.game.channels.text_village);
				resolve();
			});
		});
	}

	async #youngGirlAction() {
		// TODO youngGirlAction
		if (!this.game.roleTypeCanPlay(Role.YOUNG_GIRL))
			return null;

		const message = await new MyEmbed("Les loups sont en pleine réflexion !", Utils.BLUE_COLOR).desc(`Tu peux tenter de les observer discrètement afin de les identifier.\nAttention, ils pourront peut-être te voir !`).send(this.game.channels.text_littleGirl, [
			new Discord.MessageButton().setCustomId("watch_button").setLabel(`J'ouvre les yeux`).setEmoji(`👀`).setStyle("PRIMARY")
		]);
		MyEmbed.showTimer(message, "Fin de la possibilité dans", this.type.turnDuration);

		const filter    = (interaction) => interaction.user.id === this.player.user.id;
		const collector = message.createMessageComponentCollector({ filter, time: this.type.turnDuration * 1000, max: 1 });

		collector.on("end", async (collected, reason) => {
			if (reason === "time") {
				console.log(`The young girl didn't open his eyes`);
				await message.edit({ embeds: [new MyEmbed(`Le temps est écoulé`, Utils.RED_COLOR)], components: [] });
				return null;
			}

			// The reason is an interaction collected
			await message.edit({ embeds: [new MyEmbed(`Tu ouvre les yeux discrètement`, Utils.BLUE_COLOR).desc(`Tu perçois les messages échangés les loups.`)], components: [] });

			if (Math.random() <= 0.25) {
				// The little girl has been viewed by the wolves
				console.log(`The young girl open his eyes and is detected by wolves.`);
				await new MyEmbed("Vous sentez qu'on vous écoute", Utils.WHITE_COLOR).desc(`Vous repérez la petite-fille mais ne vous souvenez plus de son nom.\nD'après vos souvenirs, **la lettre "${this.player.name[Math.floor(Math.random() * this.player.name.length)]}"** figure dans son nom.`).send(this.game.channels.text_werewolf);
			} else {
				console.log(`The young girl open his eyes.`);
			}

			const messageFilter = m => !m.author.bot;
			// const messageCollector = this.game.channels.text_werewolf.createMessageCollector({ messageFilter, time: this.type.turnDuration - tempsécouléEntreDébutCollecteurEtRéponse});
		});
	}

	/**
	 * Perform the Hunter once killed action.
	 * @returns {Promise<void>}
	 */
	#hunterKilledAction() {
		return new Promise(async (resolve) => {

			const messageContent = new MyEmbed("Dans son dernier souffle de vie, le chasseur tente d'utiliser son fusil");
			// noinspection JSUnusedAssignment
			const message        = await messageContent.send(this.game.channels.text_village[
				new Discord.MessageSelectMenu({
					customId: "select_target",
					placeholder: "Choisi une cible",
					maxValues: 1,
					options: this.game.getAlivePlayers().map(p => p = { label: p.name, value: p.user.id })
				})
				]);
			MyEmbed.showTimer(message, "Perte de conscience dans", this.type.turnDuration);

			const filter    = (interaction) => interaction.user.id === this.player.user.id;
			const collector = message.createMessageComponentCollector({ filter, time: this.type.turnDuration * 1000, max: 1 });

			collector.on("end", async (collected, reason) => {
				if (reason === "time") {
					await message.edit({ embeds: [messageContent.col(Utils.BLUE_COLOR).desc("Malheureusement, il décèdera sans en avoir fait usage").col(Utils.RED_COLOR)], components: [] });
					console.log(`The hunter didn't killed anyone`);
				} else {
					// noinspection JSUnresolvedFunction
					const target = this.game.getPlayer(collected.toJSON()[0].values[0]);
					console.log(`The hunter killed ${target.name}`);
					await message.edit({ embeds: [messageContent.col(Utils.BLUE_COLOR).desc(`Il tire ainsi sur ${target.name} qui meurt à son tour`).col(Utils.GREEN_COLOR)], components: [] });
					await target.kill();
				}

				resolve();
			});
		});
	}

	/**
	 * Perform the Cupidon first night action.
	 * @returns {Promise<void>}
	 */
	#cupidonAction() {
		return new Promise(async (resolve) => {
			await new MyEmbed("Cupidon se réveille et choisi deux joueurs ayant des atomes crochus").send(this.game.channels.text_village);

			const messageContent = new MyEmbed("Choisi un couple sur lequel décocher tes flèches charnelles.", Utils.WHITE_COLOR);
			// noinspection JSUnusedAssignment
			const message        = await messageContent.send(this.game.channels.text_cupidon, [
				new Discord.MessageSelectMenu({
					customId: "select_lovers",
					placeholder: "Choisi un couple",
					minValues: 2,
					maxValues: 2,
					options: this.game.getAlivePlayers().map(p => p = { label: p.name, value: p.user.id })
				})
			]);
			MyEmbed.showTimer(message, "Les flèches partiront dans", this.type.turnDuration);

			const filter    = (interaction) => interaction.user.id === this.player.user.id;
			const collector = message.createMessageComponentCollector({ filter, time: this.type.turnDuration * 1000, max: 1 });

			collector.on("end", async (collected, reason) => {
				let loverOne, loverTwo;
				if (reason === "time") {
					loverOne = randomItem(this.game.getAlivePlayers());
					loverTwo = randomItem(removeItems(this.game.getAlivePlayers(), loverOne));
					console.log(`Cupidon randomly choose players "${loverOne.name}" & "${loverTwo.name}" as lovers`);
					await message.edit({ embeds: [messageContent.col(Utils.ORANGE_COLOR).desc(`Le choix s'est porté au hasard sur :\n${loverOne.name} & ${loverTwo.name}`)], components: [] });
				} else {
					// noinspection JSUnresolvedFunction
					[loverOne, loverTwo] = [this.game.getPlayer(collected.toJSON()[0].values[0]), this.game.getPlayer(collected.toJSON()[0].values[1])];
					console.log(`Cupidon choose players "${loverOne.name}" & "${loverTwo.name}" as lovers`);
					await message.edit({ embeds: [messageContent.col(Utils.GREEN_COLOR).desc(`Tu a choisi :\n${loverOne.name} & ${loverTwo.name}`)], components: [] });
				}

				[loverOne, loverTwo].forEach(p => p.isLover = true);
				await new MyEmbed("Cupidon se rendort").send(this.game.channels.text_village);
				resolve();
			});
		});
	}

	/**
	 * Perform the which night action.
	 * @returns {Promise<void>}
	 */
	#witchAction() {
		return new Promise(async resolve => {
			if (this.potions.length === 0) {
				// No more potion to use
				console.log(`The witch doesn't have any more potion`);
				return resolve();
			}

			await new MyEmbed("La sorcière se réveille et décide d'utiliser ou non l'une de ses potions").send(this.game.channels.text_village);

			const werewolfNightVictim = this.game.getNightVictimsPlayers().pop(); // The werewolf's victim.
			const canUseSavingPotion  = this.game.getNightVictimsPlayers().length > 0 && this.potions.contains(Role.WITCH_SAVING_POTION);
			const canUseKillingPotion = this.potions.contains(Role.WITCH_KILLING_POTION);
			const messageContent      = new MyEmbed("Veux-tu utiliser une de tes potions ?", Utils.WHITE_COLOR).desc(`La __potion de soin__ te permet de sauver la victime choisie par les loups cette nuit : **${werewolfNightVictim ? werewolfNightVictim.name : "Personne"}**.\nLa __potion de poison__ te permet de tuer un villageois de ton choix.`);
			// noinspection JSUnusedAssignment
			const message             = await messageContent.send(this.game.channels.text_witch, [
				new Discord.MessageButton().setCustomId("heal_button").setLabel("Potion de soin").setStyle("PRIMARY").setEmoji("🩹").setDisabled(!canUseSavingPotion),
				new Discord.MessageButton().setCustomId("kill_button").setLabel("Potion de poison").setStyle("PRIMARY").setEmoji("☠️").setDisabled(!canUseKillingPotion),
				new Discord.MessageButton().setCustomId("abort_button").setLabel("Aucune potion").setStyle("SECONDARY").setEmoji(`❌`)
			]);
			MyEmbed.showTimer(message, "Fin du choix dans", this.type.turnDuration);

			const filter    = (interaction) => interaction.user.id === this.player.user.id;
			const collector = message.createMessageComponentCollector({ filter, time: this.type.turnDuration * 1000 });

			collector.on("collect", async (i) => {
				if (i.isButton()) {
					if (i.customId === "heal_button") {
						this.potions.shift();
						werewolfNightVictim.isNightVictim = false;

						console.log(`The which decided to save "${werewolfNightVictim.name}"`);
						await message.edit({ embeds: [messageContent.col(Utils.RED_COLOR).desc(`Tu a choisi d'utiliser ta potion de soin pour sauver ${werewolfNightVictim.name}.`)], components: [] });
						return collector.stop("heal");
					}

					if (i.customId === "kill_button") {

						// noinspection JSCheckFunctionSignatures,JSUnusedAssignment
						await message.edit({
							embeds: [messageContent.col(Utils.RED_COLOR).desc(`Sur qui va-tu utiliser ton poison ?`)], components: [
								new Discord.MessageActionRow().addComponents([
									new Discord.MessageSelectMenu({
										customId: "select_villager",
										placeholder: "Choisi un villageois",
										minValues: 1,
										maxValues: 1,
										options: this.game.getAlivePlayers().map(p => p = { label: p.name, value: p.user.id })
									})
								])
							]
						});
						return null;
					}

					// Abort button
					collector.stop("abort");
				}

				// The interaction is a selectMenu that correspond to the killed choice with potion
				// noinspection JSUnresolvedVariable
				const villagerToKill         = this.game.getPlayer(i.values[0]);
				villagerToKill.isNightVictim = true;
				this.potions.pop();

				console.log(`The witch decided to kill "${villagerToKill.name}"`);
				await message.edit({ embeds: [messageContent.col(Utils.RED_COLOR).desc(`Tu a utilisé ton poison sur "${villagerToKill.name}".`)], components: [] });
				return collector.stop("kill");
			});

			collector.on("end", async (collected, reason) => {

				if (reason === "time") {
					console.log(`Witch didn't had time to use any potion`);
					await message.edit({ embeds: [messageContent.col(Utils.RED_COLOR).desc(`Le temps est écoulé, tu pourra effectuer à nouveau ce choix la nuit prochaine.`)], components: [] });
				}

				await new MyEmbed("La sorcière se rendort").send(this.game.channels.text_village);
				resolve();
			});
		});
	}

	/**
	 * Perform the Psychic night action.
	 * @returns {Promise<void>}
	 */
	#psychicAction() {
		return new Promise(async (resolve) => {
			await new MyEmbed("La voyante se réveille et désigne un villageois dont elle va sonder la vraie identité").send(this.game.channels.text_village);

			const messageContent = new MyEmbed("Choisi un villageois pour connaitre son vrai rôle.", Utils.WHITE_COLOR);
			// noinspection JSUnusedAssignment
			const message        = await messageContent.send(this.game.channels.text_psychic, [
				new Discord.MessageSelectMenu({
					customId: "select_villager",
					placeholder: "Choisi un villageois",
					minValues: 2,
					maxValues: 2,
					options: removeItems(this.game.getAlivePlayers(), [this.player]).map(p => p = { label: p.name, value: p.user.id })
				})
			]);
			MyEmbed.showTimer(message, "Fin du choix dans", this.type.turnDuration);

			const filter    = (interaction) => interaction.user.id === this.player.user.id;
			const collector = message.createMessageComponentCollector({ filter, time: this.type.turnDuration * 1000, max: 1 });

			collector.on("end", async (collected, reason) => {

				if (reason === "time") {
					console.log(`Psychic didn't looked anybody's role`);
					await message.edit({ embeds: [messageContent.col(Utils.RED_COLOR).desc(`Tu n'a pas choisi de villageois à temps : retente le coup à la prochaine nuit.`)], components: [] });
				} else {
					// noinspection JSUnresolvedFunction
					const villager = this.game.getPlayer(collected.toJSON()[0].values[0]);
					console.log(`Psychic looked "${villager}"'s role`);
					await message.edit({ embeds: [messageContent.col(Utils.GREEN_COLOR).desc(`Tu découvre la vraie personnalité de **${villager.name}** : il/elle est **${villager.role.name}**`)], components: [] });
				}

				await new MyEmbed("La voyante se rendort").send(this.game.channels.text_village);
				resolve();
			});
		});
	}

	/**
	 * Perform the Medium night action.
	 * @returns {Promise<void>}
	 */
	#mediumAction() {
		return new Promise(async resolve => {
			if (this.game.getNonAlivePlayers().length === 0) {
				console.log(`There are not already dead players to communicate with`);
				return resolve();
			}

			console.log(`Start of Médium communication`);
			await new MyEmbed("Le médium entre en communion avec les esprits des défunts").send(this.game.channels.text_village);

			// Medium info message
			const MediumMessage = await new MyEmbed("Tu peux entendre les esprits se rapprocher", Utils.BLUE_COLOR).desc("Tu n'entends que ceux qui osent te parler").send(this.game.channels.text_medium);
			MyEmbed.showTimer(MediumMessage, "Fin de la communion dans", this.type.turnDuration);

			// Ghosts writing enabled
			for (const player of this.game.getNonAlivePlayers())
				await this.game.channels.text_ghostsMedium.permissionOverwrites.edit(player.guildMember, { SEND_MESSAGES: true });

			// Ghosts info message
			const ghostsMessage = await new MyEmbed("Le médium est prêt à vous écouter", Utils.BLUE_COLOR).desc("Il vous entend mais ne peut ni répondre, ni vous identifier.").send(this.game.channels.text_ghostsMedium);
			MyEmbed.showTimer(ghostsMessage, "Fin de la communion dans", this.type.turnDuration);

			// End of turn callback
			setTimeout(async () => {
				// Ghosts writing disabled
				for (const player of this.game.getNonAlivePlayers())
					await this.game.channels.text_ghostsMedium.permissionOverwrites.edit(player.guildMember, { SEND_MESSAGES: false });

				// Medium info message
				await new MyEmbed("Tu es épuisé, le lien est rompu", Utils.ORANGE_COLOR).send(this.game.channels.text_medium);

				// Ghosts info message
				await new MyEmbed("Le médium est épuisé, le lien est rompu", Utils.ORANGE_COLOR).send(this.game.channels.text_ghostsMedium);

				console.log(`End of Médium communication`);
				resolve();
			}, this.type.turnDuration * 1000);
		});
	}
}

module.exports = Role;