/**
 * @file Les fonctions concernant la génération du fichier récapitulatif pdf de fin de partie
 * @module loup_garou/pdf.js
 * @author Killian
 */


//==============================================================================
// Les logs (fin de partie)
//==============================================================================


/**
 * Génére un pdf récapitulatif des différents évenements de la partie pour l'envoyer ensuite au joueurs
 *
 * @param {Number} guildId L'identifiant du serveur courant
 * @return {Promise}
 */
function genererPDFLogs(guildId) {
	return new Promise(function (resolve, reject) {
		const datas = obtenirDatasCourante(guildId);
		bot.fs.writeFile(generationLogs + "_evenements-" + guildId + ".md", datas.LOUP.resume.texteLogs, async function (err) {
			if (err) console.log("generation evenements logs erreur : " + err);

			// finNouvellePage(datas); // Pour les logs -> Fermer la page de titre
			await pageJoueurs(guildId); // Pour les logs -> génération de la page des joueurs

			bot.markdownpdf({ paperBorder: "1cm", cssPath: "./modules/werewolf/logs/style.css", remarkable: { html: true } }).concat.from(["./modules/werewolf/logs/exemple/resumePartie_intro.md", generationLogs + "_participants-" + guildId + ".md", generationLogs + "_stats-" + guildId + ".md", generationLogs + "_evenements-" + guildId + ".md"]).to(generationLogs + "-" + guildId + ".pdf", function () {
				resolve();
			});
		});
	});
}

/**
 * Permet d'écrire sur une nouvelle page (ne pas oublier d'appeler finNouvellePage() lors de la fin de l'utilisation)
 *
 * @param {Map}  datas Les données du serveur courant
 * @param {type} [titre=null]     Le titre de la page
 * @param {type} [sousTitre=null] Un sous-titre pour la page
 * @return {Void} Modifie simplement la page des logs en cours de génération
 */
function nouvellePage(datas, titre = null, sousTitre = null) {
	datas.LOUP.resume.texteLogs += "<section>";
	if (titre) {
		datas.LOUP.resume.texteLogs += "<center>\n\n# " + titre + "\n";
		if (sousTitre) datas.LOUP.resume.texteLogs += sousTitre + "\n";
		datas.LOUP.resume.texteLogs += "</center>\n";
	}
	enregistrerModifsDatas(datas);
}

/**
 * Ferme la page courante dans laquelle nous écrivons
 *
 * @param {JsonObject} datas Les données du serveur courant
 * @return {Void} Modifie simplement la page des logs en cours de génération
 */
function finNouvellePage(datas) {
	datas.LOUP.resume.texteLogs += "</section>\n";
	enregistrerModifsDatas(datas);
}

/**
 * Ajoute un événement non linéaire au logs avec les paramètres
 * Un événement non linéaire est un événement qui se déroule en même temps qu'un autre (par exemple la petite fille qui écoute les loups pendant qu'ils délibèrent)
 *
 * @param {JsonObject} datas Les données du serveur courant
 * @param {List<List<String, String>>} liste Un liste avec deux listes de deux éléments (image, titre). 2 sous listes car il y a deux événements en même temps donc ce sont les infos de chaque événement
 * @return {Void} Modifie juste la page des logs en cours de génération
 */
function evenementNonLineaire(datas, liste) {
	datas.LOUP.resume.texteLogs += "<ul class='evenement-non-lineaire border-left'>\n";
	for (const [image, message] of liste) {
		datas.LOUP.resume.texteLogs += "<div>\n";
		espace(datas);
		datas.LOUP.resume.texteLogs += "<div>\n<img class='icons-carre' alt='icone information' src='./modules/werewolf/logs/icons/" + image + "'/>\n" + message + "\n</p>\n</div>\n";
		espace(datas);
		datas.LOUP.resume.texteLogs += "</div>\n";
	}
	datas.LOUP.resume.texteLogs += "</ul>\n";
	enregistrerModifsDatas(datas);
}

/**
 * Ajoute un événement standard aux logs à partir des paramètres
 * À utiliser pour les évenements importants (comme les votes, jour, nuit etc...)
 *
 * @param {JsonObject} datas Les données du serveur courant
 * @param {String} [image="information.svg"]    Le liens vers l'icone illustrant l'événement que l'on créé
 * @param {String} [titre="Annonce sans titre"] Un titre pour l'événement
 * @param {String} [style=""] Les classes pour ajouter du style à l'événement (voir l'exemple des logs)
 * @param {String} [explication=null] Un texte explicatif de ce qu'il s'est passé lors de cet événement
 * @return {Void} Modifie juste la page des logs en cours de génération
 */
function evenement(datas, image = "information.svg", titre = "Annonce sans titre", style = "", explication = null) {
	datas.LOUP.resume.texteLogs += "<div class='evenement " + style + "'>\n<p>\n<img class='icons' alt='icone information' src='./modules/werewolf/logs/icons/" + image + "'/>\n" + titre + "\n</p>\n";
	if (explication) datas.LOUP.resume.texteLogs += "<p>" + explication + "</p>";
	datas.LOUP.resume.texteLogs += "</div>\n";
	enregistrerModifsDatas(datas);
}

/**
 * Ajoute un événement (avec affichage plus discret) aux logs à partir des paramètres
 * À utiliser pour les évenements peu important comme les actions des divers roles
 *
 * @param {JsonObject} datas Les données du serveur courant
 * @param {String} [image="information.svg"]    Le lien vers l'icone illustrant l'événement
 * @param {String} [titre="Annonce sans titre"] Le titre de l'événement
 * @param {String} [style"] Les classes à rajouter pour modifier le style donné de l'événement (voir fichier exemple des logs)
 * @return {Void} Modifie simplementLa page des logs en cours de génération
 */
function evenementBulle(datas, image = "information.svg", titre = "Annonce sans titre", style = "") {
	datas.LOUP.resume.texteLogs += "<div class='bulle-colle'>\n<p>\n<img class='icons-bulle " + style + "' alt='icone information' src='./modules/werewolf/logs/icons/" + image + "'/>\n" + titre + "\n</p>\n</div>\n\n";
	enregistrerModifsDatas(datas);
}

/**
 * Ajoute un séparateur pour les événements (Le trait gris vertical reliant deux événements)
 *
 * @param {JsonObject} datas Les données du serveur courant
 * @return {Void} Modifie simplement l'apparence finale de la page des logs en cours de génération
 */
function separateur(datas) {
	datas.LOUP.resume.texteLogs += "\n<div class='evenement-sep'></div>\n\n";
	enregistrerModifsDatas(datas);
}

/**
 * Ajouter un espace vertical de x cm (0.5, 1 ou 2)
 * Pour 0.5, il faut appeler la fonction avec "05" comme paramètre
 *
 * @param {JsonObject} datas Les données du serveur courant
 * @param {String | Number} [taille="05"] La taille de l'espace voulu
 * @return {Void} Modifie simplement l'apparence finale de la page des logs en cours de génération
 */
function espace(datas, taille = "05") {
	datas.LOUP.resume.texteLogs += "\n<div class='space" + taille + "'></div>\n";
	enregistrerModifsDatas(datas);
}

/**
 * Génére la page avec la liste des joueurs et de leurs roles
 *
 * @param {Number} guildId L'identifiant du serveur courant
 * @return {Promise}
 */
function pageJoueurs(guildId) {
	return new Promise(function (resolve, reject) {
		const datas = obtenirDatasCourante(guildId);
		let texte   = "";
		texte += "<section>\n<center>\n\n# Participants et rôles\nAssociation des rôles aux joueurs\n</center>\n\n<div class='bords max tr'>\n\n| Joueur | Rôle principal | Mort ? | Durée de vie | Rôle(s) secondaire |\n |:---:|:---|:---|:---|:---|\n";

		for (const [user, liste] of datas.LOUP.resume.copieLesJoueurs) {
			let estMort = "oui.png";
			let temps;
			if (datas.LOUP.lesJoueurs.get(user)) {
				estMort = "non.png";
				temps   = datas.LOUP.resume.valeurTimerGlobal;
			} else {
				temps = datas.LOUP.lesMorts.get(user)[3];
			}

			const estCapi     = (datas.LOUP.roles.capitaine && datas.LOUP.roles.capitaine == user); // La personne est capi
			const estAmoureux = (datas.LOUP.roles.lesAmoureux.length > 0 && datas.LOUP.roles.lesAmoureux.includes(user)); // La personne fait partie des personnes désignées par Cupidon
			texte += "| " + user.username + " | " + liste[0] + " | <img class='icons-bilan-tableau' alt='icone information' src='./modules/werewolf/logs/icons/" + estMort + "'/> | " + Math.floor(temps / 60) + " min " + temps % 60 + " s ";

			if (estCapi && estAmoureux) { // La personne est capi et amoureuse
				texte += "|Capitaine & Amoureu |\n";
			} else {
				if (estCapi) { // La personne est capi
					texte += "|Capitaine |\n";
				} else if (estAmoureux) { // La personne est Amoureuse
					texte += "|Amoureu |\n";
				} else { // La personne n'a pas de role secondaire
					texte += "| |\n";
				}
			}
		}
		texte += "\n</div>\n\n</section>";

		bot.fs.writeFile(generationLogs + "_participants-" + guildId + ".md", texte, function (err) { // Génération du fichier avec la liste des joueurs
			if (err) console.log("Géneration page participants logs erreur : " + err);
			resolve();
		});
	});
}

/**
 * Génére la page de statistiques des logs
 *
 * @param {String} gagnants Les gagnats de la partie de Loup-Garou
 * @param {JsonObject} datas Les données du serveur courant
 * @return {Promise}
 */
function pageStats(gagnants, datas) {
	return new Promise(function (resolve) {
		let texte = "";

		texte += "<section>\n<center>\n\n# Merci d'avoir joué</center>\n";
		texte += "<div class='max'>\n\n|  |  |  |  |\n|:-------:|:-------:|:-------:|:-------:|\n|<img class='icons-bilan' alt='icone information' src='./modules/werewolf/logs/icons/soleil.svg'/><p>" + datas.LOUP.resume.nbJours + " jour(s)</p>|<img class='icons-bilan' alt='icone information' src='./modules/werewolf/logs/icons/groupe.svg'/><p>" + datas.LOUP.resume.copieLesJoueurs.size + " joueur(s)</p>|<img class='icons-bilan' alt='icone information' src='./modules/werewolf/logs/icons/trophe.svg'/><p>" + gagnants + "</p>|<img class='icons-bilan' alt='icone information' src='./modules/werewolf/logs/icons/lune.svg'/><p>" + datas.LOUP.resume.nbNuits + " nuit(s)</p>|\n\n</div>\n\n";

		texte += "\n<div class='space1'></div>\n";
		if (gagnants == "perdants") {
			texte += "<center>\n<img style='border-radius: 5px;' alt='image des gagnants' src='./modules/werewolf/logs/icons/gagnants_egalite.png'/>\n</center>\n\n";
		} else {
			texte += "<center>\n<img style='border-radius: 5px; height: 22cm;' alt='image des gagnants' src='./modules/werewolf/logs/temporaire/gagnants-" + datas.guildId + ".png'/>\n</center>\n\n";
		}
		texte += "\n<div class='space1'></div>\n";

		clearInterval(datas.LOUP.resume.timerGlobal);
		texte += "<div class='max'>\n\n|  |  ||  |\n|:-------:|:-------:|:-------:|:-------:|\n| <img class='icons-bilan' alt='icone information' src='./modules/werewolf/logs/icons/chrono.svg'/><p>" + Math.floor(datas.LOUP.resume.valeurTimerGlobal / 60) + " min " + datas.LOUP.resume.valeurTimerGlobal % 60 + " s</p> | <img class='icons-bilan' alt='icone information' src='./modules/werewolf/logs/icons/satisfaction.svg'/><p>Note moyenne: " + datas.LOUP.resume.noteMoyennePartie + "</p> || <img class='icons-bilan' alt='icone information' src='./modules/werewolf/logs/icons/mort.svg'/><p>" + datas.LOUP.lesMorts.size + " mort(s)</p> |\n\n</div>\n\n";
		texte += "</section>\n";

		bot.fs.writeFile(generationLogs + "_stats-" + datas.guildId + ".md", texte, function (err) {
			if (err) console.log("generation stats logs erreur : " + err);
			resolve();
		});
	});
}

/**
 * Envoye un message pour demander au joueurs de donner une note de 0 à 5 concernant leur ressenti de la partie en général
 *
 * @param {String} etatVictoire L'équipe gagnante de la partie
 * @param {JsonObject} datas        Les données du serveur courant
 * @return {Message} Envoie différents messages (notation + pdf de fin de partie)
 */
function notationPartie(etatVictoire, datas) {
	setTimeout(function () {
		if (bot.DEBUG) console.log("  == Debut notation ==");
		if (!datas.LOUP.salons.channelTextuelVillage) return;

		datas.LOUP.salons.channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Quelle belle partie !").setDescription("Vous avez apprécié cette partie ? Notez la de 0 à 5 points :thumbsup:").addField("Temps restant", datas.LOUP.temps.notationPartie + "s").setColor(couleurEmbedVert)).then(async function (message) {
			let listeReactions = [];
			for (let i = 0; i < 6; i++) {
				listeReactions.push(NUMBERS.get(i));
			}

			let cpt   = 0;
			let timer = setInterval(function () {
				message.edit(new Discord.MessageEmbed().setTitle("Quelle belle partie !").setDescription("Vous avez apprécié cette partie ? Notez la de 0 à 5 points :thumbsup:").addField("Temps restant", (datas.LOUP.temps.notationPartie - cpt) + "s").setColor(couleurEmbedVert)).catch(error => gestionErreures(error, "pdf.js", "notationPartie", "edit"));
				if (cpt >= datas.LOUP.temps.notationPartie) clearInterval(timer);
				cpt++;
			}, 1000);

			const filter    = (reaction, user) => listeReactions.includes(reaction.emoji.name) && !user.bot;
			const collector = message.createReactionCollector(filter, { time: datas.LOUP.temps.notationPartie * 1000 });
			let cptMoyenne  = 0;

			for (let i = 0; i < 6; i++) {
				await message.react(NUMBERS.get(i)).catch(error => gestionErreures(error, "pdf.js", "notationPartie", "react")); // Réactions après le collector pour éviter que le temps que toutes les réactions soient mises, les votes des joueurs ne soient pas pris en compte
			}

			collector.on("collect", async function (r, user) {
				for (const reac of message.reactions.cache.filter(reac => reac.users.cache.has(user.id)).values()) {
					if (reac != r) await reac.users.remove(user.id);
				}
			});

			collector.on("end", async function (collected) {
				clearInterval(timer);
				await message.delete().catch(error => gestionErreures(error, "pdf.js", "notationPartie", "delete"));
				if (collected.size > 0) {
					let somme      = 0;
					let cptMoyenne = 0;
					for (let [emoji, reactionManager] of collected) {
						somme += reactionManager.count;
						switch (emoji) {
							case NUMBERS.get(0):
								break;
							case NUMBERS.get(1):
								cptMoyenne += 1 * reactionManager.count;
								break;
							case NUMBERS.get(2):
								cptMoyenne += 2 * reactionManager.count;
								break;
							case NUMBERS.get(3):
								cptMoyenne += 3 * reactionManager.count;
								break;
							case NUMBERS.get(4):
								cptMoyenne += 4 * reactionManager.count;
								break;
							case NUMBERS.get(5):
								cptMoyenne += 5 * reactionManager.count;
								break;
							default:
								console.log("Vote de fin de partie, emoji non reconnu : " + emoji);
						}
					}
					datas.LOUP.resume.noteMoyennePartie = cptMoyenne / somme + " / 5";
					await datas.LOUP.salons.channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Merci d'avoir voté").setDescription("La partie étant terminée, tous les channels vont êtres supprimés").addField("Une interrogation ?", "Vous disposerez bientôt d'un résumé de la partie").setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "pdf.js", "notationPartie", "send"));
					;
				} else {
					datas.LOUP.resume.noteMoyennePartie = "Pas de vote";
					await datas.LOUP.salons.channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Personne n'a voté mais ce n'est pas grave").setDescription("La partie étant terminée, tous les channels vont êtres supprimés").addField("Une interrogation ?", "Vous disposerez bientôt d'un résumé de la partie").setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "pdf.js", "notationPartie", "send"));
					;
				}

				switch (etatVictoire) {
					case "vl": // Victoire des loups
						await pageStats("loups", datas); // Pour les logs
						break;
					case "vv": // Victoire village
						await pageStats("village", datas); // Pour les logs
						break;
					case "vp": // Pas de victoire
						await pageStats("perdants", datas); // Pour les logs
						break;
					case "va": // Victoire des amoureux
						await pageStats("amoureux", datas); // Pour les logs
						break;
				}

				if (bot.DEBUG) console.log("  == Fin notation ==");

				setTimeout(async function () {
					await genererPDFLogs(datas.guildId);
					datas.LOUP.salons.channelPrincipal.send(new Discord.MessageEmbed().setTitle("Le résumé est arrivé !").attachFiles([generationLogs + "-" + datas.guildId + ".pdf"]).setDescription("Retrouvez tous les détails croustillant de la partie ici :D")).catch(error => gestionErreures(error, "pdf.js", "notationPartie", "send"));
					arreterPartieEncours(datas);
				}, datas.LOUP.temps.annonceResultats * 1000);
			});
		}).catch(error => gestionErreures(error, "pdf.js", "notationPartie", "send"));
	}, 10000); // 10 secondes avant d'avoir le message de notation
}
