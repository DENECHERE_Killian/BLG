/**
 * @file Le fichier principal du module Loup Garou
 * @module loup_garou/main.js
 * @author Killian & Xavier
 */

// memberBot → Le member correspondant au bot sur la guilde courante (le serveur actuel de la partie)
// serveur → Le serveur local servant à la mise en ligne du site avec le tableau des rôles encore en partie

// CONSTANTES
// Voir les temps dans commands.js
const generationLogs    = "./modules/werewolf/logs/temporaire/resumePartie"; // Le fichier pdf généré à la fin de la partie
const generationTableau = "./modules/werewolf/tableau/temporaire/infoPartie"; // Le fichier pdf généré à la fin de la partie
const lesRoles          = new Map([
	["Petite-fille", new Map([
		["special", false],
		["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Petite-fille.png"],
		["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Petite-fille.png"]
	])],
	["Chasseur", new Map([
		["special", false],
		["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Chasseur.png"],
		["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Chasseur.png"]
	])],
	["Cupidon", new Map([
		["special", false],
		["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Cupidon.png"],
		["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Cupidon.png"]
	])],
	["Sorciere", new Map([
		["special", false],
		["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Sorciere.png"],
		["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Sorciere.png"]
	])],
	["Voyante", new Map([
		["special", false],
		["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Voyante.png"],
		["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Voyante.png"]
	])],
	["Loup-garou", new Map([
		["special", true],
		["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Loup-garou.png"],
		["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Loup-garou.png"]
	])],
	["Villageois", new Map([
		["special", true],
		["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Villageois.png"],
		["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Villageois.png"]
	])],
	["Capitaine", new Map([
		["special", true],
		["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Capitaine.png"],
		["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Capitaine.png"]
	])],
	["Medium", new Map([
		["special", false],
		["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Medium.png"],
		["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Medium.png"]])]]);
// Association des rôles à leurs images sur Google drive ainsi qu'à un boolean qui nous dit si le role est spécial comme le loupg garou et villageois qui peut-être donné plusieurs fois et le capitaine qui est un role secondaire

// CHANNELS
// channelTextuelLoupsGarous → Le channel textuel réservé aux loups-garous
// channelTextuelPetiteFille → Le channel textuel réservé à la petite-fille
// channelTextuelMediumMort → Le channel textuel réservé au médium (envois des messages des morts)
// channelTextuelSorciere → Le channel textuel réservé à la socière
// channelTextuelCupidon → Le channel textuel réservé à Cupidon
// channelTextuelVoyante → Le channel textuel réservé à la voyante
// channelTextuelVillage → Le channel dans lequel raconter l'histoire
// channelTextuelMedium → Le channel textuel réservé au médium (réception du message)
// channelVocalVillage → Le channel vocal dans lequel se trouve tout le monde
// categoryChannelLG → La catégorie pour les channels (textuel et vocaux)
// channelPrincipal → Le channel textuel ou le message de démarrage a été envoyé

// ROLES
// lesAmoureux → Liste des amoureux (User)
// rolePrincipalBot → Le role automatiquement assigné au bot lors de sa venue sur un serveur
// petiteFille → La petite fille (User) si rôle existant
// capitaine → Le capitaine de la partie (User)
// chasseur → Le chasseur (User) si rôle existant
// sorciere → La sorcière (User) si rôle existant
// cupidon → Cupidon (User) si rôle existant
// voyante → La voyante (User) si rôle existant
// medium → Le medium (User) si rôle existant

// LOGS
// copieLesJoueurs → Copie de la map lesJoueurs (copie effectuée au début de la partie et ne sauvegardant pas l'avancée de la partie)
// timerGlobal → Chronomètre pour le temps de la partie
// noteMoyennePartie → La note pour la partie qui vient de se finir et déterminée en fonction du vote des joeurs.
// infoPartie → Les informations sur les rôles participants à la partie (seront inscrites dans le fichier infoPartie.json)
// [nbJours, nbNuits, valeurTimerGlobal] = [0, 0, 0] → Compteurs pour le nb de jours et de nuits ainsi que pour le temps de la partie
// texteLogs → String contenant tout les évenements du jeu avec une certaine mise en forme pour faire un compte rendu à la fin
// messagesTemporaires → Liste pour stocker des messages le temps de l'execution d'une fonction pour ensuite mettre ces messages dans les logs

// AUTRES
// connectionVocaleBot → La connection vocale du bot
// listeLoups → La liste des loups de la partie (User)
// listePotionsSorciere → [true, true]; // La liste des booleans indiquant si la potion a été utilisé au cours de la partie. Ordre des potions : guérison, empoisonement
// mortsDeLaNuit → La liste contenant la ou les personnes tuées durant la nuit courante en attente de l'annonce de leur mort. (User)
// lesJoueurs → Dictionnaire avec (User)user → [ (String) role , (Role) role , (GuildMember) member]
// stop → Dictionnaire ["roles", []], ["salons", []], ["messages", []] → pour la supression des channels, des rôles etc...
// lesMorts → Dictionnaire avec (User)user → [(String)surnom, (Role) role, (GuildMember)member, (number)tempsVie]
// lienSite → Le lien de la page mise en ligne pour le tableau des rôles
