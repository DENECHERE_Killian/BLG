<section>
  <center>

# Jour n°1

X morts
  </center>

#### Déroulement linéaire

  <div class="evenement nuit">
    <p>
      <img class="icons" alt="icone d'information" src="..une.png"/>
      Annonce de la nuit
    </p>
  </div>

  <div class="evenement-sep"></div>

  <div class="evenement">
    <p>
      <img class="icons" alt="icone d'information" src="..nformation.svg"/>
      Annonce classique
    </p>
    <p>Voici une explication de ce qui se passe</p>
  </div>

  <div class="evenement-sep"></div>

  <div class="evenement">
    <p>
      <img class="icons" alt="icone d'information" src="..nformation.svg"/>
      Annonce classique sans explication
    </p>
  </div>

  <div class="evenement-sep"></div>

  <div class="evenement liserai-rouge">
    <p>
      <img class="icons" alt="icone d'information" src="..nformation.svg"/>
      Annonce classique avec liserai
    </p>
    <p>Couleur dispo en <span style="color:#bf3d25;">rouge</span>, <span style="color:#35ae57;">vert</span>, <span style="color:#2a7ebc;">bleu</span> et <span style="color:#f29e00;">jaune</span>
    </p>
  </div>

  <div class="evenement-sep"></div>

  <div class="evenement liserai-vert">
    <p>
      <img class="icons" alt="icone d'information" src="..ontent.svg"/>
      Des icones en plus
    </p>
    <p>
    Avec ce lien vers ionicons<a href="https://ionicons.com/"></a>, tu peut télécharger pleins d'icones comme celle en haut de ce message
    </p>
  </div>

  <div class="evenement-sep"> On peut aussi préciser 2-3 trucs entre 2 annonces</div>

  <div class="evenement jour">
    <p>
      <img class="icons" alt="icone d'information" src="..oleil.svg"/>
      Annonce du jour
    </p>
  </div>

  <div class="evenement-sep"></div>

  <div>
    <p>
      <img class="icons-bulle" alt="icone d'information" src="..ort.svg"/>
      Un style moins tape à l'oeil (avec l'icone personnalisable biensur)
    </p>
  </div>

  <div class="evenement-sep"></div>

  <div class="bulle-colle">
    <p>
      <img class="icons-bulle" alt="icone d'information" src="..ort.svg"/>
      Un style moins tape à l'oeil qui est collé au niveau de l'icone
    </p>
  </div>

  <div class="evenement-sep"></div>

  <div class="bulle-colle">
    <p>
      <img class="icons-bulle back-rouge" alt="icone d'information" src="..ort.png"/>
      Fond disponible <span style="color:#bf3d25;">rouge</span>, <span style="color:#35ae57;">vert</span>, <span style="color:#2a7ebc;">bleu</span> et <span style="color:#f29e00;">jaune</span>
    </p>
  </div>

</section>


<!-- Nouvelle page -->


<section>
  <center>

# Jour n°2

X morts
  </center>

#### Déroulement non linéaire

  <div class="evenement">
    <p>
      <img class="icons" alt="icone d'information" src="..nformation.svg"/>
      Annonce classique
    </p>
    <p>Le déroulement non linéaire correspond à l'éxécution de deux actions effectuée en simultané. Ce comportement est donc inclus au sein d'un déroulemet linéaire.</p>
  </div>

  <div class="evenement-sep-non-lineaire"></div>

  <ul class="evenement-non-lineaire">
    <div>
      <div class="evenement-sep"></div>
      <div>
        <p>
          <img class="icons-bulle" alt="icone d'information" src="..hat.svg"/>
          Les loups discutent
        </p>
      </div>
      <div class="evenement-sep"></div>
    </div>
    <div>
      <div class="evenement-sep"></div>
      <div>
        <p>
          <img class="icons-bulle" alt="icone d'information" src="..reille.svg"/>
          La petite fille écoute
        </p>
      </div>
      <div class="evenement-sep"></div>
    </div>
  </ul>

  <div class="evenement-sep-non-lineaire-inverse"></div>

  <div class="evenement">
    <p>
      <img class="icons" alt="icone d'information" src="..nformation.svg"/>
      Annonce classique
    </p>
    <p>Et voila comment reprendre en linéaire après un non linéaire.</p>
  </div>

  <div class="space1"></div>

  <ul class="evenement-non-lineaire">
    <div>
      <div class="evenement-sep"></div>
      <div>
        <p>
          <img class="icons-bulle" alt="icone d'information" src="..hat.svg"/>
          Les loups discutent
        </p>
      </div>
      <div class="evenement-sep"></div>
    </div>
    <div>
      <div class="evenement-sep"></div>
      <div>
        <p>
          <img class="icons-bulle" alt="icone d'information" src="..reille.svg"/>
          La petite fille écoute
        </p>
      </div>
      <div class="evenement-sep"></div>
    </div>
  </ul>

  <div class="space1"></div>

  <div class="evenement">
    <p>
      <img class="icons" alt="icone d'information" src="..nformation.svg"/>
      Annonce classique
    </p>
    <p>On peut aussi voir une différence de présentation des liens entre les bulles</p>
  </div>

  <ul class="evenement-non-lineaire">
    <div>
      <div class="evenement-sep"></div>
      <div>
        <p>
          <img class="icons-bulle" alt="icone d'information" src="..hat.svg"/>
          Les loups discutent
        </p>
      </div>
      <div class="evenement-sep"></div>
    </div>
    <div>
      <div class="evenement-sep"></div>
      <div>
        <p>
          <img class="icons-bulle" alt="icone d'information" src="..reille.svg"/>
          La petite fille écoute
        </p>
      </div>
      <div class="evenement-sep"></div>
    </div>
  </ul>

  <div class="evenement liserai-bleu">
    <p>
      <img class="icons" alt="icone d'information" src="..nformation.svg"/>
      Annonce classique
    </p>
    <p>Et ici sans espaces. Tout en sachant que la bulle dans laquelle se trouve ce texte peut conserver les styles présentés dans la page précédente</p>
  </div>

  <div class="space1"></div>

  <ul class="evenement-non-lineaire">
    <div>
      <div>
        <p>
          <img class="icons-bulle back-vert" alt="icone d'information" src="..hat.svg"/>
          Les loups discutent
        </p>
      </div>
    </div>
    <div>
      <div>
        <p>
          <img class="icons-bulle" alt="icone d'information" src="..reille.svg"/>
          La petite fille écoute
        </p>
      </div>
    </div>
  </ul>
</section>


<!-- Nouvelle page -->


<section>
  <center>

# Jour n°3

X morts
  </center>

## Tableaux

#### Avec fonds

On peut préciser si on veut des bords sur les th
  <center class="bords th">

| Titres | centré | aligné à droite |
  |-------|:-------:|-------:|
| gauche | centre | droite |

  </center>

#### Sans fond

  <center class="th">

| Titres | centré | aligné à droite |
  |-------|:-------:|-------:|
| gauche | centre | droite |

  </center>

#### Etiré

  <center class="bords max">

| Titres | centré | aligné à droite |
  |-------|:-------:|-------:|
| gauche | centre | droite |

  </center>

  <div class="space2"></div>

## Surlignage

<mark>Stabilo par defaut</mark> | <mark class="back-rouge">Stabilo rouge</mark> | <mark class="back-vert">Stabilo vert</mark> | <mark class="back-bleu">Stabilo bleu</mark> | <mark class="back-jaune">Stabilo jaune</mark></section>


<!-- Nouvelle page -->


<section>
  <center>

# Merci d'avoir joué !

  </center>

  <div class="space1"></div>

  <div class="max">

|  |  |  |  |
  |:-------:|:-------:|:-------:|:-------:|
|<img class="icons-bilan" alt="icone d'information" src="./modules/loup_garou/logs/icons/trophe.svg"/><p>Loups-garous</p>|<img class="icons-bilan" alt="icone d'information" src="./modules/loup_garou/logs/icons/chrono.svg"/><p>5:43 min</p>|<img class="icons-bilan" alt="icone d'information" src="./modules/loup_garou/logs/icons/capitaine.svg"/><p>Ticky</p>|<img class="icons-bilan" alt="icone d'information" src="./modules/loup_garou/logs/icons/amoureux.svg"/><p>Ticky & Enola</p>|

  </div>

  <div class="space1"></div>

  <div class="bords max tr">

|     | Roles présents | Joueur(s)     | Morts             |
  |:---:|:---------------|:--------------|:------------------|
| x 2 | Loups-Garous   | Zorg & Ticky  | <img class="icons-bilan-tableau" alt="icone d'information" src="./modules/loup_garou/logs/icons/oui.png"/> <img class="icons-bilan-tableau" alt="icone d'information" src="./modules/loup_garou/logs/icons/non.png"/> |
| x 2 | Villageois     | Zombie & Grim | <img class="icons-bilan-tableau" alt="icone d'information" src="./modules/loup_garou/logs/icons/oui.png"/> <img class="icons-bilan-tableau " alt="icone d'information" src="./modules/loup_garou/logs/icons/oui.png"/> |
| x 1 | Sorcière       | Volrode       | <img class="icons-bilan-tableau" alt="icone d'information" src="./modules/loup_garou/logs/icons/oui.png"/> |
| x 1 | Chasseur       | Astro         | <img class="icons-bilan-tableau" alt="icone d'information" src="./modules/loup_garou/logs/icons/oui.png"/> |
| x 1 | Cupidon        | Enola         | <img class="icons-bilan-tableau" alt="icone d'information" src="./modules/loup_garou/logs/icons/oui.png"/> |
| x 2 | Amoureux       | Enola & Ticky | <img class="icons-bilan-tableau" alt="icone d'information" src="./modules/loup_garou/logs/icons/oui.png"/> <img class="icons-bilan-tableau" alt="icone d'information" src="./modules/loup_garou/logs/icons/non.png"/> |
| x 2 | Amoureux       | Enola & Ticky | <img class='icons-bilan-tableau' alt='icone

d'information'
src='./modules/loup_garou/logs/icons/oui.png'/> |

  </div>

  <div class="space1"></div>

  <div class="max">

|                                                                                                                     |  |  |  |
---------------------------------------------------------------------------------------------------------------------|:-------:|:-------:|:-------:|:-------:|
| <img class="icons-bilan" alt="icone d'information" src="./modules/loup_garou/logs/icons/soleil.svg"/><p>4 jours</p> |<img class="icons-bilan" alt="icone d'information" src="./modules/loup_garou/logs/icons/groupe.svg"/><p>9 joueurs</p>|<img class="icons-bilan" alt="icone d'information" src="./modules/loup_garou/logs/icons/chat.svg"/><p>122 messages</p>|<img class="icons-bilan" alt="icone d'information" src="./modules/loup_garou/logs/icons/lune.svg"/><p>3 nuits</p>|

  </div>

On pourra rajouter des graphiques de style flat design ou minimaliste pour rester cohérent avec le style globale du doc (on peut cliquer sur l'image pour avoir des exemples).

  <center>
    <a href="https://www.edrawsoft.com/fr/infographics/popular-infographic-charts.php"><img style="height: 6cm;" alt="graph" src="https://www.edrawsoft.com/fr/infographics/images/various-infographic-bar-charts.png" /></a>
  </center>

Pourquoi pas rajouter d'autres informations comme les languages utilisés, des remerciements, d'autres informations sur le jeu etc ...

</section>
