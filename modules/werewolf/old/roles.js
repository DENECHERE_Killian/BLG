/**
 * @file Les fonctions concernant les différents rôles existant pour le jeu Loup Garou
 * @module loup_garou/role.js
 * @author Killian & Xavier
 */

//==============================================================================
// VOYANTE
//==============================================================================

/**
 * Joue le tour de la voyante
 *
 * @param {Number} guildId L'identifiant du serveur courant
 * @return {Promise}
 */
function tourVoyante(guildId) {
	if (bot.DEBUG) console.log("  == Début du tour de la voyante ==");
	return new Promise(function (resolve, reject) {
		const datas = obtenirDatasCourante(guildId);

		datas.LOUP.salons.channelTextuelVillage.send("```La voyante se réveille et désigne un joueur dont elle veut sonder la véritable personnalité !```").catch(error => gestionErreures(error, "role.js", "tourVoyante", "send"));
		const [textEmbed, lesMembres, listeEmojis] = getListeVillageAvecLettres(guildId);
		datas.LOUP.salons.channelTextuelVoyante.send(new Discord.MessageEmbed().setTitle("De quel joueur veut-tu connaitre le rôle ?").addField("Temps restant", datas.LOUP.temps.choixVoyante + "s").setDescription(textEmbed).setColor(couleurEmbedVert)).then(async function (newMessage) {

			let cpt   = 0;
			let timer = setInterval(function () {
				newMessage.edit(new Discord.MessageEmbed().setTitle("De quel joueur veut-tu connaitre le rôle ?").addField("Temps restant", (datas.LOUP.temps.choixVoyante - cpt) + "s").setDescription(textEmbed).setColor(couleurEmbedVert)).catch(error => gestionErreures(error, "role.js", "tourVoyante", "edit"));
				if (cpt >= datas.LOUP.temps.choixVoyante) {
					clearInterval(timer);
					datas.LOUP.salons.channelTextuelVillage.send("```La voyante se rendort```").catch(error => gestionErreures(error, "role.js", "tourVoyante", "send"));
					resolve(); // Pour dire à la promesse que le travail est finit
				}
				cpt++;
			}, 1000);

			const filter    = (reaction, user) => listeEmojis.includes(reaction.emoji.name) && !user.bot; // On vérifie ue l'émojis est parmis les réactions original
			const collector = newMessage.createReactionCollector(filter, { time: datas.LOUP.temps.choixVoyante * 1000, max: 1 }); // On arrête à la première réaction récoltée

			for (let i = 0; i < lesMembres.length; i++) {
				await newMessage.react(LETTERS.get(i)).catch(error => gestionErreures(error, "roles.js", "tourVoyante", "react")); // On réagi après avoir déclaré le collector
			}

			collector.on("end", async function (collected) { // Collected est le map des emojis collectés
				let user;
				if (collected.size > 0) {
					user = lesMembres[listeEmojis.indexOf(collected.firstKey())]; // Le User correspondant à la lettre choisie
					evenementBulle(datas, "voyante.svg", "<b>La voyante</b> sonde le rôle de <b>" + user.username + "</b> (" + datas.LOUP.lesJoueurs.get(user)[0] + ")"); // Pour les logs
					newMessage.edit(new Discord.MessageEmbed().setTitle(user.username)).catch(error => gestionErreures(error, "role.js", "tourVoyante", "edit"));
					await expliquerRole(datas.LOUP.lesJoueurs.get(user)[0], datas.LOUP.salons.channelTextuelVoyante);
					if (bot.DEBUG) console.log("    La voyante sonde " + user.username + " et voit qu'il est " + datas.LOUP.lesJoueurs.get(user)[0]);
				} else {
					evenementBulle(datas, "voyante.svg", "<b>La voyante</b> ne sonde personne cette nuit"); // Pour les logs
					newMessage.edit(new Discord.MessageEmbed().setTitle("Vous n'avez sondé personne")).catch(error => gestionErreures(error, "role.js", "tourVoyante", "edit"));
					if (bot.DEBUG) console.log("    La voyante n'a sondé personne");
				}
			});
		}).catch(error => gestionErreures(error, "role.js", "tourVoyante", "send"));
		if (bot.DEBUG) console.log("  == Fin du tour de la voyante ==");
	});
}

//==============================================================================
// LOUP-GAROU
//==============================================================================

/**
 * Joue le tour des loups-garou
 *
 * @param {Number} guildId L'identifiant du serveur courant
 * @return {Promise}
 */
function tourLoupGarou(guildId) {
	if (bot.DEBUG) console.log("  == Début du tour des loups-garou ==");
	const datas = obtenirDatasCourante(guildId);

	return new Promise(function (resolve, reject) {
		if (!datas.LOUP.salons.channelTextuelVillage) reject();
		datas.LOUP.salons.channelTextuelVillage.send("```Les Loups-Garous se réveillent et discutent pour désigner une victime```").catch(error => gestionErreures(error, "role.js", "tourLoupGarou", "send"));

		changerPermissionEcritureLoups(true, datas); // On met les droits d'écriture pour les loups-garous dans leur channel textuel.

		if (datas.LOUP.roles.petiteFille) {
			tourPetiteFille(guildId);
		} else {
			datas.LOUP.resume.messagesTemporaires[1] = "<b>La Petite-Fille</b> n'est pas/plus présente dans la partie, son tour passe"; // Pour les logs
		}

		const [textEmbed, lesMembres, listeEmojis] = getListeVillageAvecLettres(guildId, true);

		datas.LOUP.salons.channelTextuelLoupsGarous.send(new Discord.MessageEmbed().setTitle("À vous de jouer !").setDescription("Choisissez une victime : mettez-vous tous d'accord.\nLe système choisira au hasard en cas d'égalité.\n\n" + textEmbed).addField("Temps restant", datas.LOUP.temps.debatLoups + "s").setFooter("Votez tous pour une personne pour valider le vote").setColor(couleurEmbedVert)).then(async function (message) {

			let cpt   = 0;
			let timer = setInterval(function () {
				message.edit(new Discord.MessageEmbed().setTitle("À vous de jouer !").setDescription("Choisissez une victime : mettez-vous tous d'accord.\nLe système choisira au hasard en cas d'égalité.\n\n" + textEmbed).addField("Temps restant", (datas.LOUP.temps.debatLoups - cpt) + "s").setFooter("Votez tous pour une personne pour valider le vote").setColor(couleurEmbedVert)).catch(error => gestionErreures(error, "role.js", "tourLoupGarou", "edit"));
				if (cpt >= datas.LOUP.temps.debatLoups) {
					clearInterval(timer);
				}
				cpt++;
			}, 1000);


			const filter    = (reaction, user) => listeEmojis.includes(reaction.emoji.name) && !user.bot;
			const collector = message.createReactionCollector(filter, { time: datas.LOUP.temps.debatLoups * 1000 });

			for (let i = 0; i < lesMembres.length; i++) {
				await message.react(LETTERS.get(i)).catch(error => gestionErreures(error, "roles.js", "tourLoupGarou", "react")); // On met une réaction pour chaque personne qui n'est pas loup
			}

			collector.on("end", async function (collected, reason) {
				clearInterval(timer);
				await message.delete().catch(error => gestionErreures(error, "role.js", "tourLoupGarou", "delete"));

				if (collected.size == 0) {
					if (bot.DEBUG) console.log("    Les loups-garou n'ont pas choisie de victime");
					datas.LOUP.resume.messagesTemporaires[0] = "<b>Les Loups-Garous</b> ne choisissent personne cette nuit"; // Pour les logs
					datas.LOUP.salons.channelTextuelLoupsGarous.send(new Discord.MessageEmbed().setTitle("Temps écoulé").setDescription("Vous n'avez finalement pas réagis donc personne ne mourra cette nuit.").setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "role.js", "tourLoupGarou", "send"));
				} else {
					let mort;
					if (collected.size > 1) {
						if (bot.DEBUG) console.log("    Les loups-garou ont fait une égalité sur le vote");
						mort                                     = lesMembres[listeEmojis.indexOf(collected.randomKey())];
						datas.LOUP.resume.messagesTemporaires[0] = "<b>Les Loups-Garous</b> ne sont pas d'accord, c'est <b>" + mort.username + "</b> (" + datas.LOUP.lesJoueurs.get(mort)[0] + ") qui est désigné au hasard parmis leurs choix"; // Pour les logs
						datas.LOUP.salons.channelTextuelLoupsGarous.send(new Discord.MessageEmbed().setTitle("Vous n'êtes pas d'accord !").setDescription("C'est donc **" + mort.username + "** qui à été choisi au hasard parmis vos choix").setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "role.js", "tourLoupGarou", "send"));
					} else { // Une seule personne à été choisi
						if (bot.DEBUG) console.log("    Les loups-garou ont fait un vote unanime");
						mort                                     = lesMembres[listeEmojis.indexOf(collected.firstKey())];
						datas.LOUP.resume.messagesTemporaires[0] = "<b>Les Loups-Garous</b> choisissent de tuer <b>" + mort.username + "</b> (" + datas.LOUP.lesJoueurs.get(mort)[0] + ")"; // Pour les logs
						datas.LOUP.salons.channelTextuelLoupsGarous.send(new Discord.MessageEmbed().setTitle("Vous avez choisi !").setDescription("Vous avez choisi de tuer **" + mort.username + "**").setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "role.js", "tourLoupGarou", "send"));
					}
					if (bot.DEBUG) console.log("    Le mort des loups-garou est : " + mort.username);
					datas.LOUP.mortsDeLaNuit.push(mort);
				}
				changerPermissionEcritureLoups(false, datas);
				datas.LOUP.salons.channelTextuelVillage.send("```Les Loups-garous ont finit de débattre !```").catch(error => gestionErreures(error, "role.js", "tourLoupGarou", "send"));
				resolve();
			});
		}).catch(error => gestionErreures(error, "role.js", "tourLoupGarou", "send"));
		if (bot.DEBUG) console.log("  == Fin du tour des loups-garou ==");
	});
}

//==============================================================================
// SORCIÈRE
//==============================================================================

/**
 * Joue le tour de la sorcière
 *
 * @param {Number} guildId L'identifiant du serveur courant
 * @return {Promise}
 */
function tourSorciere(guildId) {
	if (bot.DEBUG) console.log("  == Début du tour de la sorcière ==");
	return new Promise(function (resolve) {
		const datas = obtenirDatasCourante(guildId);

		datas.LOUP.salons.channelTextuelVillage.send("```La Sorcière se réveille, je lui montre la victime des Loups-Garous. Va-t-elle user de ses potions ?```").catch(error => gestionErreures(error, "role.js", "tourSorciere", "send"));

		let [guerison, empoisonement] = datas.LOUP.potionsSorciere;
		if (guerison || empoisonement) { // S'il lui reste au moins une potion à utiliser

			let listeVotesPossibles = ["👍", "👊", "👎"];
			let desc                = "";
			if (guerison && datas.LOUP.mortsDeLaNuit[0]) { // Les loups ont bien choisi une victime
				desc += "Les Loups-Garous ont choisi de tuer **" + datas.LOUP.mortsDeLaNuit[0].username + "**\n\n:thumbsup: Potion de guérison pour soigner la cible des Loups-Garous.\n";
			} else if (datas.LOUP.mortsDeLaNuit[0]) { // Plus la potion mais un mort
				desc += "Les Loups-Garous ont choisi de tuer **" + datas.LOUP.mortsDeLaNuit[0].username + "**\n\n:thumbsup: ~~Potion de guérison pour soigner la cible des Loups-Garous.~~\n";
			} else { // Ni potion ni mort ou juste pas de mort
				desc += "Les Loups-Garous n'ont fait aucune victime cette nuit\n\n:thumbsup: ~~ Potion de guérison pour soigner la cible des Loups-Garous.~~\n";
			}

			desc += ":punch: Ne rien faire\n";

			if (empoisonement) {
				desc += ":thumbsdown: Potion d'empoisonnement pour tuer une personne de votre choix";
			} else {
				desc += ":thumbsdown: ~~Potion d'empoisonnement pour tuer une personne de votre choix~~";
			}

			datas.LOUP.salons.channelTextuelSorciere.send(new Discord.MessageEmbed().setTitle("À vous de jouer !").setDescription(desc).addField("Temps restant", datas.LOUP.temps.choixSorciere + "s").setFooter("Vous pouvez choisir de n'utiliser aucune de vos potions\nUne fois utilisée, une potion ne sera plus utilisable.").setColor(couleurEmbedVert)).then(async function (message) {

				if (guerison && datas.LOUP.mortsDeLaNuit[0]) { // Condition pour sauver quelqu'un
					if (bot.DEBUG) console.log("    Proposition de la potion de vie pour sauver : " + datas.LOUP.mortsDeLaNuit[0].username);
					await message.react(listeVotesPossibles[0]).catch(error => gestionErreures(error, "roles.js", "tourVoyante", "react"));
				} else {
					await message.react("⛔").catch(error => gestionErreures(error, "roles.js", "tourVoyante", "react"));
				}
				await message.react(listeVotesPossibles[1]).catch(error => gestionErreures(error, "roles.js", "tourVoyante", "react"));
				if (empoisonement) {
					if (bot.DEBUG) console.log("    Proposition de la potion d'empoisonnement");
					await message.react(listeVotesPossibles[2]).catch(error => gestionErreures(error, "roles.js", "tourVoyante", "react"));
				} else {
					await message.react("⛔").catch(error => gestionErreures(error, "roles.js", "tourVoyante", "react"));
				}

				const filter    = (reaction, user) => listeVotesPossibles.includes(reaction.emoji.name) && !user.bot; // On s'assure que l'emoji soit bien un des 3 acceptés et que ce ne soit pas une réaction du bot
				const collector = message.createReactionCollector(filter, { time: datas.LOUP.temps.choixSorciere * 1000 });

				let cpt   = 0;
				let timer = setInterval(async function () {
					message.edit(new Discord.MessageEmbed().setTitle("À vous de jouer !").setDescription(desc).addField("Temps restant", (datas.LOUP.temps.choixSorciere - cpt) + "s").setFooter("Vous pouvez choisir de n'utiliser aucune de vos potions\nUne fois utilisée, une potion ne sera plus utilisable.").setColor(couleurEmbedVert)).catch(error => gestionErreures(error, "role.js", "tourSorciere", "edit"));
					if (cpt >= datas.LOUP.temps.choixSorciere) {
						clearInterval(timer);
						if (!collector.ended) {
							await collector.stop("time");
						}
						await datas.LOUP.salons.channelTextuelVillage.send("```La sorcière se rendort```").catch(error => gestionErreures(error, "role.js", "tourSorciere", "send"));
						resolve();
					}
					cpt++;
				}, 1000);

				collector.on("collect", async function (r) {
					switch (r.emoji.name) {
						case listeVotesPossibles[0]: // Sauver
							if (bot.DEBUG) console.log("    Utilisation de la potion de soin");
							evenementBulle(datas, "sorciere.svg", "<b>La sorciere</b> choisi de sauver la victime des Loups-Garous (" + datas.LOUP.mortsDeLaNuit[0].username + ")"); // Pour les logs
							await datas.LOUP.salons.channelTextuelSorciere.send(new Discord.MessageEmbed().setTitle("Vous avez choisi de sauver **" + datas.LOUP.mortsDeLaNuit[0].username + "**").setDescription("Vous ne pourrez plus utiliser votre potion de guérison à l'avenir").setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "role.js", "tourSorciere", "send"));
							datas.LOUP.potionsSorciere[0] = false;
							datas.LOUP.mortsDeLaNuit      = [];
							collector.stop("choix");
							clearInterval(timer);
							break;

						case listeVotesPossibles[2]: // Empoisonner
							if (bot.DEBUG) console.log("    Utilisation de la potion de poison");
							const [textEmbed, lesMembres, listeEmojis] = getListeVillageAvecLettres(guildId);
							datas.LOUP.salons.channelTextuelSorciere.send(new Discord.MessageEmbed().setTitle("Vous avez choisi d'utiliser votre potion d'empoisonnement mais sur qui ?").setDescription("Choisisez une victime dans la liste suivante\n" + textEmbed).setColor(couleurEmbedBleu)).then(async function (newMessage) {

								const filter2    = (reaction, user) => listeEmojis.includes(reaction.emoji.name) && !user.bot; // On vérifie que l'émojis est parmis les réactions original
								const collector2 = newMessage.createReactionCollector(filter2, { max: 1 }); // Limite fixée à 1 réaction qui ne vient pas du bot

								for (let i = 0; i < lesMembres.length; i++) {
									await newMessage.react(LETTERS.get(i)).catch(error => gestionErreures(error, "roles.js", "tourVoyante", "react")); // On réagi après la déclaration du collector
								}

								collector2.on("end", function (collected) { // Collected est le map des emojis collectés
									let user = lesMembres[listeEmojis.indexOf(collected.firstKey())]; // Le User correspondant à la lettre choisie

									evenementBulle(datas, "sorciere.svg", "<b>La sorciere</b> choisi d'utiliser sa potion de poison sur <b>" + user.username + "</b> (" + datas.LOUP.lesJoueurs.get(user)[0] + ")"); // Pour les logs
									datas.LOUP.salons.channelTextuelSorciere.send(new Discord.MessageEmbed().setTitle("Vous avez choisi d'empoisonner **" + user.username + "**").setDescription("Vous ne pourrez plus utiliser votre potion d'empoisonement à l'avenir").setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "role.js", "tourSorciere", "send"));
									datas.LOUP.potionsSorciere[1] = false; // Enregistrer l'utilisation de la potion
									datas.LOUP.mortsDeLaNuit.push(user); // Ajouter la personne empoisonnée dans la liste des morts de la nuit
									newMessage.delete().catch(error => gestionErreures(error, "role.js", "tourLoupGarou", "delete"));
									collector.stop("choix");
								});
							}).catch(error => gestionErreures(error, "role.js", "tourSorciere", "send"));
							break;

						case listeVotesPossibles[1]: // On ne fait rien
							if (bot.DEBUG) console.log("    La sorcière n'agit pas");
							evenementBulle(datas, "sorciere.svg", "<b>La sorciere</b> ne fait rien"); // Pour les logs
							datas.LOUP.salons.channelTextuelSorciere.send(new Discord.MessageEmbed().setTitle("Vous avez choisi de ne rien faire").setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "role.js", "tourSorciere", "send"));
							collector.stop("choix");
							break;
					}
				});

				collector.on("end", function (collected, reason) {
					if (bot.DEBUG) console.log("    La sorcière n'agit pas par manque de temps");
					if (reason == "time") { // S'il n'y a pas de raison indiquant pourquoi on arrive à la fin, c'est qu'on est à court de temps
						evenementBulle(datas, "sorciere.svg", "<b>La sorciere</b> est à court de temps"); // Pour les logs
						datas.LOUP.salons.channelTextuelSorciere.send(new Discord.MessageEmbed().setTitle("À court de temps !").setDescription("Vous êtes à court de temps malheureusement").setColor(couleurEmbedRouge)).catch(error => gestionErreures(error, "role.js", "tourSorciere", "send"));
					}
				});
			}).catch(error => gestionErreures(error, "role.js", "tourSorciere", "send"));
		} else {
			datas.LOUP.salons.channelTextuelSorciere.send(new Discord.MessageEmbed().setTitle("C'est votre tour").setDescription("Vous n'avez plus aucune potion à utiliser mais pour éviter que tout le monde ne le sache, vous devez attendre " + datas.LOUP.temps.choixSorciere + "s pour simuler votre choix").addField("Temps restant", datas.LOUP.temps.choixSorciere + "s").setColor(couleurEmbedBleu)).then(function (newMessage) {
				let cpt   = 0;
				let timer = setInterval(function () {
					newMessage.edit(new Discord.MessageEmbed().setTitle("C'est votre tour").setDescription("Vous n'avez plus aucune potion à utiliser mais pour éviter que tout le monde ne le sache, vous devez attendre " + datas.LOUP.temps.choixSorciere + "s pour simuler votre choix").addField("Temps restant", (datas.LOUP.temps.choixSorciere - cpt) + "s").setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "role.js", "tourSorciere", "edit"));
					if (cpt >= datas.LOUP.temps.choixSorciere) {
						clearInterval(timer);
						datas.LOUP.salons.channelTextuelVillage.send("```La sorcière se rendort```").catch(error => gestionErreures(error, "role.js", "tourSorciere", "send"));
						resolve();
					}
					cpt++;
				}, 1000);
			}).catch(error => gestionErreures(error, "role.js", "tourSorciere", "send"));
		}
		if (bot.DEBUG) "== Fin du tour de la sorcière ==";
	});
}

//==============================================================================
// PETITE-FILLE
//==============================================================================

/**
 * Joue le tour de la petite-fille en même temps que les loups-garous
 *
 * @param {Number} guildId L'identifiant du serveur courant
 * @return {Message} Envoie différents messages en fonction des actions de la petite-fille
 */
function tourPetiteFille(guildId) {
	if (bot.DEBUG) console.log("== Début du tour de la petite-fille ==");
	const datas = obtenirDatasCourante(guildId);

	if (!datas.LOUP.salons.channelTextuelPetiteFille) return;

	datas.LOUP.salons.channelTextuelPetiteFille.send(new Discord.MessageEmbed().setTitle("À vous de jouer ?").setDescription("Les Loups-Garous sont en pleine réflexion,\nVous pouvez épier leur discussion une fois par nuit pendant " + datas.LOUP.temps.obervationPetiteFille + "s.\nAppuyez sur les yeux pour les observer 👀").setFooter("Faites attention, vous avez une certaine probabilité de vous faire repérer ! Cela veut dire qu'ils recevront potentiellement des lettres de votre pseudo.").setColor(couleurEmbedVert)).then(function (message) {
		message.react("👀").catch(error => gestionErreures(error, "roles.js", "tourPetiteFille", "react"));

		let timer = setInterval(function () {
			if (message.reactions.cache.get("👀").count > 1) {
				if (bot.DEBUG) console.log("La petite-fille ouvre les yeux");
				clearInterval(timer);

				message.delete().catch(error => gestionErreures(error, "role.js", "tourPetiteFille", "delete"));
				datas.LOUP.salons.channelTextuelPetiteFille.send(new Discord.MessageEmbed().setTitle("Vous ouvrez les yeux discrètement").setDescription("Vous tendez le casque et entendez ce qu'ils se disent !").addField("Temps restant", datas.LOUP.temps.obervationPetiteFille + "s").setFooter("Si les Loups-Garous discutent, vous verrez apparaitre les messages ici.").setColor(couleurEmbedBleu)).then(function (editedMessage) {

					let cpt   = 0;
					let timer = setInterval(function () {
						editedMessage.edit(new Discord.MessageEmbed().setTitle("Vous ouvrez les yeux discrètement").setDescription("Vous tendez le casque et entendez ce qu'ils se disent !").addField("Temps restant", (datas.LOUP.temps.obervationPetiteFille - cpt) + "s").setFooter("Si les Loups-Garous discutent, vous verrez apparaitre les messages ici.").setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "role.js", "tourPetiteFille", "edit"));
						if (cpt >= datas.LOUP.temps.choixVoyante) {
							clearInterval(timer);
						}
						cpt++;
					}, 1000);

					const filter    = m => m.content != "" && !m.author.bot;
					const collector = datas.LOUP.salons.channelTextuelLoupsGarous.createMessageCollector(filter, { time: datas.LOUP.temps.obervationPetiteFille * 1000 }); // Premier paramètre à true car on ets sencé mettre un filtre qui en réalité renvoie un boolean pour ajouter ou non dans le collector le message envoyé. Ici à true, tous les messages sont acceptés
					collector.on("collect", function (m) {
						datas.LOUP.salons.channelTextuelPetiteFille.send(m.content).catch(error => gestionErreures(error, "role.js", "tourPetiteFille", "send"));
					});

					collector.on("end", function (collected) {
						let description = "";
						let couleur;
						if (Math.floor(Math.random() * (datas.LOUP.listeLoups.length * 2)) == 0) { // 1 chance sur le double du nombre de loups restant de se faire repérer
							lettrePseudo = randomItem(datas.LOUP.roles.petiteFille.username);
							if (bot.DEBUG) console.log("La petite-fille a été repéré -> lettre annoncée : '" + lettrePseudo + "'");
							datas.LOUP.resume.messagesTemporaires[1] = "<b>La Petite-Fille</b> choisi d'épier la discussion des Loups mais se fait repérer : la lettre <b>" + lettrePseudo + "</b> est découverte"; // Pour les logs
							datas.LOUP.salons.channelTextuelLoupsGarous.send(new Discord.MessageEmbed().setTitle("Votre discussion a fuité").setDescription("L'un d'entre vous à aperçu une petite-fille ouvrir les yeux : \nSon nom d'utilisteur contiendrait la lettre '" + lettrePseudo + "' d'après lui.").setFooter("Prenez garde 👀").setColor(couleurEmbedRouge)).catch(error => gestionErreures(error, "role.js", "tourPetiteFille", "send"));
							description = "😞 Malheureusement, vous avez été repéré : la lettre '" + lettrePseudo + "' de votre pseudo a été découverte !";
							couleur     = couleurEmbedRouge;
						} else {
							if (bot.DEBUG) console.log("La petite-fille n'a pas été repéré");
							datas.LOUP.resume.messagesTemporaires[1] = "<b>La Petite-Fille</b> choisi d'épier la discussion des Loups et n'est pas repérée"; // Pour les logs
							description                              = "😅 Ouf, vous n'avez pas été repéré cette nuit !";
							couleur                                  = couleurEmbedBleu;
						}
						datas.LOUP.salons.channelTextuelPetiteFille.send(new Discord.MessageEmbed().setTitle("Bilan").setDescription(description).setColor(couleur)).catch(error => gestionErreures(error, "role.js", "tourPetiteFille", "send"));
					});
				}).catch(error => gestionErreures(error, "role.js", "tourPetiteFille", "send"));
			}
		}, 1000);
	}).catch(error => gestionErreures(error, "role.js", "tourPetiteFille", "send"));
	if (bot.DEBUG) console.log("== Fin du tour de la petite-fille ==");
}

//==============================================================================
// CHASSEUR
//==============================================================================

/**
 * Lorsque ce rôle est tué, il peut choisir de tuer une personne parmi les joueurs dans son dernier souffle : cette fonction gère cela
 *
 * @param {Number} guildId L'identifiant du serveur courant
 * @return {Promise}
 */
function tirChasseur(guildId) {
	if (bot.DEBUG) console.log("== Début de l'action du chasseur ==");
	return new Promise(async function (resolve, reject) {
		const datad                                = obtenirDatasCourante(guildId);
		const [textEmbed, lesMembres, listeEmojis] = getListeVillageAvecLettres(guildId, false, false, true);// On retire le chasseur de la liste des personnes potentiellement tuable (il est déjà mort alors ...)

		await datas.LOUP.salons.channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Le chasseur choisi sa cible").setDescription("Les cibles possibles sont :\n\n" + textEmbed).addField("Temps restant", datas.LOUP.temps.eliminationChasseur + "s").setFooter("Faites feu dans le temps imprti ou je crois bien que vous allez éternuer").setColor(couleurEmbedVert)).then(async function (messageChasseur) {

			let cpt   = 0;
			let timer = setInterval(function () {
				messageChasseur.edit(new Discord.MessageEmbed().setTitle("Le chasseur choisi sa cible").setDescription("Les cibles possibles sont :\n\n" + textEmbed).addField("Temps restant", (datas.LOUP.temps.eliminationChasseur - cpt) + "s").setFooter("Faites feu dans le temps imprti ou je crois bien que vous allez éternuer").setColor(couleurEmbedVert)).catch(error => gestionErreures(error, "role.js", "tirChasseur", "edit"));
				if (cpt >= datas.LOUP.temps.eliminationChasseur) {
					clearInterval(timer);
				}
				cpt++;
			}, 1000);

			const filter    = (reaction, user) => listeEmojis.includes(reaction.emoji.name && user == datas.LOUP.roles.chasseur); // On vérifie ue l'émojis est parmi les réactions original et que c'est le chasseur qui réagit
			const collector = messageChasseur.createReactionCollector(filter, { time: datas.LOUP.temps.eliminationChasseur * 1000, max: 1 }); // Un maximum de 1 réaction

			for (let j = 0; j < lesMembres.length; j++) {
				await messageChasseur.react(LETTERS.get(j)).catch(error => gestionErreures(error, "roles.js", "tirChasseur", "react")); // On réagis après avoir déclaré le collector
			}

			collector.on("end", async function (collected, reason) { // Collected est le map des emojis collectés
				const usernameChasseur = datas.LOUP.roles.chasseur.username;
				let user;
				if (reason == "limit") {
					user = lesMembres[listeEmojis.indexOf(collected.firstKey())];
					if (bot.DEBUG) console.log("    Le chasseur choisi de tuer : " + user.username);
					evenementBulle(datas, "mort.svg", "<b>Le chasseur | " + usernameChasseur + "</b> décède et choisit d'assassiner au passage <b>" + user.username + "</b> (" + datas.LOUP.lesJoueurs.get(user)[0] + ")"); // Pour les logs
					await messageChasseur.edit(new Discord.MessageEmbed().setTitle("Tir du chasseur").setDescription("Dans un dernier soupire vous abattez : **" + user.username + "**.").setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "role.js", "tirChasseur", "edit"));
				} else {
					user = randomItem(lesMembres);
					if (bot.DEBUG) console.log("    Le chasseur tue au hasard : " + user.username);
					evenementBulle(datas, "mort.svg", "<b>Le chasseur | " + usernameChasseur + "</b> décède et assassine au hasard <b>" + user.username + "</b> (" + datas.LOUP.lesJoueurs.get(user)[0] + ")"); // Pour les logs
					await messageChasseur.edit(new Discord.MessageEmbed().setTitle("Tir du chasseur").setDescription("Alors que la mort vous traverse vous tirez un coup de fusil. De façon inattendu le coup touche et tue : **" + user.username + "**.").setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "role.js", "tirChasseur", "edit"));
				}

				datas.LOUP.roles.chasseur = undefined;

				datas.LOUP.salons.channelTextuelVillage.send(new Discord.MessageEmbed().setTitle(":skull: " + user.username + " était... :skull:")).then(function (message) {
					setTimeout(async function () {
						message.delete().catch(error => gestionErreures(error, "role.js", "tirChasseur", "delete"));
						await retirerJoueur(user, guildId);
						resolve();
					}, datas.LOUP.temps.annonceRolePersonneElimine * 1000);
				}).catch(error => gestionErreures(error, "role.js", "tirChasseur", "send"));
			});
		}).catch(error => gestionErreures(error, "role.js", "tirChasseur", "send"));
		if (bot.DEBUG) console.log("== Fin de l'action du chasseur ==");
	});
}

/**
 * Permettre au capitaine, lors de sa mort, de désigner un nouveau capitaine qui prendra le relai
 *
 * @param {Number} guildId L'identifiant du serveur courant
 * @return {Promise}
 */
function definirSuccesseurCapitaine(guildId) {
	if (bot.DEBUG) console.log("== Début de la succession capitaine ==");
	return new Promise(async function (resolve) {
		const datas                                = obtenirDatasCourante(guildId);
		const [textEmbed, lesMembres, listeEmojis] = getListeVillageAvecLettres(guildId, false, true); // On retire le capitaine de la liste des successeurs possibles alors ...)
		const capi                                 = datas.LOUP.roles.capitaine;

		await datas.LOUP.salons.channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("À " + capi.username + " de choisir !").setDescription("Choisissez un successeur parmi :\n\n" + textEmbed).addField("Temps restant", datas.LOUP.temps.maxChoixSuccesseur + "s").setFooter("Seul votre choix compte, à la fin du décompte, il seras validé").setColor(couleurEmbedVert)).then(async function (message) {
			let cpt   = 0;
			let timer = setInterval(function () {
				message.edit(new Discord.MessageEmbed().setTitle("À " + capi.username + " de choisir !").setDescription("Choisissez un successeur parmi :\n\n" + textEmbed).addField("Temps restant", (datas.LOUP.temps.maxChoixSuccesseur - cpt) + "s").setFooter("Seul votre choix compte, à la fin du décompte, il seras validé").setColor(couleurEmbedVert)).catch(error => gestionErreures(error, "role.js", "definirSuccesseurCapitaine", "edit"));
				if (cpt >= datas.LOUP.temps.maxChoixSuccesseur) clearInterval(timer);
				cpt++;
			}, 1000);

			const filter    = (reaction, user) => listeEmojis.includes(reaction.emoji.name) && user == capi;
			const collector = message.createReactionCollector(filter, { time: datas.LOUP.temps.maxChoixSuccesseur * 1000 });

			for (let j = 0; j < lesMembres.length; j++) {
				await message.react(LETTERS.get(j)).catch(error => gestionErreures(error, "roles.js", "definirSuccesseurCapitaine", "react")); // On réagi après avoir déclaré le collector
			}

			collector.on("end", async function (collected, reason) { // Collected est le map des emojis collectés
				clearInterval(timer);
				let successeur; // Si le capitaine ne choisis persone, user reste à undefined
				if (collected.size == 1) { // Un choix de successeur
					successeur = lesMembres[listeEmojis.indexOf(collected.firstKey())];
					if (bot.DEBUG) console.log("    Le capitaine a choisi : " + successeur.username);
				} else if (collected.size > 1) { // Plusieurs choix de successeurs
					successeur = lesMembres[listeEmojis.indexOf(collected.randomKey())];
					if (bot.DEBUG) console.log("    Le capitaine a choisi au hasard parmi : " + successeur.username);
				}


				let memberCap = datas.LOUP.lesJoueurs.get(capi)[2];
				if (!memberCap.hasPermission("ADMINISTRATOR")) memberCap.setNickname(memberCap.nickname.substr(1)).catch(error => gestionErreures(error, "deroulement.js", "definirSuccesseurCapitaine", "setNickname"));

				await message.delete().catch(error => gestionErreures(error, "role.js", "definirSuccesseurCapitaine", "delete"));
				datas.LOUP.roles.capitaine = successeur;
				enregistrerModifsDatas(datas); // On enregistre le nouveau capitaine

				if (successeur) {
					await datas.LOUP.salons.channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("C'est " + successeur.username + " qui à été choisi").setDescription("Bravo pour ton élection, la médaille te reviens : soit digne de ce rôle !").setImage(lesRoles.get("Capitaine").get("custom")).setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "role.js", "definirSuccesseurCapitaine", "send"));
					evenement(datas, "capitaine.svg", "Passation de pouvoir", "liserai-vert", "<b>Le capitaine | " + capi.username + "</b> étant décédé, il désigne <b>" + successeur.username + "</b> en tant que successeur"); // Pour les logs

					memberCap = datas.LOUP.lesJoueurs.get(successeur)[2];
					if (memberCap.hasPermission("ADMINISTRATOR")) {
						memberCap.send((new Discord.MessageEmbed().setTitle("Médaille").setDescription("Nous vous décernons cette médaille de Capitaine").setImage(lesRoles.get("Capitaine").get("custom")).setColor(couleurEmbedBleu).setFooter("Tu as trop de droits, je ne peux pas te la passer autour du cou malheureusement !"))).catch(error => gestionErreures(error, "role.js", "definirSuccesseurCapitaine", "send"));
					} else if (memberCap.nickname) {
						memberCap.setNickname("🎖️" + memberCap.nickname).catch(error => gestionErreures(error, "deroulement.js", "definirSuccesseurCapitaine", "setNickname"));
					} else {
						memberCap.setNickname("🎖️" + memberCap.user.username).catch(error => gestionErreures(error, "deroulement.js", "definirSuccesseurCapitaine", "setNickname"));
					}
				} else {
					await datas.LOUP.salons.channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Aucun successeur n'a été désigné").setDescription("Malheureusement, " + capi.username + " est décédé(e) avant d'avoir pu nommer un successeur.\nLe village à donc décider de ne pas attribuer ce rôle à un autre joueur pour le moment").setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "role.js", "definirSuccesseurCapitaine", "send"));
					evenement(datas, "capitaine.svg", "Passation de pouvoir", "liserai-rouge", "<b>Le capitaine | " + capi.username + "</b> est décédé(e) mais ne désigne pas de successeur"); // Pour les logs
				}
				resolve();
			});
		}).catch(error => gestionErreures(error, "role.js", "definirSuccesseurCapitaine", "send"));
		if (bot.DEBUG) console.log("== Fin de la succession capitaine ==");
	});
}

//==============================================================================
// CUPIDON
//==============================================================================

/**
 * Jouer le tour de Cupidon appelé la première nuit du jeu
 *
 * @param {Number} guildId L'identifiant du serveur courant
 * @return {Promise} Envoie des messages aux amoureux et au village pour indiquer l'avancée du choix de cupidon. Cette fonction retourne une Promise après avoir envoyé un message
 */
async function tourCupidon(guildId) {
	if (bot.DEBUG) console.log("  == Début du tour de cupidon ==");
	const datas = obtenirDatasCourante(guildId);
	await datas.LOUP.salons.channelTextuelVillage.send("```Cupidon se réveille !```").catch(error => gestionErreures(error, "role.js", "tourCupidon", "send"));
	const [textEmbed, lesMembres, listeEmojis] = getListeVillageAvecLettres(guildId);

	await datas.LOUP.salons.channelTextuelCupidon.send(new Discord.MessageEmbed().setTitle("Vote pour les amoureux").setDescription(textEmbed).addField("Temps restant", datas.LOUP.temps.choixCupidon + "s").setFooter("Vote pour les deux personnes à lier par l'amour").setColor(couleurEmbedVert)).then(await function (message) {
		return new Promise(async function (resolve) {

			let cpt   = 0;
			let timer = setInterval(async function () {
				cpt++;
				await message.edit(new Discord.MessageEmbed().setTitle("Vote pour les amoureux").setDescription(textEmbed).addField("Temps restant", (datas.LOUP.temps.choixCupidon - cpt) + "s").setFooter("Vote pour les deux personnes à lier par l'amour").setColor(couleurEmbedVert)).catch(error => gestionErreures(error, "role.js", "tourCupidon", "edit"));
			}, 1000);

			const filter    = (reaction, user) => listeEmojis.includes(reaction.emoji.name) && !user.bot; // On s'assure que l'emoji soit bien un proposé par le bot et que ce ne soit pas la réaction du bot
			const collector = message.createReactionCollector(filter, { time: datas.LOUP.temps.choixCupidon * 1000 });

			for (let i = 0; i < datas.LOUP.lesJoueurs.size; i++) {
				await message.react(LETTERS.get(i)).catch(error => gestionErreures(error, "roles.js", "tourCupidon", "react")); // On réagi après avoir déclaré le collector
			}

			collector.on("end", async function (collected) {
				clearInterval(timer);
				if (collected.size < 2) {
					await message.delete().catch(error => gestionErreures(error, "role.js", "tourCupidon", "delete"));
					await datas.LOUP.salons.channelTextuelCupidon.send(new Discord.MessageEmbed().setTitle("Vote pour les amoureux").setDescription("Vous n'avez pas choisi deux amoureux parmi les propositions : tampis !").setFooter("Il n'y aura donc aucun amoureux dans cette partie").setColor(couleurEmbedRouge)).catch(error => gestionErreures(error, "role.js", "tourCupidon", "send"));
					if (bot.DEBUG) console.log("    Cupidon n'a pas voté pour les amoureux");
					evenementBulle(datas, "amoureux.svg", "<b>Cupidon</b> joue mais ne désigne pas d'amoureux"); // Pour les logs
				} else {
					let choix1, choix2; // Les deux amoureux

					if (collected.size == 2) {
						choix1 = lesMembres[listeEmojis.indexOf(collected.firstKey())]; // Le User correspondant à la lettre choisie
						choix2 = lesMembres[listeEmojis.indexOf(collected.lastKey())]; // Le User correspondant à la lettre choisie
						if (bot.DEBUG) console.log("    Cupidon a voté pour deux personnes : " + choix1.username + " et " + choix2.username);
					} else { // collected.size > 2
						if (bot.DEBUG) console.log("    Cupidon a voté pour plus deux personnes, sont choisies: " + choix1.username + " et " + choix2.username);
						choix1 = lesMembres[listeEmojis.indexOf(collected.randomKey())]; // Le User choisi au hasard parmis les lettres votées
						choix2 = lesMembres[listeEmojis.indexOf(collected.randomKey())]; // Le User choisi au hasard parmis les lettres votées
						while (choix1 == choix2) {
							choix2 = lesMembres[listeEmojis.indexOf(collected.randomKey())]; // Le User est RE-choisi au hasard parmis les lettres votées
						}
					}

					evenementBulle(datas, "amoureux.svg", "<b>Cupidon</b> joue et désigne deux amoureux"); // Pour les logs
					evenementNonLineaire(datas, [["chat.svg", "<b>" + choix1.username + "</b> est notifié(e)"], ["chat.svg", "<b>" + choix2.username + "</b> est notifié(e)"]]); // Pour les logs

					await datas.LOUP.salons.channelTextuelCupidon.send(new Discord.MessageEmbed().setTitle("Vote pour les amoureux").setDescription("Vous avez choisi de lier **" + choix1.username + "** et **" + choix2.username + "**").setFooter("Ils recevront un message pour être informés de leur attirance soudaine").setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "role.js", "tourCupidon", "send"));
					await datas.LOUP.salons.channelTextuelVillage.send("```Les amoureux se réveillent et reçoivent un message de leur amant sur leur smartphone, en prennent connaissance et se rendorment !```").catch(error => gestionErreures(error, "role.js", "tourCupidon", "send"));
					await datas.LOUP.lesJoueurs.get(choix1)[2].send("```Coucou chéri(e),\n\nJe te laisse ce petit message nocturne pour te dire que je t'aime à la vie à la mort !\n\n" + choix2.username + "```").catch(error => gestionErreures(error, "role.js", "tourCupidon", "send"));
					await datas.LOUP.lesJoueurs.get(choix2)[2].send("```Coucou chéri(e),\n\nJe te laisse ce petit message nocturne pour te dire que je t'aime à la vie à la mort !\n\n" + choix1.username + "```").catch(error => gestionErreures(error, "role.js", "tourCupidon", "send"));
					if (bot.DEBUG) console.log("    Les amoureux ont été notifié");

					datas.LOUP.roles.lesAmoureux = [choix1, choix2];
				}

				await datas.LOUP.salons.channelTextuelVillage.send("```Cupidon se rendort```").catch(error => gestionErreures(error, "role.js", "tourCupidon", "send"));
				resolve();
			});
		});
		if (bot.DEBUG) console.log("  == Fin du tour de cupidon ==");
	}).catch(error => gestionErreures(error, "role.js", "tourCupidon", "send"));
}

//==============================================================================
// MEDIUM
//==============================================================================

/**
 * Choisi un pseudo au hasard pour tous les joueurs morts de sorte à les anonymiser dans le cas de l'envoie de message au medium
 *
 * @param {JsonObject} datas Les données du serveur courant
 * @return {Void}
 */
function attribuerNomMort(datas) {
	if (bot.DEBUG) console.log("    == Début attribution du nom des morts ==");
	let numEsprit = [];
	for (const [user, liste] of datas.LOUP.lesMorts) {
		let randomNum = Math.floor(Math.random() * (datas.LOUP.lesMorts.size)) + 1;
		while (numEsprit.includes(randomNum)) {
			randomNum = Math.floor(Math.random() * (datas.LOUP.lesMorts.size)) + 1;
		}
		numEsprit.push();
		if (bot.DEBUG) console.log("      Joueur : " + user.username + ", nom de mort : esprit" + randomNum);
		datas.LOUP.lesMorts.set(user, ["esprit" + randomNum, liste[1], liste[2]]);
	}
	enregistrerModifsDatas(datas);
	if (bot.DEBUG) console.log("    == Fin attribution du nom des morts ==");
}

/**
 * Les actions du médium lors du commencement de la nuit
 *
 * @param {JsonObject} datas Les données du serveur courant
 * @return {Void | Message} Envoie un/des messages dans le cas ou des joueurs morts lui en envoie et rien sinon
 */
function tourMediumDebutNuit(datas) {
	if (bot.DEBUG) console.log("  == Début du début tour du médium ==");
	attribuerNomMort(datas);
	changerPermissionMediumMort(true, datas);
	const filter    = m => m.content != "" && !m.author.bot;
	collectorMedium = datas.LOUP.salons.channelTextuelMediumMort.createMessageCollector(filter); // Débuter le collecteur
	collectorMedium.on("collect", m => {
		datas.LOUP.salons.channelTextuelMedium.send(datas.LOUP.lesMorts.get(m.author)[0] + " : " + m.content).catch(error => gestionErreures(error, "role.js", "tourMediumDebutNuit", "send"));
	});
	if (bot.DEBUG) console.log("  == Fin du début tour du médium ==");
}

/**
 * Les actions du médium lors de la fin de la nuit
 *
 * @param {JsonObject} datas Les données du serveur courant
 * @return {Void}
 */
function tourMediumFinNuit(datas) {
	if (bot.DEBUG) console.log("  == Début de la fin du tour du médium ==");
	changerPermissionMediumMort(false, datas);
	collectorMedium.stop("fin nuit"); // Stopper le collecteur
	collectorMedium.on("stop", function (collected) {
		separateur(datas); // Pour les logs
		evenementBulle(datas, "medium.png", "<b>Le Médium</b> a reçu " + collected.size + " messages"); // Pour les logs
	});
	if (bot.DEBUG) console.log("  == Fin de la fin du tour du médium ==");
}

//==============================================================================
// TOUS LES RÔLES
//==============================================================================

/**
 * Création d'un channel textuel pour un role du loup-garou partagé à tous les joueurs ayant ce rôle
 *
 * @param {String} roleparticulier    L'intitulé du role à expliquer
 * @param {TextChannel} channelParticulier  Un channel spécifique dans lequel envoyer l'info -> utile pour l'envoyer à une seule personne qui demanderais des infos
 * @return {Promise}
 */
function expliquerRole(roleparticulier, channelParticulier) {
	if (bot.DEBUG) console.log("== Début de l'explication des rôles ==");
	return new Promise(async function (resolve) {
		switch (roleparticulier) {
			case "Loup-garou":
				if (bot.DEBUG) console.log("  Explication Loup-garou");
				await channelParticulier.send(new Discord.MessageEmbed().setTitle("Loups-Garous").setColor(couleurEmbedBleu).setThumbnail(lesRoles.get("Loup-garou").get("custom"))
					.setDescription("Chaque nuit, **ils dévorent** un Villageois. Le jour, ils essaient de **masquer leur identité nocturne** pour échapper à la vindicte populaire. Ils sont 1 à 6 suivant le nombre de joueurs.")
					.addField("Spécificité", "En aucun cas un Loup-Garou ne peut dévorer un autre Loup-Garou."))
					.catch(error => gestionErreures(error, "role.js", "expliquerRole", "send"));
				break;
			case "Villageois":
				if (bot.DEBUG) console.log("  Explication Villageois");
				await channelParticulier.send(new Discord.MessageEmbed().setTitle("Villageois").setColor(couleurEmbedBleu).setThumbnail(lesRoles.get("Villageois").get("custom"))
					.setDescription("Ses seules armes sont la **capacité d’analyse** des comportements pour identifier les Loups-Garous, et la **force de conviction** pour empêcher l’exécution de l’innocent qu’il est.")
					.addField("Spécificité", "Il n’a aucune compétence particulière."))
					.catch(error => gestionErreures(error, "role.js", "expliquerRole", "send"));
				break;
			case "Petite-fille":
				if (bot.DEBUG) console.log("  Explication Petite-fille");
				await channelParticulier.send(new Discord.MessageEmbed().setTitle("La Petite-Fille").setColor(couleurEmbedBleu).setThumbnail(lesRoles.get("Petite-fille").get("custom"))
					.setDescription("La Petite-Fille peut **espionner les Loups-Garous (uniquement) pendant leur réveil**.")
					.addField("Spécificité", "Pour les besoins de notre adaptation, la Petite-fille a accès à un salon textuel supplémentaire qui lui permettra, lors de la séquence des Loups-Garous, d'observer les discussions de ces derniers **sans connaitre leur identité**.\n Soyez attentif, une rédaction particulière pourrait trahir son auteur :wink:."))
					.catch(error => gestionErreures(error, "role.js", "expliquerRole", "send"));
				break;
			case "Chasseur":
				if (bot.DEBUG) console.log("  Explication Chasseur");
				await channelParticulier.send(new Discord.MessageEmbed().setTitle("Chasseur").setColor(couleurEmbedBleu).setThumbnail(lesRoles.get("Chasseur").get("custom"))
					.setDescription("S’il se fait dévorer par les Loups-Garous ou exécuter malencontreusement par les joueurs, le Chasseur **doit répliquer** avant de rendre l’âme, en éliminant immédiatement n’importe quel autre joueur de son choix.")
					.addField("Spécificité", "Il a un fusil :gun:."))
					.catch(error => gestionErreures(error, "role.js", "expliquerRole", "send"));
				break;
			case "Cupidon":
				if (bot.DEBUG) console.log("  Explication Cupidon");
				await channelParticulier.send(new Discord.MessageEmbed().setTitle("Cupidon").setColor(couleurEmbedBleu).setThumbnail(lesRoles.get("Cupidon").get("custom"))
					.setDescription("En décochant ses célèbres flèches magiques, Cupidon a le pouvoir de rendre **2 personnes amoureuses à jamais**.")
					.addField("La première nuit", "Il **désigne** les 2 joueurs amoureux. Cupidon peut, s’il le veut, se désigner comme l’un des deux amoureux.")
					.addField("Spécificité", "Si l’un des amoureux est éliminé, **l’autre meurt de chagrin** immédiatement. Un amoureux ne doit jamais voter contre son aimé, ni lui porter aucun préjudice **(même pour faire semblant !)**.")
					.addField("Attention", "Attention : si l’un des deux amoureux est un **Loup-Garou** et l’autre un **Villageois**, le but de la partie change pour eux. Pour vivre en paix leur amour et gagner la partie, ils doivent éliminer tous les autres joueurs, Loups-Garous et Villageois, en respectant les règles de jeu."))
					.catch(error => gestionErreures(error, "role.js", "expliquerRole", "send"));
				break;
			case "Sorciere":
				if (bot.DEBUG) console.log("  Explication Sorcière");
				await channelParticulier.send(new Discord.MessageEmbed().setTitle("Sorcière").setColor(couleurEmbedBleu).setThumbnail(lesRoles.get("Sorciere").get("custom"))
					.setDescription("Elle sait concocter 2 potions extrêmement puissantes :\n• Une potion de guérison, pour ressusciter le joueur tué par les Loups-Garous\n• Une potion d’empoisonnement, utilisée la nuit pour éliminer un joueur")
					.addField("Attention", "Elle ne peut utiliser chaque potion **qu'une seule fois dans la partie**. Elle est limitée à l'utilisation d'une seule potion par nuit.")
					.addField("Spécificité", "La Sorcière peut utiliser les potions à son profit, et donc se guérir elle-même si elle vient d’être attaquée par les Loups-Garous ou s'auto-empoisonner."))
					.catch(error => gestionErreures(error, "role.js", "expliquerRole", "send"));
				break;
			case "Voyante":
				if (bot.DEBUG) console.log("  Explication Voyante");
				await channelParticulier.send(new Discord.MessageEmbed().setTitle("Voyante").setColor(couleurEmbedBleu).setThumbnail(lesRoles.get("Voyante").get("custom"))
					.setDescription("Chaque nuit, **elle découvre la vraie personnalité** d’un joueur de son choix. Elle doit aider les autres Villageois, mais rester discrète pour ne pas être démasquée par les Loups-Garous.")
					.addField("Spécificité", "Elle aime les licornes :unicorn:."))
					.catch(error => gestionErreures(error, "role.js", "expliquerRole", "send"));
				break;
			case "Capitaine":
				if (bot.DEBUG) console.log("  Explication Capitaine");
				await channelParticulier.send(new Discord.MessageEmbed().setTitle("Capitaine").setColor(couleurEmbedBleu).setThumbnail(lesRoles.get("Capitaine").get("custom"))
					.setDescription("Cette carte est confiée à un des joueurs, en plus de sa carte personnage. Le Capitaine est élu(e) **par vote à la majorité**. On **ne peut refuser l’honneur** d’être Capitaine.")
					.addField("Spécificité", "Dorénavant, les votes de ce joueur comptent pour 2 voix. Si il se fait éliminer, dans son dernier souffle il désigne son successeur."))
					.catch(error => gestionErreures(error, "role.js", "expliquerRole", "send"));
				break;
			case "Medium":
				if (bot.DEBUG) console.log("  Explication Médium");
				await channelParticulier.send(new Discord.MessageEmbed().setTitle("Medium").setColor(couleurEmbedBleu).setThumbnail(lesRoles.get("Medium").get("custom"))
					.setDescription("Le(a) Médium peut **entendre les morts**. Chaque nuit, les joueurs décédés pourront parler avec lui(elle) **sans réponse de sa part**")
					.addField("Spécificité", "Une petite partie de ouija ?"))
					.catch(error => gestionErreures(error, "role.js", "expliquerRole", "send"));
				break;
			default:
				if (bot.DEBUG) console.log("  Explication rôle non reconnu");
				await channelParticulier.send(new Discord.MessageEmbed().setTitle("Rôle non reconnu").setDescription("Je ne reconnais pas le rôle nommé : \n**" + roleparticulier + "**").setFooter("Prévenez le modérateur si cela vous semble anormal :thumbup:").setColor(couleurEmbedRouge).setThumbnail("http://icons.iconarchive.com/icons/paomedia/small-n-flat/256/sign-error-icon.png"))
					.catch(error => gestionErreures(error, "role.js", "expliquerRole", "send"));
		}
		if (bot.DEBUG) console.log("== Fin de l'explication des rôles ==");
		resolve();
	});
}

/**
 * Retourne la liste des joueurs avec les lettre bleues à coté (pour les votes)
 *
 * @param {Number} guildId L'identifiant du serveur courant
 * @param {Boolean} [loups=false] Indique s'il faut retirer les loups de la liste retournée
 * @param {Boolean} [capi=false]  Indique s'il faut retirer le capitaine de la liste retournée
 * @param {Boolean} [chas=false]  Indique s'il faut retirer le chasseur de la liste retournée
 * @return {List<String, List<User>, List<String>>} Retourne une liste de trois infos, un string associant une lettre (sous forme d'émoji) avec le nom d'un participant, une liste des User listés dans le message, une liste des émojis utilisés
 */
function getListeVillageAvecLettres(guildId, loups = false, capi = false, chas = false) {
	if (bot.DEBUG) console.log("    == Début association lettre utilisateur ==");
	const datas     = obtenirDatasCourante(guildId);
	let textEmbed   = "";
	let i           = 0;
	let lesMembres  = [];
	let listeEmojis = []; // La liste des emojis acceptés pour le filter

	for (const [user, liste] of datas.LOUP.lesJoueurs) { // !loups = on les gardes
		if ((user != datas.LOUP.roles.capitaine || !capi) && (liste[0] != "Loup-garou" || !loups) && (liste[0] != "Chasseur" || !chas)) { // Si loups est à true, on les retire de la liste finale des joueurs. Pareil pour capi et chas
			textEmbed += LETTERS.get(i) + " " + user.username + "\n";
			lesMembres.push(user); // User
			listeEmojis.push(LETTERS.get(i));
			if (bot.DEBUG) console.log("      Association du joueur : " + user.username + " à " + LETTERS.get(i));
			i++;
		}
	}
	if (bot.DEBUG) console.log("    == Fin association lettre utilisateur ==");
	return [textEmbed, lesMembres, listeEmojis];
}
