/**
 * @file Les commandes du bot pour le module Loup Garou
 * @module loup_garou/commandes.js
 * @author Killian & Xavier
 */


//==============================================================================
// Les commandes du bot pour lancer le jeu
//==============================================================================

const commandesLG = ["play", "start", "stop", "regles", "role", "perms", "unmute", "test"];

/**
 * Traite une commande pour le jeu loup-garou
 *
 * @param {String} message   Le message reçu d'un utilisateur
 * @param {String} commande  La commande reconnu par le ot pour ce module
 */
async function commandeLoupGarou(message, commande) {
	const datas = obtenirDatasCourante(message.guild.id); // On récupère les données pour le serv courant

	switch (commande) {
		case "start":
		case "play":
			if (datas.loupEnCours) return messageErreur(message.channel, "Une partie est déjà en cours !");
			if (datas.musiqueEnCours) return messageErreur(message.channel, "Une musique est en cours, je ne peut pas me connecter en vocal pour l'instant");

			datas.LOUP.salons.channelPrincipal = message.channel;
			datas.LOUP.roles.rolePrincipalBot  = datas.memberBot.roles.cache.find(role => role.managed == true);
			enregistrerModifsDatas(datas);
			lancerPartieLoup(datas);
			break;
		case "stop":
			if (!datas.loupEnCours) return messageErreur(message.channel, "Aucune partie n'est en cours");
			// Si la commande est bien envoyé depuis le serveur ayant lancé la partie et que la personne l'ayant lancé à bien le role requis pour manage le bot
			if (!message.guild.members.cache.find(guilMember => guilMember.user.id == message.author.id).roles.cache.find(role => role.name == "ManageBLG")) return messageErreur(message.channel, "Il te faut un role nommé 'ManageBLG' pour que tu ai le droit d'arrêter une partie avant la fin normale de celle-ci");

			arreterPartieEncours(datas);
			break;
		case "regles":
			await expliquerRegles(message.author);
			break;
		case "role":
			if (!datas.loupEnCours) return messageErreur(message.channel, "Aucune partie n'est en cours");
			if (!datas.LOUP.lesJoueurs.get(message.author)) return messageErreur(message.channel, "Tu n'as pas de rôle pour cette partie !");

			await expliquerRole(datas.LOUP.lesJoueurs.get(message.author)[0], message.author);
			break;
		case "perms":
			datas.LOUP.roles.rolePrincipalBot = datas.memberBot.roles.cache.find(role => role.managed == true);
			enregistrerModifsDatas(datas);
			verifierPermissions(true, message.channel, message.guild.id);
			break;
		case "unmute":
			const member = message.guild.members.cache.get(message.author.id);

			if (!member.voice.channel) return messageErreur(message.channel, "Vous devez être connecté à un salon vocal.");
			if (datas.loupEnCours && datas.LOUP.lesJoueurs.get(member.user) != undefined && member.voice.channel == datas.LOUP.salons.channelVocalVillage) return messageErreur(message.channel, "Vous êtes avec les autres joueurs d'une partie en cours, je ne peut pas vous unmute.");

			member.voice.setMute(false);
			messageValidation(message.channel, "C'est bon !", "Votre voix a bien été rétablie");
			break;
		case "test":
			break;
	}
}


//==============================================================================
// FONCTIONS APPELÉES
//==============================================================================


/**
 * Lancer un sondage permettant de remplir la liste des participants au prochain loup garou
 *
 * @param {JsonObject} datas Les données pour le serveur courant
 */
function lancerPartieLoup(datas) {
	definirTemps(datas); // Donne une valeur à tous les temps du jeu
	datas.loupEnCours = true;
	enregistrerModifsDatas(datas);

	datas.LOUP.salons.channelPrincipal.send(new Discord.MessageEmbed().setTitle("Nouvelle partie").setDescription("Qui veut participer à la prochaine partie de Loup-Garou ?").addField("Temps restant", datas.LOUP.temps.lancementPartie + "s").setColor(couleurEmbedVert)).then(function (sondage) {
		sondage.react("✋").catch(error => gestionErreures(error, "commands.js", "lancerPartieLoup", "react"));// On met une réaction pour faire voter les gens

		let cpt   = 0;
		let timer = setInterval(function () {
			if (cpt == datas.LOUP.temps.lancementPartie) {

				clearInterval(timer); // On arrête le décompte
				const reaction = sondage.reactions.cache.get("✋");

				if ((bot.TEST && reaction.count > 1) || (!bot.TEST && reaction.count > 3)) { // En période de test : au moins une personne. Pas en période de test : au moins 2 personnes.
					if (bot.DEBUG) console.log("====== Début Partie ======");
					let listePersonnes = [];
					for (let elem of reaction.users.cache) {
						if (!elem[1].bot) {
							listePersonnes.push(elem[1]); // On récupère les personnes qui participent
						}
					}
					verifierPositionnerRoleBot(listePersonnes, datas);
					if (!verifierPermissions(false, datas.LOUP.salons.channelPrincipal, datas.guildId))
						return messageErreur(datas.LOUP.salons.channelPrincipal, "Il me manque des droits pour effectuer la partie comme il se doit.\nVous pouvez vous aider de `=ms perms` pour résoudre ce problème."); // Vérification des droits du bot pour être capable de lancer la partie

					datas.LOUP.resume.timerGlobal = setInterval(function () {
						datas.LOUP.resume.valeurTimerGlobal++;
					}, 1000);
					RAZJson(datas.guild);
					choixRoleRandom(listePersonnes, datas); // On assigne les roles
				} else {
					messageErreur(datas.LOUP.salons.channelPrincipal, "Apparemment personne n'a accepté l'invitation.", "😭 Il n'y a aucun volontaire 😭");
					datas.loupEnCours = false;
					enregistrerModifsDatas(datas);
				}
				sondage.delete().catch(error => gestionErreures(error, "commands.js", "lancerPartieLoup", "delete"));
			} else {
				sondage.edit(new Discord.MessageEmbed().setTitle("Nouvelle partie").setDescription("Qui veut participer à la prochaine partie de Loup-Garou ?").addField("Temps restant", (datas.LOUP.temps.lancementPartie - cpt) + "s").setColor(couleurEmbedVert)).catch(error => gestionErreures(error, "commands.js", "lancerPartieLoup", "edit"));
			}
			cpt++;
		}, 1000); // Éxécution toutes les secondes
	}).catch(error => gestionErreures(error, "commands.js", "lancerPartieLoup", "send"));
}

/**
 * Arrête la partie de Loup Garou en cours sur le serveur ayant émis la commande
 *
 * @param {JsonObject} datas Les données pour le serveur courant
 * @return {Void}
 */
async function arreterPartieEncours(datas) {
	console.log("~~~~ Arrêt de la partie ~~~~");

	// Roles
	if (datas.LOUP.stop.roles != []) {
		for (const role of datas.LOUP.stop.roles) {
			if (!role.deleted) await role.delete("Fin de la partie");
		}
	}

	// Messages
	if (datas.LOUP.stop.messages != []) {
		for (const message of datas.LOUP.stop.messages) {
			if (!message.deleted) await message.delete().catch(error => gestionErreures(error, "commands.js", "arreterPartieEncours", "delete"));
		}
	}

	await bot.ngrok.disconnect();
	if (datas.LOUP.serveur) datas.LOUP.serveur.close();

	if (datas.LOUP.roles.capitaine) {
		const member = datas.LOUP.lesJoueurs.get(datas.LOUP.roles.capitaine)[2];
		if (!member.hasPermission("ADMINISTRATOR") && member.nickname && member.nickname[0] == "🎖" && member.nickname.length >= 2) {
			member.setNickname(member.nickname.substr(1)).catch(error => gestionErreures(error, "commands.js", "arreterPartieEncours", "setNickname"));
		}
	}

	// Salons
	if (datas.LOUP.stop.salons != []) {
		for (const salon of datas.LOUP.stop.salons) {
			if (!salon.deleted) await salon.delete("Fin de la partie"); // Le salon a pu être supprimé auparavant comme celui de Cupidon
		}
	}

	// Catégorie
	if (datas.LOUP.categoryChannelLG) await datas.LOUP.categoryChannelLG.delete("Fin de la partie").catch(error => gestionErreures(error, "commands.js", "arreterPartieEncours", "delete"));

	RAZJson(datas.guild);
	supprimerDonnees(datas.guildId, "loup");
	if (bot.DEBUG) console.log("//===================\\\\\n|| Fin de l'histoire ||\n\\\\===================//");
}

/**
 * Détermine si oui ou non le bot a assez de droits pour lancer la partie. Permet aussi d'afficher la liste des droits que possède le bot ainsi que ceux qu'il devrait posséder pour lancer une partie de Loup Garou
 *
 * @param {Boolean} envoyerMessage Indication de la volonté d'envoyer le résultat de la vérification par message
 * @param {TextChannel} salon          Le salon dans lequel envoyer le message
 * @param {Number} guildId        L'identifiant du serveur courant
 * @return {Boolean | Message} Le booleen indique si le bot a assez de droits pour assurer une partie de Loup Garou ou non. Le message est envoyé en cas d'erreur
 */
function verifierPermissions(envoyerMessage, salon, guildId) {
	const datas = obtenirDatasCourante(guildId);
	const role  = datas.LOUP.roles.rolePrincipalBot;
	let res     = true;
	let message = "**Etat possible des permissions**\n:green_square: **Requise et mise**    | :red_square: **Requise et manquante** \n:orange_square: **Non requise et mise** | :blue_square: **Non requise et non mise**\n\n*Le droit administrator peut être requis le temps qu'une optimisation soit effectuée*\n";

	role.permissions.has("ADMINISTRATOR") ? message += ":orange_square: ADMINISTRATOR \n" : message += ":blue_square: ADMINISTRATOR \n";
	role.permissions.has("CREATE_INSTANT_INVITE") ? message += ":orange_square: CREATE_INSTANT_INVITE \n" : message += ":blue_square: CREATE_INSTANT_INVITE \n";
	role.permissions.has("KICK_MEMBERS") ? message += ":orange_square: KICK_MEMBERS \n" : message += ":blue_square: KICK_MEMBERS \n";
	role.permissions.has("BAN_MEMBERS") ? message += ":orange_square: BAN_MEMBERS \n" : message += ":blue_square: BAN_MEMBERS \n";
	if (role.permissions.has("MANAGE_CHANNELS")) {
		message += ":green_square: MANAGE_CHANNELS \n";
	} else {
		res = false;
		message += ":red_square: MANAGE_CHANNELS \n";
	}
	if (role.permissions.has("MANAGE_GUILD")) {
		message += ":green_square: MANAGE_GUILD \n";
	} else {
		res = false;
		message += ":red_square: MANAGE_GUILD \n";
	}
	if (role.permissions.has("ADD_REACTIONS")) {
		message += ":green_square: ADD_REACTIONS \n";
	} else {
		res = false;
		message += ":red_square: ADD_REACTIONS \n";
	}
	role.permissions.has("VIEW_AUDIT_LOG") ? message += ":orange_square: VIEW_AUDIT_LOG \n" : message += ":blue_square: VIEW_AUDIT_LOG \n";
	role.permissions.has("PRIORITY_SPEAKER") ? message += ":orange_square: PRIORITY_SPEAKER \n" : message += ":blue_square: PRIORITY_SPEAKER \n";
	role.permissions.has("STREAM") ? message += ":orange_square: STREAM \n" : message += ":blue_square: STREAM \n";
	if (role.permissions.has("VIEW_CHANNEL")) {
		message += ":green_square: VIEW_CHANNEL \n";
	} else {
		res = false;
		message += ":red_square: VIEW_CHANNEL \n";
	}
	if (role.permissions.has("SEND_MESSAGES")) {
		message += ":green_square: SEND_MESSAGES \n";
	} else {
		res = false;
		message += ":red_square: SEND_MESSAGES \n";
	}
	role.permissions.has("SEND_TTS_MESSAGES") ? message += ":orange_square: SEND_TTS_MESSAGES \n" : message += ":blue_square: SEND_TTS_MESSAGES \n";
	if (role.permissions.has("MANAGE_MESSAGES")) {
		message += ":green_square: MANAGE_MESSAGES \n";
	} else {
		res = false;
		message += ":red_square: MANAGE_MESSAGES \n";
	}
	if (role.permissions.has("EMBED_LINKS")) {
		message += ":green_square: EMBED_LINKS \n";
	} else {
		res = false;
		message += ":red_square: EMBED_LINKS \n";
	}
	if (role.permissions.has("ATTACH_FILES")) {
		message += ":green_square: ATTACH_FILES \n";
	} else {
		res = false;
		message += ":red_square: ATTACH_FILES \n";
	}
	if (role.permissions.has("READ_MESSAGE_HISTORY")) {
		message += ":green_square: READ_MESSAGE_HISTORY \n";
	} else {
		res = false;
		message += ":red_square: READ_MESSAGE_HISTORY \n";
	}
	role.permissions.has("MENTION_EVERYONE") ? message += ":orange_square: MENTION_EVERYONE \n" : message += ":blue_square: MENTION_EVERYONE \n";
	role.permissions.has("USE_EXTERNAL_EMOJIS") ? message += ":orange_square: USE_EXTERNAL_EMOJIS \n" : message += ":blue_square: USE_EXTERNAL_EMOJIS \n";
	// if (role.permissions.has("VIEW_GUILD_INSIGHTS"))  { message += ":green_square: VIEW_GUILD_INSIGHTS \n";}  else {res = false; message += ":red_square: VIEW_GUILD_INSIGHTS \n";}
	if (role.permissions.has("CONNECT")) {
		message += ":green_square: CONNECT \n";
	} else {
		res = false;
		message += ":red_square: CONNECT \n";
	}
	if (role.permissions.has("SPEAK")) {
		message += ":green_square: SPEAK \n";
	} else {
		res = false;
		message += ":red_square: SPEAK \n";
	}
	if (role.permissions.has("MUTE_MEMBERS")) {
		message += ":green_square: MUTE_MEMBERS \n";
	} else {
		res = false;
		message += ":red_square: MUTE_MEMBERS \n";
	}
	role.permissions.has("DEAFEN_MEMBERS") ? message += ":orange_square: DEAFEN_MEMBERS \n" : message += ":blue_square: DEAFEN_MEMBERS \n";
	if (role.permissions.has("MOVE_MEMBERS")) {
		message += ":green_square: MOVE_MEMBERS \n";
	} else {
		res = false;
		message += ":red_square: MOVE_MEMBERS \n";
	}
	role.permissions.has("USE_VAD") ? message += ":orange_square: USE_VAD \n" : message += ":blue_square: USE_VAD \n";
	if (role.permissions.has("CHANGE_NICKNAME")) {
		message += ":green_square: CHANGE_NICKNAME \n";
	} else {
		res = false;
		message += ":red_square: CHANGE_NICKNAME \n";
	}
	if (role.permissions.has("MANAGE_NICKNAMES")) {
		message += ":green_square: MANAGE_NICKNAMES \n";
	} else {
		res = false;
		message += ":red_square: MANAGE_NICKNAMES \n";
	}
	if (role.permissions.has("MANAGE_ROLES")) {
		message += ":green_square: MANAGE_ROLES \n";
	} else {
		res = false;
		message += ":red_square: MANAGE_ROLES \n";
	}
	role.permissions.has("MANAGE_WEBHOOKS") ? message += ":orange_square: MANAGE_WEBHOOKS \n" : message += ":blue_square: MANAGE_WEBHOOKS \n";
	role.permissions.has("MANAGE_EMOJIS") ? message += ":orange_square: MANAGE_EMOJIS \n" : message += ":blue_square: MANAGE_EMOJIS \n";

	if (!role.permissions.has("SEND_MESSAGES")) console.log("Je ne peut pas envoyer de message, je n'ai pas le droit !");

	if (envoyerMessage && role.permissions.has("EMBED_LINKS")) {
		salon.send(new Discord.MessageEmbed().setTitle("Permissions du bot").setDescription(message).setFooter("Tant que vous aurez des carrés rouge, le jeu ne se lancera pas.\nSi après avoir changé les droits du role associé au rôle du bot, cet affichage ne change pas, essayez de kick ce dernier puis de le réinviter.")).catch(error => gestionErreures(error, "commands.js", "verifierPermissions", "send"));
	} else if (envoyerMessage) {
		messageErreur(salon, "Il faut me rajouter le droit 'EMBED_LINKS' pour obtenir cette information.");
	}

	return res;
}

/**
 * Envoie un message expliquant les règles à suivre lors d'une partie de Loup-Garou
 *
 * @param {type} channelParticulier Un channel spécifique dans lequel envoyer les règles (utile pour l'envoyer à une seule personne qui demanderais des infos). Valeur par default sur le salon textuel du village pour donner à tous les règles si aucun salon spécifique n'est précisé
 * @return {type} description
 */
function expliquerRegles(channelParticulier) {
	return new Promise(async function (resolve) {
		await channelParticulier.send(new Discord.MessageEmbed().setTitle("Conditions de victoire").addFields(
			{ name: "Les Villageois", value: "Ils gagnent en éliminant tous les Loups-Garous." },
			{ name: "Les Loups-Garous", value: "Ils gagnent s'ils éliminent le dernier villageois." },
			{ name: "Cas spécial", value: "S'il existe deux amoureux dont l'un est Loup-Garou et l'autre villageois, ils gagnent si tous les autres participants meurent." })
			.setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "commands.js", "expliquerRegles", "send"));

		await channelParticulier.send(new Discord.MessageEmbed().setTitle("Étapes d'une journée").setDescription("Une journée se déroule en plusieurs étapes _(ré-expliquées au cours de la partie)_").addFields(
			{ name: "Tour 1", value: "‣ Le serveur s'endort\n‣ Appel de cupidon _(si présent)_ puis des amoureux\n‣ Le serveur se réveille" },
			{ name: "Tours normaux", value: "‣ Le serveur s'endort\n‣ Les rôles sont appelés 1 par 1 pour effectuer leurs actions\n‣ Le serveur se réveille\n‣ Le serveur débat\n‣ Le serveur vote\n‣ Le serveur se rendort" })
			.setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "commands.js", "expliquerRegles", "send"));

		await channelParticulier.send(new Discord.MessageEmbed().setTitle("Le village")
			.setDescription("Chaque nuit, il est la cible des Loups-Garous. Chaque joueur villageois **dévoré** est éliminé du jeu. Les villageois survivants se réunissent le lendemain matin et essaient de **remarquer**, chez les autres joueurs, **les signes** qui trahiraient leur identité nocturne de mangeur d’homme. Après discussions _(J'ai bien dit **discussion** benjamin)_, ils **votent** contre un suspect qui sera **éliminé** du jeu, jusqu’à la prochaine partie...")
			.addField("Attention", "Sont considérés villageois tous ceux n'ayant pas la carte Loup-garou.")
			.setColor(couleurEmbedBleu)).catch(error => gestionErreures(error, "commands.js", "expliquerRegles", "send"));

		for (let [role, map] of lesRoles) {
			await expliquerRole(role, channelParticulier);
		}

		resolve();
	});
}

/**
 * Donne une valeur à chacune des variables de temps du jeu en fonction de la variable de test du bot
 *
 * @param {JsonObject} datas   Les données du serveur courant
 * @return {Void}
 */
function definirTemps(datas) {
	datas.LOUP.temps = {
		"choixSorciere": 30, // Secondes → Temps laissé à la sorcière pour choisir d'utiliser une potion ou non
		"choixCupidon": 30, // Secondes → Temps laissé à cupidon pour choisir deux amoureux
		"choixVoyante": bot.TEST ? 20 : 30, // Secondes → Temps laissé à la voyante pour choisir un joueur
		"notationPartie": bot.TEST ? 10 : 15, // Secondes → Pour le message de fin de partie permettant de noter de 1 à 5 la partie qui viens de se terminer
		"annonceRolePersonneElimine": 5, // Secondes → Entre l'annonce de la personne éliminée et l'affichage de son role
		"observationPetiteFille": 20, // Secondes → Temps donné à la petite-fille par nuit durant lequel tous les messages des loups lui seront renvoyés par le bot
		"finSondage": bot.TEST ? 10 : 30, // Secondes → Temps laissé pour répondre aux sondages (début de partie, proposition règles)
		"lancementPartie": bot.TEST ? 5 : 40, // Secondes → Temps pour lire les infos reçus en MP sur les rôles (avant de commecer avec l'annonce des règles)
		"annonceResultats": 10, // Secondes → Temps d'affichage des gagants de la partie avant la supression des channels/supression des données de partie
		"debatLoups": bot.TEST ? 10 : 30, // Secondes → Temps disponible pour laisser débatre les loups-garous sur le choix d'une victime.
		"eliminationChasseur": bot.TEST ? 10 : 30, // Secondes → Temps laissé au chasseur pour choisir un joueur
		"lancementJour": bot.TEST ? 5 : 10, // Secondes → Temps entre la fin de la nuit et le lancement du jour et inversement
		"maxChoixSuccesseur": bot.TEST ? 10 : 30, // Secondes → Temps laissé au capitaine lors de sa mort pour désigner un successeur
		"electionCapitaine": bot.TEST ? 10 : 40, // Secondes → Temps disponible pour laisser débatre le serveur à propos du choix du Capitaine
		"voteElimination": bot.TEST ? 15 : 120 // Secondes → Temps pour le vote chaque jour
	};
	enregistrerModifsDatas(datas);
}
