const listeRoles        = ["villageois", "loupGarou", "sorciere", "petiteFille", "voyante", "chasseur", "cupidon", "medium", "autre"];
const tempsParDefautMAJ = 10; // En secondes
var majAuto             = true;
var contrast            = true;

window.onload = function () {
	updateTableau();
};

var chrono = 0;
setInterval(async function () {
	if (chrono < tempsParDefautMAJ) {
		chrono++;
	} else {
		if (majAuto) {
			await updateTableau();
		} else {
			chrono++;
		}
	}
	document.getElementById("outils").lastElementChild.lastElementChild.lastElementChild.innerHTML = "il y a " + chrono + " s.";
}, 1000);

function updateTableau() {
	const idServeur = window.location.search.substring(1).split("=")[1];
	chrono          = 0;
	return new Promise(function (resolve) {
		console.log("update");
		fetch(new Request("temporaire/infoPartie-" + idServeur + ".json"))
			.then(response => response.json())
			.then(data => {
				$("#tableau").empty();
				document.getElementById("outils").children[2].lastElementChild.innerHTML = data.nomServeur;
				for (let role of listeRoles) {
					const tr = document.createElement("tr");

					const td1 = document.createElement("td");
					const img = document.createElement("img");
					if (data.roles[role].image != "") {
						img.src = "./custom/" + data.roles[role].image;
					} else {
						img.src = "./ressources/bug.svg";
					}
					td1.appendChild(img);
					tr.appendChild(td1);

					const td2   = document.createElement("td");
					const p     = document.createElement("p");
					p.innerHTML = data.roles[role].nom;
					td2.appendChild(p);
					tr.appendChild(td2);

					const td3 = document.createElement("td");
					const p2  = document.createElement("p");
					if (data.roles[role].nb > 0) {
						tr.style.backgroundColor = "#00776b";
						p2.innerHTML             = data.roles[role].nb + " restant";
					} else {
						if (data.roles[role].present == 1) {
							tr.style.backgroundColor = "#c0392b";
							p2.innerHTML             = "Tous morts";
						} else {
							tr.style.backgroundColor = "grey";
							p2.innerHTML             = "Non présent";
						}
					}
					td3.appendChild(p2);
					tr.appendChild(td3);
					$("#tableau").append(tr);
				}
				resolve();
			}).catch(error => {
			console.log("Une erreur est arrivé lors de la lecture des informations : Soit la partie est terminée, soit l'identifiant du serveur n'est pas le bon, soit le site a reçu trop de connexions");
		});
	});
}

function MAJauto() {
	if (majAuto) {
		document.getElementById("outils").children[0].lastElementChild.lastElementChild.innerHTML = "Off";
	} else {
		document.getElementById("outils").children[0].lastElementChild.lastElementChild.innerHTML = "On";
	}
	majAuto = !majAuto;
}

function changerContrast() {
	if (contrast) {
		document.body.style.backgroundColor                                               = "#252830";
		document.getElementsByTagName("main")[0].style.backgroundColor                    = "#7e8c8d";
		document.getElementsByTagName("main")[0].nextElementSibling.style.backgroundColor = "#252830";
		document.getElementsByClassName("example")[0].style.backgroundColor               = "#7e8c8d";
		for (let tr of document.getElementById("tableau").children) {
			tr.children[1].style.borderLeft  = "1px solid #7e8c8d";
			tr.children[1].style.borderRight = "1px solid #7e8c8d";
		}
	} else {
		document.body.style.backgroundColor                                               = "#009688";
		document.getElementsByTagName("main")[0].style.backgroundColor                    = "lightgrey";
		document.getElementsByTagName("main")[0].nextElementSibling.style.backgroundColor = "#009688";
		document.getElementsByClassName("example")[0].style.backgroundColor               = "lightgrey";
		for (let tr of document.getElementById("tableau").children) {
			tr.children[1].style.borderLeft  = "1px solid lightgrey";
			tr.children[1].style.borderRight = "1px solid lightgrey";
		}
	}
	contrast = !contrast;
}

function easter() {
	for (let elem of document.getElementsByClassName("card")) {
		elem.style.transform = "rotateY(180deg)";
	}
}

function deEaster() {
	for (let elem of document.getElementsByClassName("card")) {
		elem.style.transform = "rotateY(0deg)";
	}
}
