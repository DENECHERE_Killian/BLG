/**
 * @file Le tableau présentant sur une page web les rôles encore dans la partie de Loup Garou courante
 * @module loup_garou/tableau.js
 * @author Killian & Xavier
 */


/**
 * Initialise le dictionnaire qui sera inscrit dans le fichier infoPartie.json servant à l'affichage du tableau des derniers rôles encore en partie sur la page web
 *
 * @param {Guild} guild La guild courante
 * @return {Void} Écrit simplement dans un fichier json temporaire
 */
function RAZJson(guild) {
	const datas                             = obtenirDatasCourante(guild.id);
	datas.LOUP.resume.infoPartie.nomServeur = guild.name;
	datas.LOUP.resume.infoPartie.roles      = {
		villageois: { nb: 0, present: 0, nom: "Villageoi(s)", image: "Villageois.png" },
		loupGarou: { nb: 0, present: 0, nom: "Loup-Garou(s)", image: "Loup-garou.png" },
		sorciere: { nb: 0, present: 0, nom: "Sorcière", image: "Sorciere.png" },
		petiteFille: { nb: 0, present: 0, nom: "Petite-fille", image: "Petite-fille.png" },
		voyante: { nb: 0, present: 0, nom: "Voyante", image: "Voyante.png" },
		chasseur: { nb: 0, present: 0, nom: "Chasseur", image: "Chasseur.png" },
		cupidon: { nb: 0, present: 0, nom: "Cupidon", image: "Cupidon.png" },
		medium: { nb: 0, present: 0, nom: "Médium", image: "Medium.png" },
		autre: { nb: 0, present: 0, nom: "Autre", image: "../Verso.png" }
	};
	bot.fs.writeFile(generationTableau + "-" + guild.id + ".json", JSON.stringify(datas.LOUP.resume.infoPartie, null, 4), err => { //on écrit dans le fichier 'infoPartie.json' les informations précédentes
		if (err) throw err;
	});
}

/**
 * Créé le tableau des rôles présents dans la partie qui est publié en ligne pour permettre l'accès publique
 *
 * @param {Number} guildId        L'identifiant du serveur courant
 * @return {Promise}
 */
function genererTableauRoles(guildId) {
	return new Promise(async function (resolve, reject) {
		let datas           = obtenirDatasCourante(guildId);
		const portServeur   = 5000;
		datas.LOUP.serveur  = await serveurLocal(portServeur, ["./modules/werewolf/tableau/", "./modules/werewolf/role_pictures/"]);
		const url           = await bot.ngrok.connect(portServeur);
		datas.LOUP.lienSite = url;
		enregistrerModifsDatas(datas);
		console.log(url + "/joueurs.html?id=" + guildId);
		resolve();
	});
}
