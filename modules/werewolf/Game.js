const voiceManager = require("@discordjs/voice");
const Discord      = require("discord.js");
const MyEmbed      = require("../../utilities/MyEmbed");
const Period       = require("./Period");
const Role         = require("./Role");
const _            = require("lodash");

const LOVERS_WINNING  = "amoureux";
const WOLVES_WINNING  = "loups-garou";
const VILLAGE_WINNING = "villageois";

const MAYOR_ELECTION_TIME = process.env.TEST === "true" ? 20 : 50; // seconds available for villagers to vote for a mayor.
const GAME_DELETION_TIME  = process.env.TEST === "true" ? 10 : 30; // seconds available before the game category and all game channels are deleted.
const ROLE_REVEAL_TIME    = 5; // seconds before the morning announcement of the last night killed player(s)'s role.

class Game {

	static MIN_PLAYERS_AMOUNT = process.env.TEST === "true" ? 1 : 4;
	static MAX_PLAYERS_AMOUNT = 18;

	/**
	 * Create a Game object representing the process of a werewolf game.
	 * @param werewolf {Werewolf} The werewolf module this game is taking place.
	 */
	constructor(werewolf) {
		/** @type {Werewolf} */
		this.werewolf = werewolf;
		/** @type {Player[]} */
		this.players = [];
		/** @type {boolean} */
		this.isRunning = false;
		/** @type {Period} */
		this.period = null;
		/** @type {Discord.CategoryChannel} */
		this.channelsCategory = null;

		/** This variable can be left empty in this constructor because it's filled later but this make possible the auto-completion of the IDE. */
		this.channels = {
			/** @type Discord.TextChannel */
			text_outGame: null,
			/** @type Discord.TextChannel */
			text_ghostsOnly: null,
			/** @type Discord.TextChannel */
			text_ghostsMedium: null,
			/** @type Discord.TextChannel */
			text_werewolf: null,
			/** @type Discord.TextChannel */
			text_village: null,
			/** @type Discord.VoiceChannel */
			voice_village: null,
			/** @type Discord.TextChannel */
			text_littleGirl: null,
			/** @type Discord.TextChannel */
			text_cupidon: null,
			/** @type Discord.TextChannel */
			text_medium: null,
			/** @type Discord.TextChannel */
			text_psychic: null,
			/** @type Discord.TextChannel */
			text_witch: null
		};
	}

	// ------------------
	// PLAYERS MANAGEMENT
	// ------------------

	/**
	 * Add a player to this game before launching it.
	 * @param player {Player} A person that will participate in this game. This player does not have any role yet assigned.
	 */
	addPlayer(player) {
		if (!player)
			return null;

		this.players.push(player);
	}

	/**
	 * Get an independent deep copy of the players list.
	 * @returns {Player[]} The deep copied players list.
	 */
	getPlayersDeepCopy() {
		// return JSON.parse(JSON.stringify(this.players));
		return _.cloneDeep(this.players);
	}

	/**
	 * Retrieve the players list that play a certain role in the game.
	 * @param roleTypeId {number} The role type we want to know the player/players.
	 * @returns {?Player[]} A list filled with no player if the role isn't in the game or one or more Players depending on the amount of players on this role.
	 */
	getRoleTypePlayers(roleTypeId) {
		if (!roleTypeId)
			return null;

		return this.players.filter(player => player.role.type.id === roleTypeId);
	}

	/**
	 * Retrieve a player of the current game that plays the role required. If there are multiple players playing this role, it returns one of them.
	 * @param roleTypeId {number} The role type we want to know the player/players.
	 * @returns {?Player} A player that plays with this role on the game. Null if there is no.
	 */
	getOneRoleTypePlayer(roleTypeId) {
		if (!roleTypeId)
			return null;

		const players = this.getRoleTypePlayers(Role.CUPIDON);
		return players ? players.pop() : null;
	}

	/**
	 * Get the alive players list of this game.
	 * Players that are in the way of being killed are not included in this list.
	 * This is the case of night victims players waiting for the day period to start in order to be definitely eliminated.
	 * @returns {Player[]} The list of alive players.
	 */
	getAlivePlayers() {
		return this.players.filter(player => player.isAlive && !player.isNightVictim);
	}

	/**
	 * Get the ghosts players list of this game.
	 * Players that are in the way of being killed are not included in this list.
	 * @returns {Player[]} The list of non-alive players.
	 */
	getNonAlivePlayers() {
		return this.players.filter(player => !player.isAlive);
	}

	/**
	 * Get the in love players that were appointed by Cupidon.
	 * @returns {Player[]} The lover players list. Empty list if there are no lovers yet in this game.
	 */
	getInLovePlayers() {
		return this.players.filter(player => player.isLover);
	}

	/**
	 * Get the list of players that were killed last night.
	 * @returns {Player[]} Last night killed players list. Empty list if there was no victim.
	 */
	getNightVictimsPlayers() {
		return this.players.filter(player => player.isNightVictim);
	}

	/**
	 * Find the Player object associated with a user id of someone playing in this game.
	 * @param userId {Discord.Snowflake} The user id.
	 * @returns {?Player} The Player with this user id, null otherwise.
	 */
	getPlayer(userId) {
		if (!userId)
			return null;

		return this.players.find(p => p.user.id === userId);
	}

	/**
	 * Tell if a discord user identified by its id participate in the game or not.
	 * @param userId {Discord.Snowflake} The {@link Discord.User} id.
	 * @returns {boolean} True if the user play to this game, false otherwise.
	 */
	isUserInGame(userId) {
		return this.players.map(player => player.user.id).includes(userId);
	}

	// ----------------
	// ROLES MANAGEMENT
	// ----------------

	/**
	 * Select a role for each player of the game and showList them explanations about it.
	 * @returns {boolean} If the assignment was well performed. If False, it's probably because there is not enough roles included in the game.
	 */
	#assignRoles() {
		const playersListCopy = this.getPlayersDeepCopy();

		let werewolvesCount = Math.floor(playersListCopy.length * 0.40);
		let villagersCount  = (werewolvesCount - 1) > 0 ? (werewolvesCount - 1) : 1; // 1 villager at minimum.
		let onePlayerRoles  = [Role.YOUNG_GIRL, Role.HUNTER, Role.CUPIDON, Role.WITCH, Role.PSYCHIC, Role.MEDIUM];

		// Sufficient available roles verification
		if (playersListCopy.length - (werewolvesCount + villagersCount + onePlayerRoles.length) > 0)
			return false;

		// Werewolves & villagers
		while (werewolvesCount > 0) {
			const player = deleteRandomItem(playersListCopy);
			this.players.find(p => p.guildMember.id === player.guildMember.id).setRole(Role.WEREWOLF);
			werewolvesCount--;
		}
		while (villagersCount > 0) {
			const player = deleteRandomItem(playersListCopy);
			this.players.find(p => p.guildMember.id === player.guildMember.id).setRole(Role.VILLAGER);
			villagersCount--;
		}

		// Other roles
		while (playersListCopy.length > 0) {
			const player = deleteRandomItem(playersListCopy);
			this.players.find(p => p.guildMember.id === player.guildMember.id).setRole(deleteRandomItem(onePlayerRoles));
		}

		this.players.forEach(async player => await player.explainRole());

		return true;
	}

	/**
	 * Get the roles list assigned for this game.
	 * @param [uniqueValues=false] {boolean} If the result list should contain unique values or not.
	 * @returns {Role[]} The roles list that can contains several times the same role.
	 */
	getAssignedRoles(uniqueValues = false) {
		const rolesList = this.players.map(player => player.role);
		if (uniqueValues)
			return rolesList.filter((value, index, self) => {
				return self.indexOf(value) === index;
			});

		return rolesList;
	}


	/**
	 * Know if a role is played by someone in this game or not.
	 * @param roleTypeId {number} The role type we want to know if it's played.
	 * @returns {boolean} True if the role type is played by someone in the game.
	 */
	isRoleTypeInGame(roleTypeId) {
		return !!this.players.find(player => player.role.type.id === roleTypeId);
	}

	/**
	 * Tell if there is at least one player of this role type that is still alive in the current game.
	 * @param roleTypeId {number} The role type we want to know if a player is still alive with.
	 * @returns {boolean} True if at least one player if this role type is still alive in the game.
	 */
	isRoleTypePlayerAlive(roleTypeId) {
		if (!this.isRoleTypeInGame(roleTypeId))
			return false;

		return this.getRoleTypePlayers(roleTypeId).some(p => p.isAlive);
	}

	/**
	 * Tell if a specific role type action can be launched because at least one player of this role can play.
	 * @param roleTypeId {number} The role type we want to know if its action can be played.
	 * @returns {boolean} True if this role type action can be played, false otherwise.
	 */
	roleTypeCanPlay(roleTypeId) {
		return this.isRoleTypeInGame(roleTypeId) && this.isRoleTypePlayerAlive(roleTypeId);
	}

	// -----------
	// GAME EVENTS
	// -----------

	/**
	 * Start a new game by sending a poll to ask for players.
	 * @param interaction {Discord.CommandInteraction} The command interaction used to start the poll.
	 * @returns {?Promise<void>}
	 */
	async startGame(interaction) {
		if (!this.#assignRoles()) {
			// noinspection JSValidateTypes,JSCheckFunctionSignatures
			await interaction.editReply({ embeds: [new MyEmbed("Il n'y a pas assez de rôles pour le nombre de joueurs actuel :/", Utils.RED_COLOR)] });
			return null;
		}

		this.channels.text_outGame = interaction.channel;
		this.isRunning             = true;

		await this.#createGameEnvironment();

		const pollContent = new MyEmbed(`Connecte toi au salon \`🔊 ${this.channels.voice_village.name}\` et regarde le rôle qui t'a été attribué.`).desc(`${this.werewolf.getGuild().roles.everyone}\n${this.players.map(player => `🟥 ${player.name}`).toString().replaceAll(",", "\n")}`);
		const poll        = await pollContent.allowMentions().send(this.channels.text_village, [
			new Discord.MessageButton().setCustomId("ready_button").setLabel("Je suis prêt(e)").setEmoji(`✔️`).setStyle("SUCCESS"),
			new Discord.MessageButton().setCustomId("rules_button").setLabel("Rappel des règles").setEmoji(`📜`).setStyle("PRIMARY"),
			new Discord.MessageButton().setCustomId("stop_button").setLabel("STOP").setEmoji(`❌`).setStyle("DANGER")
		]);

		let readyPlayersId = [];
		const filter       = (interaction) => this.isUserInGame(interaction.user.id);
		const collector    = poll.createMessageComponentCollector({ filter });

		collector.on("collect", async i => {
			if (i.customId === "rules_button")
				return (await i.user.createDM()).send({ embeds: this.rulesToEmbeds() });

			if (i.customId === "stop_button")
				return collector.stop("stop_button");

			if (readyPlayersId.includes(interaction.user.id))
				return null;

			readyPlayersId.push(i.user.id);
			await poll.edit({ embeds: [pollContent.desc(this.players.map(player => i.user.id === player.user.id ? `🟩 ${player.name}` : `🟥 ${player.name}`).toString().replaceAll(",", "\n"))] });

			if (readyPlayersId.length === this.players.length)
				collector.stop("all_ready");
		});

		collector.on("end", async (collected, reason) => {
			if (reason === "stop_button") {
				console.log("The game was aborted (stop_button).");
				await poll.edit({ embeds: [new MyEmbed("Partie arrêtée", Utils.ORANGE_COLOR)], components: [] }).then(deleteMessage);
				return this.endGame();
			}

			console.log("Everyone is ready, starting story.");
			await poll.edit({ embeds: [new MyEmbed("La partie va commencer.", Utils.GREEN_COLOR)], components: [] }).then(deleteMessage);

			this.period = new Period(this, Period.FIRST_NIGHT);
			// noinspection ES6MissingAwait
			this.period.start();
		});
	}

	/**
	 * Reset the parameters that need to be reset (not all attributes are reset because this Object will be erased after the end of the game).
	 * Also send the game result message to the game starting channel.
	 */
	async endGame() {
		const winner = this.getWinningTeam();

		const message = await new MyEmbed(`La partie est terminée !`, Utils.GREEN_COLOR).desc(`Les résultats se trouvent dans le salon ${this.channels.text_outGame}.\nMerci d'avoir joué !`).send(this.channels.text_village);
		MyEmbed.showTimer(message, "Les salons seront supprimés dans :", GAME_DELETION_TIME);

		if (winner) {
			console.log(`The game was won by "${winner}"`);
			const result = this.#getWinnersAndLosers();
			await new MyEmbed(`Les gagnants sont les **${winner}** !`, Utils.GREEN_COLOR).desc(`**Toute mes félicitations à:**\n${result.winners.toString().replaceAll(",", "\n")}${result.losers.length > 0 ? `\n\n**Ah oui, sans oublier les perdants :**\n${result.losers.toString().replaceAll(",", "\n")}` : ""}`).send(this.channels.text_outGame);
		} else {
			console.log("The game ended and no one won.");
			await new MyEmbed(`Personne n'a gagné`, Utils.ORANGE_COLOR).desc(`Merci d'avoir participé ! Voici les rôles :\n${this.players.toString().replaceAll(",", "\n")}`).send(this.channels.text_outGame);
		}

		this.players.forEach(player => {
			player.setMute(false);
			player.setMayor(false); // To reset the nickname of the real mayor.
		});

		this.isRunning = false;
		setTimeout(this.#deleteGameEnvironment.bind(this), GAME_DELETION_TIME * 1000);
	}

	/**
	 * Announce the victims of the night and perform some actions depending on their role.
	 * @returns {Promise<null>}
	 */
	async nightVictimsAdvert() {
		const victims = this.getNightVictimsPlayers();
		if (victims.length === 0) {
			await new MyEmbed(`Cette nuit, **personne n'est mort**`, Utils.BLUE_COLOR).send(this.channels.text_village);
			return null;
		}

		const embed   = new MyEmbed(`Cette nuit, **${victims.length}** ${victims.length === 1 ? "a disparu" : "ont disparues"}`).desc(`${victims.length === 1 ? "Et c'est ..." : "Et ce sont ..."}`);
		const message = await embed.send(this.channels.text_village);
		MyEmbed.showTimer(message, `Réponse dans`, ROLE_REVEAL_TIME);
		await setTimeout(async () => {
			// await message.edit({ embeds: [embed.desc(victims.toString().replaceAll(",", "\n"))] });

			for (const player of victims)
				await player.kill();

		}, ROLE_REVEAL_TIME * 1000);
	}

	/**
	 * Allow the village to select a mayor between them.
	 * @returns {Promise<unknown>}
	 */
	mayorVote() {
		return new Promise(async resolve => {
			const villagers      = this.getAlivePlayers();
			const villagersVotes = {};
			villagers.forEach(p => villagersVotes[p.user.id] = { mayorId: null, validatedVote: false });

			// noinspection JSUnusedAssignment,JSCheckFunctionSignatures
			const poll = await this.channels.text_village.send({
				embeds: [new MyEmbed(`Choix du maire`, Utils.WHITE_COLOR).desc(`Le maire possède un vote équivalant à 2 villageois lors des débats.\n${villagers.map(player => `🟥 ${player.name} n'a pas voté`).toString().replaceAll(",", "\n")}`)],
				components: [
					new Discord.MessageActionRow().addComponents([
						new Discord.MessageSelectMenu({
							customId: "select_player",
							placeholder: "Je vote pour ...",
							maxValues: 1,
							options: villagers.map(p => p = { label: p.name, value: p.user.id })
						})])
					// new Discord.MessageActionRow().addComponents([
					//     new Discord.MessageButton({
					//         customId: 'validate_button',
					//         label: "Je valide mon vote",
					//         style: 'PRIMARY'
					//     })
					// ])
				]
			});

			MyEmbed.showTimer(poll, "Fin du vote dans", MAYOR_ELECTION_TIME);
			const filter    = (interaction) => villagers.map(p => p.user.id).includes(interaction.user.id);
			const collector = poll.createMessageComponentCollector({ filter, time: MAYOR_ELECTION_TIME * 1000 });

			collector.on("collect", i => {
				if (i.isSelectMenu()) {
					// noinspection JSUnresolvedVariable
					villagersVotes[i.user.id] = { mayorId: i.values[0], validatedVote: true };
					// villagersVotes[i.user.id] = { mayorId: i.values[0], validatedVote: false }; // When a button is displayed to validate the vote
				}

				// if (i.isButton() && villagersVotes[i.user.id].mayorId)
				//     villagersVotes[i.user.id].validatedVote = true;

				// poll.edit({ embeds: [new MyEmbed(`Choix du maire`, Utils.WHITE_COLOR).desc(`Le maire possède un vote équivalant à 2 villageois lors des débats.\n${villagers.map(player => villagersVotes[player.user.id].validatedVote ? `🟩 ${player.name} a voté` : `🟥 ${player.name} n'a pas voté`).toString().replaceAll(",", "\n")}`)] });
				poll.edit({ embeds: [new MyEmbed(`Choix du maire`, Utils.WHITE_COLOR).desc(`Le maire possède un vote équivalant à 2 villageois lors des débats.\n${villagers.map(player => villagersVotes[player.user.id].validatedVote ? `🟩 ${player.name} a voté` : `🟥 ${player.name} n'a pas voté`).toString().replaceAll(",", "\n")}`)], components: [] });

				if (villagers.every(player => villagersVotes[player.user.id].validatedVote)) // Everyone voted
					collector.stop("everyone_voted");
			});

			collector.on("end", async () => {
				let candidates = {}; // Each mayor id is assigned to its vote amount
				Object.values(villagersVotes).forEach(vote => {
					if (vote.mayorId && vote.validatedVote)
						candidates[vote.mayorId] ? candidates[vote.mayorId]++ : candidates[vote.mayorId] = 1;
				});

				/** @type Player */
				let mayor;
				if (Object.keys(candidates).length === 0) {
					// Nobody voted, need to randomly choose one.
					mayor = randomItem(villagers);
				} else {
					const maxVoteAmount    = Math.max(...Object.values(candidates));
					const mostVotedPlayers = Object.entries(candidates).filter(v => v[1] === maxVoteAmount);

					// A person was mainly chosen if length is 1.
					// Otherwise, multiples persons were selected, need to randomly choose one
					mayor = mostVotedPlayers.length === 1 ? this.getPlayer(mostVotedPlayers[0][0]) : randomItem(mostVotedPlayers);
				}

				await poll.edit({ embeds: [new MyEmbed(`Choix du maire`, Utils.GREEN_COLOR).desc(`Le vote est clos. Seuls les votes soumis dans les temps ont été pris en compte.\n**Le Maire est ${mayor.name}**`)], components: [] });
				await mayor.setMayor(true);
				resolve();
			});
		});
	}

	async villageVote() {
		// TODO: Village vote
		console.log("Need to implement the village vote");
	}

	// --------------
	// WIN MANAGEMENT
	// --------------

	/**
	 * Tell if the game is in a winning state or not yet.
	 * @returns {boolean} True if the game is won by someone. False otherwise.
	 */
	isWon() {
		return !!this.getWinningTeam();
	}


	/**
	 * Return a winner if there is one in the game, otherwise null.
	 * A result different of null means the game should be ended.
	 * @returns {?string} The winners of the game : it can be a {@link LOVERS_WINNING}, {@link WOLVES_WINNING} or {@link VILLAGE_WINNING}.
	 */
	getWinningTeam() {
		const alivePlayers = this.getAlivePlayers();

		if (alivePlayers.length === 2 && alivePlayers.every(player => player.isLover))
			return LOVERS_WINNING;

		if (alivePlayers.every(player => !player.role.type.isVillageFriendly))
			return WOLVES_WINNING;

		if (alivePlayers.every(player => player.role.type.isVillageFriendly))
			return VILLAGE_WINNING;

		return null;
	}


	/**
	 * Get the list of players that won or loose the game. Null if the game is not won already.
	 * @returns {?{winners: Player[], losers: Player[]}}
	 */
	#getWinnersAndLosers() {
		const winner = this.getWinningTeam();
		/** @type {Player[]} */
		let winners;

		switch (winner) {
			case LOVERS_WINNING:
				winners = this.players.filter(player => player.isLover);
				break;

			case WOLVES_WINNING:
				winners = this.players.filter(player => !player.role.type.isVillageFriendly);
				break;

			case VILLAGE_WINNING:
				winners = this.players.filter(player => player.role.type.isVillageFriendly);
				break;

			default:
				return null;
		}

		return {
			winners: winners,
			losers: removeItems(this.players, winners)
		};
	}


	// ----------------
	// GAME ENVIRONMENT
	// ----------------

	/**
	 * Create a discord channels category to add newly created channels needed to play.
	 * @returns {Promise<void>}
	 */
	async #createGameEnvironment() {
		this.channelsCategory       = await this.werewolf.getGuild().channels.create("🐺 Partie LG 🐺", { type: "GUILD_CATEGORY" });
		this.channels.text_village  = await this.channelsCategory.createChannel("Village", { type: "GUILD_TEXT", position: 0, topic: "Suivez l'avancée de la partie." });
		this.channels.voice_village = await this.channelsCategory.createChannel("Village", { type: "GUILD_VOICE", position: 1, topic: "Débattez, le jour, entre villageois." });

		// Create a channel for each role that need it.
		// Channels from roles that were assigned to several players are created a unique time.
		for (const role of this.getAssignedRoles(true)) {
			if (role.type.channels !== []) {
				for (const channel of role.type.channels) {
					if (channel.playersPerms) { // Apply same default permissions to every player that have this role.
						channel.options.permissionOverwrites = [];
						this.getRoleTypePlayers(role.type).forEach(player => channel.options.permissionOverwrites.push(channel.playersPerms.id = player.guildMember.id));
					}

					// Create the channel requested for this role.
					this.channels[channel.varName] = await this.channelsCategory.createChannel(channel.name, channel.options);
				}
			}
		}

		this.channels.text_ghostsOnly = await this.channelsCategory.createChannel("Fantômes only", { type: "GUILD_TEXT", topic: "Vous êtes décédé, profitez de votre repos éternel." });
		if (this.isRoleTypeInGame(Role.MEDIUM)) {
			this.channels.text_ghostsMedium = await this.channelsCategory.createChannel("Fantômes Médium", { type: "GUILD_TEXT", topic: "Envoyez des indications au Médium pour l'aider." });

			// Message collector
			const filter    = m => !m.author.bot && this.isUserInGame(m.author.id) && !this.getPlayer(m.author.id).isAlive;
			const collector = this.channels.text_ghostsMedium.createMessageCollector({ filter });
			collector.on("collect", m => {
				new MyEmbed(m.content).send(this.channels.text_medium);
			});
		}

		// Connect the bot to the village vocal
		await this.checkVoiceConnection();
	}


	/**
	 * Delete all the created channels that were used during the game.
	 * Also delete the channels' category.
	 */
	#deleteGameEnvironment() {
		Object.values(this.channels).forEach(channel => {
			if (channel && channel.parentId === this.channelsCategory)
				channel.delete("Fin de la partie");
		});
		this.channels = {};
		this.werewolf.disconnectVoiceConnection();
		this.channelsCategory.delete("Fin de la partie").then(() => console.log("Game channels deleted"));
		this.channelsCategory = null;
		this.werewolf.game    = null; // Destroy the reference to this ended game.
	}

	// -------------
	// OTHER METHODS
	// -------------


	/**
	 * Provide a list of embeds message that explains the rules of this game.
	 * @returns {MyEmbed[]} The list of Embeds.
	 */
	rulesToEmbeds() {
		// noinspection JSCheckFunctionSignatures
		return [
			new MyEmbed("Pour gagner", Utils.WHITE_COLOR).addFields([
				{ name: "Les Villageois", value: "Brûler tous les Loups-Garous." },
				{ name: "Les Loups-Garous", value: "Dévorer tous les villageois." },
				{ name: "Les couples formés par Cupidon", value: "Si l'un d'eux est Loup-Garou et l'autre villageois, leur but est d'être les derniers survivants." }]),
			new MyEmbed("Déroulement de la partie", Utils.WHITE_COLOR).addFields([
				{ name: "Tour 1", value: "‣ Le serveur s'endort\n‣ Appel de cupidon _(si présent)_ puis des amoureux\n‣ Le serveur se réveille" },
				{ name: "Tours suivant", value: "‣ Le serveur s'endort\n‣ Les rôles sont appelés un à un pour effectuer leurs actions\n‣ Le serveur se réveille\n‣ Le serveur débat\n‣ Le serveur vote\n‣ Le serveur se rendort" }]),
			new MyEmbed("Le village", Utils.WHITE_COLOR).desc("Chaque nuit, il est la cible des Loups-Garous : un villageois **dévoré** est éliminé du jeu. Les survivants se réunissent le lendemain matin et essaient de démasquer les mangeurs d’hommes nocturne." +
				"Après discussions _(j'ai bien dit **discussion** 👀)_, ils **votent** contre un suspect qui sera **éliminé** du jeu, jusqu’à la prochaine partie...")
		];
	}


	/**
	 * Connect the bot to the village voice channel.
	 */
	async checkVoiceConnection() {
		return new Promise((resolve, reject) => {
			if (!this.channels.voice_village)
				reject("The village voice channel is not created");

			if (!this.channels.voice_village.joinable) {
				// noinspection JSIgnoredPromiseFromCall
				new MyEmbed(`Je ne peut pas accéder au salon vocal "${this.channels.voice_village.name}" :/`, Utils.RED_COLOR).send(this.channels.text_village);
				return reject("Cannot access to voice channel");
			}

			const voiceChannel = this.werewolf.getVoiceConnectionChannel();
			if (voiceChannel && voiceChannel.id === this.channels.voice_village.id)
				return resolve("Already connected");

			const existingConnection = this.werewolf.createVoiceConnection(this.channels.voice_village);
			existingConnection.on(voiceManager.VoiceConnectionStatus.Ready, () => {
				console.log("Voice connected");
				resolve("Well Connected");
			});

			existingConnection.on(voiceManager.VoiceConnectionStatus.Disconnected, async () => {
				console.log("Voice disconnected");
				try {
					await Promise.race([
						voiceManager.entersState(existingConnection, voiceManager.VoiceConnectionStatus.Signalling, 5_000),
						voiceManager.entersState(existingConnection, voiceManager.VoiceConnectionStatus.Connecting, 5_000)
					]);// Seems to be reconnecting to a new channel - ignore disconnect
				} catch (error) {
					// Seems to be a real disconnect which SHOULDN'T be recovered from

					existingConnection.destroy();
					console.log("I disconnected with no problem");
					resolve();
				}
			});
		});
	}
}

module.exports = Game;