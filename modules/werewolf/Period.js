const voiceManager = require("@discordjs/voice");
const MyEmbed      = require("../../utilities/MyEmbed");
const Role         = require("./Role");
// noinspection JSUnusedLocalSymbols
const Game         = require("./Game");

const ROLES_ORDER_NIGHT = [Role.MEDIUM, Role.PSYCHIC, Role.WEREWOLF, Role.WITCH];

/**
 * This class represent a game time period between first day, first night, classic day and classic night.
 */
class Period {

	// GAME PERIODS
	static FIRST_NIGHT = 1;
	static FIRST_DAY   = 2;
	static DAY         = 3;
	static NIGHT       = 4;

	/**
	 * Create a period of time for a game.
	 * @param game {Game} The game where this period is taking place.
	 * @param periodType {number} The period of a game between {@link FIRST_NIGHT}, {@link FIRST_DAY}, {@link DAY} and {@link NIGHT}.
	 */
	constructor(game, periodType) {
		if (!game || !periodType || (periodType !== Period.FIRST_NIGHT && periodType !== Period.FIRST_DAY && periodType !== Period.DAY && periodType !== Period.NIGHT))
			return;

		/** @type {voiceManager.AudioPlayer} */
		this.audioPlayer = null;
		this.periodType = periodType;
		this.game       = game;
	}

	/**
	 * Start the actual game period.
	 */
	async start() {

		switch (this.periodType) {
			case Period.FIRST_NIGHT:
				console.log("First night");
				await this.#setAmbiance(Period.NIGHT);

				if (this.game.isRoleTypeInGame(Role.CUPIDON))
					await this.game.getOneRoleTypePlayer(Role.CUPIDON).play();

				break;

			case Period.FIRST_DAY:
				console.log("First day");
				await this.#setAmbiance(Period.DAY);

				await this.game.mayorVote();
				break;

			case Period.DAY:
				console.log("Day");
				await this.#setAmbiance(Period.DAY);

				// Announce night dead peoples
				await this.game.nightVictimsAdvert();

				// Check for victory
				if (this.game.isWon()) {
					// noinspection ES6MissingAwait
					this.game.endGame();
					return null;
				}

				// Village vote
				await this.game.villageVote();

				// Check for victory
				if (this.game.isWon()) {
					// noinspection ES6MissingAwait
					this.game.endGame();
					return null;
				}

				break;

			case Period.NIGHT:
				console.log("Night");
				await this.#setAmbiance(Period.NIGHT);

				for (const roleType of ROLES_ORDER_NIGHT)
					if (this.game.roleTypeCanPlay(roleType))
						await this.game.getOneRoleTypePlayer(roleType).play();

				break;

			default:
				console.log(`A game period was not recognized : ${this.periodType}`);
		}

		this.#next();
	}

	/**
	 * Check if a player won the game. Depending on the result, either stop the game or switch the period to following one and start it.
	 */
	#next() {
		switch (this.periodType) {
			case Period.FIRST_NIGHT:
				this.periodType = Period.FIRST_DAY;
				break;

			case Period.FIRST_DAY:
			case Period.DAY:
				this.periodType = Period.NIGHT;
				break;

			case Period.NIGHT:
				this.periodType = Period.DAY;
				break;

			default:
				console.log(`A game period was not recognized : ${this.periodType}`);
				return null;
		}

		// noinspection JSIgnoredPromiseFromCall
		this.start();
	}

	// --------
	// AMBIANCE
	// --------

	#createAmbianceAudioPlayer() {
		this.audioPlayer = voiceManager.createAudioPlayer(); // Create the player that will be used to play sounds.

		this.audioPlayer.on(voiceManager.AudioPlayerStatus.Playing, () => {
			console.log("Ambiance song is playing");
		});

		this.audioPlayer.on(voiceManager.AudioPlayerStatus.Idle, () => {
			console.log("Ambiance song ended");
		});

		this.audioPlayer.on(voiceManager.AudioPlayerStatus.Paused, () => {
			console.log("Ambiance song paused");
		});

		this.audioPlayer.on("error", error => {
			console.log(`Player error: "${error.message}"`);
			console.log(error);
		});
	}

	/**
	 * Change the current game vibe depending on the period.
	 * Actually, mute/unmute the villagers, play a certain sound, authorise/unauthorise the writing ...
	 * @param periodType {number} The period between {@link DAY} and {@link NIGHT}.
	 */
	async #setAmbiance(periodType) {
		if (!periodType || (periodType !== Period.DAY && periodType !== Period.NIGHT))
			return;

		if (!this.audioPlayer)
			this.#createAmbianceAudioPlayer();

		await this.game.checkVoiceConnection();
		const connection = this.game.werewolf.getVoiceConnection();
		let source;

		// Change the ambiance
		switch (periodType) {
			case Period.DAY:
				console.log("Change ambiance to day");
				await new MyEmbed(":sunny: Le serveur se démute... euh se réveille... :sunny:", Utils.ORANGE_COLOR).send(this.game.channels.text_village);

				// Unmute each survivor and allow him to send messages on the village channel.
				this.game.getAlivePlayers().forEach(player => {
					player.setMute(false);
					player.allowVillageWriting(true);
				});

				// Play day sounds
				source = await playDl.stream("https://www.youtube.com/watch?v=rYoZgpAEkFs").catch(e => console.log(`An error occurred while trying to play a song : ${e}`));
				break;

			case Period.NIGHT:
				console.log("Change ambiance to night");
				await new MyEmbed(":first_quarter_moon: Le serveur s'endort, les joueurs se mutent :last_quarter_moon: ", Utils.DARK_BLUE_COLOR).send(this.game.channels.text_village);

				// Mute each survivor and disallow him to send messages on the village channel.
				this.game.getAlivePlayers().forEach(player => {
					player.setMute(true);
					player.allowVillageWriting(false);
				});

				// Play night sounds
				source = await playDl.stream("https://www.youtube.com/watch?v=GpfY2P1mXr8").catch(e => console.log(`An error occurred while trying to play a song : ${e}`));
				break;

			default:
				console.log(`An ambiance change was requested but not recognized : ${periodType}`);
		}


		try {
			// noinspection JSCheckFunctionSignatures
			const ressource = voiceManager.createAudioResource(source.stream, { inputType: source.type, inlineVolume: true, metadata: { period: periodType } });
			connection.subscribe(this.audioPlayer);
			this.audioPlayer.play(ressource);
		} catch (e) {
			console.log(`Unable to play music because of ${e}.`);
		}
	}
}

module.exports = Period;