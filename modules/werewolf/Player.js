const Discord = require("discord.js");
const MyEmbed = require("../../utilities/MyEmbed");
const Role    = require("./Role");
// noinspection JSUnusedLocalSymbols
const Game    = require("./Game");

const MAYOR_SUCCESSOR_CHOICE_TIME = process.env.TEST === "true" ? 10 : 30; // seconds available for the mayor to choose his successor when killed.

class Player {

	/**
	 * Create a Player Object representing a member of a guild that is participating in a werewolf game.
	 * @param game {Game} The game where this player is participating.
	 * @param user {Discord.User} The discord User that will be given this role.
	 */
	constructor(game, user) {
		if (!user || user.bot)
			return;

		/** @type {Game} */
		this.game = game;
		this.user        = user;
		this.guildMember = this.game.werewolf.getGuild().members.resolve(user);
		this.name        = this.guildMember.nickname ? this.guildMember.nickname : user.username;
		this.dm          = user.dmChannel;
		/** @type {Role} */
		this.role = null;

		this.isAlive       = true;
		this.isLover       = false;
		this.isMayor       = false;
		this.isNightVictim = false; // Wheter by the wolves or the witch
	}

	/**
	 * Set the role of this player.
	 * @param roleType {number} The Role that has been associated with this player for this game. It can be {@link WEREWOLF}, {@link VILLAGER}, {@link YOUNG_GIRL}, {@link HUNTER}, {@link CUPIDON}, {@link WITCH}, {@link PSYCHIC} or {@link MEDIUM}.
	 */
	setRole(roleType) {
		this.role = new Role(this, roleType);
		console.log(`${this.name} est ${this.role.type.name}`);
	}

	/**
	 * Send to this players DM his role card.
	 * @returns {?Promise<Discord.Message>} The message sent.
	 */
	async explainRole() {
		if (!this.role)
			return null;

		await (this.role.toEmbed()).send(this.dm ? this.dm : await this.user.createDM());
	}

	/**
	 * Mute/unmute this player.
	 * @param state {boolean} If the player should be muted or not.
	 */
	async setMute(state) {
		await this.guildMember.voice.setMute(state).catch(() => console.log(`Unable to ${state ? "mute" : "unmute"} player "${this.name}"`));
	}

	/**
	 * Change the mayor state of this player.
	 * @param state {boolean} True of this player is the village mayor, false if it is not anymore.
	 * @returns {Promise<null>}
	 */
	async setMayor(state) {
		if (this.isMayor === state)
			return null;

		this.isMayor = state;
		await this.guildMember.setNickname(state ? `🎖️${this.name}` : this.name).catch(() => console.log(`Cannot ${state ? "give" : "remove"} the mayor medal to player "${this.name}"`));
	}

	/**
	 * Allow this player to send messages on the village text channel.
	 * @param isAllowed {boolean} True f this player is allowed to send messages, false otherwise.
	 * @returns {Promise<void>}
	 */
	async allowVillageWriting(isAllowed) {
		await this.game.channels.text_village.permissionOverwrites.edit(this.guildMember, { SEND_MESSAGES: isAllowed }).catch(() => console.log(`Unable to change message sending permission of player "${this.name}"`));
	}

	/**
	 * Launch this player's role main action.
	 * @returns {Promise<void>}
	 */
	async play() {
		await this.role.play();
	}

	/**
	 * Launch actions performed when this player is killed.
	 * @returns {Promise<void>}
	 */
	async kill() {
		if (!this.isAlive)
			return;

		// Inform the others players of this one's role. Change the players rights.
		await new MyEmbed(`**${this.name}** était :`).desc(`${this.role.type.name} ${this.isMayor ? "- Maire" : ""} ${this.isLover ? "- Amoureux" : ""}`).send(this.game.channels.text_village);
		await this.setMute(true);
		await this.allowVillageWriting(false);
		await this.game.channels.text_ghostsOnly.permissionOverwrites.edit(this.guildMember, { VIEW_CHANNEL: true });
		if (await this.game.channels.text_ghostsMedium) // Only if there is a medium in the game
			await this.game.channels.text_ghostsMedium.permissionOverwrites.edit(this.guildMember, { VIEW_CHANNEL: true });

		this.isAlive       = false;
		this.isNightVictim = false;

		// Perform an action before being killed of one has been specified. The hunter for example can shoot someone.
		// Even if the game is in a winning state, this action is performed and can change the final result.
		await this.role.kill();

		// Lover actions
		if (this.isLover && this.isAlive) { // Alive verification because if this player is also hunter, he can kill it's loved one and kill himself by the way.
			const otherLover = removeItems(this.game.getInLovePlayers(), [this]).pop();
			if (otherLover.isAlive) {
				await new MyEmbed(`**${otherLover.name}** est fou amoureux de **${this.name}**`).desc(`Le coeur brisé, ${otherLover.name} saute dans le puit sans fond du village.\nNous ne le reverrons plus !`).send(this.game.channels.text_village);
				await otherLover.kill();
			}
		}

		// Mayor actions
		if (this.isMayor) {
			await new MyEmbed(`Le maire est mort`).desc(`Un nouveau vote va se tenir au sein du village.`).send(this.game.channels.text_village);
			await this.#mayorSuccession();
		}
	}

	/**
	 * The string appearance of this player.
	 * @returns {string} The string equivalent of this player.
	 */
	toString() {
		return `${this.isAlive ? "😀" : "💀"} ${this.name} - \`${this.role.type.name}${this.isMayor ? " - Maire" : ""}${this.isLover ? " - Amoureux" : ""}\``;
	}

	/**
	 * If this player is the village mayor, when he is killed, he can give its mayor status to another player
	 * @returns {Promise<void>}
	 */
	async #mayorSuccession() {
		await this.setMayor(false);

		// noinspection JSUnusedAssignment,JSCheckFunctionSignatures
		const poll = await this.game.channels.text_werewolf.send({
			embeds: [new MyEmbed(`Choix du nouveau maire`).desc(`${this.name}, malgré le statut qui t'a été accordé, tu n'es plus de ce monde et doit donc choisir ton successeur parmi la liste`)],
			components: [new Discord.MessageActionRow().addComponents([
				new Discord.MessageSelectMenu({
					customId: "select_player",
					placeholder: "Le nouveau maire sera ...",
					maxValues: 1,
					options: this.game.getAlivePlayers().map(p => p = { label: p.name, value: p.user.id })
				})
			])]
		});
		MyEmbed.showTimer(poll, "Choix automatique dans", MAYOR_SUCCESSOR_CHOICE_TIME);

		const filter    = (interaction) => interaction.user.id === this.user.id;
		const collector = poll.createMessageComponentCollector({ filter, time: MAYOR_SUCCESSOR_CHOICE_TIME * 1000, maxComponents: 1 });

		collector.on("collect", async i => {
			// noinspection JSUnresolvedVariable
			const nextMayor = this.game.getPlayer(i.values[0]);
			await new MyEmbed(`Le nouveau maire est ${nextMayor.name}, félicitations !`).send(this.game.channels.text_village);
			await nextMayor.setMayor(true);
		});

		collector.on("end", async (collected, reason) => {
			if (reason === "time") {
				const nextMayor = randomItem(this.game.getAlivePlayers());
				await new MyEmbed(`Le nouveau maire est ${nextMayor.name}, félicitations !`).desc("Ce fut un choix aléatoire car la réponse n'a pas été donnée dans les temps.").send(this.game.channels.text_village);
				await nextMayor.setMayor(true);
				console.log(`Random mayor successor choice : player "${nextMayor.name}"`);
			}
		});
	}
}

module.exports = Player;