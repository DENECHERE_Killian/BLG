const voiceManager = require("@discordjs/voice");
const Discord      = require("discord.js");
const MyEmbed      = require("../../utilities/MyEmbed");
const Player       = require("./Player");
const Game         = require("./Game");
// noinspection JSUnusedLocalSymbols
const Bot          = require("../../utilities/Bot");

const PLAYERS_JOIN_TIME = process.env.TEST === "true" ? 5 : 40; // seconds available for members to join the new game.
// const testList = [{name: "Killian", user: {id: "112548"}}, {name: "Hugo", user: {id: "15898"}}];

class Werewolf {

	/**
	 * Create a werewolf module for a specific guild.
	 * @param guildId {Discord.Snowflake} The current guild id.
	 */
	constructor(guildId) {
		this.guildId = guildId;

		/**
		 * @type {Game}
		 */
		this.game = null;
	}

	runCommand(command, interaction) {
		switch (command) {
			case "play":
				if (process.env.TEST !== "true")
					return interaction.reply({ content: "Ce module n'est pas encore disponible :/", ephemeral: true });

				// noinspection JSIgnoredPromiseFromCall
				this.#playCommand(interaction);
				break;

			case "clear":
				this.getGuild().channels.cache.filter(channel => channel.type === "GUILD_CATEGORY" && channel.name === "🐺 Partie LG 🐺").forEach(category => {
					// noinspection JSUnresolvedVariable
					if (category.children)
						category.children.forEach(channel => channel.delete());
					category.delete();
				});
				interaction.reply({ embeds: [new MyEmbed("Suopression effectuée", Utils.GREEN_COLOR)], ephemeral: true });

				break;

			case "stop":
				if (!this.game || !this.game.isRunning)
					return interaction.reply({ embeds: [new MyEmbed("Aucune partie n'est en cours", Utils.RED_COLOR)], ephemeral: true });

				if (interaction.memberPermissions.has(Discord.Permissions.ADMINISTRATOR)) {
					// noinspection JSIgnoredPromiseFromCall
					this.game.endGame();
				} else {
					return interaction.reply({ embeds: [new MyEmbed("Tu n'a pas les droits suffisants pour arrêter la partie en cours.\nVois ça avec un admin.", Utils.RED_COLOR)], ephemeral: true });
				}
				break;

			default:
				console.log(`A werewolf module command was received but not recognized: "${command}"`);
		}
	}

	// --------
	// COMMANDS
	// --------

	/**
	 * Start the werewolf game if there is no game and no music currently running.
	 * @param interaction {Discord.CommandInteraction} The command interaction to answer.
	 */
	async #playCommand(interaction) {
		if (this.game && this.game.isRunning)
			return interaction.reply({ embeds: [new MyEmbed("Une partie est déjà en cours :/", Utils.RED_COLOR)], ephemeral: true });

		// TODO: Check the voice usage before starting a game (if the bot is running music).

		const question = await interaction.reply({ embeds: [new MyEmbed("**Lancement d'une partie de Loup-Garou**\nQui veut participer ?", Utils.BLUE_COLOR)], fetchReply: true });
		MyEmbed.showTimer(question, "Début dans", PLAYERS_JOIN_TIME);
		await question.react("✋");
		const filter    = (reaction, user) => reaction.emoji.name === "✋" && !user.bot;
		const collector = question.createReactionCollector({ filter, time: PLAYERS_JOIN_TIME * 1000, maxUsers: Game.MAX_PLAYERS_AMOUNT });

		collector.on("end", async (collected) => {
			await question.reactions.removeAll();

			if (collector.total < Game.MIN_PLAYERS_AMOUNT) {
				// noinspection JSCheckFunctionSignatures
				return interaction.editReply({ embeds: [new MyEmbed("**Partie annulée**\nIl n'y a pas assez de joueurs.", Utils.RED_COLOR)], fetchReply: true }).then(deleteMessage);
			}

			this.game = new Game(this);
			collected.get("✋").users.cache.forEach(user => {
				if (!user.bot)
					this.game.addPlayer(new Player(this.game, user));
			});

			// noinspection JSCheckFunctionSignatures
			await interaction.editReply({ embeds: [new MyEmbed("La partie va commencer, rends-toi dans la nouvelle catégorie.", Utils.GREEN_COLOR)], fetchReply: true }).then(deleteMessage);
			// noinspection ES6MissingAwait
			this.game.startGame(interaction);
		});
	}

	/**
	 * Get the guild Object corresponding to the current guild id.
	 * @returns {?Discord.Guild} The guild that the bot is related.
	 */
	getGuild() {
		if (!this.guildId)
			return null;

		return global.bot.guilds.cache.get(this.guildId);
	}


	// ----------------
	// VOICE MANAGEMENT
	// ----------------
	// This part should probably be placed inside the Bot.js class, but it does not work.

	/**
	 * Return the voice connection of this bot if there is one (if it is connected to a VoiceChannel). Otherwise, return null.
	 * @returns {?voiceManager.VoiceConnection}
	 */
	getVoiceConnection() {
		return voiceManager.getVoiceConnection(this.guildId);
	}

	/**
	 * Disconnect the bot from a Vocal Channel if it is connected.
	 */
	disconnectVoiceConnection() {
		const exisitingConnection = Utils.getVoiceConnection();
		if (!exisitingConnection)
			return;

		exisitingConnection.disconnect();
	}

	/**
	 * Connect the bot to the specified VoiceChannel.
	 * @param voiceChannel {Discord.VoiceChannel} The voice channel to connect the bot to.
	 * @returns {?voiceManager.VoiceConnection} The created connection that can be used ti wait for events (ready, paused etc ...).
	 */
	createVoiceConnection(voiceChannel) {
		if (!voiceChannel)
			return null;

		return voiceManager.joinVoiceChannel({
			channelId: voiceChannel.id,
			guildId: this.guildId,
			adapterCreator: voiceChannel.guild.voiceAdapterCreator
		});
	}

	/**
	 * Get the voice channel where the bot currently is connected on.
	 * @returns {?Discord.VoiceChannel} The voice Channel where the bot is connected in this guild.
	 */
	getVoiceConnectionChannel() {
		return this.getGuild().me.voice.channel;
	}
}

module.exports = Werewolf;