// noinspection JSUnusedGlobalSymbols,JSUnusedLocalSymbols

const Discord = require("discord.js");

class RoleType {

	/**
	 * This class represent the structure of a role in this game.
	 * @param id {number} Identifier used to compare two instances of a same RoleType.
	 * @param name {string} The name of this role type (Villager, werewolf ...)
	 * @param turnDuration {number} The duration in seconds of this role action (this represents the time available for players that play a role of this type to make a choice when they are asked to during the game).
	 * @param isVillageFriendly {boolean} If this role type play for or against the villagers.
	 * @param description {string} A descriptions of what the role is supposed to do. This description will be sent to the players when the game start.
	 * @param specificity {string} A thing to know about this role that could be important.
	 * @param originalCardURL {string} The URL to the card image of the original game.
	 * @param customCardURL {string} The URL to the custom card image.
	 * @param {function} [normalAction=null] Actions this role performs each game turn.
	 * @param {function} [killedAction=null]  Actions this role performs when it's killed.
     // * @param {[{varName: string, name: string, options: {topic: string, position: number, type: string}}]} [channels=[]]  A channels list that are required to play this role. Leave an empty list if they are no necessity.
	 * @param {string} channels.varName  The name of the variable that would be associated to this channel in the code.
	 * @param {string} channels.name  The name of this channel in Discord.
	 * @param {Discord.ChannelData} channels.options  The Discord channel options for creation. Leave empty the "permissionOverwrites" ! See the playersPerms attribute for it.
	 * @param {Discord.OverwriteData} channels.playersPerms  The Discord channel permissions to apply to all players that will have this role in the game.
	 */
	constructor(id, name, turnDuration, isVillageFriendly, description, specificity, originalCardURL, customCardURL, normalAction = null, killedAction = null, channels = []) {
		this.id                = id;
		this.name              = name;
		this.turnDuration      = turnDuration;
		this.isVillageFriendly = isVillageFriendly;
		this.description       = description;
		this.specificity       = specificity;
		this.originalCardURL   = originalCardURL;
		this.customCardURL     = customCardURL;
		this.normalAction      = normalAction;
		this.killedAction      = killedAction;
		this.channels          = channels;
	}
}

module.exports = RoleType;