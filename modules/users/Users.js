const MyEmbed = require("../../utilities/MyEmbed");
const crypto  = require("crypto");
const Utils   = require("../../utilities/Utils");

class Users {
	/**
	 * Create a Users module for specific guild
	 * @param guildId {Discord.Snowflake} The current guild id.
	 */
	constructor(guildId) {
		this.guildId = guildId;
	}

	/**
	 * Handle a command on the Users module.
	 * @param command {string} The command name.
	 * @param interaction {Discord.CommandInteraction} The originating command.
	 */
	runCommand(command, interaction) {
		switch (command) {
			case "link":
				console.log(`\t↪ User link command`);
				this.#linkCommand(interaction).then();
				break;
			case "register":
				console.log(`\t↪ User register command`);
				this.#registerUser(interaction).then();
				break;
		}
	}

	/**
	 * Give the link to the user.
	 * @param {Discord.CommandInteraction} interaction The command interaction created by the user.
	 * @returns {Promise<any>}
	 */
	#linkCommand(interaction) {
		if (!(interaction.guildId === process.env.PDDS_GUILD_ID || interaction.guildId === process.env.DEV_GUILD_ID))
			return false;

		return interaction.reply({
			embeds: [
				new MyEmbed("Lien pour te connecter", Utils.BLUE_COLOR)
					.desc(`${global.ngrokUrl}`)
			],
			ephemeral: true
		}).catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));
	}

	/**
	 * Add the user to the database.
	 * @param {Discord.CommandInteraction} interaction The command interaction created by the user.
	 * @returns {Promise<boolean>} true if the user was added, false otherwise
	 */
	async #registerUser(interaction) {
		if (!(interaction.guildId === process.env.PDDS_GUILD_ID || interaction.guildId === process.env.DEV_GUILD_ID))
			return false;

		if (await global.database.userExist(interaction.member.user.id)) {
			interaction.reply({
				embeds: [
					new MyEmbed("Déjà enregistré(e)", Utils.ORANGE_COLOR)
						.desc(`Tu es déjà enregistré(e). Regarde dans tes messages privés avec moi pour retrouver tes identifiants et rendez-vous ici : ${global.ngrokUrl}`)
				],
				ephemeral: true
			}).catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));
			return false;
		}

		const id = interaction.member.user.id;

		if (!await Users.insertUser(id))
			return false;

		interaction.reply({
			embeds: [
				new MyEmbed("Enregistrement terminé", Utils.BLUE_COLOR)
					.desc("Tu as bien été enregistré(e). Je t'ai envoyé un message avec tes identifiants de connexion.")
					.setFooter({text: "Tu peux avoir le lien vers le site en utilisant `/us link` dans un serveur où je suis présent !"})
			],
			ephemeral: true
		});
		return true;
	}

	/**
	 * Insert a user in the database
	 * @param id {string} The id of the user to insert.
	 * @returns {Promise<boolean>} A promise containing the success status of the insertion
	 */
	static async insertUser(id) {
		if (await global.database.userExist(id))
			return false;

		const pwd       = `${parseInt(id) * 2}`.split("").sort(_ => 0.5 - Math.random()).join("").substring(0, 18);
		const hashedPwd = crypto.createHash("sha256").update(pwd).digest("hex");

		const dbResponse = await global.database.insertUser(id, hashedPwd);

		if (!dbResponse)
			return false;

		global.bot.users.fetch(id)
			.then(user => {
				user.send({
					embeds: [
						new MyEmbed("Infos de connexion", Utils.BLUE_COLOR)
							.desc("Voilà ton identifiant et ton mot de passe pour te connecter sur le site web. Ne le communique à personne !")
							.addFields([
								{name: "Identifiant :", value: id},
								{name: "Mot de passe :", value: pwd}])
							.setFooter({text: "Tu peux avoir le lien vers le site en utilisant `/us link` dans un serveur où je suis présent !"})
					]
				}).catch(err => console.error(`Failed to send credential to user ${id}:\n${err}`))
			})
			.catch(err => console.error(`Failed to fetch user: ${err}`));

		return true
	}
}

module.exports = Users;