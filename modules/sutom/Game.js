const { Canvas, Image } = require("canvas");
const mergeImages       = require("merge-images");
const Discord           = require("discord.js");
const MyEmbed           = require("../../utilities/MyEmbed");
const Words             = require("an-array-of-french-words");
const Utils             = require("../../utilities/Utils");

const attachmentsLocation = "https://cdn.discordapp.com/attachments/952340321268793434/";
const LETTER_LINKS        = new Map([
	["A", `${attachmentsLocation}953597524504092722/letter_A.png`],
	["B", `${attachmentsLocation}953597524759949332/letter_B.png`],
	["C", `${attachmentsLocation}953597525053538304/letter_C.png`],
	["D", `${attachmentsLocation}953597525464588289/letter_D.png`],
	["E", `${attachmentsLocation}953597525842096148/letter_E.png`],
	["F", `${attachmentsLocation}953597526106341386/letter_F.png`],
	["G", `${attachmentsLocation}953597526370570330/letter_G.png`],
	["H", `${attachmentsLocation}953597526609653800/letter_H.png`],
	["I", `${attachmentsLocation}953597597065551893/letter_I.png`],
	["J", `${attachmentsLocation}953597597266903060/letter_J.png`],
	["K", `${attachmentsLocation}953597597472419840/letter_K.png`],
	["L", `${attachmentsLocation}953597597698883584/letter_L.png`],
	["M", `${attachmentsLocation}953597597912801300/letter_M.png`],
	["N", `${attachmentsLocation}953597598093160479/letter_N.png`],
	["O", `${attachmentsLocation}953597598307078164/letter_O.png`],
	["P", `${attachmentsLocation}953597598726488124/letter_P.png`],
	["Q", `${attachmentsLocation}953597598902665226/letter_Q.png`],
	["R", `${attachmentsLocation}953602042671288361/letter_R.png`],
	["S", `${attachmentsLocation}953597617454063646/letter_S.png`],
	["T", `${attachmentsLocation}953597617772826654/letter_T.png`],
	["U", `${attachmentsLocation}953597618099990558/letter_U.png`],
	["V", `${attachmentsLocation}953597618439749652/letter_V.png`],
	["W", `${attachmentsLocation}953597618695598080/letter_W.png`],
	["X", `${attachmentsLocation}953597619022725140/letter_X.png`],
	["Y", `${attachmentsLocation}953597619211493396/letter_Y.png`],
	["Z", `${attachmentsLocation}953597617173069854/letter_Z.png`],
	[".", `${attachmentsLocation}953597524281806848/dot.png`]]);

const SQUARES = {
	yellow: `${attachmentsLocation}952512968011153508/yellow_square.png`,
	blue: `${attachmentsLocation}952512871047254036/blue_square.png`,
	red: `${attachmentsLocation}952512955004649482/red_square.png`
};

const LETTER_PADDING = 0;
const SQUARE_HEIGHT  = 180;
const GUESS_NUMBER   = 6;

/**
 * This class represent a Sutom game.
 * It saved all the datas about a game.
 */
class Game {

	guesses;

	/**
	 * Create a games' manager object.
	 */
	constructor(word, playerName) {
		this.guessLeft = GUESS_NUMBER;
		this.gameName  = `Partie de ${playerName}`;
		this.word      = word.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toUpperCase();
	}

	/**
	 * Handle the guess of a player for his game.
	 * @param guessWord {string} The word proposed by the player.
	 * @param interaction {Discord.CommandInteraction} The interaction to reply to.
	 * @returns {Promise<boolean>} The result of the guess. True if player guess the good word else return false.
	 */
	async guess(guessWord, interaction) {
		guessWord = guessWord.toLowerCase();

		if (!this.isValidWord(guessWord)) {
			interaction.reply({
				embeds: [new MyEmbed(this.gameName, Utils.RED_COLOR)
					.desc("Le mot que tu as porposé n'est pas valide ! N'utilises que des caractères alphabétiques !")], ephemeral: true
			})
				.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));
			return false;
		}

		if (guessWord.length !== this.word.length) {
			interaction.reply({
				embeds: [new MyEmbed(this.gameName, Utils.RED_COLOR)
					.desc(`Le mot que tu as proposé est de taille ${guessWord.length} alors que tu cherches une mot de taille ${this.word.length} !`)], ephemeral: true
			})
				.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));
			return false;
		}

		guessWord = guessWord.normalize("NFD").replace(/[\u0300-\u036f]/g, "");

		if (!this.isInDictionary(guessWord)) {
			interaction.reply({
				embeds: [new MyEmbed(this.gameName, Utils.RED_COLOR)
					.desc("Le mot que tu as proposé n'est pas dans mon dictionnaire !")], ephemeral: true
			})
				.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));
			return false;
		}

		guessWord = guessWord.toUpperCase();
		await this.analyse(guessWord);
		this.guessLeft -= 1;

		if (guessWord === this.word) {
			this.success(interaction);
			return true;
		}

		if (this.guessLeft === 0) {
			this.failed(interaction);
			return true;
		}

		interaction.reply({
			embeds: [
				new MyEmbed(this.gameName, Utils.BLUE_COLOR)
					.desc(`Il te reste ${this.guessLeft} coup(s).\n\n`)
					.setImage("attachment://guess.png")
			],
			files: [this.guesses]
		}).catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));

		return false;
	}

	/**
	 * Check if a given string is a valid word.
	 * @param guessWord {string} The string to check.
	 * @returns {boolean} True if it is a word else false.
	 */
	isValidWord(guessWord) {
		for (const char of guessWord.normalize("NFD").replace(/[\u0300-\u036f]/g, "")) {
			if (!char.match(/[a-zA-Z]/))
				return false;
		}
		return true;
	}

	/**
	 * Check if a given word is in the game dictionary.
	 * @param guessWord {string} The word to check.
	 * @returns {boolean} True if the word is in the dictionary else false.
	 */
	isInDictionary(guessWord) {
		for (let w of Words) {
			if (w.normalize("NFD").replace(/[\u0300-\u036f]/g, "") === guessWord)
				return true;
		}
		return false;
	}

	/**
	 * Send the victory message and end the game.
	 * @param interaction {Discord.CommandInteraction} The interaction to reply to.
	 */
	success(interaction) {
		interaction.reply({
			embeds: [
				new MyEmbed(this.gameName, Utils.GREEN_COLOR)
					.desc(`Félicitation ! 🏆\nLe mot était ${this.word}. Tu l'as trouvé en ${GUESS_NUMBER - this.guessLeft} coup(s).\n\n`)
					.setImage("attachment://guess.png")
			],
			files: [this.guesses]
		}).catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));
	}

	/**
	 * Abort the game and inform the player.
	 * @param interaction {Discord.CommandInteraction} The interaction to reply to.
	 */
	abort(interaction) {
		const embed = new MyEmbed(this.gameName, Utils.ORANGE_COLOR)
			.desc(`Tu as mis fin à la partie !\nLe mot était ${this.word}. Il te restait ${this.guessLeft} coup(s).\n`);

		if (this.guessLeft !== GUESS_NUMBER) {
			embed.setImage("attachment://guess.png");

			return interaction.reply({ embeds: [embed], files: [this.guesses] })
				.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));
		}

		interaction.reply({ embeds: [embed] })
			.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));
	}

	/**
	 * Send the defeat message and end the game.
	 * @param interaction {Discord.CommandInteraction} The interaction to reply to.
	 */
	failed(interaction) {
		interaction.reply({
			embeds: [
				new MyEmbed(this.gameName, Utils.ORANGE_COLOR)
					.desc(`Dommage !\n Le mot était ${this.word}. Tu n'es pas parvenu(e) à le trouver avec les ${GUESS_NUMBER} coups.\n`)
					.setImage("attachment://guess.png")
			],
			files: [this.guesses]
		}).catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));
	}

	initialState() {
		let images = [
			{ src: SQUARES.blue, x: 0, y: 0 },
			{ src: LETTER_LINKS.get(this.word.charAt(0)), x: LETTER_PADDING, y: LETTER_PADDING }
		];

		for (let i = 1; i < this.word.length; i++) {
			images.push({ src: SQUARES.blue, x: i * SQUARE_HEIGHT, y: 0 });
			images.push({ src: LETTER_LINKS.get("."), x: i * SQUARE_HEIGHT + LETTER_PADDING, y: LETTER_PADDING });
		}

		return mergeImages(images, { Canvas: Canvas, Image: Image, height: SQUARE_HEIGHT, width: SQUARE_HEIGHT * this.word.length })
			.then(b64Image => new Discord.MessageAttachment(new Buffer.from(b64Image.split(",")[1], "base64"), "initialState.png"))
			.catch(err => console.log("ERROR: ", err));
	}

	/**
	 * Analyse the given word according to the Sutom game rules and send the message to the player to inform him about the game state.
	 * @param guessWord {string} The word to analyse.
	 * @returns {Promise<void>}
	 */
	async analyse(guessWord) {
		let images       = [];
		let goodLetters  = [];
		let wrongLetters = {};

		if (this.guesses)
			images = [{ src: this.guesses.attachment, x: 0, y: 0 }];

		for (let i = 0; i < guessWord.length; i++) {
			if (guessWord.charAt(i) === this.word.charAt(i))
				goodLetters.push(i);
		}

		for (let i = 0; i < guessWord.length; i++) {
			if (goodLetters.includes(i))
				continue;

			const char         = this.word.charAt(i);
			wrongLetters[char] = wrongLetters[char] ? wrongLetters[char] + 1 : 1;
		}

		const guessUse = GUESS_NUMBER - this.guessLeft;

		for (let i = 0; i < guessWord.length; i++) {
			if (goodLetters.includes(i)) {
				images.push({ src: SQUARES.red, x: SQUARE_HEIGHT * i, y: guessUse * SQUARE_HEIGHT });
			} else if (!wrongLetters[guessWord.charAt(i)] || wrongLetters[guessWord.charAt(i)] === 0) {
				images.push({ src: SQUARES.blue, x: SQUARE_HEIGHT * i, y: guessUse * SQUARE_HEIGHT });
			} else {
				wrongLetters[guessWord.charAt(i)] -= 1;
				images.push({ src: SQUARES.yellow, x: SQUARE_HEIGHT * i, y: guessUse * SQUARE_HEIGHT });
			}

			images.push({ src: LETTER_LINKS.get(guessWord.charAt(i)), x: SQUARE_HEIGHT * i + LETTER_PADDING, y: guessUse * SQUARE_HEIGHT + LETTER_PADDING });
		}

		await mergeImages(images, { Canvas: Canvas, Image: Image, height: SQUARE_HEIGHT * (guessUse + 1), width: SQUARE_HEIGHT * this.word.length })
			.then(b64Image => this.guesses = new Discord.MessageAttachment(new Buffer.from(b64Image.split(",")[1], "base64"), "guess.png"))
			.catch(err => console.log("ERROR: ", err));
	}
}

module.exports = Game;
