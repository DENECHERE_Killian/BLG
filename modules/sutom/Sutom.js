// noinspection JSUnusedLocalSymbols
/**
 * @author Xavier
 */

const Discord = require("discord.js"); // Use for doc
const Words   = require("an-array-of-french-words");

const MyEmbed = require("../../utilities/MyEmbed");
const Utils   = require("../../utilities/Utils");
const Game    = require("./Game");

const MAX_WORD_LENGTH = 9;
const MIN_WORD_LENGTH = 5;

/**
 * This class represent the Sutom module of this bot.
 * It is used to play Sutom game via CommandInteraction.
 */
class Sutom {

	/**
	 * Create a sutom module for a specific guild
	 * @param guildId {Discord.Snowflake} The current guild id.
	 */
	constructor(guildId) {
		this.guildId = guildId;
		this.games   = {};
	}

	/**
	 * Handle a command on the Sutom module.
	 * @param command {string} The command name.
	 * @param interaction {Discord.CommandInteraction} The originating command.
	 */
	runCommand(command, interaction) {
		switch (command) {
			case "start":
				console.log(`\t↪ Sutom start command`);
				this.#startCommand(interaction).then();
				break;

			case "end":
				console.log(`\t↪ Sutom end command`);
				// noinspection JSIgnoredPromiseFromCall
				this.#endCommand(interaction);
				break;

			case "guess":
				console.log(`\t↪ Sutom guess command`);
				this.#guessCommand(interaction).then();
				break;
		}
	}

	/**
	 * Initialize a game for the command sender if he isn't already playing.
	 * The player can choose the length of the word.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @returns {Promise<void>}
	 */
	async #startCommand(interaction) {
		if (!this.#memberNotInGame(interaction.member.id))
			return interaction.reply({
				embeds: [new MyEmbed(`Tu es déjà dans une partie !`, Utils.ORANGE_COLOR)
					.desc("Fini ta partie avant de pouvoir en recommencer une nouvelle.")], ephemeral: true
			})
				.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));

		const wordLengthOption = interaction.options.get("length")?.value;
		const wordLength       = wordLengthOption ?? Math.floor(Math.random() * (MAX_WORD_LENGTH + 1 - MIN_WORD_LENGTH) + MIN_WORD_LENGTH);

		if (wordLength < MIN_WORD_LENGTH || wordLength > MAX_WORD_LENGTH)
			return;

		const word = this.#pickAWord(wordLength);
		console.log(`\t↪ Word to guess : ${word}`);

		if (word === "")
			return interaction.reply({ embeds: [new MyEmbed(`Un problème est survenu lors du choix d'un mot de longueur ${wordLength}.`, Utils.RED_COLOR)], ephemeral: true })
				.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));

		const newGame                     = new Game(word, interaction.member.user.username);
		this.games[interaction.member.id] = newGame;
		await newGame.initialState().then(messageAttachement => {
			interaction.reply({
				embeds: [
					new MyEmbed(`Partie de ${interaction.member.user.username}`, Utils.BLUE_COLOR)
						.desc("Ta partie est prête !")
						.setImage("attachment://initialState.png")
				],
				files: [messageAttachement]
			});
		});
	}

	/**
	 * End the game of the player that send the command if he is currently playing.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 */
	#endCommand(interaction) {
		if (this.#memberNotInGame(interaction.member.id))
			return interaction.reply({ embeds: [new MyEmbed(`Tu n'es dans aucune partie !`, Utils.ORANGE_COLOR)], ephemeral: true })
				.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));

		this.games[interaction.member.id].abort(interaction);
		delete this.games[interaction.member.id];
	}

	/**
	 * Handle the guessing from a player.
	 * @param interaction {Discord.CommandInteraction} The originating command of this call.
	 * @returns {Promise<void>}
	 */
	async #guessCommand(interaction) {
		if (this.#memberNotInGame(interaction.member.id))
			return interaction.reply({ embeds: [new MyEmbed(`Tu n'es dans aucune partie !`, Utils.ORANGE_COLOR)], ephemeral: true })
				.catch(err => console.error(`\t↪ Cannot reply to the interaction :\n${err}`));

		if (await this.games[interaction.member.id].guess(interaction.options.get("word").value, interaction))
			delete this.games[interaction.member.id];
	}

	/**
	 * Choose a word for the game.
	 * @param length {int} The length of the word.
	 * @returns {string} The chosen word.
	 */
	#pickAWord(length) {
		if (length < MIN_WORD_LENGTH || length > MAX_WORD_LENGTH)
			return "";

		const acceptable = Words.filter(w => {
			if (w.length === length) {
				w = w.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
				for (const char of w) {
					if (!char.match(/[a-z]/)) {
						return false;
					}
				}
				return true;
			}
			return false;
		});

		return acceptable.length === 0 ? "" : acceptable[Math.floor(Math.random() * acceptable.length)];
	}

	#memberNotInGame(id) {
		return this.games[id] === undefined;
	}
}

module.exports = Sutom;
