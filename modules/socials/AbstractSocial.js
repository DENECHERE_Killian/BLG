const MyEmbed = require("../../utilities/MyEmbed");
const Discord = require("discord.js");
const Utils   = require("../../utilities/Utils");

/**
 * @author Killian
 */
const TOO_HEAVY     = 413; // real code
const TOO_MUCH_TIME = 500; // real code

class AbstractSocial {
	static NO_SESSION_ID     = 100; // invented code
	static NO_USER_ID        = 101; // invented code
	static SESSION_OUTDATED  = 102; // invented code
	static BAD_REQUEST       = 400; // real code
	static ACCESS_DENIED     = 403; // real code
	static NOT_FOUND         = 404; // real code
	static TOO_MANY_REQUESTS = 429; // real code
	static BAD_URL           = 805; // invented code
	static NOT_EMBEDDABLE    = 806; // invented code

	PLATFORM_COLOR = Utils.WHITE_COLOR;
	PLATFORM_NAME  = "Default";
	DOMAINS        = []; // Recognized urls

	constructor() {
		if (this.constructor === AbstractSocial)
			throw new TypeError("Abstract class \"AbstractSocial\" cannot be instantiated");
	}

	searchForLink(message) {
		const messageContent = message.content;
		// noinspection HttpUrlsUsage
		if (!this.DOMAINS.find(domain => messageContent.startsWith(`https://${domain}/`) || messageContent.startsWith(`http://${domain}/`)))
			return null;

		console.log(`${this.PLATFORM_NAME} link identified`);
		// noinspection JSIgnoredPromiseFromCall
		this.sendEmbed(message);
	}

	/**
	 * Send the linked media in a Discord embed if possible.
	 * If the media is too heavy to be embedded or sent as an attechment, sent its direct link in a new message.
	 * @param message {Discord.Message} The message containing the link.
	 * @returns {Promise<void>}
	 */
	async sendEmbed(message) {
		const content = message.content;
		const clean   = await this.cleanLink(content);

		if (clean.isNotEmbeddable) {
			if (clean.isNotEmbeddable === AbstractSocial.BAD_URL)
				return console.log(`\t↪ The link is not well formed`);

			return console.log(`\t↪ The link is not embedable`);
		}
		console.log(`\t↪ Cleared link : ${clean.link}`);

		// Generate the embed message that will be sent
		const media = await this.getMediaEmbed(clean.type, clean.link, message.author)
			.catch(async (err) => {
				if (err === AbstractSocial.NOT_EMBEDDABLE)
					return console.log(`\t↪ Not Embeddable`);

				if (err === AbstractSocial.BAD_URL)
					return console.log(`\t↪ Wrong formed link`);

				if (err === AbstractSocial.NOT_FOUND) // Probably a private account
					return console.log(`\t↪ Not found, account probably private if an Instagram media`);

				if (err === AbstractSocial.TOO_MANY_REQUESTS) // Too many requests
					return console.log(`\t↪ Too many requests`);

				if (err === AbstractSocial.NO_SESSION_ID) // The headers do not contain session id
					return console.log(`\t↪ No session Id`);

				if (err === AbstractSocial.NO_USER_ID) // The headers do not contain user id
					return console.log(`\t↪ No user Id`);

				if (err === AbstractSocial.SESSION_OUTDATED) // The session id is probably outdated
					return console.log(`\t↪ Maximum redirect : The Instagram session ID is outdated (need to update the encrypted password in .env file) or the API changed (need to copy the new ones from a navigator).`);

				if (err === AbstractSocial.ACCESS_DENIED)
					return console.log(`\t↪ Access denied`);

				return console.error(`\t↪ Cannot embed media :\n${err}`);
			});

		if (!media)
			return null; // In case of error

		await this.#sendMedia(message, clean.link, media);
	}

	cleanLink() {
		throw new Error("You must implement this function");
	}

	getMediaEmbed() {
		throw new Error("You must implement this function");
	}

	async #sendMedia(message, link, media) {
		// Inform the user that a preview is loading
		const preview = await message.channel.send({ embeds: [new MyEmbed("Je charge l'aperçu", Utils.BLUE_COLOR)] })
			.catch((err) => console.error(`\t↪ Cannot send loading message : \n${err}`));

		// noinspection JSCheckFunctionSignatures
		const components = [new Discord.MessageActionRow().addComponents(new Discord.MessageButton().setLabel(this.PLATFORM_NAME).setStyle("LINK").setURL(link))];

		// If a content is specified, a new message is sent before the inforamtions embed one.
		if (media.content !== "")
			await preview.edit({ content: media.content, embeds: [] })
				.catch(err => console.error(`\t↪ Cannot edit preview :\n${err}`));

		// Choose to send or edit the loading message previously sent.
		function sendOrEdit(options) {
			return media.content !== "" ? message.channel.send(options) : preview.edit(options);
		}

		console.log(`\t↪ Sending embed`);

		// Send the embed and if there is no error, delete the original message.
		sendOrEdit({
			embeds: media.embeds,
			files: media.files,
			components: components
		}).then((m) => {
			console.log(`\t↪ Message sent on channel ${Utils.getMessageLocation(m)}`);
			message.delete()
				.catch((err) => console.error(`\t↪ Cannot delete message :\n${err}`));
		}).catch(async (err) => {
			if (err.code === TOO_MUCH_TIME) {
				// Send a new message with no file attached to it.
				await message.channel.send({ embeds: media.embeds, components: components })
					.catch((err) => console.error(`\t↪ Cannot send replacing message with no media attached :\n${err}`));

				return console.warn(`\t↪ File takes too much time to be sent`);
			}

			if (err.httpStatus === TOO_HEAVY)
				return console.log(`\t↪ Cannot send embed file because too heavy :\n${err}`);

			console.error(`\t↪ Cannot send embed preview message :\n${err}`);
		});

	}
}

module.exports = AbstractSocial;