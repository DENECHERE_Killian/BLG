// noinspection JSUnresolvedFunction

/**
 * @author Killian
 */
const AbstractSocial = require("./AbstractSocial");
const sealedbox      = require("tweetnacl-sealedbox-js");
const Discord        = require("discord.js");
const crypto         = require("crypto");
const fetch          = require("node-fetch");
const Utils          = require("../../utilities/Utils");

const BASE_COOKIES = [
	'mid=Yo9u6AALAAH5wXJL53KL6hZ4fqg4',
	'ig_did=00B227BA-4176-4573-A219-87CCF410F467',
	'datr=ep6gYr09dJn3VDQdXlDRpO8X',
	'dpr=1.25',
	'shbid="16814\\0544318180139\\0541688242132:01f79d14b835616769c38688f93fb56b76a15d77ddc423eb7da14da5b2a2652820ec67e1"',
	'shbts="1656706132\\0544318180139\\0541688242132:01f7e3d06f08fa266c16412d0dcb36a068c14ac0d40729196585c5ead0991b246e79bfb5"',
	'rur="RVA\\05453169207461\\0541688293910:01f748d7efa3004f6a9c89bef35bd02ee003ff1c54781407a71b45881708b65b6edc51b1"'
].join("; ");

/**
 * This class represent the Instagram messages integration.
 * It is used to embed Instagram content directly into Discord without having the members of a guild to click on every link to see the related content.
 */
class Instagram extends AbstractSocial {
	PLATFORM_COLOR = "#bc3382";
	PLATFORM_NAME  = "Instagram";
	DOMAINS        = ["www.instagram.com"]; // Recognized urls

	constructor() {
		super();
		this.encryptedPassword = undefined; // Used to obtain the sessionid when login to Instagram
		this.crsfToken         = undefined; // Used to fetch datas
		this.sessionId         = undefined; // Used to fetch datas
		this.userId            = undefined; // Used to identify the account logged-in in order to fetch datas
		this.ajax              = undefined; // Used to fetch datas
	}

	/**
	 * Remove some media direct link parameters in order to get it looks a way shorter when sent as message content.
	 * @param link {string} The media link that will be sent.
	 * @returns {string} The shortened link.
	 */
	#urlShortner(link) {
		const split     = link.split("?");
		let finalResult = `${split[0]}?`;
		split[1].split("&").forEach(element => {
			if (element.startsWith("_nc_sid") ||
				element.startsWith("_nc_cat") ||
				element.startsWith("efg") ||
				element.startsWith("ccb"))
				return null;

			finalResult += `&${element}`;
		});
		return finalResult;
	}

	/**
	 * Remove useless parts of a provided Instagram link and identify the type of media hidden behind this link.
	 * @param link {string} The original Instagram media link.
	 * @returns {{link: string, type: string}|{isNotEmbeddable: number}}
	 */
	cleanLink(link) {
		console.log(`\t↪ Given link : ${link}`);
		if (!link)
			return { isNotEmbeddable: AbstractSocial.BAD_URL };

		let split    = link.split("?")[0].split("/"); // Remove link parameters
		let linkType = split[3];

		switch (linkType) {
			case "tv":
			case "p":
			case "reel":
				if (!split[4] || split[4] === "")
					return { isNotEmbeddable: AbstractSocial.BAD_URL };

				link = split.slice(0, 5).join("/"); // This is a post, a tv or a reel : we just need keep the following id
				break;

			case "stories":
				if (!split[4] || split[4] === "" ||
					!split[5] || split[5] === "")
					return { isNotEmbeddable: AbstractSocial.BAD_URL };

				link = split.slice(0, 7).join("/"); // This is a storie : we just need keep the following account name and id
				break;

			default:
				linkType = (split.length === 4 || (split.length === 5 && split[4] === "")) ? "profil" : null;
				link     = split.slice(0, 4).join("/"); // This is a profile or something else : no need of any id
		}

		return { type: linkType, link: link };
	}

	/**
	 * Create an {@link Discord.MessageEmbed} for a specific Instagram media from its link.
	 * @param linkType {string} The type of the media (a videos, a reel, ...).
	 * @param link {string} The url of the media.
	 * @param messageAuthor {Discord.User} The user that sent the link.
	 * @returns {Promise<{embeds: Discord.MessageEmbed[], files: Discord.MessageAttachment[]}>} The Discord message options that can be directly send.
	 */
	getMediaEmbed(linkType, link, messageAuthor) {
		return new Promise(async (resolve, reject) => {
			if (!this.#isEmbedable(link))
				return reject(AbstractSocial.NOT_EMBEDDABLE);

			console.log("\t↪ Fetching for media datas");

			const embedFiles = []; // Used to add files like vidéos to the embed that will be sent.
			let baseEmbed    = new Discord.MessageEmbed().setColor(this.PLATFORM_COLOR);
			let content      = "";
			let datas        = await this.#getMediaDatas(link)
				.catch(err => reject(err));

			if (process.env.TEST === "true")
				console.info(`Debug datas:`, datas);

			if (!datas)
				return null;

			if (linkType === "profil") { // The link corresponds to an instagram account
				datas = datas.graphql.user;
				console.log(`\t↪ Media type is profil`);

				// noinspection JSCheckFunctionSignatures
				baseEmbed.setAuthor({
					name: `${datas.full_name} [@${datas.username}]`,
					iconURL: datas.profile_pic_url,
					url: `https://www.instagram.com/${datas.username}/`
				})
					.setThumbnail(datas.profile_pic_url_hd)
					.setDescription(datas.biography)
					.addFields([
						{ name: "Publications", value: `${datas.edge_owner_to_timeline_media.count}`, inline: true },
						{ name: "Abonnés", value: `${datas.edge_followed_by.count}`, inline: true },
						{ name: "Abonnements", value: `${datas.edge_follow.count}`, inline: true }])
					.setFooter({ text: `Envoyé par ${messageAuthor.username} · Compte ${datas.is_private ? "privé" : "public"}`, iconURL: messageAuthor.displayAvatarURL() });


				return resolve({ content: content, embeds: [baseEmbed], files: embedFiles });
			}

			if (linkType === "stories") {
				// The link corresponds to an instagram storie
				console.log(`\t↪ Media type is storie`);

				baseEmbed.setAuthor({
					name: datas.user.username,
					iconURL: datas.user.profile_pic_url,
					url: `https://www.instagram.com/${datas.user.username}/`
				})
					.setDescription((datas.highlight ? `Story "${datas.hightlite.title}"\n\n` : ``) + `⚠️ Il faut se connecter pour voir cette story ⚠️`)
					.setFooter({ text: `Envoyé par ${messageAuthor.username}`, iconURL: messageAuthor.displayAvatarURL() });

				return resolve({ content: content, embeds: [baseEmbed], files: embedFiles });
			}

			// The link corresponds to an instagram post that can be a carousel, an image, a vidéo or a reel.
			datas            = datas.items?.[0] ?? datas.graphql.shortcode_media;
			const mediaDate  = new Date((datas.taken_at ?? datas.taken_at_timestamp) * 1000).toLocaleString();
			const embedsList = [];
			// noinspection JSCheckFunctionSignatures
			baseEmbed.setAuthor({
				name: `${(datas.user ?? datas.owner).full_name} [@${(datas.user ?? datas.owner).username}]`,
				iconURL: (datas.user ?? datas.owner).profile_pic_url,
				url: `https://www.instagram.com/${(datas.user ?? datas.owner).username}/`
			})
				.setDescription(datas.caption?.text ?? datas.edge_media_to_caption?.edges?.[0]?.node?.text ?? " ")
				.addFields([{ name: "Likes", value: `${datas.like_count ?? datas.edge_media_preview_like.count}`, inline: true },
					{ name: "Commentaires", value: `${datas.comment_count ?? datas.edge_media_preview_comment.count}`, inline: true }])
				.setFooter({ text: `Envoyé par ${messageAuthor.username} · Posté le ${mediaDate}`, iconURL: messageAuthor.displayAvatarURL() });

			embedsList.push(baseEmbed);
			// Attaching images and vidéos depending on media type
			switch (datas.media_type ?? datas.__typename) {
				case 1: // A simple image
					console.log(`\t↪ Media type is image`);
					const bestImage = await this.#getBestMediaQuality(datas.image_versions2.candidates);

					if (bestImage)
						baseEmbed.setImage(bestImage);
					else
						content = this.#urlShortner(datas.image_versions2.candidates[0].url);
					break;

				case "GraphVideo":
				case 2: // Réel / post with vidéo
					console.log(`\t↪ Media type is réel/video`);
					const bestVideo = datas.video_url ?? await this.#getBestMediaQuality(datas.video_versions);

					if (bestVideo)
						embedFiles.push(new Discord.MessageAttachment(bestVideo));
					else
						content = this.#urlShortner(datas.video_url ?? datas.video_versions[0].url);
					break;

				case 8: // Carousel of medias
					if (datas.carousel_media[0].media_type === 2) { // Carousel of vidéos
						console.log(`\t↪ Media type is video carousel`);
						const best = await this.#getBestMediaQuality(datas.carousel_media[0].video_versions);

						if (best)
							embedFiles.push(new Discord.MessageAttachment(best));
						else
							content = this.#urlShortner(datas.carousel_media[0].video_versions[0].url);

						if (datas.carousel_media_count > 1) // If there are more medias that cannot be shown in the embed
							baseEmbed.setFooter({ text: `${datas.carousel_media_count - 1} autre(s) élément(s) · Envoyé par ${messageAuthor.username} · Posté le ${mediaDate}`, iconURL: messageAuthor.displayAvatarURL() });

					} else { // Carousel of images
						console.log(`\t↪ Media type is images carousel`);
						baseEmbed.setImage(datas.carousel_media[0].image_versions2.candidates[0].url)
							.setURL("http://localhost:5000");

						let addedMediaAmount = 1;
						for (let mediaIndex = 1; mediaIndex < datas.carousel_media_count; mediaIndex++) {
							if (!datas.carousel_media[mediaIndex] || addedMediaAmount === 4)
								break;

							const best = await this.#getBestMediaQuality(datas.carousel_media[mediaIndex].image_versions2.candidates);
							if (!best) // If no sendable images, just skip to the next image of the post
								continue;

							addedMediaAmount++;
							// Add another aother that will be sent at the same time as the first one with all informations
							embedsList.push(new Discord.MessageEmbed().setURL("http://localhost:5000").setImage(best));
						}

						if (datas.carousel_media_count > 4) // If there are more medias that cannot be shown in the embed
							baseEmbed.setFooter({ text: `${datas.carousel_media_count - addedMediaAmount} autre(s) élément(s) · Envoyé par ${messageAuthor.username} · Posté le ${mediaDate}`, iconURL: messageAuthor.displayAvatarURL() });
					}
					break;

				default:
					console.warn(`\t↪ Media type not recognized : ${datas.media_type ?? datas.__typename}`);
			}

			resolve({ content: content, embeds: embedsList, files: embedFiles });
		});
	}

	/**
	 * Fetch media page to query json informations.
	 * @param link {string} The url of the embeddable media.
	 * @returns {Promise<json>} The datas scrapped from Instagram media web page.
	 */
	#getMediaDatas(link) {

		return new Promise(async (resolve, reject) => {
			// Get the instagram session id
			if (!this.sessionId) {
				const connection = await this.#loadInstagramSessionId()
					.catch(err => reject(err));
				if (!connection)
					return null;
			}

			console.log(`\t↪ Fetching ${link}?__a=1&__d=dis`);
			await fetch(`${link}?__a=1&__d=dis`, {
				maxRedirects: 3,
				headers: {
					"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
					"accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
					"cookie": `${BASE_COOKIES}csrftoken=${this.crsfToken}; ds_user_id=${this.userId}; sessionid=${this.sessionId};`
				}
			}).then(async res => {
				let datas = await res.json();
				console.log(`\t\t↪ No datas received, try fo tech a second time without cookies`);

				if (Object.keys(datas).length === 0) { // No datas were received
					datas = await fetch(`${link}?__a=1&__d=dis`, {
						maxRedirects: 3,
						headers: {
							"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
							"accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7"
						}
					}).then(async res => await res.json())
						.catch((err) => {
							if (err === AbstractSocial.NOT_FOUND)
								return reject(AbstractSocial.NOT_FOUND);
							if (err.type === "max-redirect")
								return reject(AbstractSocial.SESSION_OUTDATED);

							console.log(`\t\t↪ Second Fetch error :\n${err}`);
							reject(err);
						});

					if (Object.keys(datas).length === 0)
						return reject(AbstractSocial.NOT_FOUND);
				}

				console.log(`\t\t↪ Datas well extracted`);
				resolve(datas);

			}).catch((err) => {
				if (err === AbstractSocial.NOT_FOUND)
					return reject(AbstractSocial.NOT_FOUND);
				if (err.type === "max-redirect")
					return reject(AbstractSocial.SESSION_OUTDATED);

				console.error(`\t\t↪ Fetch error :\n${err}`);
				reject(err);
			});
		});
	}

	// noinspection JSMethodCanBeStatic
	/**
	 * Tell if the media related to this instagram link is embeddable or not.
	 * Only those types or contents are embeddable :
	 * - stories
	 * - posts
	 * - reels
	 * @param link {string} The Instagram link representing the probably embeddable media.
	 * @returns {boolean} True if this media is embeddable, false otherwise.
	 */
	#isEmbedable(link) {
		if (!link)
			return false;

		const split         = link.split("/");
		const rejectedPages = ["explore", "direct", "challenge", "accounts", "qr", "legal", "directory", "web"];

		// Return false if one of those conditions is encountered
		return !(!split[3] || // Home page
			rejectedPages.includes(split[3]) || // Page with no media
			(split[3] === "stories" && (!split[4] || !split[5])) || // Srories page with missing elements or wrong link
			((split[3] === "p" || split[3] === "reel") && !split[4])); // Post or reel with no id
	}

	/**
	 * Choose the best version of a media in its best quality while respecting the 8 Mo restriction of Discord.
	 * @param qualitiesList A list of different quality media version object.
	 * @returns {Promise<String>} The url of the version in the best quality < 8Mo.
	 */
	#getBestMediaQuality(qualitiesList) {
		return new Promise(async (resolve) => {
			let lastMediaSize;
			for (const videoQuality of qualitiesList) {
				lastMediaSize = await Utils.getOnlineMediasize(videoQuality.url);
				if (lastMediaSize < Utils.DISCORD_MAX_MEDIA_SIZE)
					return resolve(videoQuality.url);
			}

			// Even the poorest quality video is too heavy to be sent
			console.log(`\t↪ Media is too heavy to be uploaded (${lastMediaSize / 1000000}Mo is over 8Mo), a direct link will be send`);
			resolve();
		});
	}

	/***
	 * Query instagram to get the session id cookie value in order to be authentified and to get access to json instagram pages values.
	 * @returns {Promise<String>} The session id cookie value.
	 */
	async #loadInstagramSessionId() {
		console.log(`\t↪ Getting instagram session id`);
		return new Promise(async (resolve, reject) => {
			await this.#loadEncryptedPassword();

			fetch("https://www.instagram.com/accounts/login/ajax/", {
				"headers": {
					"accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
					"content-type": "application/x-www-form-urlencoded",
					"x-csrftoken": this.crsfToken,
					"x-ig-www-claim": "hmac.AR2oa8UxtioN_7VCHGCz-910IYvk6jIBYfvSxCF8FzU8hCx3",
					"x-instagram-ajax": this.ajax,
					"cookie": `${BASE_COOKIES}csrftoken=${this.crsfToken};`,
					"Referer": "https://www.instagram.com/"
				},
				"body": `enc_password=${encodeURIComponent(this.encryptedPassword)}&username=${process.env.IG_USERNAME}&queryParams=%7B%22next%22%3A%22%2F%22%7D&optIntoOneTap=false&stopDeletionNonce=&trustedDeviceRecords=%7B%7D`,
				"method": "POST"
			}).then(t => {
				if (t.status === AbstractSocial.BAD_REQUEST) {
					console.error(`\t\t↪ Bad request : 400 code`);
					return reject(AbstractSocial.BAD_REQUEST);
				}
				if (t.status === AbstractSocial.ACCESS_DENIED) {
					console.error(`\t\t↪ Access denied : 403 code`);
					return reject(AbstractSocial.ACCESS_DENIED);
				}
				if (t.status === AbstractSocial.TOO_MANY_REQUESTS) {
					console.error(`\t\t↪ Too many requests : 429 code`);
					return reject(AbstractSocial.TOO_MANY_REQUESTS);
				}
				if (t.headers.get("set-cookie").indexOf("sessionid") === -1) {
					console.log(t);
					console.log(t.headers.get("set-cookie"));
					console.error(`\t\t↪ Headers does not contains session id`);
					return reject(AbstractSocial.NO_SESSION_ID);
				}
				if (t.headers.get("set-cookie").indexOf("ds_user_id") === -1) {
					console.log(t.headers.get("set-cookie"));
					console.error(`\t\t↪ Headers does not contains user id`);
					return reject(AbstractSocial.NO_USER_ID);
				}

				const cookies  = t.headers.get("set-cookie").split("; ");
				this.userId    = cookies.find(cookie => cookie.includes("ds_user_id")).split("=")[1];
				this.sessionId = cookies.find(cookie => cookie.includes("sessionid")).split("=")[1];
				console.log(`\t\t↪ User id is : ${this.userId}`);
				console.log(`\t\t↪ Session id is : ${this.sessionId} (expire : ${cookies.find(cookie => cookie.includes("expires")).split("=")[1]})`);
				return resolve(this.sessionId);
			}).catch(err => {
				console.error(`\t\t↪ Instagram authentication failed : ${err}`);
				return reject(err);
			});
		});
	}

	#loadEncryptedPassword() {
		return new Promise(async (resolve, reject) => {

			const instaDatas = await fetch("https://www.instagram.com/data/shared_data/")
				.then(async res => await res.json())
				.catch(err => reject(err));

			if (!instaDatas)
				return;

			this.crsfToken = instaDatas.config.csrf_token;
			this.ajax      = instaDatas.rollout_hash;
			console.log(`\t\t↪ Crsf token is "${this.crsfToken}"`);

			const time         = Date.now().toString();
			const key          = crypto.pseudoRandomBytes(32);
			const iv           = Buffer.alloc(12, 0);
			const cipher       = crypto.createCipheriv("aes-256-gcm", key, iv).setAAD(Buffer.from(time));
			const aesEncrypted = Buffer.concat([cipher.update(Buffer.from(process.env.IG_PASSWORD)), cipher.final()]);
			const authTag      = cipher.getAuthTag();
			const encryptedKey = sealedbox.seal(key, Buffer.from(instaDatas.encryption.public_key, "hex"));
			const encrypted    = Buffer.concat([
				Buffer.from([1, Number(instaDatas.encryption.key_id), encryptedKey.byteLength & 255, (encryptedKey.byteLength >> 8) & 255]),
				encryptedKey,
				authTag,
				aesEncrypted
			]).toString("base64");

			this.encryptedPassword = `#PWD_INSTAGRAM_BROWSER:${instaDatas.encryption.version}:${time}:${encrypted}`;
			console.log(`\t\t↪ Encrypted password is : ${this.encryptedPassword}`);
			resolve();
		});
	}
}

module.exports = Instagram;