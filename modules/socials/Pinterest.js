// noinspection JSUnresolvedFunction, JSCheckFunctionSignatures

/**
 * @author Killian
 */
const AbstractSocial = require("./AbstractSocial");
const Discord        = require("discord.js");
const fetch          = require("node-fetch");
const Utils          = require("../../utilities/Utils");

/**
 * This class represent the Pinterest messages integration.
 * It is used to embed Pinterest content directly into Discord without having the members of a guild to click on every link to see the related content.
 */
class Pinterest extends AbstractSocial {
	PLATFORM_COLOR = "#e60023";
	PLATFORM_NAME  = "Pinterest";
	DOMAINS        = ["www.pinterest.fr", "pin.it"]; // Recognized urls

	constructor() {
		super();
	}

	/**
	 * Retrieve a clean link from any Pinterest link and the type of media hidden behind.
	 * A clean link has no url parameter.
	 * If the link was sent from mobile, the return the original one.
	 * The type of media can be a user account or a video.
	 * @param link {string} The Pinterest link to clear.
	 * @returns {Promise<{link: string, type: string}|{isNotEmbeddable: number}>}
	 */
	async cleanLink(link) {
		console.log(`\t↪ Given link : ${link}`);
		if (!link)
			return { isNotEmbeddable: AbstractSocial.BAD_URL };

		if (link.search("/pin.it/") !== -1) {
			link = await this.#getLongLink(link);
			if (!link)
				return { isNotEmbeddable: AbstractSocial.BAD_URL };
		}

		link = link.split("/sent/")[0].split("?")[0]; // Remove parameters
		return { type: "", link: link };
	}

	/**
	 * Create an {@link Discord.MessageEmbed} for a specific Pinterest media from its link.
	 * @param _ {string} The type of link given (if identifiable with the url).
	 * @param link {string} A clean Pinterest link.
	 * @param messageAuthor {Discord.User} The discord user that sent the message containing the link.
	 * @returns {Promise<{embeds: Discord.MessageEmbed[], files: Discord.MessageAttachment[]}>} The Discord message options that can be directly send.
	 */
	getMediaEmbed(_, link, messageAuthor) {
		return new Promise(async (resolve, reject) => {
			// No pin id or account name
			const split = link.split("/");
			if (split.length < 4 || split[3] === "")
				return reject(AbstractSocial.BAD_URL);

			const embedsList   = [];
			let embedFiles     = []; // Used to add files like videos to the embed that will be sent.
			let baseEmbed      = new Discord.MessageEmbed().setColor(this.PLATFORM_COLOR);
			let messageContent = "";

			// Fetch the media json datas
			console.log("\t↪ Fetching for media datas");
			const media = await this.#getMediaDatas(link);

			// Recognize the media type
			const mediaType = (() => {
				if (!media)
					return "other";
				if (media.type === "pin" && media.videos)
					return "video";
				if (media.type === "pin" && media.images)
					return "image";
				if (media.type === "user")
					return "user";
				return "other";
			})();

			// Verify if embeddable of not
			if (mediaType === "other")
				return reject(AbstractSocial.NOT_EMBEDDABLE);

			console.log(`\t↪ Media type is ${mediaType}`);
			// Attach media to embed
			switch (mediaType) {
				case "video":
					const videoURL = media.videos.video_list.V_720P.url;
					if (await Utils.getOnlineMediasize(videoURL) > Utils.DISCORD_MAX_MEDIA_SIZE)
						messageContent = videoURL;
					else
						embedFiles.push(new Discord.MessageAttachment(videoURL));
					break;

				case "image":
					const imageURL = media.images.orig.url;
					if (await Utils.getOnlineMediasize(imageURL) > Utils.DISCORD_MAX_MEDIA_SIZE)
						messageContent = imageURL;
					else
						baseEmbed.setImage(imageURL);
					break;

				case "user":
					embedsList.push(baseEmbed
						.setAuthor({
							name: `${media.full_name} [@${media.username}]`,
							iconURL: media.image_medium_url,
							url: `https://www.pinterest.fr/${media.username}/`
						})
						.setDescription(media.about ?? " ")
						.addFields([
							{ name: "Abonnés", value: `${media.follower_count}`, inline: true },
							{ name: "Abonnements", value: `${media.following_count}`, inline: true },
							{ name: "Épingles", value: `${media.pin_count}`, inline: true }])
						.setFooter({ text: `Envoyé par ${messageAuthor.username} · Dernière épingle le ${new Date(media.last_pin_save_time).toLocaleString()}`, iconURL: messageAuthor.displayAvatarURL() })
					);

					return resolve({ content: messageContent, embeds: embedsList, files: embedFiles });
				default:
					console.warn(`Media type not recognized for Pinterest embed : ${mediaType}`);
			}

			// This embed structure is not for a user embed
			baseEmbed
				.setAuthor({
					name: `${media.pinner.full_name} [@${media.pinner.username}]`,
					iconURL: media.pinner.image_medium_url,
					url: `https://www.pinterest.fr/${media.pinner.username}/`
				})
				.setDescription(media.closeup_unified_description ?? " ")
				.addFields([
					{ name: "Réactions", value: `${Object.values(media.reaction_counts).reduce((sum, reactCount) => sum += reactCount, 0)}`, inline: true },
					{ name: "Commentaires", value: `${media.aggregated_pin_data.comment_count}`, inline: true }])
				.setFooter({ text: `Envoyé par ${messageAuthor.username} · Posté le ${new Date(media.created_at).toLocaleString()}`, iconURL: messageAuthor.displayAvatarURL() });

			embedsList.push(baseEmbed);
			return resolve({ content: messageContent, embeds: embedsList, files: embedFiles });
		});
	}

	/**
	 * Fetch media page to query json informations.
	 * @param link {string} The url of the embeddable media.
	 * @returns {Promise<json>} The datas scrapped from media web page.
	 */
	#getMediaDatas(link) {
		return new Promise((resolve, reject) => {
			fetch(link).then(async res => {
				const urlId = link.split("/")[4];
				const body  = await res.text();
				const json  = JSON.parse(body
					.split("<script id=\"__PWS_DATA__\" type=\"application/json\">")[1]
					.split("</script>")[0]);

				resolve(json.props.initialReduxState.pins[urlId] ?? Object.values(json.props.initialReduxState.users).find(element => element.type !== undefined) ?? null);
			}).catch(err => {
				reject(err);
			});
		});
	}

	/**
	 * Retrieve the original Pinterest link hidden behind compressed one.
	 * @param url {string} A Pinterest compressed link. (format pin.it).
	 * @returns {Promise<string|null>} The original media link if available.
	 */
	async #getLongLink(url) {
		return new Promise((resolve, reject) => {
			fetch(url).then(async res => {
				resolve(res.url.split("/sent/")[0] ?? null);
			}).catch(err => {
				reject(err);
			});
		});
	}
}

module.exports = Pinterest;