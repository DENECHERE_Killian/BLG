/**
 * @author Killian
 */

const Instagram = require("./Instagram");
const Pinterest = require("./Pinterest");
const TikTok    = require("./TikTok");
const Reddit    = require("./Reddit");

// noinspection JSIgnoredPromiseFromCall
class SocialEmbedder {

	constructor() {
		this.pinterest = new Pinterest();
		this.instagram = new Instagram();
		this.tiktok    = new TikTok();
		this.reddit    = new Reddit();
	}

	watchForSocialLink(message) {
		this.pinterest.searchForLink(message);
		this.instagram.searchForLink(message);
		this.tiktok.searchForLink(message);
		this.reddit.searchForLink(message);
	}
}

module.exports = SocialEmbedder;