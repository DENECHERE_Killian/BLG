// noinspection JSUnresolvedFunction,JSCheckFunctionSignatures

/**
 * @author Killian
 */
const AbstractSocial = require("./AbstractSocial");
const Discord        = require("discord.js");
const Utils          = require("../../utilities/Utils");
const fetch          = require("node-fetch");

const TIMEOUT_DELAY = 5000; // ms

/**
 * This class represent the TikTok messages integration.
 * It is used to embed TikTok content directly into Discord without having the members of a guild to click on every link to see the related content.
 */
class TikTok extends AbstractSocial {
	PLATFORM_COLOR = "#fe2c55";
	PLATFORM_NAME  = "TikTok";
	DOMAINS        = ["www.tiktok.com", "vm.tiktok.com"]; // Recognized urls

	constructor() {
		super();
	}

	/**
	 * Retrieve a clean link from any TikTok link and the type of media hidden behind.
	 * A clean link has no url parameter.
	 * If the link was sent from mobile, the return the original one.
	 * The type of media can be a user account or a video.
	 * @param link {string} The TikTok link to clear.
	 * @returns {Promise<{link: string, type: string}|{isNotEmbeddable: number}>}
	 */
	async cleanLink(link) {
		console.log(`\t↪ Given link : ${link}`);
		if (!link)
			return { isNotEmbeddable: AbstractSocial.BAD_URL };

		if (link.search("/vm.tiktok.com/") !== -1) {
			link = await this.#getLongLink(link);
			if (!link)
				return { isNotEmbeddable: AbstractSocial.BAD_URL };
		}

		link         = link.split("?")[0]; // Remove link parameters
		let split    = link.split("/");
		let linkType = split[4];

		switch (linkType) {
			case "video":
				if (split.length < 6 || split[3] === "" || split[4] === "" || split[5] === "") // An url part is missing (id or account or other)
					return { isNotEmbeddable: AbstractSocial.BAD_URL };
				return { type: linkType, link: split.slice(0, 6).join("/") };

			case "playlist":
				return { isNotEmbeddable: AbstractSocial.NOT_EMBEDDABLE }; // Cannot retrieve informations about this type of media, so I do not embed them.
			// return { type: linkType, link: link };

			default:
				return { type: "profil", link: split.slice(0, 4).join("/") }; // This is a profile or something else : no need of any id
			// return { isNotEmbeddable: AbstractSocial.NOT_EMBEDDABLE }; // Cannot retrieve informations about this type of media, so I do not embed them.
		}
	}

	/**
	 * Create an {@link Discord.MessageEmbed} for a specific TikTok media from its link.
	 * @param linkType {string} The type of link provided.
	 * @param link {string} A clean TikTok link.
	 * @param messageAuthor {Discord.User} The discord user that sent the message containing the link.
	 * @returns {Promise<{embeds: Discord.MessageEmbed[], files: Discord.MessageAttachment[]}>} The Discord message options that can be directly send.
	 */
	getMediaEmbed(linkType, link, messageAuthor) {
		return new Promise(async (resolve, reject) => {
			let embedFiles     = []; // Used to add files like videos to the embed that will be sent.
			let baseEmbed      = new Discord.MessageEmbed().setColor(this.PLATFORM_COLOR);
			let messageContent = "";
			const splittedLink = link.split("/");

			switch (linkType) {
				case "video":
					const videoDatas = await this.#getMediaDatas(`https://www.tiktok.com/${splittedLink[3]}/video/${splittedLink[5]}`)
						.then(res => res.ItemModule[splittedLink[5]])
						.catch(async err => reject(err));

					if (!videoDatas)
						return;

					console.log(`\t↪ Video datas obtained`);

					baseEmbed
						.setAuthor({
							name: videoDatas.nickname,
							iconURL: videoDatas.avatarThumb,
							url: `https://www.tiktok.com/@${videoDatas.author}/`
						})
						.setDescription(videoDatas.desc)
						.addFields([
							{ name: "Partages", value: `${videoDatas.stats.shareCount}`, inline: true },
							{ name: "Commentaires", value: `${videoDatas.stats.commentCount}`, inline: true }])
						.setFooter({ text: `Envoyé par ${messageAuthor.username}`, iconURL: messageAuthor.displayAvatarURL() });

					// Decide to wether embed the video or send the direct link to video
					if (await Utils.getOnlineMediasize(videoDatas.video.downloadAddr) > Utils.DISCORD_MAX_MEDIA_SIZE)
						messageContent = videoDatas.video.downloadAddr.split("?")[0];
					else
						embedFiles.push(new Discord.MessageAttachment(videoDatas.video.downloadAddr, `${videoDatas.id}.mp4`));
					break;

				case "profil":
					const profileDatas = await this.#getMediaDatas(`https://www.tiktok.com/${splittedLink[3]}`)
						.then(res => {
							const concatenatedInfos = res.UserModule.users[splittedLink[3].replace("@", "")];
							concatenatedInfos.stats = res.UserModule.stats[splittedLink[3].replace("@", "")];
							return concatenatedInfos;
						})
						.catch(async err => reject(err));

					if (!profileDatas)
						return;

					console.log(`\t↪ Profil datas obtained`);

					baseEmbed.setAuthor({
						name: `${profileDatas.nickname} [@${profileDatas.uniqueId}]`,
						iconURL: profileDatas.avatarLarger,
						url: `https://www.tiktok.com/@${profileDatas.uniqueId}/`
					})
						.setDescription(profileDatas.signature)
						.addFields([
							{ name: "Publications", value: `${profileDatas.stats.videoCount}`, inline: true },
							{ name: "Coeurs", value: `${profileDatas.stats.heartCount}`, inline: true },
							{ name: "Abonnés", value: `${profileDatas.stats.followerCount}`, inline: true }
						])
						.setFooter({ text: `Envoyé par ${messageAuthor.username} · Compte ${profileDatas.privateAccount ? "privé" : "public"}`, iconURL: messageAuthor.displayAvatarURL() });

					break;

				default:
					console.warn(`\t↪ TikTok link type not recognized : ${linkType}`);
			}

			return resolve({ content: messageContent, embeds: [baseEmbed], files: embedFiles });
		});
	}

	/**
	 * Fetch media page to query json informations.
	 * @param link {string} The url of the embeddable media.
	 * @returns {Promise<json>} The datas scrapped from media web page.
	 */
	#getMediaDatas(link) {
		console.log(`\t↪ Fetching for media datas on ${link}`);
		return new Promise((resolve, reject) => {
			// The msToken cookie is necessary to fetch for account datas, not for media datas
			fetch(link, {
				timeout: TIMEOUT_DELAY,
				headers: {
					"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.66 Safari/537.36",
					"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
					"accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
					"cookie": "msToken=gyilIE38vc5If-EhOHC-cCqodJ8Vyyn0zXg-mKDJsMXII5NjR5EfB7CZlZf7NgPL0HeOA3UOxun-BLdLQnTTs5aKS0eFYyYcbLQXTOZZj9adZMZyWkAgBJY9SxN4E_BKjR_1CPW59WPx4eGMRvg=; "
				}
			}).then(async res => {
				const response      = await res.text();
				const breakResponse = response
					.split(`<script id="SIGI_STATE" type="application/json">`)[1]
					.split(`</script>`)[0];

				resolve(JSON.parse(breakResponse));
			}).catch(err => {
				reject(err);
			});
		});
	}

	/**
	 * Retrieve the original TikTok link hidden behind a mobile compressed link.
	 * @param url {string} A TikTok mobile shared link. (format vm.tiktok.com).
	 * @returns {Promise<string|null>} The original media link if available.
	 */
	async #getLongLink(url) {
		const req = await fetch(`${url}`, {
			timeout: TIMEOUT_DELAY,
			headers: {
				"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.66 Safari/537.36",
				"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
				"accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7"
			}
		})
			.catch(err => {
				if (err.message.indexOf("timeout") !== -1) {
					const timeoutLink = err.message.split("at: ")[1];
					console.log(`\t↪ Timed outed at : ${timeoutLink}`);

					if (timeoutLink.indexOf("www.tiktok.com") !== -1) // The redirection had been well effected
						return { url: timeoutLink };
				}

				console.error(`Cannot fetch the original link of the provided tiktok url :\n${err}`);
			});

		console.log(`\t↪ Hidden link : ${req.url}`);
		return req.url ?? null;
	}
}

module.exports = TikTok;