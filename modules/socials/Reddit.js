// noinspection JSCheckFunctionSignatures

/**
 * @author Killian
 */
const AbstractSocial = require("./AbstractSocial");
const Discord        = require("discord.js");
const fetch          = require("node-fetch");
const Utils          = require("../../utilities/Utils");

/**
 * This class represent the Reddit messages integration.
 * It is used to embed Reddit content directly into Discord without having the members of a guild to click on every link to see the related content.
 */
class Reddit extends AbstractSocial {
	PLATFORM_COLOR = "#ff581a";
	PLATFORM_NAME  = "Reddit";
	DOMAINS        = ["www.reddit.com", "v.redd.it"]; // Recognized urls

	constructor() {
		super();
	}

	/**
	 * Remove useless parts of a provided Reddit link and identify the type of media hidden behind this link.
	 * @param link {string} The original Reddit media link.
	 * @returns {Promise<{link: string, type: string}|{isNotEmbeddable: number}>}
	 */
	async cleanLink(link) {
		console.log(`\t↪ Given link : ${link}`);
		if (!link)
			return { isNotEmbeddable: AbstractSocial.BAD_URL };

		if (link.search("/v.redd.it/") !== -1) {
			link = await fetch(link)
				.then(res => res.url)
				.catch(err => console.error(err));

			if (!link)
				return { isNotEmbeddable: AbstractSocial.BAD_URL };
			console.log(`\t↪ Hidden link : ${link}`);
		}

		link           = link.split("?")[0]; // Remove link parameters
		const split    = link.split("/");
		const linkType = split[3];

		switch (linkType) {
			case "r": // This is something about a reddit sub
				return { type: "sub", link: link };

			case "u":
			case "user":
				return { type: "user", link: link };

			default:
				return { isNotEmbeddable: AbstractSocial.NOT_EMBEDDABLE };
		}
	}

	/**
	 * Create an {@link Discord.MessageEmbed} for a specific Reddit media from its link.
	 * @param _ {string} The type of link given (if identifiable with the url).
	 * @param link {string} The url of the media.
	 * @param messageAuthor {Discord.User} The user that sent the link.
	 * @returns {Promise<{embeds: Discord.MessageEmbed[], files: Discord.MessageAttachment[]}>} The Discord message options that can be directly send.
	 */
	getMediaEmbed(_, link, messageAuthor) {
		return new Promise(async (resolve, reject) => {
			let embedFiles     = []; // Used to add files like videos to the embed that will be sent.
			let baseEmbed      = new Discord.MessageEmbed().setColor(this.PLATFORM_COLOR);
			let embedsList     = [baseEmbed];
			let messageContent = "";

			console.log("\t↪ Fetching for media datas");
			// Fetching to get the exact link type
			let mediaDatas = await this.#getMediaDatas(link)
				.catch(async err => reject(err));
			if (!mediaDatas)
				return;

			if (mediaDatas instanceof Array) // When the page is about a comment, the first element concern the comment and the second concern all next answers.
				mediaDatas = mediaDatas[0].data.children[0];

			switch (mediaDatas.kind) {
				case "Listing":
					return reject(AbstractSocial.BAD_URL);

				case "t2": // user
					mediaDatas = mediaDatas.data;
					console.log(`\t↪ Media type is user`);

					baseEmbed
						.setAuthor({
							name: `${mediaDatas.name}`,
							iconURL: mediaDatas.icon_img.split("?")[0],
							url: link
						})
						.setDescription(mediaDatas.public_description ?? " ")
						.addFields([
							{ name: "Karma total", value: String(mediaDatas.total_karma), inline: true },
							{ name: "Abonnés", value: String(mediaDatas.subreddit.subscribers), inline: true }])
						.setFooter({ text: `Envoyé par ${messageAuthor.username} · Inscrit le ${new Date(mediaDatas.created * 1000).toLocaleString()}`, iconURL: messageAuthor.displayAvatarURL() });
					break;

				case "t3": // Comments that could be an image, carousel, video
					mediaDatas        = mediaDatas.data;
					const authorDatas = await this.#getMediaDatas(`https://www.reddit.com/user/${mediaDatas.author}/`)
						.then(json => json.data)
						.catch(async err => reject(err));

					if (!authorDatas)
						return;

					baseEmbed
						.setAuthor({
							name: `${mediaDatas.author} sur ${mediaDatas.subreddit_name_prefixed}`,
							iconURL: authorDatas.icon_img.split("?")[0],
							url: `https://www.reddit.com/user/${mediaDatas.author}/`
						})
						.setDescription(
							`${mediaDatas.over_18 ? "☰☰☰☰ **🔞 Contenu pour adultes 🔞** ☰☰☰☰\n\n" : ""}` +
							`${mediaDatas.thumbnail === "nsfw" ? "☰☰☰☰ **⚠️ Contenu NSFW ⚠️** ☰☰☰☰\n\n" : ""}` +
							`${mediaDatas.title}`)
						.addFields([
							{ name: "Votes positifs", value: `${mediaDatas.ups} (${mediaDatas.upvote_ratio * 100}%)`, inline: true },
							{ name: "Commentaires", value: `${mediaDatas.num_comments}`, inline: true }])
						.setFooter({ text: `Envoyé par ${messageAuthor.username} · Posté le ${new Date(mediaDatas.created * 1000).toLocaleString()}`, iconURL: messageAuthor.displayAvatarURL() });

					// Decide what to do with the media depending on its type.
					if (mediaDatas.post_hint === "image") { // Simple image
						console.log(`\t↪ Media type is Image`);
						if (await Utils.getOnlineMediasize(mediaDatas.url_overridden_by_dest) > Utils.DISCORD_MAX_MEDIA_SIZE)
							messageContent = mediaDatas.url_overridden_by_dest;
						else if (mediaDatas.thumbnail === "nsfw")
							embedFiles.push(new Discord.MessageAttachment(mediaDatas.url_overridden_by_dest, `SPOILER_FILE.jpg`));
						else
							baseEmbed.setImage(mediaDatas.url_overridden_by_dest);

					} else if (mediaDatas.post_hint?.endsWith("video")) { // Simple video
						console.log(`\t↪ Media type is Video`);
						if (mediaDatas.thumbnail !== "nsfw")
							messageContent = link; // Embed the direct link to get the benefice of the default discord integration

					} else if (mediaDatas.gallery_data) { // A carousel
						console.log(`\t↪ Media type is Carousel`);
						const mediasURLList = mediaDatas.gallery_data.items.map(galleryItem => mediaDatas.media_metadata[galleryItem.media_id].s.u.replaceAll("amp;", ""));
						mediasURLList.splice(0, 4).forEach((url, index) => {
							if (index === 0)
								baseEmbed.setImage(url).setURL("http://localhost:5000");
							else
								embedsList.push(new Discord.MessageEmbed().setURL("http://localhost:5000").setImage(url));
						});

						if (mediasURLList.length > 0) // If there are more medias that cannot be shown in the embed
							baseEmbed.setFooter({ text: `Envoyé par ${messageAuthor.username} · Posté le ${new Date(mediaDatas.created * 1000).toLocaleString()}`, iconURL: messageAuthor.displayAvatarURL() });

					} else if (mediaDatas.selftext) { // A text
						console.log(`\t↪ Media type is Text`);
						if (mediaDatas.over_18 || mediaDatas.thumbnail === "nsfw")
							mediaDatas.selftext = `||${mediaDatas.selftext}||`;
						baseEmbed.setDescription(`${baseEmbed.description}\n\n**Contenu du post textuel** :\n${mediaDatas.selftext}`);

					} else {
						console.warn(`Try to embed a not recognized media type`);
					}
					break;

				case "t5": // A entire reddit sub
					mediaDatas = mediaDatas.data;
					console.log(`\t↪ Media type is sub`);
					baseEmbed
						.setAuthor({
							name: mediaDatas.display_name_prefixed,
							iconURL: mediaDatas.header_img,
							url: link
						})
						.setDescription(mediaDatas.public_description ?? " ")
						.setThumbnail(mediaDatas.icon_img)
						.addFields([
							{ name: "Comptes actifs", value: String(mediaDatas.accounts_active), inline: true },
							{ name: "Abonnés", value: String(mediaDatas.subscribers), inline: true }])
						.setFooter({ text: `Envoyé par ${messageAuthor.username} · Créé le ${new Date(mediaDatas.created * 1000).toLocaleString()} · Sub ${mediaDatas.subreddit_type === "public" ? "public" : "privé"}`, iconURL: messageAuthor.displayAvatarURL() });
					break;

				default:
					console.log(`\t↪ Reddit media type not recognized : ${mediaDatas.kind}`);
					return reject(AbstractSocial.NOT_EMBEDDABLE);
			}

			return resolve({ content: messageContent, embeds: embedsList, files: embedFiles });
		});
	}

	/**
	 * Fetch media page to query json information.
	 * @param link {string} The url of the embeddable media.
	 * @returns {Promise<json>} The datas scrapped from Reddit media web page.
	 */
	#getMediaDatas(link) {
		return new Promise(async (resolve, reject) => {
			console.log(`\t↪ Fetching ${link}about.json`);
			await fetch(`${link}about.json`).then(async res => {
				resolve(await res.json());
			}).catch((err) => {
				reject(err);
			});
		});
	}
}

module.exports = Reddit;