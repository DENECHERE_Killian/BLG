<section style="text-align: center;">

  # BLG - Bot Discord
  <br/>
  <img src="./modules/werewolf/role_pictures/Verso.png" alt="logo" style="height:50vh" />
</section>

## Introduction
**B**ot **L**oup **G**arou (un nom très recherché) est, à l'origine, un bot utilisable sur Discord servant à jouer à une version digitale du célèbre jeu de cartes `Les Loups-Garous de Thiercelieux`.

Aujourd'hui, il a différentes fonctions sur notre serveur :
 - Intégrer le contenu derrière les liens Instagram (vidéos, images et carousels).
 - Intégrer le contenu derrière les liens TikTok (vidéos simples et vidéos compressées).
 - Intégrer le contenu derrière les liens Pinterest (vidéos, images).
 - Intégrer le contenu derrière les liens Reddit (vidéos, images).
 - Jouer de la musique dans les salons vocaux à partir de plusieurs plateformes de streaming (YouTube, Spotify, SoundCloud, Deezer).
   - Permet de jouer de musique à partir d'un lien ou d'une recherche de mots clés.
   - Possibilité de rechercher des paroles pour la musique en cours ou pour une musique spécifique.
 - Proposer des parties de Sutom.
 - Rapporter les bugs rencontrés par les utilisateurs.
 - Ajouter des réactions aux messages désirés (pour organiser des votes par exemple).
 - Enregistrer les utilisateurs pour leur permettre de se connecter au site du bot : cela permet l'utilisation d'une plateforme web pour organiser certains événements.


## Installation

Téléchargez le projet et installez les modules npm nécessaires avec la commande :
```bash
npm install
```

Lors du déploiement en production, le bot utilise une base de données MariaDB. Pour préparer pleinement le lancement en
production, assurez-vous d'avoir installé une base de données. Ensuite, placez-vous à la racine du projet, ouvrez la
base de données et lancez le script suivant `utilities/database/scripts/create_all.sql` pour créer toutes les tables nécéssaires.


## Variables d'environnement

Créez un fichier `.env` (à la racine du projet) dans lequel vous pourrez y mettre différentes variables :
```env
DC_API_TOKEN=**** # Token de connexion à l'API discord pour votre bot.
TEST=**** # "true" si vous êtes en train de tester votre bot, "false" sinon. Cela réduit les temps disponibles pour
          # certaines actions afin de tester plus rapidement l'application. Attention, certaines fonctionnalités sont
          # désactivées en mode test.
IG_PASSWORD=**** # Le mot de passe du compte Instagram qui sera utilisé pour l'intégration Instagram.
IG_USERNAME=**** # Le nom du compte Instagram utilisé pour l'intégration.
YT_API_TOKEN=**** # Token de connexion à l'API YouTube pour affectuer quelques recherches.
NGROK_TOKEN=**** # Token de connexion à Ngrok pour le mirroring du site web.
GITLAB_TOKEN=**** # Token GitLab pour la création de WebHooks pour le déploiement automatique.
LAUNCH_DB=**** # "true" pour lancer la base de données, "false" sinon.
DB_USER_PASSWORD=**** # Le mot de passe de l'utilisateur de la base de données.
DB_USERNAME=**** # Le nom d'utilisateur de la base de données.
DB_PORT=**** # Port utilisé par la base de données.
DEV_GUILD_ID=**** # L'identifiant du serveur discord de développement.
DEV_CHANNEL_ID=**** # L'identifiant du salon discord dans lequel envoyer le lien Ngrok.
PDDS_GUILD_ID=**** # L'identifiant du serveur discord où le bot est utilisé.
```


## Lancement
Au lancement, le bot se connecte à l'API Discord et attend des commandes des utilisateurs.
Pour lui envoyer une commande, il faut d'abord ajouter le bot sur un serveur Discord depuis lequel vous pourrez interagir avec lui (vous trouverez toutes les informations nécessaires depuis le [portail développeur de Discord](https://discord.com/developers/applications)).
Certaines commandes ne sont utilisables que dans le serveur dont l'identifiant est nommé `PDDS_GUILD_ID` dans le fichier `.env`.

Pour que notre bot soit connecté 24h/24 7j/7, nous hébergeons celui-ci sur une de nos machines en local (nous utilisions Heroku auparavant, puis render).
Ainsi, lors d'un déploiement du projet, lancez avec la machine hôte à la racine du projet :
```bash
node index.js
```
Une fois lancé, le programme lance à son tour un processus détaché `bot.js` qui se chargera de recevoir les commandes des utilisateurs.
De plus, le programme utilise l'outil Ngrok pour rendre public le site web du bot afin d'utiliser les fonctionnalités du site web :
   - Pour les administrateurs, le suivi des logs à distance, la gestion des utilisateurs, de la gestion des commandes et du déploiement manuel d'une nouvelle version du bot.
   - Pour les utilisateurs, les différentes fonctionnalités qui sont proposées en fonction des événements (cela peut varier).


## Génération de la documentation
La documentation du projet se fait à l'aide du module JSDOC.
Quatre commandes sont à votre disposition :
```bash
npm run doc # Efface l'ancienne documentation, génère la nouvelle et ouvre le navigateur
npm run generatedoc # Génère la nouvelle documentation à partir du code du projet
npm run opendoc # Ouvre votre navigateur internet sur la page de consultation de la documentation
```
Si ces commandes ne fonctionnent pas correctement, vous pouvez les consulter et modifier dans le fichier `package.json`.

Vous pouvez aussi lancer la génération de la documentation automatiquement avant chaque commit grâce au fichier `.git/hooks/pre-commit` rempli comme suit :
```bash
#!bin/sh

echo "~~~~"
echo "Génération de la documentation"
echo "~~~~"
npm run doc
echo "~~~~"
echo "Ajout des fichiers au commit"
echo "~~~~"
git add docs/
```

## Divers

Vous pouvez retrouver une petite animation de l'évolution du répertoire Git en cliquant sur l'image.

<section style="text-align: center;">
   <a href="https://www.youtube.com/watch?v=nlPL4UaJ-mY">
      <img src="./www/icons/yt_video_thumbnail.png" alt="thumbnail youtube" />
   </a>
   <a href="https://www.youtube.com/watch?v=nlPL4UaJ-mY">https://www.youtube.com/watch?v=nlPL4UaJ-mY</a>
</section>



