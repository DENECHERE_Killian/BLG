const ModulesManager = require("../utilities/ModulesManager");
const logger         = require("../utilities/log/Logger");

const randomToken = require("random-token").create();
const fetch       = require("node-fetch");
const ngrok       = require("ngrok");
const path        = require("path");
const fork        = require("child_process").fork;
const fs          = require("fs");

//-----------------
// INFOS
//-----------------

const LOG_PAGE_PARTS   = ["olds", "live"];
const ADMIN_PAGE_PARTS = [
	{ url: "log", title: "Logs", icon: "/icons/logs.png" },
	{ url: "deploys", title: "Déploiements", icon: "/icons/deploy.png" },
	{ url: "commands", title: "Commandes", icon: "/icons/commands.png" },
	{ url: "users", title: "Utilisateurs", icon: "/icons/users.png" }
];

const USER_PAGE_PARTS   = [
	{ url: "profil", title: "Profil", icon: "/icons/profil.png"	}
];

//-----------------
// SESSIONS
//-----------------

const sessions = {};

/**
 * Check if the token belong to an admin session.
 * @param token The token to check.
 * @returns {boolean} true if the token belong to an admin session, false otherwise
 */
function isAdmin(token) {
	return token in sessions && sessions[token].isAdmin;
}

/**
 * Check if the token correspond to an active session.
 * @param token The token to check.
 * @returns {boolean} true if the token belong to an active session, false otherwise
 */
function isLoggedIn(token) {
	return token in sessions;
}

setInterval(_ => {
	const timestamp = Date.now();
	for (const sessionToken in sessions)
		if (timestamp - sessions[sessionToken].creationTime > 43200000)
			delete sessions[sessionToken];
}, 60000); // Delete tokens older than 12 hours

//-----------------
// ROUTES
//-----------------

module.exports = {
	admin: {
		get: {
			root: async (req, res) => {
				if (isLoggedIn(req.session.token)) {
					if (isAdmin(req.session.token))
						return res.redirect("/admin/home");
					return res.redirect("/home");
				}

				return res.render("admin/login.ejs");
			},
			home: (req, res) => {
				return res.redirect(`/admin/home/${ADMIN_PAGE_PARTS[0].url}`);
			},
			homePart: async (req, res) => {
				if (!isAdmin(req.session.token))
					return res.redirect(`/admin`);

				if (!ADMIN_PAGE_PARTS.find(part => part.url === req.params.part))
					req.params.part = ADMIN_PAGE_PARTS[0].url;

				res.locals.hostname = req.hostname;
				return res.render("admin/home.ejs", {
					current_part_index: ADMIN_PAGE_PARTS.map(part => part.url).indexOf(req.params.part),
					parts_list: ADMIN_PAGE_PARTS,
					branch_list: (await global.gitlab.getBranches()).sort((branch_a, _) => branch_a.default ? -1 : 1),
					deployed_commit: await global.gitlab.getHeadCommit(),
					webhooks: (await global.gitlab.getWebhooks()).map((webhook) => {
						return { id: webhook.id, url: webhook.url };
					}),
					live_files: logger.getFilesContent("live"),
					olds_files: logger.getFilesContent("olds")
				});
			},
			disconnect: (req, res) => {
				delete sessions[req.session.token];
				req.session.destroy();
				res.redirect("/admin");
			},
			users: async (req, res) => {
				if (!isAdmin(req.session.token))
					return res.send({ success: false, message: "Tu n'es pas connecté en tant qu'admin" });

				const response = await fetch(`http://localhost:5001/users`, {
					method: "GET",
					headers: {
						"Content-type": "application/json; charset=UTF-8"
					}
				}).catch(err => {
					console.log(`Error while fetching users: ${err}`);
					return { success: false, users: [] };
				});

				res.send(await response.json());
			},
			logsByType: (req, res) => {
				if (!isAdmin(req.session.token))
					return res.send({ success: false, message: "Tu n'es pas connecté en tant qu'admin" });

				const type = req.params.type;
				if (!LOG_PAGE_PARTS.includes(type))
					return res.send({ success: false, message: `Fichiers de log ${type} introuvables` });

				res.json(logger.getFilesContent(type));
			}
		},
		post: {
			connect: async (req, res) => {
				const logResult = await global.database.logUser(req.body.user_id, req.body.hash_pwd);

				if (logResult.success) {
					if (!logResult.isAdmin)
						return res.send({ success: false, message: "Ce compte n'est pas un compte administrateur" });

					let sessionToken = randomToken(16);
					while (sessionToken in sessions)
						sessionToken = randomToken(16);

					req.session.token = sessionToken;

					sessions[sessionToken] = {
						userId: req.body.user_id,
						isAdmin: true,
						creationTime: Date.now()
					};
					return res.send(logResult);
				}

				return res.send(logResult);
			},
			deploy: async (req, res, botProcess) => {
				if (!isAdmin(req.session.token))
					return res.send({ success: false, message: "Tu n'es pas connecté en tant qu'admin" });
				if (process.env.TEST === "true")
					return res.send({ success: false, message: "Le bot est lancé en mode test, je ne peux pas deploy" });

				if (!req.body.branch)
					return res.send({ success: false, message: "Aucune branche spécifiée" });
				if (!req.body.hash)
					return res.send({ success: false, message: "Aucun hash de commit spécifié" });

				const commitDatas = await global.gitlab.getCommit(req.body.hash);
				if (!commitDatas || !commitDatas.id)
					return res.send({ success: false, message: "Ce hash ne correspond à aucun commit" });

				res.send({ success: true, message: "Je lance la mise à jour" });
				await deploy(req.body.hash, req.body.branch, botProcess);
			},
			hooks: async (req, res, botProcess) => {
				logger.log("Received a gitlab push webhook\n");
				res.sendStatus(200);

				// A push event trigger a new deployment on the bot
				if (req.header("x-gitlab-event") === "Push Hook") {
					const body   = req.body;
					const branch = body.ref.split("/").pop();
					if (branch === "master")
						await deploy(body.after, branch, botProcess);
				}
			},
			modules: (req, res) => {
				if (!isAdmin(req.session.token))
					return res.send({ success: false, message: "Tu n'es pas connecté en tant qu'admin" });

				res.send({
					success: true,
					commands: ModulesManager.getModules()
				});
			},
			module: async (req, res) => {
				if (!isAdmin(req.session.token))
					return res.send({ success: false, message: "Tu n'es pas connecté en tant qu'admin" });

				await fetch(`http://localhost:5001/module/${req.params.moduleId}`, {
					method: "POST",
					headers: {
						"Accept": "application/json",
						"Content-Type": "application/json"
					},
					body: JSON.stringify(req.body)
				});

				res.send({
					success: true
				});
			}
		},
		put: {
			userPassword: async (req, res) => {
				if (!isAdmin(req.session.token))
					return res.send({ success: false, message: "Tu n'es pas connecté en tant qu'admin" });

				const response = await fetch(`http://localhost:5001/users/${req.params.userId}/password`, {
					method: "PUT",
					headers: {
						"Content-type": "application/json; charset=UTF-8"
					}
				}).catch(err => {
					res.send({ success: false, message: err });
				});

				const result = await response.json();

				if (result.success)
					for (const sessionToken in sessions)
						if (sessions[sessionToken].userId === req.params.userId)
							delete sessions[sessionToken];

				res.send(result);
			}
		},
		delete: {
			user: async (req, res) => {
				if (!isAdmin(req.session.token))
					return res.send({ success: false, message: "Tu n'es pas connecté en tant qu'admin" });

				const response = await fetch(`http://localhost:5001/users/${req.params.userId}`, {
					method: "DELETE",
					headers: {
						"Content-type": "application/json; charset=UTF-8"
					}
				}).catch(err => {
					return { success: false, message: err };
				});

				const result = await response.json();

				if (result.success)
					for (const sessionToken in sessions)
						if (sessions[sessionToken].userId === req.params.userId)
							delete sessions[sessionToken];

				res.send(result);
			},
			hooks: async (req, res) => {
				if (process.env.TEST === "true")
					return res.send({ success: false, message: "Impossible de supprimer les webhooks lorsque le mode de test est actif" });

				if (req.hostname === "localhost" || req.hostname === "127.0.0.1")
					return res.send({ success: false, message: "Impossible de supprimer les webhooks depuis le localhost" });

				const webhooksToDelete = (await global.gitlab.getWebhooks()).filter(hook => hook.url.split("/")[2] !== req.hostname);

				if (webhooksToDelete.length === 0)
					return res.send({ success: false, message: "Aucun webhook n'est à supprimer" });

				for (const hook of webhooksToDelete)
					await global.gitlab.removeWebhook(hook.id);

				return res.send({ success: true, message: `${webhooksToDelete.length} webhook(s) supprimé(s)`, remaining: (await global.gitlab.getWebhooks()).length });
			}
		},
		websocket: {
			liveLogs: (ws, req) => {
				if (!isAdmin(req.session.token))
					return ws.send(JSON.stringify({ success: false, message: "Tu n'es pas connecté en tant qu'admin" }));

				logger.setLiveFileWatcherCallback((event) => {
					if (event === "change")
						ws.send(JSON.stringify({ success: true, event: "change", fileContent: logger.getFilesContent("live").live }));
				});

				ws.on("close", () => {
					console.log("Live logs webSocket was closed");
				});
			}
		}
	},
	user: {
		get: {
			root: (req, res) => {
				if (isLoggedIn(req.session.token))
					return res.redirect("/home");
				return res.render("user/login.ejs");
			},
			home: async (req, res) => {
				return res.redirect(`/home/${USER_PAGE_PARTS[0].url}`);
			},
			homePart: async (req, res) => {
				if (!isLoggedIn(req.session.token))
					return res.redirect(`/`);

				if (!USER_PAGE_PARTS.find(part => part.url === req.params.part))
					req.params.part = ADMIN_PAGE_PARTS[0].url;

				const response = await fetch(`http://localhost:5001/users/${sessions[req.session.token].userId}`, {
					method: "GET",
					headers: {
						"Content-type": "application/json; charset=UTF-8"
					}
				});

				const userInfos = await response.json();

				return res.render("user/home.ejs", {
					current_part_index: USER_PAGE_PARTS.map(part => part.url).indexOf(req.params.part),
					parts_list: USER_PAGE_PARTS,
					user: userInfos
				});
			},
			disconnect: (req, res) => {
				delete sessions[req.session.token];
				req.session.destroy();
				res.redirect("/");
			}
		},
		post: {
			connect: async (req, res) => {
				const logSuccess = await global.database.logUser(req.body.user_id, req.body.hash_pwd);

				if (logSuccess.success) {
					let sessionToken = randomToken(16);
					while (sessionToken in sessions) {
						sessionToken = randomToken(16);
					}

					req.session.token = sessionToken;

					sessions[sessionToken] = {
						isAdmin: logSuccess.isAdmin,
						userId: req.body.user_id,
						creationTime: Date.now()
					};
				}

				return res.send(logSuccess);
			}
		}
	}
};

//-----------------
// DEPLOY
//-----------------

/**
 * Call the node file that will update the bot files to a specific version represented by a commit hash on a branch.
 * @param {String} hash The hash of the commit to deploy
 * @param {String} branch The branch where the commit is located
 * @param {ChildProcess} botProcess The child process
 * @returns {Promise<void>}
 */
async function deploy(hash, branch, botProcess) {
	if (process.env.TEST !== "true") {
		if (!fs.existsSync(path.join(process.cwd(), "update_copy.js")))
			fs.closeSync(fs.openSync(path.join(process.cwd(), "update_copy.js"), "w"));
		fs.copyFileSync(path.join(process.cwd(), "update.js"), path.join(process.cwd(), "update_copy.js"));
		await global.gitlab.deleteWebhook();
		await ngrok.disconnect();
		await ngrok.kill();
	}

	botProcess.kill();
	fork("update_copy.js", [hash, branch], { detached: true });
	process.exit(0);
}