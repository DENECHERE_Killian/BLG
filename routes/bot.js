module.exports = {
	user: {
		get: {
			users: async (req, res) => {
				res.send(await global.bot.recoverUsers());
			},
			user: async (req, res) => {
				await global.bot.users.fetch(req.params.userId)
					.then(user => res.send({ userId: req.params.userId, username: user.username, userImageURL: user.avatarURL() }))
					.catch(_ => res.send({ userId: req.params.userId, username: "Unknown", userImageURL: "/icons/profil.png" }));
			}
		},
		put: {
			userPassword: async (req, res) => {
				res.send(await global.database.changeUserPwd(req.params.userId));
			}
		},
		delete: {
			user: async (req, res) => {
				res.send(await global.database.deleteUser(req.params.userId));
			}
		}
	},
	module: {
		post: {
			module: async (req, res) => {
				await global.bot.enableCommandModule(req.params.moduleId, req.body.toActivate);
				res.sendStatus(200);
			}
		}
	}
};
