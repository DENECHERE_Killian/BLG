const login_button          = document.getElementById("login");
const password              = document.getElementById("password");
const idField               = document.getElementById("id");
const WAIT_AFTER_LOGIN_TIME = 1500; // ms

password.addEventListener("keypress", (ev) => {
	if (ev.key === "Enter")
		login();
});

login_button.addEventListener("click", login);

async function login() {
	const writtenId = idField.value;
	if (writtenId === "")
		return toaster.display("L'identifiant ne doit pas être vide", "var(--darkred)");

	const writtenPassword = password.value;
	if (writtenPassword === "")
		return toaster.display("Le mot de passe ne doit pas être vide", "var(--darkred)");

	// Make a sha-256 version of the typed password
	const textAsBuffer = new TextEncoder().encode(writtenPassword);
	const hashBuffer   = await window.crypto.subtle.digest("SHA-256", textAsBuffer);
	const hashArray    = Array.from(new Uint8Array(hashBuffer));
	const digest       = hashArray.map(b => b.toString(16).padStart(2, "0")).join("");

	const pathname = window.location.pathname.endsWith('/') ? window.location.pathname : `${window.location.pathname}/`

	fetch(`${pathname}connect`, {
		method: "post",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			user_id: writtenId,
			hash_pwd: digest
		})
	}).then(async res => {
		res = await res.json();
		if (res.success) {
			toaster.display("Connexion ...");
			console.log(`redirect to ${pathname}home`)
			setTimeout(() => window.location.href = `${pathname}home`, WAIT_AFTER_LOGIN_TIME);
		} else {
			toaster.display(res.message, "var(--darkred)");
			password.style.borderBottomColor = "var(--darkred)";
			idField.style.borderBottomColor  = "var(--darkred)";
		}
	}).catch(err => {
		console.error(err);
	});
}