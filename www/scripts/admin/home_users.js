const users = [];

class Users {
	constructor() {
		this.dom_list = document.getElementById("users_list");
	}

	async #fetchUsers() {
		const res = await fetch("/admin/users/", {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8"
			}
		}).catch(err => {
			toaster.display(`Impossible de récupérer la liste des utilisateurs :\n${err}`, "var(--darkred)");
		});

		const response = await res.json();

		if (!response.success)
			toaster.display("Echec de récupération de la liste des utilisateurs", "var(--darkred)");

		// console.log(response.users);

		return response.users;
	}

	async changeUserPwd(userId) {
		const res = await fetch(`/admin/users/${userId}/password`, {
			method: "PUT",
			headers: {
				"Content-type": "application/json; charset=UTF-8"
			}
		}).catch(err => {
			toaster.display(`Echec du changement du mot de passe de l'utilisateur ${userId}`, "var(--darkred)");
			console.error(err);
		});

		const response = await res.json();

		if (response.success)
			toaster.display(response.message);
		else
			toaster.display(response.message, "var(--darkred)");
	}

	async deleteUser(userId) {
		fetch(`/admin/users/${userId}`, {
			method: "DELETE",
			headers: {
				"Content-type": "application/json; charset=UTF-8"
			}
		}).then(async res => {
			const response = await res.json();

			if (response.success) {
				toaster.display(response.message);
				userManager.showList();
			} else {
				toaster.display(response.message, "var(--darkred)");
			}
		}).catch(err => {
			toaster.display(`Echec de la suppression de l'utilisateur ${userId}`, "var(--darkred)");
			console.error(err);
		});
	}

	async showList() {
		const users = await this.#fetchUsers();

		if (users.length > 0) {
			this.dom_list.innerHTML = '';
			users.forEach(user => {
				const dom_user = document.createElement("li");
				dom_user.setAttribute("class", "user");
				dom_user.innerHTML = `
                    <img src="${user.picture}" alt="Icone de l'utilisateur">
                    <h3>${user.pseudo}</h3>
                    <p>${user.id}</p>
                    <div>
                    	<button onclick="userManager.changeUserPwd('${user.id}')">Changer MDP</button>
                    	<button onclick="userManager.deleteUser('${user.id}')" class="redButton"><img src="/icons/bin.png" alt="Icon de corbeille"></button>
                    </div>
                `;
				this.dom_list.appendChild(dom_user);
			});
		} else {
			this.dom_list.parentNode.innerHTML = `<p>Aucun utilisateur n'est encore inscrit</p>`;
		}
	}
}

const userManager = new Users();
userManager.showList();
