const logRefreshButton = document.getElementById("logs_refresh_button");
const downloadButton   = document.getElementById("logs_download_button");
const logSelector      = document.getElementById("log_selector");
const logSearchBar     = document.getElementById("log_search");
const logManager       = new LogManager();
let actualContent      = "";

function displayLog(type, logName) {
	let displayArea = document.getElementById("display-area");

	if (displayArea === null) {
		displayArea = document.createElement("div");
		displayArea.setAttribute("id", "display-area");
		displayArea.innerHTML = `<div id="text-area"></div>`;

		document.getElementById("log_section").appendChild(displayArea);
	}

	const logsFileContent     = document.getElementById("text-area");
	logsFileContent.innerHTML = logManager.getLogFileContent(type, logName);
	if (document.getElementById("logs_scroll_down_button").checked)
		logsFileContent.scrollTop = logsFileContent.scrollHeight; // Scoll to bottom at each refresh
}

// When a log file is selected
logSelector.onchange = async (event) => {
	await logManager.updateLiveLog();
	const split = event.target.value.split("#");
	displayLog(split[0], split[1]);
};

// When a log text is searched
logSearchBar.oninput = async (event) => {
	const logsList = document.getElementById("text-area").children;
	const research = logSearchBar.value.toLowerCase();

	for (const log of logsList) {
		if (research === "" || log.textContent.toLowerCase().includes(research))
			log.classList.remove("search-hide");
		else
			log.classList.add("search-hide");
	}
};

// When the download button is pressed
downloadButton.onclick = (event) => {
	const split = logSelector.value.split("#");
	console.log(split[0], split[1]);
	const link    = document.createElement("a");
	link.download = `${split[1]}.log`;
	link.href     = URL.createObjectURL(new Blob([logManager.getLogFileContent(split[0], split[1])], {type: "text/plain"}));
	link.click();
	URL.revokeObjectURL(link.href); // Enable the garbage collector to erase the created url
};

// When the refresh button is pressed
logRefreshButton.onclick = async () => {
	await logManager.fetchOlds();
	const optgroupOlds = document.getElementById("optgroup_olds");
	const oldValue     = document.querySelector("#log_selector option:checked").value.split("#");

	while (optgroupOlds.firstChild)
		optgroupOlds.removeChild(optgroupOlds.lastChild);

	for (const filename in logManager.olds_files) {
		const option     = document.createElement("option");
		option.value     = `olds#${filename}`;
		option.innerText = filename;
		optgroupOlds.appendChild(option);
	}

	let files = [];
	switch (oldValue[0]) {
		case "olds":
			files = Object.keys(logManager.olds_files);
			break;
		case "live":
			files = ["Live"];
			break;
		default:
			console.error(`Unknown file type "${oldValue[0]}"`);
			break;
	}

	if (files.includes(oldValue[1])) {
		logSelector.value = oldValue.join("#");
		displayLog(oldValue[0], oldValue[1]);
	} else {
		logSelector.selectedIndex = 0;
	}

	toaster.display("Fichiers de logs rafraîchis");
};

// The live log income flux
function startLiveLogsWebSocket() {
	const protocol = window.location.protocol === "https:" ? "wss:" : "ws:";
	const socket   = new WebSocket(`${protocol}//${window.location.host}/admin/logs/live`);

	socket.onmessage = (e) => {
		const serverMessage = JSON.parse(e.data);
		if (!serverMessage.success)
			return toaster.display(`Impossible de récupérer les fichiers de logs en direct : ${serverMessage.message}`, "var(--darkred)");

		logManager.live_file = serverMessage.fileContent;

		// Update the live log content
		if (logSelector.value === "live#live" && logManager.live_file !== actualContent) {
			displayLog("live", "live");
			logSearchBar.dispatchEvent(new Event("input"));
		}
	};
}

logSelector.dispatchEvent(new Event("change")); // Display the log file content
startLiveLogsWebSocket();