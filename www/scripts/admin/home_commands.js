class Commands {
	constructor() {
		this.dom_list = document.querySelector("#commands_list");
	}

	#getAll() {
		return fetch(`/admin/modules/`, {
			method: "POST",
			headers: {
				"Content-type": "application/json; charset=UTF-8"
			}
		})
			.then(async res => {
				const result = await res.json();
				// console.log("Chargement des commandes", result.commands);

				if (result.success) {
					return result.commands;
				} else {
					toaster.display(result.message, "var(--darkred)");
				}
			}).catch(err => {
				toaster.display(`Impossible de récupérer la liste des commandes : \n${err}`, "var(--darkred)");
				console.error(err);
			});
	}

	async setActivationState(moduleId, toActivate) {
		return await fetch(`/admin/module/${moduleId}`, {
			method: "POST",
			headers: {
				"Content-type": "application/json; charset=UTF-8"
			},
			body: JSON.stringify({
				toActivate: toActivate
			})
		})
			.then(async res => {
				const result = await res.json();

				if (result.success) {
					toaster.display(`Le module est maintenant ${toActivate ? "activé" : "désactivé"}`);
					console.log("result  " + result);
				} else {
					toaster.display(result.message, "var(--darkred)");
				}
			}).catch(err => {
				toaster.display(`Impossible ${toActivate ? "d'activer" : "de désactiver"} ce module : \n${err}`, "var(--darkred)");
				console.error(err);
				document.getElementById(moduleId).checked = !toActivate;
			});
	}

	async show() {
		const commands = await this.#getAll();

		commands.forEach(command_module => {
			const dom_command_module = document.createElement("li");
			dom_command_module.setAttribute("class", "command");
			dom_command_module.innerHTML = command_structure
				.replace("%SRC%", command_module.picture)
				.replace("%MODULE_NAME%", command_module.name)
				.replace("%CHECKED%", command_module.activated ? "checked" : "")
				.replaceAll("%COMMAND%", command_module.commands.name)
				.replace("%DESC%", command_module.commands.description)
				.replace("%OPTIONS%", command_module.commands.options ? command_module.commands.options.map(option => {
					return `<li>
						<h4><span>/${command_module.commands.name}</span> ${option.name}</h4>
						<p>${option.description}</p>
					</li>`;
				}).join("") : "");
			this.dom_list.appendChild(dom_command_module);
		});
	}

	activationSwitchChanged(input) {
		input.parentNode.classList.add("loading");
		input.disabled = true;
		this.setActivationState(input.id, input.checked).then(_ => {
			input.parentNode.classList.remove("loading");
			input.disabled = false;
		});
	}
}

const commandManager = new Commands();
commandManager.show();

