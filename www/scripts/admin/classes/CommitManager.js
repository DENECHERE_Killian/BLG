class CommitManager {
	constructor() {
		// Interface attributes
		this.gui_commit_list     = document.getElementById("commit_list");
		this.gui_deployed_commit = document.getElementById("deployed_commit");
		this.gui_branch_selector = document.getElementById("branch_selector");

		// Datas attributes
		this.authors_avatar = {};

		// Events
		this.gui_branch_selector.addEventListener("input", async (ev) => {
			this.branch_commit_list = await this.getBranchCommitList();
			this.showListForBranch();
		});

		// Interface first paint
		this.#firstPaint();
	}

	// ----------------------
	// Getters of new attributes values
	// ----------------------

	async getBranchCommitList() {
		return await this.#getBranchCommits(branch_selector.value);
	}

	// ----------------------
	// Interface changes
	// ----------------------

	async #firstPaint() {
		this.deployed_commit    = deployed_commit;
		this.branch_commit_list = await this.getBranchCommitList();
		this.showDeployed();
		this.showListForBranch();
	}

	async showListForBranch() {
		// Add of the commits to page
		this.gui_commit_list.innerHTML = "";
		for (const [_, commit_datas] of Object.entries(this.branch_commit_list)) {
			this.gui_commit_list.innerHTML += commit_structure
				.replace("%TITLE%", commit_datas.title ?? `Commit inconnu (tu ne l'a pas push ?)`)
				.replace("%IS_DEPLOYED_COMMIT%", (commit_datas.id === this.deployed_commit.id) ? "deployed" : "")
				.replace("%AUTHOR_NAME%", commit_datas.author_name ?? `probablement toi`)
				.replace("%AUTHOR_AVATAR%", await this.#getAuthorAvatar(commit_datas.author_email))
				.replaceAll("%AUTHOR_PROFILE%", "https://www.youtube.com/watch?v=dQw4w9WgXcQ")
				.replace("%AUTHORED_DATE%", this.#timeDifference(commit_datas.authored_date))
				.replace("%COMMIT_HASH%", commit_datas.id)
				.replace("%AUTHORED_DATE_TOOLTIP%", new Date(commit_datas.authored_date).toLocaleString())
				.replace("%COMMIT_URL%", commit_datas.web_url);
		}
	}

	async showDeployed() {
		this.gui_deployed_commit.innerHTML = commit_structure
			.replace("%TITLE%", this.deployed_commit.title ?? `Commit inconnu (tu ne l'a pas push ?)`)
			.replace("%IS_DEPLOYED_COMMIT%", (this.deployed_commit.id === this.deployed_commit.id) ? "deployed" : "")
			.replace("%AUTHOR_NAME%", this.deployed_commit.author_name ?? `probablement toi`)
			.replace("%AUTHOR_AVATAR%", await this.#getAuthorAvatar(this.deployed_commit.author_email))
			.replaceAll("%AUTHOR_PROFILE%", "https://www.youtube.com/watch?v=dQw4w9WgXcQ")
			.replace("%AUTHORED_DATE%", this.#timeDifference(this.deployed_commit.authored_date))
			.replace("%COMMIT_HASH%", this.deployed_commit.id)
			.replace("%AUTHORED_DATE_TOOLTIP%", new Date(this.deployed_commit.authored_date).toLocaleString())
			.replace("%COMMIT_URL%", this.deployed_commit.web_url);
	}

	// ----------------------
	// Gitlab API fetchers
	// ----------------------

	async #getBranchCommits(branch_name) {
		return fetch(`https://gitlab.com/api/v4/projects/20297412/repository/commits?ref_name=${branch_name}`)
			.then(async res => await res.json())
			.catch(err => console.error(err));
	}

	async #getAuthorAvatar(author_email) {
		if (!this.authors_avatar[author_email])
			await fetch(`https://gitlab.com/api/v4/avatar?email=${author_email}&size=32`)
				.then(async res => {
					const result                      = await res.json();
					this.authors_avatar[author_email] = result.avatar_url;
				});

		return this.authors_avatar[author_email];
	}

	async deploy(commit_hash) {
		if (commit_hash === "")
			return toaster.display("Le champ de hash est vide", "var(--darkred)");
		commit_hash_input.value = "";

		fetch(`/admin/deploy/`, {
			method: "POST",
			headers: {
				"Content-type": "application/json; charset=UTF-8"
			},
			body: JSON.stringify({
				branch: this.gui_branch_selector.value,
				hash: commit_hash
			})
		}).then(async res => {
			const result = await res.json();
			if (result.success)
				toaster.display(result.message);
			else
				toaster.display(result.message, "var(--darkred)");
		}).catch(err => {
			toaster.display(`Impossible de déployer cette version : \n${err}`, "var(--darkred)");
			console.error(err);
		});
	}

	async removeWebhooks() {
		const button           = document.getElementById('removeWebhooks');
		const tooMuchHooksText = document.getElementById('tooMuchHooks');
		button.disabled = true;

		fetch("/admin/hooks", {
			method: "DELETE"
		}).then(async res => {
			const result = await res.json();
			if (result.success) {
				toaster.display(result.message);
				tooMuchHooksText.innerText = `${result.remaining - 1} webhooks en trop`;
			} else {
				toaster.display(result.message, "var(--darkred)");
			}

			button.disabled = false;
		}).catch(err => {
			toaster.display(`${err}`, "var(--darkred)");
			console.error(err);
			button.disabled = false;
		});
	}

	// ----------------------
	// Usefull tools
	// ----------------------

	#timeDifference(string_date) {
		if (!string_date)
			return `il y a ... Je sais pas fréro, tu m'en demande beaucoup là`;

		const difference = (new Date() - new Date(string_date)) / 1000; // Pass from ms to s
		const minutes    = Math.round(difference / 60);

		if (minutes > 59) {
			const hours = Math.round(minutes / 60);

			if (hours > 23) {
				const days = Math.round(hours / 24);

				if (days > 30) {
					const months = Math.round(days / 30);
					return `il y a ${months} mois`;
				}

				return days > 1 ? `il y a ${days} jours` : "hier";
			}

			return `il y a ${hours} ${hours > 1 ? "heures" : "heure"}`;
		}

		return `il y a ${minutes} ${minutes > 1 ? "minutes" : "minute"}`;
	}
}

const commit_manager = new CommitManager();