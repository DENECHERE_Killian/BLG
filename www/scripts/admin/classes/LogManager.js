class LogManager {
	constructor() {
		this.live_file  = "";
		this.olds_files = {};
		this.fetchOlds();
	}

	fetchOlds() {
		fetch(`/admin/logs/olds`)
			.then(async res => {
				this.olds_files = await res.json();
				// console.log("Fichiers logs d'archive", Object.keys(this.olds_files).length);
			}).catch(err => {
			toaster.display(`Impossible de récupérer les fichiers d'archive' : ${err}`, "var(--darkred)");
			console.error(err);
		});
	}

	async updateLiveLog() {
		try {
			const res      = await (await fetch(`/admin/logs/live`)).json();
			this.live_file = res.live ?? `<pre class="errorLog">${res.message}</pre>`;
		} catch (err) {
			toaster.display(`Impossible de récupérer les logs : \n${err}`, "var(--darkred)");
			console.error(err);
		}
	}

	getLogFileContent(type, filename) {
		switch (type) {
			case "olds":
				return this.olds_files[filename];
			case "live":
				return this.live_file;
			default:
				return "";
		}
	}
}