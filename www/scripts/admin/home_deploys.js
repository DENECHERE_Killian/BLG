const commit_hash_input  = document.getElementById("commit_hash_input");
const icon               = document.getElementById("icon");
const root               = document.querySelector(":root");
let dark_theme           = false;

commit_hash_input.addEventListener("keypress", (ev) => {
	if (ev.key === "Enter")
		commit_manager.deploy(commit_hash_input.value);
});