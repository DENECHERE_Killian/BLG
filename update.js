require("dotenv").config();
const { fork, spawnSync } = require("child_process");
const logger              = require("./utilities/log/Logger");

const hash        = process.argv[2];
const branch      = process.argv[3];
const logFilename = logger.changeLiveFile();

try {
	if (branch && hash) {
		logger.update(`Starting new deployment with version ${hash} on branch ${branch}`);

		const gitResetResult = spawnSync('git reset --hard;', { shell: true }); // Remove all possible changes
		logger.update(gitResetResult.stdout.toString());
		const gitInitCheckoutResult = spawnSync(`git checkout ${branch};`, { shell: true }); // Move to the desired branch
		logger.update(gitInitCheckoutResult.stdout.toString());
		const gitPullResult = spawnSync(`git pull origin ${branch}`, { shell: true }); // Fetch last server version
		logger.update(gitPullResult.stdout.toString());
		logger.update(`Successfully pulled new version files`);

		const gitCheckoutResult = spawnSync(`git checkout ${hash}`, { shell: true }); // Go to desired version
		logger.update(gitCheckoutResult.stdout.toString());
		logger.update(`Successfully checked out to version`);

		const npmInstallResult = spawnSync('npm install', { shell: true }); // Update npm package according to the new server version
		logger.update(npmInstallResult.stdout.toString());
		logger.update(`Successfully installed npm packages`);

		const tsCompilationResult = spawnSync('npm run compileTsFiles', { shell: true }); // Compile the ts files for the play-dl files
		logger.update(tsCompilationResult.stdout.toString());
		logger.update(`Successfully compile ts files`);
	} else {
		logger.update(`Service manually started`);
	}

	const args = {
		isDeployment: true,
		liveFilename: logFilename
	};

	fork('index.js', [JSON.stringify(args)], { detached: true }); // Launch the new server
	logger.update(`Successfully forked bot process, prepare to suicide`);

	process.exit(0); // End the update process
} catch (err) {
	logger.error(err);
}
