// noinspection JSCheckFunctionSignatures
require("dotenv").config();

global.gitlab = new (require("./utilities/Gitlab"))();
global.database; // Used in the route file but declared in here to give it a value which is retrieved from an async function. That means it needs to be called from an async function in order to use the 'await' keyword and this action cannot be performed from the routes file.

const dbInstance = require("./utilities/database/Database");
const logger     = require("./utilities/log/Logger");
const router     = require("./routes/index");

const cookieParser = require("cookie-parser");
const compression  = require("compression"); // Compress the data transferred through the server
const expressWs    = require("express-ws"); // User websocket to transfer live logs file content each time it is changed
const express      = require("express"); // Server creation
const session      = require("express-session"); // Session use to keep logged-in user datas
const ngrok        = require("ngrok");
const fork         = require("child_process").fork;
const path         = require("path");

const SERVER_PORT = process.env.PORT || 5000;

const app = express();
let ngrok_url;
let bot_process;

process.on("uncaughtException", async err => {
	logger.error(`Fatal error: Main process received an uncaught exception`);

	if (process.env.TEST !== "true")
		await global.gitlab.deleteWebhook();

	logger.error(`Uncaught error: ${err.message}`);
	process.exit(1);
});

//-----------------
// SERVER SETTINGS
//-----------------

expressWs(app); // Enable the use of web socket on the server
app.use(session({
	secret: "thisismysecrctekeyfhrgfgrfrty84fwir767",
	saveUninitialized: true,
	cookie: { maxAge: 1000 * 60 * 60 * 24 }, // One day
	resave: false
}));
app.use(compression());
app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "./www/")));
app.set("views", path.join(__dirname, "./www/views/"));

//-----------------
// ADMIN ROUTES
//-----------------

// Views
app.get("/admin", router.admin.get.root);
app.get("/admin/home", router.admin.get.home);
app.get("/admin/home/:part", router.admin.get.homePart);

// Panel login
app.post("/admin/connect", router.admin.post.connect);
app.get("/admin/disconnect", router.admin.get.disconnect);

// Version management
app.post("/admin/deploy", (req, res) => {
	router.admin.post.deploy(req, res, bot_process);
});
app.post("/admin/hooks", (req, res) => {
	router.admin.post.hooks(req, res, bot_process);
});
app.delete("/admin/hooks", router.admin.delete.hooks);

// Modules management
app.post("/admin/modules", router.admin.post.modules);
app.post("/admin/module/:moduleId", router.admin.post.module);

// Users management
app.get("/admin/users", router.admin.get.users);
app.put("/admin/users/:userId/password", router.admin.put.userPassword);
app.delete("/admin/users/:userId", router.admin.delete.user);

// Logs management
app.get("/admin/logs/:type", router.admin.get.logsByType);
app.ws("/admin/logs/live", router.admin.websocket.liveLogs);

//-----------------
// USER ROUTES
//-----------------

// Views
app.get("/", router.user.get.root);
app.get("/home", router.user.get.home);
app.get("/home/:part", router.user.get.homePart);

// Panel login
app.post("/connect", router.user.post.connect);
app.get("/disconnect", router.user.get.disconnect);

//-----------------
// GLOBAL ROUTES
//-----------------

app.use((req, res) => res.redirect("/")); // 404 route

app.listen(SERVER_PORT, start);

//-----------------
// FORK bot.js TO START BOT
//-----------------

/**
 * Called when the server is ready and the other components relying on the server can be preapared
 * @param recursiveCall {boolean=false} True when the forked process bot.js crashes and need to be restarted.
 * @returns {Promise<void>}
 */
async function start(recursiveCall = false) {
	const callArguments = JSON.parse(process.argv[2] ?? "{}");
	if (callArguments?.liveFilename)
		logger.LIVE_LOGS_FILENAME = callArguments.liveFilename;

	// Start ngrok mirroring
	if (process.env.TEST !== "true" && !recursiveCall) {
		ngrok_url = await ngrok.connect({ region: "eu", proto: "http", addr: SERVER_PORT });
		global.gitlab.createWebhook(ngrok_url);
	}

	// Start database connection
	if (process.env.LAUNCH_DB === "true" && !recursiveCall)
		global.database = await dbInstance.getInstance();

	// Set the log output file
	if (!callArguments?.isDeployment || recursiveCall)
		logger.changeLiveFile();

	logger.log(`======== NEW BOT LAUNCH ========\n`, "startLog");
	logger.log(`Public pages are at http://localhost:${SERVER_PORT} ${ngrok_url ? `& ${ngrok_url}` : ""}\n`);
	logger.log(`Admin pages are at http://localhost:${SERVER_PORT}/admin ${ngrok_url ? `& ${ngrok_url}/admin` : ""}\n`);
	ngrok_url ? logger.log(`Server mirroring is at ${ngrok_url}\n`) : "";
	logger.log(`Starting the bot\n`);
	bot_process = fork(`bot.js`, [ngrok_url ? ngrok_url : ""], { stdio: ["pipe", "pipe", "pipe", "ipc"] });

	bot_process.stdout.on("data", (data) => {
		const message = data.toString();

		if (message.startsWith("INFO"))
			logger.info(message.slice(5));
		else
			logger.log(message);
	});

	// Error thrown by the forked process leading to classic string messages
	bot_process.stderr.on("data", (data) => {
		const message = data.toString();

		if (message.startsWith("WARN"))
			logger.warn(message.slice(5));
		else
			logger.error(message);
	});

	// Error thrown by Node.js process leading to an error Object
	bot_process.on("error", logger.error);

	bot_process.on("close", (code, signal) => {
		logger.log(`The node app has been closed.\n\t↪ Code ${code}\n\t↪ Signal ${signal}\n`);
		bot_process.kill();

		if (process.env.TEST !== "true") { // Restarting the bot because it should not have been closed
			logger.log(`Bringing back to life the bot\n`);
			start(true);
		}
	});
}
